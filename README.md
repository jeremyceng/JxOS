# JxOS

#### Description

面向MCU的小型前后台系统，提供消息、事件等服务，以及软件定时器，低功耗管理，按键，led等常用功能模块。在此基础上实现了基于433的简单无线网络功能。此项目的设计思想是：功能模块与硬件高度解耦，提高代码模块的可复用性；不使用复杂的数据结构和语法以提高不同硬件平台和编译器之间的兼容性，实现工程在不同MCU之间的快速移植；提供实用稳定常用的功能模块，实现项目的快速开发；定义标准应用开发框架，减轻应用开发的工作量和难度。已使用此系统用于开发项目的硬件平台有：N76E003、STM8S103K、PC、KF8TS2716

#### Software Architecture
![输入图片说明](jxos/JXOS.jpg)
![输入图片说明](fun_module_demo/module_interface.jpg)

```shell
JxOS
├───demo_code				参考代码，未整理代码
│
├───jxos					jxos
│   ├───bsp					过时分类，将逐步归类到 driver 和 lib 中
│   ├───kernel				内核，提供系统内核功能：任务、事件、消息、公告板、邮箱、管道、注册、内存分配……
│   ├───driver				使用 静态 初始化的功能代码模块，使用config.h文件进行配置；此目录下的代码大部分与硬件相关，如：传感器、按键扫描、屏幕…… （driver 与 lib 有时并没有清晰的界限，如driver:led_blink 就有可能抽象成 lib:sw_pwm）
│   ├───lib					使用 动态 初始化的功能代码模块，使用初始化函数进行配置；此目录下的模代码大部分与硬件无关，如：环形缓冲区、crc16、栈、软件定时器…… 
│   ├───sys_service			基于任务实现的系统服务，如：低功耗管理、软件定时器、打印输出……（只有对外接口，不发出对外消息）
│   └───std_app				基于任务实现的常用标准应用，如独立按键扫描、按键多次点击、指示灯闪烁……（此类模块和user app属于同一级别，只不过已经标准化；此类模块发出对外消息，也有对外接口，用于兼容不能支持消息功能的硬件平台）
│
├───platform
│   ├───N76E003    ┐
│   ├───N76E003    ├───		基于 jxos 实现的，使用不同平台硬件的具体项目
│   └───STM8S103K3 ┘
│
├───JSnet   ┐
├───....    ├───			基于 jxos 实现的功能模块
└───SRTnet  ┘
```

1. kernel        提供系统内核功能：任务、事件、消息、公告板、邮箱、管道、注册、内存分配……
2. driver        使用 静态 初始化的功能代码模块，使用config.h文件进行配置；此目录下的代码大部分与硬件相关，如：传感器、按键扫描、屏幕…… （driver 与 lib 有时并没有清晰的界限，如driver:led_blink 就有可能抽象成 lib:sw_pwm）
3. lib           使用 动态 初始化的功能代码模块，使用初始化函数进行配置；此目录下的模代码大部分与硬件无关，如：环形缓冲区、crc16、栈、软件定时器…… 
4. sys_service   基于任务实现的系统服务，如：低功耗管理、软件定时器、打印输出……（只有对外接口，不发出对外消息）
5. std_app       基于任务实现的常用标准应用，如独立按键扫描、按键多次点击、指示灯闪烁…… （此类模块和user app属于同一级别，只不过已经标准化；此类模块发出对外消息，也有对外接口，用于兼容不能支持消息功能的硬件平台）
6. platform           基于 jxos 实现的，使用不同平台硬件的具体项目
7. 在某些编译器环境下可能出现以某些限制（不支持函数指针，不支持中断处理函数中调用其他函数，不支持函数指针带参数，不支持结构体），因此 std_app 提供的消息可能无法使用，作为补充std_app 也会同时提供全局变量来传出信息和函数接口来传入信息
8. 为提高代码的兼容性；尽量不要使用函数指针
9. 为提高代码的可复用性，std_app 中的功能尽量在 driver 和 lib 中实现，std_app 中的代码仅作为提供系统tick和统一对外接口的功能
10. 模块和 std_app 可以提供多个不同实现方法的文件，但保持对外接口的统一
11. 对外提供两套接口方案 jxos_public.h 提供高级接口（消息 事件等），jxos_public_lite.h 提供低级接口（全局变量， 函数）

#### Installation

#### Instructions

1. 导入 jxos_public.h 即可调用系统所有的对外接口
2. 每个项目通过 jxos_init_config.c 和 jxos_config.h 对系统进行配置，即每个项目有独立配置文件
3. 在 jxos_init_config.c 实现需要硬件支持的系统模块硬件初始化，以及用户任务的初始化顺序
4. 在 jxos_config.h 配置系统模块的参数和使能
5. event 用于任务内部的信号同步，主要是将中断信号传递到任务
6. msg 用于任务之间的消息传递，使用 jxos_msg_get_handle 可通过消息名（字符串）获取消息句柄

```shell
如何建立新项目：
	获取 Jxos 所有文件和目录结构
	在 \platform 目录下建立当前工程的目录（platform\芯片型号\工程命名\）
	在 \工程名称 目录下新建工程文件（keil、iar 等IDE的newproject）
	在 \工程名称 目录下新建 \config 目录，增加 jxos_config.h type.h 系统配置文件
	在 \config 目录下增加工程用到的功能模块的配置文件 （如 button_config.h sim_timer_config.h ...）
	在 \工程名称 目录下新建 \framework 目录
	在 \framework 目录下增加 main.c ，实现main函数，在main函数中调用jxos_run() （ void main(void) {jxos_run();} ）
	在 \framework 目录下增加 isr.c ，实现工程需要的中断处理函数
	在 \framework 目录下增加 callback_handler.c ，实现工程用到的功能模块的callback函数
	建议在 \工程名称 目录下新建 \app 目录，实现用户功能任务
	建议在 \芯片型号 目录下增加芯片需要用到的库文件目录
	在IDE的新工程中导入 jxos.c
	在IDE的新工程中导入需要的Jxos模块的文件（kernel、sys_service ...）
	在IDE的新工程中导入 main.c， isr.c， callback_handler.c
	在IDE的新工程中导入用户任务文件 （\工程名称\app\）
	在IDE的新工程中增加include目录，包括：\joxs，\platform\芯片型号\工程命名\config
	在IDE的新工程中增加其他的include目录，如芯片库文件的头文件
	按照框架实现用户任务

如何配置与使用 kernel
如何配置与使用 sys_service
如何配置与使用 std_app
如何使用标准框架的 driver 模块
如何使用标准框架的 lib 模块

```

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request

#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
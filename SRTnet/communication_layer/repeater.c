
#include "bsp/bsp_rf_driver.h"
#include <srtnet_config.h>
#include "transmitter.h"
#include "receiver.h"
#include "repeater.h"

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
void srtnet_comm_layer_repeater_device_handler(void)
{
    //not ok
    //with out event or msg, the chip will goto sleep.
    if(srtnet_comm_layer_receiver_device_receiver_ok_falg == 1){
        srtnet_comm_layer_receiver_device_receiver_ok_falg = 0;
        srtnet_comm_layer_transmitter_device_send_count = 0;
        bsp_rf_send(srtnet_comm_layer_receiver_device_receiver_buffer, srtnet_comm_layer_receiver_device_receiver_frame_len);
    }
}
#endif


#include "application_layer_service.h"
#include <srtnet_config.h>

#if (SERVICE_TEMPERATURE_MASTER_ENABLE == 1)

void service_temperature_send_cmd_report_val(uint8_t master_service_serila_mun, uint16_t report_temperature_val)
{
	uint8_t param[3];
	param[0] = 2;
	param[1] = (report_temperature_val>>8);
	param[2] = (report_temperature_val&0x00ff);
    srtnet_app_service_master_send_cmd(false, master_service_serila_mun,
									SERVICE_TEMPERATURE_ID,
									SERVICE_TEMPERATURE_CMD_REPORTVAL,
									&param);
}

#endif


#ifndef _APP_LAYER_BUNDLING_TABLE_H
#define _APP_LAYER_BUNDLING_TABLE_H

#include "lib/type.h"

void srtnet_app_layer_binding_table_clear(uint8_t slave_service_serila_mun);
uint8_t srtnet_app_layer_binding_table_add(uint16_t master_addr, uint8_t master_service_serila_mun, uint8_t master_service_id,
							uint8_t slave_service_serila_mun);
uint8_t srtnet_app_layer_binding_table_del(uint16_t master_addr, uint8_t master_service_serila_mun, uint8_t master_service_id,
							uint8_t slave_service_serila_mun);
void srtnet_app_layer_binding_table_lookup_reset(void);
uint8_t srtnet_app_layer_binding_table_lookup(uint16_t master_addr, uint8_t master_service_serila_mun, 
									uint8_t master_service_id, uint8_t* slave_service_serila_mun);
uint8_t srtnet_app_layer_binding_table_is_empty(void);
uint8_t srtnet_app_layer_binding_table_recover(void);

#endif

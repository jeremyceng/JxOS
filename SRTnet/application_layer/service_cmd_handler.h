#ifndef __SRTNET_SERCICE_CMD_HANDLER_H
#define __SRTNET_SERCICE_CMD_HANDLER_H

/************************************************************************/
//general service
//onoff
void service_onoff_rec_cmd_on_handler(uint8_t slave_service_serila_mun);
void service_onoff_rec_cmd_off_handler(uint8_t slave_service_serila_mun);
void service_onoff_rec_cmd_toggle_handler(uint8_t slave_service_serila_mun);

/************************************************************************/
void srtnet_app_layer_service_id_temperature_rec_cmd_report_val(uint8_t serila_mun, uint16_t report_temperature_val);

/************************************************************************/
//default service
//binding
void service_binding_rec_cmd_bind_handler(uint8_t* param);
void service_binding_rec_cmd_unbind_handler(uint8_t* param);

#endif


#ifndef __SRTNET_SERCICE_H
#define __SRTNET_SERCICE_H

#include "lib/type.h"


/************************************************************************/
enum
{
	SERVICE_CONFIG_TABLE_SERVICE_ID_OBJECT = 0,
	SERVICE_CONFIG_TABLE_SERVICE_M_S_OBJECT,
};
extern uint8_t service_config_table[][2];
extern uint8_t SERVICE_CONFIG_TABLE_LEN;
/************************************************************************/

/************************************************************************/
void srtnet_app_layer_service_slave_receive_cmd_handler(uint8_t slave_service_serila_mun, uint8_t service_id, uint8_t service_cmd_id,
												uint8_t* service_cmd_param);
uint8_t srtnet_app_layer_service_master_send_cmd(uint8_t is_broadcast, uint8_t master_service_serila_mun, uint8_t service_id, uint8_t service_cmd_id,
										uint8_t* service_cmd_param, uint8_t service_cmd_param_len);
/************************************************************************/

/************************************************************************/
uint8_t srtnet_app_layer_service_set_active_service(uint8_t serila_mun);
uint16_t srtnet_app_layer_service_get_active_service_mark(void);
void srtnet_app_layer_service_no_active_service(void);
/************************************************************************/


#endif

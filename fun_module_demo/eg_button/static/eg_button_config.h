
#ifndef __BUTTON_CONFIG_H
#define __BUTTON_CONFIG_H


/**********************************************************/
//config_attribute
#define button_press_state_config 		GPIO_PIN_LOW_LEVEL
#define jitter_tick_config				1
#define long_press_tick_config			100
#define long_press_repeat_tick_config	50

#endif

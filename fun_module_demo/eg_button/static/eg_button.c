

#include "eg_button.h"
/*********************************/
//config files
#include "eg_button_config.h"
//every project using this module has their own config files
//the config filse inchulde this files:
//module_config.h, module_config_callback.c, module_event_callback.c


/**********************************************************/
//config_attribute
//button_press_state_config 		
//jitter_tick_config				
//long_press_tick_config			
//long_press_repeat_tick_config

/***********************************************************/
//config_callbakc
extern uint8_t button_init_config_callbakc(uint8_t button_index);			//hal init
extern uint8_t button_read_state_config_callbakc(uint8_t button_index);		//read pin level

/***********************************************************/
//event_callback
extern void button_press_event_callback(uint8_t button_index);
extern void button_release_event_callback(uint8_t button_index);
extern void button_long_press_event_callback(uint8_t button_index);

/**********************************************************/
//public_action
void button_tick_scan_handler(BUTTON_STRUCT* button, uint8_t button_index, uint8_t tick)
{
	uint8_t button_state;
	if(button_read_state_config_callbakc != 0){
		button_state = button_read_state_config_callbakc(button_index);
		if(button_state == !(button_press_state_config)){
			if(button->last_state == !(button_press_state_config)){
				//button free
			}else{
				if(button->press_tick_count >= jitter_tick_config){
					if(button_release_event_callback != 0){
						button_release_event_callback(button_index);
					}
				}
			}
			button->press_tick_count = 0;
		}
		else{
			if(button->last_state == !(button_press_state_config)){
				if(button->press_tick_count == 0){
					if(button_press_event_callback != 0){
						button_press_event_callback(button_index);
					}
				}
			}else{
				if(button->press_tick_count >= long_press_tick_config){
					if(button_long_press_event_callback != 0){
						button_long_press_event_callback(button_index);
					}
					button->press_tick_count -= long_press_repeat_tick_config;
				}
			}
			button->press_tick_count += tick;
		}
	}
	button->last_state = button_state;
}

void button_new(BUTTON_STRUCT* button)
{
	/**********************************************************/
	//private_attribute
	button->last_state = 0;
	button->press_tick_count = 0;
	/**********************************************************/
	//public_attribute
}

/*********************************
//how to use
//1. Create new config filse for the project using this module
//2. Define the config_attributes in module_config.h
//3. Implement the config_callbacks in module_config_callback.c 
//4. Implement the event_callbacks in module_event_callback.c

#define button_num_max 10
void main(void)
{
	uint8_t i;
	uint8_t tick;
	BUTTON_STRUCT n_button[button_num_max];
	
	for(i = 0; i < button_num_max; i++){
		button_new(&(n_button[i]));
	}
	
	/***********************************************************
	for(i = 0; i < button_num_max; i++){
		button_init_config_callbakc(i);
	}

	while(1){
		tick = get_tick();
		for(i = 0; i < button_num_max; i++){
			button_tick_scan_handler((&(n_button[i]), i, tick);
		}
	}
}
*********************************/
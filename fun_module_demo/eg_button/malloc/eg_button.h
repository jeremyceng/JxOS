
#ifndef __BUTTON__H
#define __BUTTON__H


#include "type.h"

enum {
	BUTTON_RELEASE_STATE = 0,
	BUTTON_PRESS_STATE,
};

typedef struct __BUTTON_STRUCT__ BUTTON_STRUCT;
struct __BUTTON_STRUCT__{
	/**********************************************************/
	//private_attribute
	uint8_t last_state;
	uint8_t press_tick_count;
	/**********************************************************/
	//public_attribute

	/**********************************************************/
	//config_attribute
	uint8_t button_press_state_config;
	uint8_t jitter_tick_config;
	uint8_t long_press_tick_config;
	uint8_t long_press_repeat_tick_config;
	
	/**********************************************************/
	//private_action

	/**********************************************************/
	//public_action
	void button_tick_scan(BUTTON_STRUCT* button, uint8_t tick);
	uint8_t button_state_read(BUTTON_STRUCT* button);
	
	/***********************************************************/
	//config_callbakc
	uint8_t (*button_init_config_callbakc)(BUTTON_STRUCT* button);			//hal init
	uint8_t (*button_read_state_config_callbakc)(BUTTON_STRUCT* button);	//read pin level
	
	/***********************************************************/
	//event_callback
	void (*button_press_event_callback)(BUTTON_STRUCT* button);
	void (*button_release_event_callback)(BUTTON_STRUCT* button);
	void (*button_long_press_event_callback)(BUTTON_STRUCT* button);
};

BUTTON_STRUCT* button_new(BUTTON_STRUCT* button);

#endif



void origin_fun_blocked_delay()
{
	start;
	
	...
	aaa;
	delay(100);
	bbb;
	...
	
	...
	ccc;
	delay(2000);
	ddd;
	...
	
	...
	ccc;
	delay(5000);
	ddd;
	...
	
	
	if(){
		eee;
		delay(100);
		fff;
		...
		ggg;
		delay(100);
		hhh;
	}
	else if()
	{
		iii;
		delay(100);
		jjj;	
	}
	else{
		kkk;
		delay(100);
		lll;
	}
	
	//loop is not allow to convert to unblocked delay frame
	
	end;
}


#define delay_lable_0_DEF		0
#define delay_lable_1_DEF		1
#define delay_lable_2_DEF		2
#define delay_lable_3_DEF		3
#define delay_lable_4_DEF		4
#define delay_lable_5_DEF		5
#define delay_lable_6_DEF		6
#define delay_lable_NONE_DEF	0xff

uint16_t delay_counter = 0;
uint8_t delay_lable_id = delay_lable_NONE_DEF;

void tick_handler_unblocked_delay()
{
/*******************************************/
	if(delay_counter > 0){
		delay_counter--;
	}
	if(delay_counter > 0){
		return;
	}
        switch(delay_lable_id){
			case delay_lable_0_DEF:
				goto delay_lable_0;
				break;

			case delay_lable_1_DEF:
				goto delay_lable_1;
				break;

			case delay_lable_2_DEF:
				goto delay_lable_2;
				break;

			case delay_lable_3_DEF:
				goto delay_lable_3;
				break;

			case delay_lable_4_DEF:
				goto delay_lable_4;
				break;

			case delay_lable_5_DEF:
				goto delay_lable_5;
				break;

			case delay_lable_6_DEF:
				goto delay_lable_6;
				break;

			case delay_lable_NONE_DEF:
				goto delay_lable_none;
			default:
				break;
        }
	}
/*******************************************/

delay_lable_none:	
	start;
	
	...
	aaa;
//	delay(100);
/*******************************************/
	delay_counter = 100;
	delay_lable_id = delay_lable_0_DEF;
	return;
delay_lable_0:	
/*******************************************/
	bbb;
	...
	
	...
	ccc;
//	delay(2000);
/*******************************************/
	delay_counter = 2000;
	delay_lable_id = delay_lable_1_DEF;
	return;
delay_lable_1:	
/*******************************************/
	ddd;
	...
	
	...
	ccc;
//	delay(4000);
/*******************************************/
	delay_counter = 4000;
	delay_lable_id = delay_lable_2_DEF;
	return;
delay_lable_2:	
/*******************************************/
	ddd;
	...
	
	
	if(){
		eee;
//		delay(100);
/*******************************************/
	delay_counter = 100;
	delay_lable_id = delay_lable_3_DEF;
	return;
delay_lable_3:	
/*******************************************/
		fff;
		...
		ggg;
//		delay(100);
/*******************************************/
	delay_counter = 100;
	delay_lable_id = delay_lable_4_DEF;
	return;
delay_lable_4:	
/*******************************************/
		hhh;
		
/*******************************************/
//when there was a "if" code segment, 
//must add this code at the end of the "if" code segment, 
//with as here:
	delay_lable_id = delay_lable_NONE_DEF;
/*******************************************/
	}
	else if()
	{
		iii;
//		delay(100);
/*******************************************/
	delay_counter = 100;
	delay_lable_id = delay_lable_5_DEF;
	return;
delay_lable_5:	
/*******************************************/
		jjj;

/*******************************************/
//when there was a "if" code segment, 
//must add this code at the end of the "if" code segment, 
//with as here:
	delay_lable_id = delay_lable_NONE_DEF;
/*******************************************/
	}
	else{
		kkk;
//		delay(100);
/*******************************************/
	delay_counter = 100;
	delay_lable_id = delay_lable_6_DEF;
	return;
delay_lable_6:	
/*******************************************/
		lll;

/*******************************************/
//when there was a "if" code segment, 
//must add this code at the end of the "if" code segment, 
//with as here:
	delay_lable_id = delay_lable_NONE_DEF;
/*******************************************/
	}
	
	end;
}

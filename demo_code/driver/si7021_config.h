#ifndef _SI7021_CONFIG_H
#define _SI7021_CONFIG_H

#include "stdint.h"

void si7021_hal_delay_ms(uint32_t ms);

uint8_t	si7021_hal_i2c_write_device(uint8_t addr, uint8_t *data, uint8_t len);
uint8_t	si7021_hal_i2c_read_device(uint8_t addr, uint8_t *data, uint8_t len);
uint8_t	si7021_hal_i2c_write_device_reg(uint8_t addr, uint8_t reg_addr, uint8_t *data, uint8_t len);
uint8_t	si7021_hal_i2c_read_device_reg(uint8_t addr, uint8_t reg_addr, uint8_t *data, uint8_t len);

#endif
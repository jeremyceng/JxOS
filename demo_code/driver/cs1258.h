#ifndef __CS_1258_H__
#define __CS_1258_H__

#include "stm32f10x.h"

void CS1258_CS_HIGH(void);
void CS1258_CS_LOW(void);
void CS1258_PIN_INIT(void);
void CS1258_write(uint8_t send_data);
uint8_t CS1258_read(void);

void CC1258_init(void);

#endif


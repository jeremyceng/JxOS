
#include "stdio.h"
//HAL
#include "hal.h"
//BSP
//OS
#include "jxos_task.h"
//LIB
#include "ring_buff.h"
//SYS TASK
#include "sys_task.h"

//static uint8_t console_task_tx_buff_init_flag = 0;
//#define console_task_tx_buff_space_len 32
//RINGBUFF_MGR console_task_tx_buff;
//static RINGBUFF_OBG_TYPE console_task_tx_buff_space[console_task_tx_buff_space_len];

//static uint8_t console_task_id;
void console_task(uint8_t task_id, void * parameter)
{	
	/*
	RINGBUFF_OBG_TYPE c[console_task_tx_buff_space_len];
	uint16_t len = 0;
	uint16_t i = 0;

	len = ringbuff_check_use_space(&console_task_tx_buff);
	if(len != 0){
		ringbuff_read_data(&console_task_tx_buff, 
						   len, &c);

		for(i = 0; i < len; i++){
			Send_Data_To_UART0(c[i]);
		}
	}
	*/
	return;
}

void console_task_init(void)
{
	//HAL
	InitialUART0_Timer1(115200);           //UART0 Baudrate initial,T1M=0,SMOD=0
	
	//BSP

	//OS
//	jxos_task_create(console_task, "console_task", 0, &console_task_id);

	//LIB
//	RingBuff(&console_task_tx_buff,
//			 console_task_tx_buff_space_len,
//			 console_task_tx_buff_space);
//	console_task_tx_buff_init_flag = 1;
	
	//SYS TASK
}

char putchar(char c)
{
//	Send_Data_To_UART0(c);
    TI = 0;
    SBUF = c;
    while(TI==0);
	return 0;
}

/*
void print_int(uint16_t value)
{
    uint8_t p[5];
    uint8_t n = 0;
    uint8_t f = 0;

	if(value == 0){
		f += '0';
		ringbuff_write_data(&console_task_tx_buff, 1, &f);
	}
	else{
		memset(p, 0, 6);
		
		p[0] = (value%100000)/10000 + '0';
		p[1] = (value%10000)/1000 + '0';
		p[2] = (value%1000)/100 + '0';
		p[3] = (value%100)/10 + '0';
		p[4] = (value%10)/1 + '0';
		
		for(n = 0; n < 5; n++){
			if(p[n] == '0'){
				if(f != 0){
					if(p[n] != 0){
						ringbuff_write_data(&console_task_tx_buff, 1, &(p[n]));
					}
				}
			}
			else{
				if(p[n] != 0){
					f = 1;
					ringbuff_write_data(&console_task_tx_buff, 1, &(p[n]));
				}
			}
		}
	}
}

void print_str(const char* s)
{
	while(*s != 0){
		ringbuff_write_data(&console_task_tx_buff, 1, s);
		s++;
	}
}
*/


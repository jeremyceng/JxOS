#include "link_control.h"
#include "device_struct.h"
#include "time_slice_list.h"

/*************************************************************************************/
//以下为net层对link层的接口实现

static volatile uint8_t net_transfer_sync_flag = 0;
void net_transfer_start_interface(void)
{
	if((time_slice_list[time_slice_list_pionter] != 0)&&
			(time_slice_list_init_ok == 1)){
			
			link_channel = time_slice_list[time_slice_list_pionter]->channel;
			link_target_addr = time_slice_list[time_slice_list_pionter]->addr;
			link_tx_data_buffer = time_slice_list[time_slice_list_pionter]->tx_buffer;
			link_tx_data_buffer_len = time_slice_list[time_slice_list_pionter]->tx_buffer_len;
			
			link_rssi = &(time_slice_list[time_slice_list_pionter]->rssi);
			link_rx_data_buffer = time_slice_list[time_slice_list_pionter]->rx_buffer;
			link_rx_data_buffer_len = &(time_slice_list[time_slice_list_pionter]->rx_buffer_len);

			time_slice_list[time_slice_list_pionter]->state = DEVICE_STATE_BUZY;
		}
	else{
			link_target_addr = NOP_ADDR;
		}
}

//net_transfer_fail_interface 和 net_transfer_finish_interface 将在 timer 中断中执行
//所以只有 1 TICK 的时间，不能超过
void net_transfer_fail_interface(void)
{
	if((time_slice_list[time_slice_list_pionter] != 0)&&
			(time_slice_list_init_ok == 1)){
			time_slice_list[time_slice_list_pionter]->state = DEVICE_STATE_ERR;
			time_slice_list[time_slice_list_pionter]->fail_count++;
			time_slice_list[time_slice_list_pionter]->finish_count = 0;			
		}
	time_slice_list_pionter_run();
}

void net_transfer_finish_interface(void)
{
	if((time_slice_list[time_slice_list_pionter] != 0)&&
			(time_slice_list_init_ok == 1)){
			time_slice_list[time_slice_list_pionter]->state = DEVICE_STATE_OK;
			time_slice_list[time_slice_list_pionter]->finish_count++;
			time_slice_list[time_slice_list_pionter]->fail_count = 0;
			//		net_transfer_sync_flag = 1;
		}
	time_slice_list_pionter_run();
}

#ifndef _time_slice_list_h_
#define _time_slice_list_h_


#include "device_struct.h"

#define TIME_SLICE_TOTAL	50
//#if(TIME_SLICE_TOTAL < 6)
//  #define TIME_SLICE_TOTAL 6
//#endif

extern DEVICE_STRUCT* time_slice_list[];
extern uint8_t	time_slice_list_pionter;
extern uint8_t	time_slice_list_init_ok;

void time_slice_list_init(void);
uint8_t time_slice_list_search_null(void);
void time_slice_list_pionter_run(void);
void time_slice_list_pionter_set(uint8_t mun);
void time_slice_list_logon(DEVICE_STRUCT* device_info, uint8_t time_slice_list_pionter_mun);
void time_slice_list_logoff(uint8_t time_slice_list_pionter_mun);

#endif


//TIME_SLICE_TOTAL = n*TICK_TIMER_TOTAL


#include "string.h"




//HOST不能死，死了以后 TIME_TICK 和  time_slice_list_pionter 就不同步了，
//SLAVE 需从新与 HOST 同步。（slave 复位）

//流程
//链路层初始化					link_init();
//时间片列表初始化				time_slice_list_init();
//设备解构体初始化				device_struct_init();
//设备结构体注册到时间片列表	time_slice_list_logon();
//link层回调接口赋值
//link层传输参数赋值			(已在回调中的net_transfer_start_interface接口中实现)
//链路层使能					link_enable();


//HOST 始终处于 time_slice_list_pionter_run 状态
//SLAVE 只有收到 BROADCAST_SYNC 才开始 time_slice_list_pionter_run 状态，否则将一直监听 BROADCAST_SYNC。
	

//从设备全时段接收功能，用于失去时间片基准的恢复定位



/*************************************************************************************
//以下为广播时间片初始化接口

#define BROADCAST_ADDR							0x00

#define BROADCAST_SYNC_CHANNEL					0x00
#define BROADCAST_REPLY_CHANNEL					0x01
#define BROADCAST_REGISTER_CHANNEL				0x02

#define BROADCAST_SYNC_TIME_SLICE				0x00
#define BROADCAST_REPLY_TIME_SLICE_1			0x01
#define BROADCAST_REGISTER_IME_SLICE_1			0x02

#if(TIME_SLICE_TOTAL > 15)
#define BROADCAST_REPLY_TIME_SLICE_2			TIME_SLICE_TOTAL/2 + 1
#define BROADCAST_REGISTER_IME_SLICE_2			TIME_SLICE_TOTAL/2 + 2
#endif

DEVICE_STRUCT broadcast_sync;
DEVICE_STRUCT broadcast_reply;
DEVICE_STRUCT broadcast_register;

static uint8_t	broadcast_sync_init_ok = 0;
void broadcast_sync_init(void)
{
//	device_struct_init(BROADCAST_ADDR, BROADCAST_SYNC_CHANNEL, &broadcast_sync);
	time_slice_list_logon(&broadcast_sync, BROADCAST_SYNC_TIME_SLICE);
	broadcast_sync_init_ok = 1;
}

void broadcast_reply_init(void)
{
//	device_struct_init(BROADCAST_ADDR, BROADCAST_REPLY_CHANNEL, &broadcast_reply);
	time_slice_list_logon(&broadcast_reply, BROADCAST_REPLY_TIME_SLICE_1);

#if(TIME_SLICE_TOTAL > 15)
	time_slice_list_logon(&broadcast_reply, BROADCAST_REPLY_TIME_SLICE_2);
#endif
}

void broadcast_register_init(void)
{
//	device_struct_init(BROADCAST_ADDR, BROADCAST_REGISTER_CHANNEL, &broadcast_register);

	time_slice_list_logon(&broadcast_register, BROADCAST_REGISTER_IME_SLICE_1);
#if(TIME_SLICE_TOTAL > 15)
	time_slice_list_logon(&broadcast_register, BROADCAST_REGISTER_IME_SLICE_2);
#endif
}


*************************************************************************************
//以下为网络使能接口

void net_enable(void)
{
	if((time_slice_list_init_ok == 1)
		&&(net_transfer_interface_init_ok == 1)
		&&(broadcast_sync_init_ok == 1)){
		link_enable();
	}
}

***********************************************************
void time_slice_list_print(void)
{
	uint8_t i;

	for(i = 0; i < TIME_SLICE_TOTAL; i++){
//		printf("%d: ", i);
		if(time_slice_list[i] != 0){
//			printf("addr: %d, channel: %d",
//					time_slice_list[i]->addr,
//					time_slice_list[i]->channel);
			if(time_slice_list[i]->addr == BROADCAST_ADDR){
//				printf(" -> 广播设备");
			}
//			printf("\r\n");
		}
		else{
//			printf("空设备\r\n");
		}
	}
}


*************************************************************************************
//以下为 HOST SLAVE 低速任务

#if(TIME_SLICE_TOTAL > 15)
#define SLAVE_CONNECT_MUN_MAX	(TIME_SLICE_TOTAL - 5)/2
#else
#define SLAVE_CONNECT_MUN_MAX	(TIME_SLICE_TOTAL)
#endif
DEVICE_STRUCT slave_device_list[SLAVE_CONNECT_MUN_MAX];

void slave_device_list_init(void)
{
	uint8_t i;
	for(i = 0; i < SLAVE_CONNECT_MUN_MAX; i++) {
		slave_device_list[i].addr = 0xff;
	}
}

DEVICE_STRUCT* slave_device_list_add(void)
{
	uint8_t i;
	for(i = 0; i < SLAVE_CONNECT_MUN_MAX; i++) {
		if(slave_device_list[i].addr == 0xff){
			break;
		}
	}

	if(i < SLAVE_CONNECT_MUN_MAX){
		return 	&(slave_device_list[i]);
	}
	else{
		return 	0;
	}
}

void slave_device_list_del(DEVICE_STRUCT *target)
{
	uint8_t i;
	for(i = 0; i < SLAVE_CONNECT_MUN_MAX; i++) {
		if(&(slave_device_list[i]) == target){
			slave_device_list[i].addr = 0xff;
			break;
		}
	}
}

uint8_t BROADCAST_REPLY_STATE = 0;
uint8_t BROADCAST_REGISTER = 0;
void net_host_logon_manager(void)
{
	DEVICE_STRUCT* device_p;
	uint8_t temp;

	if(BROADCAST_REPLY_STATE == 1){
		BROADCAST_REPLY_STATE = 0;
//		printf("net_host_logon_manager_ok\r\n");
		temp = time_slice_list_search_null();
		if(TIME_SLICE_TOTAL != temp){
			device_p = slave_device_list_add();
			if(device_p != 0){
//				device_struct_init(temp, temp, device_p);
				time_slice_list_logon(device_p, temp);
			}
//			printf("time_slice_list_logon\r\n");
			BROADCAST_REGISTER = 1;	//回复注册信息
		}
		else{
//			printf("fail\r\n");
			BROADCAST_REGISTER = 0;	//不回复
		}
	}
}

void net_host_logon_request(void)
{
//	printf("net_host_logon_request\r\n");
	BROADCAST_REPLY_STATE = 1;
}


void net_host_init(void)
{
	net_transfer_interface_init();

	time_slice_list_init();
	broadcast_sync_init();
	broadcast_reply_init();
	broadcast_register_init();
	time_slice_list_print();

	slave_device_list_init();
}
**************************************************/


/***
uint8_t BROADCAST_GET_REPLY_OK = 0;
uint8_t BROADCAST_GET_REPLY_OK = 0;
uint8_t net_save_logon_state = 0;
void net_save_logon_manager(void)
{
	if(net_save_logon_state == 0){
		if(BROADCAST_GET_REPLY_OK == 1)&&(){

		}

	}
}
***/
void lick_slave_init(void)
{
	//准备同步广播通道，回复广播通道，确认广播通道的数据；
}

//从设备通过住注册同步时间片编号，并得到属于自己的时间片
//设置当前时间片接口
void lick_s_task(void)	//执行时间 <= 一个循环时间
{
	//个时间片的RX数据处理（搬运到缓存池）
	//准备需要写入的数据
	//注册申请处理
}
/*********
void time_slice_list_manager(void)
{
	if(time_slice_list[time_slice_list_pionter]->addr != BROADCAST_ADDR){
		if(time_slice_list[time_slice_list_pionter]->fail_count > 10){
			time_slice_list_del();
		}
	}
}

**********/
/*******************************************
//net层接口
//初始化
//获取设备列表 	-> 已注册的设备地址表
//读设备		-> 设备状态缓存结构体
//写设备		-> 设备状态缓存结构体
//紧急事件回调	-> 上层提供的回调接口


//DEVICE_STRUCT test_1;
//DEVICE_STRUCT test_2;
//DEVICE_STRUCT test_3;

 add_test(void)
{//	device_struct_init(0x01, 0x01, &test_1);
//	device_struct_init(0x02, 0x02, &test_2);


	time_slice_list_logon(&test_1, 3);
	time_slice_list_logon(&test_2, 5);

}

void del_test(void)
{
	time_slice_list_logoff(3);

}

*****/







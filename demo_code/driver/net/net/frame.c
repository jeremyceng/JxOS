
#include "string.h"
#include "net_stdint.h"

/**************************************************************************/
//帧校验模块
static uint8_t chack_sum(uint8_t *dat, uint8_t len, uint8_t *chack)
{
	unsigned int i;
	*chack = 0;

	if(len == 0){
		return 0;
	}

	for(i = 0; i < len; i++)
	{
		*chack ^= *dat;
		dat++;
	}

	return 1;
}
//**************************************************************************/
//类型	长度(数据体)	数据体	校验(包含起始位)
//1B	1B				LEN		1B		
//组帧解帧模块
uint8_t framing(uint8_t cmd, uint8_t* dat, uint8_t data_len, uint8_t* frame, uint8_t* frame_len)
{
	uint8_t chack;
	uint8_t* frame_temp = frame;

	//判断输入参数
	if(data_len == 0){
		return 0;
	}
	if(data_len > 21){
		return 0;
	}

	//组帧
	*frame_temp = cmd;
	frame_temp++;
	*frame_temp = data_len;
	frame_temp++;

	memcpy(frame_temp, dat, data_len);
	frame_temp += data_len;

	chack_sum(frame, data_len+2, &chack);
	*frame_temp = chack;

	*frame_len = data_len+3;
	return 1;
}

uint8_t unframing(uint8_t* frame, uint8_t frame_len, uint8_t* dat, uint8_t* data_len, uint8_t* cmd)
{
	uint8_t chack = 0;
	uint8_t* frame_temp = frame;


	*cmd = *frame_temp;
	frame_temp++;

	*data_len = *frame_temp;
	if(*data_len  == 0){
		return 0;
	}
	frame_temp++;
	memcpy(dat, frame_temp, *data_len);
	frame_temp += *data_len;

	chack_sum(frame, *data_len+2, &chack);
	if(chack != *frame_temp){
		return 0;
	}

	return 1;
}


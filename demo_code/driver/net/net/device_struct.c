#include "string.h"
#include "device_struct.h"
/*************************************************************************************/
//以下为设备结构体初始化接口

DEVICE_STRUCT* device_struct_init(uint8_t addr, uint8_t channel, DEVICE_STRUCT* device_struct)
{
	memset(device_struct, 0x00, sizeof(DEVICE_STRUCT));
	device_struct->addr = addr;
	device_struct->channel = channel;
	device_struct->state = DEVICE_STATE_FREE;

	device_struct->finish_count = 0;
	device_struct->fail_count = 0;
	
	return device_struct;
}

void device_struct_writer_tx_buffer(DEVICE_STRUCT* device_struct, uint8_t* tx_data, uint8_t tx_data_len)
{
	memcpy(device_struct->tx_buffer, tx_data, tx_data_len);
	device_struct->tx_buffer_len = tx_data_len;
}

void device_struct_read_tx_buffer(DEVICE_STRUCT* device_struct, uint8_t* rx_data, uint8_t* rx_data_len)
{
//	*rx_data_len = *(device_struct->rx_buffer_len);
//	memcpy(rx_data, device_struct->tx_buffer, *rx_data_len);
}

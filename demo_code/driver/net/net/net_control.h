
#ifndef _net_control_h_
#define _net_control_h_

#include "net_stdint.h"

#define BROADCAST_ADDR							0x00
#define BROADCAST_SYNC_CHANNEL					0x00
#define BROADCAST_SYNC_TIME_SLICE				0x00


#define FRAME_TYPE_HOST_BROADCAST_SYNC	0X01
#define FRAME_TYPE_HOST_LOGON_REPONSE	0X02

#define FRAME_TYPE_SLAVE_LOGON_APPLY	0X81



#endif


#ifndef _transfer_control_config_h_
#define _transfer_control_config_h_

extern void (*EXTI_falling_cb)(void);
extern void (*EXTI_rising_cb)(void);
extern void (*TIMER_cb)(void);

#define HOST_DEVICE
//#define SLAVE_DEVICE

#endif // _transfer_control_config_h_

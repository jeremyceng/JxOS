#ifndef _tick_timer_
#define _tick_timer_

#include "net_stdint.h"
void tick_timer_task_list_init(void);
uint8_t tick_timer_task_register(void (*tick_timer_task)(void), uint8_t tick_timer_task_num);
void tick_timer_pionter_set(uint8_t tick_timer_pionter_num);

void tick_timer_enable(void);
void tick_timer_disable(void);
void tick_timer_process(void);	//ִ��


#endif

#include "net_stdint.h"
#include "tick_timer.h"
#include "link_control.h"
#include "link_control_config.h"
#include "cc1101.h"

#include "stdio.h"

//void uart_send(uint8_t c);
//void uart_print(uint8_t *str);

static uint8_t link_ack_frame[] = {0x00, 0x03, 'A', 'C', 'K', 0X00};

//link_target_addr = NOP_ADDR 无效设备地址
//当无效设备时，不操作RF设备！！！！！！！！！！！！
/******************************************************************/
//以下为 LINK 层的内部变量

/******************************************************************/
//以下为 LINK 层的对外变量
//输入变量
uint8_t link_force_sleep_enable;

uint8_t link_channel;
uint8_t link_target_addr;
uint8_t* link_tx_data_buffer;
uint8_t link_tx_data_buffer_len;

//输出变量
uint8_t* link_rssi;
uint8_t* link_rx_data_buffer;
uint8_t* link_rx_data_buffer_len;


/******************************************************************/
//以下为 LINK 层回调
//是否有必要传出 接收成功 和 发送成功 两个状态//假设发送不会有失败的情况，只需传出 传输成功 一个这，如果失败一定是接收失败
void (*link_start_cb)(void);
void (*link_fail_cb)(void);
void (*link_finish_cb)(void);


/******************************************************************/
//以下为 LINK 层的实现
#ifdef HOST_DEVICE
enum {
	LINK_HOST_PWD,
	LINK_HOST_IDLE,
	LINK_HOST_SCAL,

	LINK_HOST_SEND_TO_SLAVE_SET,
	LINK_HOST_SEND_TO_SLAVE_START,
	LINK_HOST_SEND_TO_SLAVE_OK,


	LINK_HOST_RECEIVE_SLAVE_REPLY_SET,
	LINK_HOST_RECEIVE_SLAVE_REPLY_START,
	LINK_HOST_RECEIVE_SLAVE_REPLY_OK, 			//->  LINK_HOST_IDLE,

	LINK_HOST_ERROR
};
static volatile uint8_t link_host_state = LINK_HOST_IDLE;

static void link_host_wakeup(void)
{
//	if(link_start_cb != 0) {
//		link_start_cb();
//	}
	if(link_target_addr != NOP_ADDR) { //无效地址
		CC1101WriteCmd(CC1101_SIDLE);
		link_host_state = LINK_HOST_IDLE;
	}
	else{
//	  uart_print("NOP\r\n");
	}
}


static void link_host_scal(void)
{
	if(link_start_cb != 0) {
		link_start_cb();
	}
	if(link_target_addr != NOP_ADDR) { //无效地址
		//校准
		CC1101WriteCmd( CC1101_SCAL );
//		uart_send('0');
		link_host_state = LINK_HOST_SCAL;
	}
	else{
//		printf("NOP\r\n");
	}
}

static void link_host_stanby(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		//清除FIFO
		CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
		CC1101WriteCmd( CC1101_SFTX );
		CC1101WriteCmd( CC1101_SFRX );

		//设置信道
//		CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);

		//写发送数据
		//如果上层没有数据注入，则发送空包 -> 空包格式
		if(link_tx_data_buffer_len > 0){
			CC1101WriteCtrlReg(CC1101_TXFIFO, (link_tx_data_buffer_len + 1));
			CC1101WriteCtrlReg(CC1101_TXFIFO, link_target_addr);
			CC1101WriteMultiCtrlReg(CC1101_TXFIFO, link_tx_data_buffer, \
									link_tx_data_buffer_len);
			link_tx_data_buffer_len = 0;
		}
		else{
		//写入空包
			CC1101WriteCtrlReg(CC1101_TXFIFO, (6 + 1));
			CC1101WriteCtrlReg(CC1101_TXFIFO, link_target_addr);
			CC1101WriteMultiCtrlReg(CC1101_TXFIFO, link_ack_frame, 6);
		}
		
//		uart_send('1');
		link_host_state =  LINK_HOST_IDLE;
	}
}

static void link_host_send_to_slave(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		//进入TX模式
		CC1101WriteCmd( CC1101_STX );
//		uart_send('2');
		link_host_state = LINK_HOST_SEND_TO_SLAVE_SET;
	}
}

static void link_host_receive_slave_reply(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_host_state){
			case LINK_HOST_IDLE:
				break;
				
			case LINK_HOST_SEND_TO_SLAVE_SET:
			case LINK_HOST_SEND_TO_SLAVE_START:				
			case LINK_HOST_SEND_TO_SLAVE_OK:				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//				uart_send('5');
				link_host_state = LINK_HOST_RECEIVE_SLAVE_REPLY_SET;				
				break;
				
			default:
				CC1101WriteCmd(CC1101_SIDLE);	
				link_host_state = LINK_HOST_IDLE;
				break;
		}
	}
}

static void link_host_link_process_not_ok(void)			//错误，失败，超时处理
{
	uint8_t link_process_ok_flag = 0;
		
	if(link_target_addr != NOP_ADDR) { //是否无效地址
		if(link_host_state == LINK_HOST_RECEIVE_SLAVE_REPLY_OK){	
			//读出RXFIFO接收到的数据
			*link_rx_data_buffer_len = (CC1101ReadStatusReg( CC1101_RXBYTES )  & NUM_RXBYTES);
			if(*link_rx_data_buffer_len > 0) {
				*link_rx_data_buffer_len = (CC1101ReadCtrlReg(CC1101_RXFIFO) - 1);
				if(*link_rx_data_buffer_len > 24){
					*link_rx_data_buffer_len = 24;
				}
				link_target_addr = CC1101ReadCtrlReg(CC1101_RXFIFO);
				//如果没有地址过滤功能，在此判断地址是否正确
				//if(CC1101ReadCtrlReg(CC1101_RXFIFO) != link_target_addr);
				
				CC1101ReadMultiCtrlReg(CC1101_RXFIFO, link_rx_data_buffer, *link_rx_data_buffer_len);
				*link_rssi = CC1101ReadCtrlReg(CC1101_RXFIFO);
				
				//进入IDLE模式 自动进入IDLE
				//CC1101WriteCmd(CC1101_SIDLE);
				printf("get slave\r\n");
//				uart_send('7');
//				uart_print("\r\n");
				
				link_process_ok_flag = 1;
			}
		}
		
//		if(link_force_sleep_enable == 1){
//			//进入PWD模式
//			CC1101WriteCmd(CC1101_SIDLE);
//			CC1101WriteCmd( CC1101_SPWD );	
//			link_host_state = LINK_SLAVE_PWD;
//			uart_send('s');
//			uart_print("\r\n");
//		}
//		else{
			if(link_host_state != LINK_HOST_IDLE){
				CC1101WriteCmd(CC1101_SIDLE);	
				link_host_state = LINK_HOST_IDLE;
			}
//		}
				
		if(link_process_ok_flag == 1){
			if(link_finish_cb != 0) {
				link_finish_cb();
			}		
		}
		else{
			if(link_fail_cb != 0) {
				link_fail_cb();
			}		
		}
		
	}
	else {	//无效地址必须失败
		if(link_fail_cb != 0) {
			link_fail_cb();
		}
	}
}

static void link_host_gpo_falling_cb(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_host_state) {
			case LINK_HOST_SEND_TO_SLAVE_SET:
				link_host_state = LINK_HOST_SEND_TO_SLAVE_START;
//				uart_send('3');
				break;


			case LINK_HOST_RECEIVE_SLAVE_REPLY_SET:
//				uart_send('6');
				link_host_state = LINK_HOST_RECEIVE_SLAVE_REPLY_START;
				break;

			default:	//LINK_HOST_ERROR
				link_host_state = LINK_HOST_ERROR;
				break;
		}
	}
}

static void link_host_gpo_rising_cb(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_host_state) {
			case LINK_HOST_SEND_TO_SLAVE_START:
				link_host_state = LINK_HOST_SEND_TO_SLAVE_OK;
//				uart_send('4');
				break;

			case LINK_HOST_RECEIVE_SLAVE_REPLY_START:
				link_host_state = LINK_HOST_RECEIVE_SLAVE_REPLY_OK;			
				break;

			default:	//LINK_HOST_ERROR
				link_host_state = LINK_HOST_ERROR;
				break;
		}
	}
}

#endif // HOST_DEVICE

/************************/

#ifdef SLAVE_DEVICE
enum {
	LINK_SLAVE_PWD,
	LINK_SLAVE_IDLE,
	LINK_SLAVE_SCAL,

	LINK_SLAVE_RECEIVE_HOST_SET,
	LINK_SLAVE_RECEIVE_HOST_START,
	LINK_SLAVE_RECEIVE_HOST_OK,

	LINK_SLAVE_WRITE_REPLY_DATA,
	
	LINK_SLAVE_REPLY_TO_HOST_SET,
	LINK_SLAVE_REPLY_TO_HOST_START,
	LINK_SLAVE_REPLY_TO_HOST_OK,

	LINK_SLAVE_ERROR
};
static volatile uint8_t link_slave_state = LINK_SLAVE_IDLE;

static void link_slave_wakeup(void)
{
	if(link_start_cb != 0) {
		link_start_cb();
	}
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state){
//			case LINK_SLAVE_RECEIVE_HOST_SET:
//				break;
				
			case LINK_SLAVE_PWD:
				//唤醒 需要等待到唤醒为止
				CC1101WriteCmd(CC1101_SIDLE);
//				uart_send('0');
				link_slave_state = LINK_SLAVE_IDLE;
				break;
				
			default:
				//清除FIFO
				CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//			uart_send('Y');
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;
				break;
		}
	}
	else{
//	  uart_print("NOP\r\n");
	}
}

static void link_slave_scal(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state){
			case LINK_SLAVE_RECEIVE_HOST_SET:
				break;
				
			case LINK_SLAVE_IDLE:
				//校准
				CC1101WriteCmd( CC1101_SCAL );
//			uart_send('1');
				link_slave_state = LINK_SLAVE_SCAL;
				break;
				
			default:
				//清除FIFO
				CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//			uart_send('Y');
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;
				break;
		}
	}
}

static void link_slave_receive_host(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state){
			case LINK_SLAVE_RECEIVE_HOST_SET:
				break;
				
			case LINK_SLAVE_SCAL:		
			default:
				//清除FIFO
				CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//				uart_send('2');
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;
				break;
		}
	}
}

static void link_slave_wite_reply_data(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state){
			case LINK_SLAVE_RECEIVE_HOST_SET:
				break;
			
			case LINK_SLAVE_RECEIVE_HOST_OK:
				//写发送数据
				//如果上层没有数据注入，则发送空包 -> 空包格式
				if(link_tx_data_buffer_len > 0){
					CC1101WriteCtrlReg(CC1101_TXFIFO, (link_tx_data_buffer_len + 1));
					CC1101WriteCtrlReg(CC1101_TXFIFO, link_target_addr);
					CC1101WriteMultiCtrlReg(CC1101_TXFIFO, link_tx_data_buffer, \
											link_tx_data_buffer_len);
					link_tx_data_buffer_len = 0;
				}
				else{
					//写入空包
					CC1101WriteCtrlReg(CC1101_TXFIFO, (6 + 1));
					CC1101WriteCtrlReg(CC1101_TXFIFO, link_target_addr);
					CC1101WriteMultiCtrlReg(CC1101_TXFIFO, link_ack_frame, 6);
				}
//				uart_send('5');	
				link_slave_state = LINK_SLAVE_WRITE_REPLY_DATA;
				break;
		
			default:
				//清除FIFO
				CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//			uart_send('Y');
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;
				break;
		}
	}
}

static void link_slave_reply_to_host(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state){
			case LINK_SLAVE_RECEIVE_HOST_SET:
				break;
				
			case LINK_SLAVE_WRITE_REPLY_DATA:
				//进入TX模式
				CC1101WriteCmd( CC1101_STX );
//				uart_send('6');
				link_slave_state = LINK_SLAVE_REPLY_TO_HOST_SET;
				break;
				
			default:
				//清除FIFO
				CC1101WriteCmd( CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
//			uart_send('Y');
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;
				break;
		}
	}
}
	
static void link_slave_link_process_not_ok(void)	//错误，失败，超时处理
{
	uint8_t link_process_ok_flag = 0;
	
	if(link_target_addr != NOP_ADDR) { //是否无效地址
		//应继续保持RX模式，保证下一个时间间隔能收到数据
		//由于有地址滤波，无需担心收到属于其他设备的数据
		//如果没有硬件地址滤波功能，可以用软件在 link_process_ok_flag = 1 中实现
		
		//如果没有收到HOST,保持RX状态（只要收到LINK_SLAVE_RECEIVE_HOST_START，就算收到HOST）
		//只要收到了HOST，并且进入TX状态就算传输成功，并重新开始新的一轮

		if(link_slave_state == LINK_SLAVE_REPLY_TO_HOST_OK){
			//读出RXFIFO接收到的数据
			*link_rx_data_buffer_len = (CC1101ReadStatusReg( CC1101_RXBYTES )  & NUM_RXBYTES);
			if(*link_rx_data_buffer_len > 0) {
				*link_rx_data_buffer_len = (CC1101ReadCtrlReg(CC1101_RXFIFO) - 1);
				if(*link_rx_data_buffer_len > 24){
					*link_rx_data_buffer_len = 24;
				}
				link_target_addr = CC1101ReadCtrlReg(CC1101_RXFIFO);
				//如果没有地址过滤功能，在此判断地址是否正确
				//if(CC1101ReadCtrlReg(CC1101_RXFIFO) != link_target_addr);
				
				CC1101ReadMultiCtrlReg(CC1101_RXFIFO, link_rx_data_buffer, *link_rx_data_buffer_len);
				*link_rssi = CC1101ReadCtrlReg(CC1101_RXFIFO);
				
				link_process_ok_flag = 1;
//				uart_send('9');
			}
		}
		
		if((link_force_sleep_enable == 1)||(link_process_ok_flag == 1)){
			//进入PWD模式
			CC1101WriteCmd(CC1101_SIDLE);
			CC1101WriteCmd( CC1101_SPWD );	
			link_slave_state = LINK_SLAVE_PWD;
//			uart_send('s');
//			uart_print("\r\n");
		}
		else{
			if(link_slave_state != LINK_SLAVE_RECEIVE_HOST_SET){
				//清除FIFO
				CC1101WriteCmd(CC1101_SIDLE);	//MUST BE IDLE MODE
				CC1101WriteCmd( CC1101_SFTX );
				CC1101WriteCmd( CC1101_SFRX );
				
				//设置信道
				//CC1101WriteCtrlReg(CC1101_CHANNR, link_channel);
				
				//进入RX模式
				CC1101WriteCmd( CC1101_SRX );
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_SET;			
			}
//			uart_send('x');
//			uart_print("\r\n");		  
		}
		
		if(link_process_ok_flag == 1){
			if(link_finish_cb != 0) {
				link_finish_cb();
			}		
		}
		else{
			if(link_fail_cb != 0) {
				link_fail_cb();
			}		
		}

	}
	else {	//无效地址必须失败
		if(link_fail_cb != 0) {
			link_fail_cb();
		}
	}
}


static void link_slave_gpo_falling_cb(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state) {
			case LINK_SLAVE_RECEIVE_HOST_SET:
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_START;
				//对时
				tick_timer_pionter_set(5);
//				uart_send('3');
				break;

			case LINK_SLAVE_REPLY_TO_HOST_SET:
				link_slave_state = LINK_SLAVE_REPLY_TO_HOST_START;
//				uart_send('7');
				break;

			default:
				link_slave_state = LINK_SLAVE_ERROR;
				break;
		}
	}
}

static void link_slave_gpo_rising_cb(void)
{
	if(link_target_addr != NOP_ADDR) { //无效地址
		switch(link_slave_state) {
			case LINK_SLAVE_RECEIVE_HOST_START:
				link_slave_state = LINK_SLAVE_RECEIVE_HOST_OK;				
//				uart_send('4');
				break;

			case LINK_SLAVE_REPLY_TO_HOST_START:
				link_slave_state = LINK_SLAVE_REPLY_TO_HOST_OK;
//				uart_send('8');
				break;

			default:
				link_slave_state = LINK_SLAVE_ERROR;
				break;
		}
	}
}

#endif


/******************************************************************/
void link_enable(void)
{
	tick_timer_enable();
}

void link_init(void)
{
//输入变量
	link_force_sleep_enable = 0;
	
	link_channel = 0;
	link_target_addr = 0;
	link_tx_data_buffer = 0;
	link_tx_data_buffer_len = 0;

//输出变量
	link_rssi = 0;
	link_rx_data_buffer = 0;
	link_rx_data_buffer_len = 0;

	link_start_cb = 0;
	link_fail_cb = 0;
	link_finish_cb = 0;


	TIMER_cb = tick_timer_process;
	tick_timer_task_list_init();

#ifdef HOST_DEVICE
	CC1101SetAddress(0XFE);
	
	link_host_state = LINK_HOST_IDLE;

	EXTI_falling_cb = link_host_gpo_falling_cb; 
	EXTI_rising_cb = link_host_gpo_rising_cb;	
	tick_timer_task_register(link_host_scal, 1);
	tick_timer_task_register(link_host_stanby, 3);
	tick_timer_task_register(link_host_send_to_slave, 4);
	tick_timer_task_register(link_host_receive_slave_reply, 7);
	tick_timer_task_register(link_host_link_process_not_ok, 11);
#endif // HOST_DEVICE

#ifdef SLAVE_DEVICE
	CC1101SetAddress(0X05);

	link_slave_state = LINK_SLAVE_PWD;//LINK_SLAVE_IDLE;	//也可以？？

	EXTI_falling_cb = link_slave_gpo_falling_cb;
	EXTI_rising_cb = link_slave_gpo_rising_cb;
	tick_timer_task_register(link_slave_wakeup, 0);
	tick_timer_task_register(link_slave_scal, 1);
	tick_timer_task_register(link_slave_receive_host, 3);
	tick_timer_task_register(link_slave_wite_reply_data, 7);
	tick_timer_task_register(link_slave_reply_to_host, 8);
	tick_timer_task_register(link_slave_link_process_not_ok, 11);
#endif // SLAVE_DEVICE
}

void link_reset(void)
{
	link_init();

//	enableInterrupts();        

	link_enable();
}
  

/*********************************************
static uint8_t link_channel;
static uint8_t link_target_addr;
static uint8_t *link_tx_buffer_p;
static uint8_t *link_tx_buffer_len;
static uint8_t *link_rx_buffer_p;
static uint8_t *link_rx_buffer_len;
static uint8_t *link_rssi;

void link_load_param(uint8_t channel,
                      uint8_t addr,
                      uint8_t *tx_buffer_p,
                      uint8_t *tx_buffer_len,
                      uint8_t *rx_buffer_p,
                      uint8_t *rx_buffer_len,
                      uint8_t *rssi)
{
	link_channel			=  channel;
	link_target_addr          	=  addr;
	link_tx_buffer_p   	=  tx_buffer_p;
	link_tx_buffer_len 	=  tx_buffer_len;
	link_rx_buffer_p   	=  rx_buffer_p;
	link_rx_buffer_len 	=  rx_buffer_len;
	link_rssi				=  rssi;

//	printf("注入参数 -> 地址: %d, 信道: %d\r\n", link_target_addr, link_channel );
}
********************************************/

/**************************************************
//HOST
//流程
1.link_host_stanby
  //写数据到TXFIFO
  //设置信道
 //校准

->延时，保证校准完成

2.link_host_send_to_slave
	//进入TX模式

->等待GOD发生变化		（等待前导发送完成）
->等待GOD再次发生变化	（等待数据发送完成）

3.link_host_send_to_slave_ok_cb
  	//进入RX模式	（开始接收回复）

->等待GOD发生变化		（等待前导接收完成）
->等待GOD再次发生变化	（等待数据接收完成）->4
->在一定时间内没有等待到数据接收完成（超时）->5

4.link_host_receive_slave_reply_ok_cb
  //读出RXFIFO
  //进入IDLE模式
  //清除TX RX FIFO

->返回 1.

5.link_host_receive_slave_reply_not_ok
  //进入IDLE模式
  //清除TX RX FIFO

  ->返回 1.
**************************************************/
/**************************************************
//SLAVE
//流程
1.link_slave_wakeup_receive_host
  //唤醒
  //延时240us	??

  //进入RX模式

->延时，保证校准完成

2.link_host_send_to_slave
	//进入TX模式

->等待GOD发生变化		（等待前导发送完成）
->等待GOD再次发生变化	（等待数据发送完成）

3.link_host_send_to_slave_ok_cb
  	//进入RX模式	（开始接收回复）

->等待GOD发生变化		（等待前导接收完成）
->等待GOD再次发生变化	（等待数据接收完成）->4
->在一定时间内没有等待到数据接收完成（超时）->5

4.link_host_receive_slave_reply_ok_cb
  //读出RXFIFO
  //进入IDLE模式
  //清除TX RX FIFO

->返回 1.

5.link_host_receive_slave_reply_not_ok
  //进入IDLE模式
  //清除TX RX FIFO

  ->返回 1.
***************************************************/

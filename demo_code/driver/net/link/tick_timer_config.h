#ifndef _TIME_TICK_CONFIG_
#define _TIME_TICK_CONFIG_

/********************************
//void tick_timer_timer_source_reset_cb_regist(void (*tick_timer_timer_source_reset)(void));
//复位时钟源定时器函数示例
void tick_timer_timer_source_reset(void)
{
	timer_int_disable();
	timer_stop();
	timer_cuont_reg_reset();
	timer_start();
	timer_int_enable();
}
********************************/

void TIM2_reset(void);
#define tick_timer_timer_source_reset()	 TIM2_reset()	//void tick_timer_timer_source_reset(void);

#define TICK_TIMER_TOTAL 12

#endif

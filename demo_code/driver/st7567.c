#include "stdint.h"		//M0数据结构
#include "M051.h"
#include "Register_Bit.h"
#include "system.h"

//Define DATA_OUT's Port
#define RS 			P44_DOUT
#define RW 			P45_DOUT 	//WR
#define E  			P41_DOUT  	//RD
#define RST			P27_DOUT
#define CS1 		P26_DOUT
#define DATA_OUT	P0_DOUT
#define DATA_IN		P0_PIN

//Define Key's Input
#define AUTO 0x80		//P2^7: Input auto_run_id
#define ADDU 0x40		//P2^6: Display picture UP to end
#define SUBD 0x20		//P2^5: Display picture Down to begin
#define ADUP 0x10		//P2^4: For COG module:adjust Register's value to add
#define ADDN 0x08		//P2^3: For COG moduel:adjust Register's value to sub
#define KEY_IN	P2

//Instruction Table
#define DISPLAY_ON		0xAF
#define DISPLAY_OFF		0xAE
#define STE_START_LINE	



//For LCD to work
#define Rab		0x27
#define v0		0x19  	//9.0V
#define bias	0xa2	//1/9
unsigned char constr;	//对比度调节

//Control the program to run
#define BmpMax 8
unsigned char autoid;	//Indicate that LCD display automatically
unsigned char bmp_id;	//The picture's id to display
unsigned char dispid;	//To reflash the display picture


unsigned char lcd12864_addr;		//0~127
unsigned char lcd12864_page;		//0~7

#define dl0 500

void st7567_busy(void)
{
	unsigned char	busybit = 0xff;
	unsigned char	time = 0;
	DATA_OUT = 0xff;
	do{
	RS = 0;           // 读忙信号
	RW = 1;
	E = 0;
	delay(1);
	E = 1;
	delay(1);
	delay(1);
	busybit = DATA_OUT&0x80;
	E = 0;
	time++;
	}while((busybit != 0)&&(time <= 50));
}

unsigned char st7567_rreg (void)
{
	unsigned char temp;
//	st7567_busy();
	DATA_OUT = 0xff;
	RS = 0;          
	RW = 1;
	E = 0;
	delay(1);
	E = 1;
	delay(1);
	delay(1);
	temp = DATA_IN;
	E = 0;
	return temp;
}

unsigned char st7567_rdat (void)
{
	unsigned char temp;
//	st7567_busy();
	DATA_OUT = 0xff;
	RS = 1;          
	RW = 1;
	E = 0;
	delay(1);
	E = 1;
	delay(1);
	delay(1);
	temp = DATA_IN;
	E = 0;
	return temp;
}

void st7567_wcmd(unsigned char wcmd)    //6800
{
//	st7567_busy();
	RS = 0; 					//A0 置低，示意进行寄存器操作 
	RW = 0; 					//RW 置低，示意进行写入操作 
	E = 0; 						//EP 先置低，以便后面产生跳变沿 
	DATA_OUT = wcmd; 				//装载数据置总线 
//	delay(1);
	E = 1;						//产生有效的跳变沿 
//	delay(1);
//	delay(1);
	E = 0;						//EP 先置低，以便后面产生跳变沿 
//	delay(1);
}

void st7567_wdat(unsigned char wdat)
{
//	st7567_busy();
	RS = 1; 					//A0 置低，示意进行寄存器操作 
	RW = 0;						//RW 置低，示意进行写入操作 
	E = 0; 						//EP 先置低，以便后面产生跳变沿 
	DATA_OUT = wdat; 				//装载数据置总线 
//	delay(1);
	E = 1; 						//产生有效的跳变沿 
//	delay(1);
//	delay(1);
	E = 0; 						//EP 先置低，以便后面产生跳变沿 
//	delay(1);
}

unsigned char lcd12864_get_page(void)	//page 0~7
{
	return lcd12864_page;
}

unsigned char lcd12864_get_addr(void)	//addr 0~127
{
	return lcd12864_addr;
}

void lcd12864_set_page(unsigned char page)	//page 0~7
{
	lcd12864_page = page;
	if(page > 7)
		page = 7;
	st7567_wcmd(0xb0 + page);
}

void lcd12864_set_addr(unsigned char addr)	//addr 0~127
{
	lcd12864_addr = addr;
	if(addr > 127)
		addr = 127;	
	st7567_wcmd(0x10+(addr>>4));
	st7567_wcmd(addr&0x0f);
}

void lcd12864_wdat(unsigned char dat)
{
	st7567_wdat(dat);
	lcd12864_addr++;		//功能在低层完成
	if(lcd12864_addr >= 128){
		lcd12864_set_addr(0);
		lcd12864_set_page(lcd12864_page+1);	
	}
}


void lcd12864_init(void)
{
	P0_PMD = 0x0000aaaa;	//开漏模式

	P2_PMD &= ~Px6_PMD;		//P2.6
	P2_PMD |= Px6_OD;		//开漏模式

	P2_PMD &= ~Px7_PMD;		//P2.7
	P2_PMD |= Px7_OD;		//开漏模式
	
	P4_PMD &= ~Px1_PMD;		//P4.1
	P4_PMD |= Px1_OD;		//开漏模式

	P4_PMD &= ~Px4_PMD;		//P4.4
	P4_PMD |= Px4_OD;		//开漏模式

	P4_PMD &= ~Px5_PMD;		//P4.5
	P4_PMD |= Px5_OD;		//开漏模式

	CS1=0;
	RW =0;
	E = 0; 
	RS = 0; 	
	delay(100);
	RST=0;
	delay(400);
	RST=1;
	delay(400);
	st7567_wcmd(0xe2);	//software reset
	delay(100);

	st7567_wcmd(0xa0);	//SEG MX
	st7567_wcmd(0xa6);	//INV
	st7567_wcmd(0xa4);	//AP
	st7567_wcmd(0xa2);	//BS
	st7567_wcmd(0xE0);	//Read-modify-Write mode,Column address increment: Read:+0 , Write:+1
	st7567_wcmd(0xc8);	//COM Direction,Set output direction of COM
	st7567_wcmd(0x2f); 	//Power Control,Control built-in power circuit ON/OFF:VB VR VF 

	st7567_wcmd(0x81);	//SET EV
	st7567_wcmd(0x09);

	st7567_wcmd(0x20+6);//Regulation Ratio,Select regulation resistor ratio

	delay(100);
	st7567_wcmd(0xAF);	//Display ON/OFF
	
	delay(100);
	lcd12864_set_page(0);
	lcd12864_set_addr(0);
	st7567_wcmd(0x40); 	//Set Start Line 

/*
	DATA_OUT=0;
	RS =0;
	RW =0;
	E  =0;
	RST=0;
	CS1=0;
	RST=1;

	constr=v0;	
	autoid=1;
	bmp_id=0;		
	dispid=1;

	st7567_wcmd(0xa4);	//EON=0,normal display
	st7567_wcmd(0xa6);	//REV=0,normal display
	st7567_wcmd(0xa0);	//ADC=0,SEG0->SEG131
	st7567_wcmd(0x40);	//Initial display line for COM0
	st7567_wcmd(0xc8);	//SHL select,COM63->COM0
	delay(1);
	delay(1);


    st7567_wcmd(0x81);	//SET Register
    st7567_wcmd(constr);

    st7567_wcmd(0XF8);
	st7567_wcmd(0X00); //4
 	st7567_wcmd(0x27);	//SET Ra/Rb

	delay(1);
	delay(1);

    st7567_wcmd(bias);	//SET BIAS

	delay(1);
	delay(1);
    st7567_wcmd(0x2C);	//SET POWER CONTROL
	delay(1);
	delay(1);
    st7567_wcmd(0x2E);
	delay(1);
	delay(1);
    st7567_wcmd(0x2F);
	delay(1);
	delay(1);
	st7567_wcmd(0xaf);	//dispaly on
*/
}


void lcd12864_clear(void)
{
	unsigned int i;
	lcd12864_set_page(0);
	lcd12864_set_addr(0);
	for(i = 0;i < 1024;i++){			
		lcd12864_wdat(0x00);
	}
	lcd12864_set_page(0);
	lcd12864_set_addr(0);
}

void lcd12864_dot(unsigned char x,unsigned char y,unsigned char on)
{
	unsigned char page,dot,j,temp;
	if((x < 128)&&(y < 64)){			
		page=y/8;	
		j=y-8*page;
		dot=0x01<<j;

		lcd12864_set_addr(x);
		lcd12864_set_page(page);
		if(on==0)
			temp = st7567_rdat()&~dot;
		else
			temp = st7567_rdat()|dot;
		lcd12864_set_addr(x);
		lcd12864_wdat(temp);
	}
}


#ifndef __MOTION_E93196_H__
#define __MOTION_E93196_H__	

void e93196_init(void);
void e93196_int_handler(void);

extern void (*e93196_int_callback)(uint8_t pin_level);

#endif

		  

void st7567_wcmd(unsigned char wrcmd);
void st7567_wdat(unsigned char wrdata);
unsigned char st7567_rreg (void);
unsigned char st7567_rdat (void);

unsigned char lcd12864_get_page(void);	//page 0~7
unsigned char lcd12864_get_addr(void);	//addr 0~127
void lcd12864_set_page(unsigned char page);	//page 0~7
void lcd12864_set_addr(unsigned char addr);	//addr 0~127
void lcd12864_wdat(unsigned char dat);
void lcd12864_init(void);
void lcd12864_clear(void);
void lcd12864_dot(unsigned char x,unsigned char y,unsigned char on);

/*****************88
void disp_frame();
void disp_all();
void Draw_word(unsigned int d_where,unsigned char page_f, //显示一个字或多个字函数
unsigned char page_sum,unsigned char column_f,unsigned char column_sum );
******************/

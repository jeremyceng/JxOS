/*******************************************************************************
 * @file    motion_E93196.h
 * @brief   the motion e9316 driver
 * @author  Jim, yaoxiangjie@leedarson.com
 * @version v1.0.0
 * @date    Tuesday, 2017-09-05
 * @par     Copyright
 * Copyright (c) Leedarson Lighting. All rights reserved.
 *
 * @par     History
 * 1.Date        :Tuesday, 2017-09-05
 *   Author      :Jim
 *   Version     :v1.0.0
 *   Modification:Create file, 
*******************************************************************************/
#ifndef __MOTION_E93196_H__
#define __MOTION_E93196_H__	

/*******************************************************************************
 * @brief SetE93196 to set e93196 register
 *
 * @param sensitivity [in] uint8_t, the sensitivity to set
 * @param pulse_counter [in] uint8_t, the pulse_counter to set
 * @param motion_enable [in] uint8_t, the motion_enable to set
 *
 * @return none
*******************************************************************************/
void SetE93196(uint8_t sensitivity , uint8_t pulse_counter, uint8_t motion_enable);

/*******************************************************************************
 * @brief SetE93196 Sensitivity level
 *
 * @param level [in] uint8_t, the level to set, total 4 level
 *
 * @return none
*******************************************************************************/
void SetE93196_Sensitivity_Level(uint8_t level);

/*******************************************************************************
 * @brief initial motion sensor
 *
 * @param none
 *
 * @return none
*******************************************************************************/
void Initial_E93196_Motion_Sensor(void);

/*******************************************************************************
 * @brief clear motion interrupt pin 
 *
 * @param none
 *
 * @return none
*******************************************************************************/
void E93196_Clear_Interrupt_Pin(void);
void E93196_read(void);

#endif
//#include <stdio.h>
//#include <stdlib.h>
void _itoa(int value, char *string)
{
	char tmp[33];
	char *tp = tmp;
	char i;
	unsigned int v;
	char sign;
	char *sp;

	if (string == 0) {
		return 0;
	}

	sign = (value < 0);
	if (sign)
		v = -value;
	else
		v = (unsigned)value;
	while (v || tp == tmp) {
		i = v % 10;
		v = v / 10;
		if (i < 10)
			*tp++ = i + '0';
		else
			*tp++ = i + 'a' - 10;
	}
	sp = string;
	if (sign)
		*sp++ = '-';
	while (tp > tmp)
		*sp++ = *--tp;
	*sp = 0;
}
/*
int main (void)
{
	char str[6] = {0};
	int T = 0;
	int R = 0;
	int R_1 = 2;

	int a = -278;
	int b = 5431227;//5479265;//5283189;

	int q = 5000;

	R = q * R_1;

	R = 10000;
	printf("R:%d\r\n", R);

	T = a * R + b;
	printf("T:%d\r\n", T);

	T = 2500000;
	printf("T:%d\r\n", T);
	R = (T - b) / a;

	printf("R:%d\r\n", R);

	q = (50*10)+(300/100);
//	q = 12345;
	_itoa(q, str);
	printf("%s", str);

}
*/
#define NULL (void*) 0

void __console_putc(char c)
{
	putc(c);
}

#define CONSILE_NAME_LEN 10
typedef struct __console_obj_struct{
	int (*handler)(int* p);
	char* name;
	char* instruction;
}CONSOLE_OBJ_STRUCT;

void console_obj_print_instruction(CONSOLE_OBJ_STRUCT* console_obj)
{
	for(;console_obj->instruction != '\0'; console_obj->instruction++)
		__console_putc(*console_obj->instruction);
}

void console_obj_print_name(CONSOLE_OBJ_STRUCT* console_obj)
{
	for(;console_obj->name != '\0'; console_obj->name++)
		__console_putc(*console_obj->name);
}

#define CONSILE_LIST_LEN 3
CONSOLE_OBJ_STRUCT console_list[CONSILE_LIST_LEN];

CONSOLE_OBJ_STRUCT* console_list_search(CONSOLE_OBJ_STRUCT console_list[], char* name)
{
	unsigned int i;
	CONSOLE_OBJ_STRUCT* ret;
	for(i = 0; i < CONSILE_LIST_LEN; i++){
		if(strcmp(console_list[i].name, name) == 0){
			ret = &console_list[i];
			break;
		}
	}
}

unsigned char console_list_add(CONSOLE_OBJ_STRUCT console_list[], int (*handler)(int* p), char* name, char* instruction)
{
	unsigned int i;
	unsigned char ret = 0;
	for(i = 0; i < CONSILE_LIST_LEN; i++){
		if(console_list[i].name == NULL){
			console_list[i].name = name;
			console_list[i].handler = handler;
			console_list[i].instruction = instruction;
			ret = 1;
			break;
		}
	}
	return ret;
}


char t_name[] = "t_mane";
char t_ins[] = "t_ins";

int t_fun(int* p)
{
	printf("t_fun\r\n");
	return 0;
}

int main (void)
{
	CONSOLE_OBJ_STRUCT* c;
	console_list_add(console_list, t_fun, t_name, t_ins);
	c = console_list_search(console_list, "t_mane");
	c->handler((int*)c);
}

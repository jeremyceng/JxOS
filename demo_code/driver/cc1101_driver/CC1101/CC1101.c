/*
==========================未经授权，转载需保留此信息============================
Function : Operation for cc1101
Taobao website:http://zeyaotech.taobao.com/
Official website:http://www.ashining.com/
Written by:Lijian
TEL:028-64891123(Sales),028-64891129(Technology),e-mail: service@ashining.com
================================================================================
*/
/*

Description : This module contains the low level operations for CC1101

*/

/***********************************************************
CC1101的寄存器的地址有效值为 BIT0 ~ BIT5。

BIT7 为读写控制位，为1，表示读；为0，表示写。（最高位）
BIT6 为是否连续操作（突发访问）， 为1，表示连续读写；为0，表示单个读写。

CC1101的寄存器有 命令/状态寄存器、 控制（配置）寄存器 两种类型。

特别说明 命令/状态寄存器 的操作方式如下：
当读（BIT7 = 1） 命令/状态寄存器 地址时，即为读状态；
当写（BIT7 = 0） 命令/状态寄存器 地址时，即为写命令。

如发送0x30表示写复位命令，发送0XF0（0X30 bit7为1，表示读，
bit6为1，表示连续）则表示读 PARTNUM 状态寄存器
似乎读状态寄存器只能连续读？（bit6必须为1？）
***************************************/

//#include "CC1101.H"
/*********************************************/
#define CC1101_SRES         0x30        // Reset chip.
#define CC1101_SFSTXON      0x31        // Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1).
                                        // If in RX/TX: Go to a wait state where only the synthesizer is
                                        // running (for quick RX / TX turnaround).
#define CC1101_SXOFF        0x32        // Turn off crystal oscillator.
#define CC1101_SCAL         0x33        // Calibrate frequency synthesizer and turn it off
                                        // (enables quick start).
#define CC1101_SRX          0x34        // Enable RX. Perform calibration first if coming from IDLE and
                                        // MCSM0.FS_AUTOCAL=1.
#define CC1101_STX          0x35        // In IDLE state: Enable TX. Perform calibration first if
                                        // MCSM0.FS_AUTOCAL=1. If in RX state and CCA is enabled:
                                        // Only go to TX if channel is clear.
#define CC1101_SIDLE        0x36        // Exit RX / TX, turn off frequency synthesizer and exit
                                        // Wake-On-Radio mode if applicable.
#define CC1101_SAFC         0x37        // Perform AFC adjustment of the frequency synthesizer
#define CC1101_SWOR         0x38        // Start automatic RX polling sequence (Wake-on-Radio)
#define CC1101_SPWD         0x39        // Enter power down mode when CSn goes high.
#define CC1101_SFRX         0x3A        // Flush the RX FIFO buffer.
#define CC1101_SFTX         0x3B        // Flush the TX FIFO buffer.
#define CC1101_SWORRST      0x3C        // Reset real time clock.
#define CC1101_SNOP         0x3D        // No operation. May be used to pad strobe commands to two
                                        // INT8Us for simpler software.

#define CC1101_PARTNUM      0x30
#define CC1101_VERSION      0x31
#define CC1101_FREQEST      0x32
#define CC1101_LQI          0x33
#define CC1101_RSSI         0x34
#define CC1101_MARCSTATE    0x35
#define CC1101_WORTIME1     0x36
#define CC1101_WORTIME0     0x37
#define CC1101_PKTSTATUS    0x38
#define CC1101_VCO_VC_DAC   0x39
#define CC1101_TXBYTES      0x3A
#define CC1101_RXBYTES      0x3B

#define CC1101_PATABLE      0x3E
#define CC1101_TXFIFO       0x3F
#define CC1101_RXFIFO       0x3F

#define BYTES_IN_RXFIFO     0x7F  						//接收缓冲区的有效字节数
#define CRC_OK              0x80 						//CRC校验通过位标志

/*********************************************/
typedef enum { TX_MODE, RX_MODE }TRMODE;
typedef enum { BROAD_ALL, BROAD_NO, BROAD_0, BROAD_0AND255 }ADDR_MODE;
typedef enum { BROADCAST, ADDRESS_CHECK} TX_DATA_MODE;

typedef unsigned char  INT8U;
typedef signed   char  INT8S;
typedef unsigned int   INT16U;
typedef signed   int   INT16S;
typedef unsigned long  INT32U;
typedef signed   long  INT32S;

#define CC1101_IOCFG2       0x00        // GDO2 output pin configuration
#define CC1101_IOCFG1       0x01        // GDO1 output pin configuration
#define CC1101_IOCFG0       0x02        // GDO0 output pin configuration
#define CC1101_FIFOTHR      0x03        // RX FIFO and TX FIFO thresholds
#define CC1101_SYNC1        0x04        // Sync word, high INT8U
#define CC1101_SYNC0        0x05        // Sync word, low INT8U
#define CC1101_PKTLEN       0x06        // Packet length
#define CC1101_PKTCTRL1     0x07        // Packet automation control
#define CC1101_PKTCTRL0     0x08        // Packet automation control
#define CC1101_ADDR         0x09        // Device address
#define CC1101_CHANNR       0x0A        // Channel number
#define CC1101_FSCTRL1      0x0B        // Frequency synthesizer control
#define CC1101_FSCTRL0      0x0C        // Frequency synthesizer control
#define CC1101_FREQ2        0x0D        // Frequency control word, high INT8U
#define CC1101_FREQ1        0x0E        // Frequency control word, middle INT8U
#define CC1101_FREQ0        0x0F        // Frequency control word, low INT8U
#define CC1101_MDMCFG4      0x10        // Modem configuration
#define CC1101_MDMCFG3      0x11        // Modem configuration
#define CC1101_MDMCFG2      0x12        // Modem configuration
#define CC1101_MDMCFG1      0x13        // Modem configuration
#define CC1101_MDMCFG0      0x14        // Modem configuration
#define CC1101_DEVIATN      0x15        // Modem deviation setting
#define CC1101_MCSM2        0x16        // Main Radio Control State Machine configuration
#define CC1101_MCSM1        0x17        // Main Radio Control State Machine configuration
#define CC1101_MCSM0        0x18        // Main Radio Control State Machine configuration
#define CC1101_FOCCFG       0x19        // Frequency Offset Compensation configuration
#define CC1101_BSCFG        0x1A        // Bit Synchronization configuration
#define CC1101_AGCCTRL2     0x1B        // AGC control
#define CC1101_AGCCTRL1     0x1C        // AGC control
#define CC1101_AGCCTRL0     0x1D        // AGC control
#define CC1101_WOREVT1      0x1E        // High INT8U Event 0 timeout
#define CC1101_WOREVT0      0x1F        // Low INT8U Event 0 timeout
#define CC1101_WORCTRL      0x20        // Wake On Radio control
#define CC1101_FREND1       0x21        // Front end RX configuration
#define CC1101_FREND0       0x22        // Front end TX configuration
#define CC1101_FSCAL3       0x23        // Frequency synthesizer calibration
#define CC1101_FSCAL2       0x24        // Frequency synthesizer calibration
#define CC1101_FSCAL1       0x25        // Frequency synthesizer calibration
#define CC1101_FSCAL0       0x26        // Frequency synthesizer calibration
#define CC1101_RCCTRL1      0x27        // RC oscillator configuration
#define CC1101_RCCTRL0      0x28        // RC oscillator configuration
#define CC1101_FSTEST       0x29        // Frequency synthesizer calibration control
#define CC1101_PTEST        0x2A        // Production test
#define CC1101_AGCTEST      0x2B        // AGC test
#define CC1101_TEST2        0x2C        // Various test settings
#define CC1101_TEST1        0x2D        // Various test settings
#define CC1101_TEST0        0x2E        // Various test settings

#include "STM8l10x_conf.h"
INT8U SPI_ExchangeByte( INT8U input );
#define cc1101_spi_cs_low()    		GPIO_ResetBits( GPIOB, GPIO_Pin_4 )//while( GPIO_ReadInputDataBit( GPIOB, GPIO_Pin_7 ) != 0 );
#define cc1101_spi_cs_high()    	GPIO_SetBits( GPIOB, GPIO_Pin_4 )
#define cc1101_spi_transfer(n)    	SPI_ExchangeByte(n)
//#define cc1101_blocking_delay_us(n)
 
//10, 7, 5, 0, -5, -10, -15, -20, dbm output power, 0x12 == -30dbm
const INT8U PaTabel[] = { 0xc0, 0xC8, 0x84, 0x60, 0x68, 0x34, 0x1D, 0x0E};

// Sync word qualifier mode = 30/32 sync word bits detected 
// CRC autoflush = false 
// Channel spacing = 199.951172 
// Data format = Normal mode 
// Data rate = 2.00224 
// RX filter BW = 58.035714 
// PA ramping = false 
// Preamble count = 4 
// Whitening = false 
// Address config = No address check 
// Carrier frequency = 400.199890 
// Device address = 0 
// TX power = 10 
// Manchester enable = false 
// CRC enable = true 
// Deviation = 5.157471 
// Packet length mode = Variable packet length mode. Packet length configured by the first byte after sync word 
// Packet length = 255 
// Modulation format = GFSK 
// Base frequency = 399.999939 
// Modulated = true 
// Channel number = 1 
// PA table 
#define PA_TABLE {0xc2,0x00,0x00,0x00,0x00,0x00,0x00,0x00,}

static const INT8U CC1101InitData[24][2]= 
{
  {CC1101_IOCFG0,      0x46},
  {CC1101_FIFOTHR,     0x07},			//0x47
  {CC1101_PKTCTRL1,    0x07},		//地址滤波
  {CC1101_PKTCTRL0,    0x05},		//可变数据包长度（FIFO.Length决定)
  {CC1101_CHANNR,      0x00},///430M	//0x96
  {CC1101_FSCTRL1,     0x0C},	//？？
  {CC1101_FREQ2,       0x10},	//基频
  {CC1101_FREQ1,       0xA7},	//基频
  {CC1101_FREQ0,       0x62},	//基频
  {CC1101_MDMCFG4,     0x2D},	//速率
  {CC1101_MDMCFG3,     0x3B},	//速率
  {CC1101_MDMCFG2,     0x13},	//速率	//??93//同步字重复
  {CC1101_MDMCFG1,     0x22},	//前导长度
  {CC1101_DEVIATN,     0x62},
  {CC1101_MCSM1,       0x30},
  {CC1101_MCSM0,       0x08},	//校准模式
  {CC1101_FOCCFG,      0x1D},
  {CC1101_WORCTRL,     0xFB},
  {CC1101_FSCAL3,      0xEA},	
  {CC1101_FSCAL2,      0x2A},
  {CC1101_FSCAL1,      0x00},
  {CC1101_FSCAL0,      0x1F},
  {CC1101_TEST2,       0x81},
  {CC1101_TEST1,       0x35},
};

/**
  {CC1101_IOCFG0,      0x06},
  {CC1101_FIFOTHR,     0x07},			//0x47
  {CC1101_PKTCTRL0,    0x05},
  {CC1101_CHANNR,      0x00},///430M	//0x96
  {CC1101_FSCTRL1,     0x06},
  {CC1101_FREQ2,       0x0F},
  {CC1101_FREQ1,       0x62},
  {CC1101_FREQ0,       0x76},
  {CC1101_MDMCFG4,     0xF6},	//0xF6 	//0xFC
  {CC1101_MDMCFG3,     0x43},	//0x43	//0xF8
  {CC1101_MDMCFG2,     0x13},
  {CC1101_DEVIATN,     0x15},
  {CC1101_MCSM0,       0x18},
  {CC1101_FOCCFG,      0x16},
  {CC1101_WORCTRL,     0xFB},
  {CC1101_FSCAL3,      0xE9},	//0xE9	//0xEA
  {CC1101_FSCAL2,      0x2A},
  {CC1101_FSCAL1,      0x00},
  {CC1101_FSCAL0,      0x1F},
  {CC1101_TEST2,       0x81},
  {CC1101_TEST1,       0x35},
  {CC1101_MCSM1,       0x3B},
  
  
  
  static const registerSetting_t preferredSettings[]= 
{
  {CC1101_IOCFG2,            0x29},
  {CC1101_IOCFG1,            0x2E},
  {CC1101_IOCFG0,            0x06},
  {CC1101_FIFOTHR,           0x07},
  {CC1101_SYNC1,             0xD3},
  {CC1101_SYNC0,             0x91},
  {CC1101_PKTLEN,            0xFF},
  {CC1101_PKTCTRL1,          0x04},
  {CC1101_PKTCTRL0,          0x05},
  {CC1101_ADDR,              0x00},
  {CC1101_CHANNR,            0x00},
  {CC1101_FSCTRL1,           0x0C},
  {CC1101_FSCTRL0,           0x00},
  {CC1101_FREQ2,             0x10},
  {CC1101_FREQ1,             0xA7},
  {CC1101_FREQ0,             0x62},
  {CC1101_MDMCFG4,           0x2D},
  {CC1101_MDMCFG3,           0x3B},
  {CC1101_MDMCFG2,           0x13},
  {CC1101_MDMCFG1,           0x22},
  {CC1101_MDMCFG0,           0xF8},
  {CC1101_DEVIATN,           0x62},
  {CC1101_MCSM2,             0x07},
  {CC1101_MCSM1,             0x30},
  {CC1101_MCSM0,             0x18},
  {CC1101_FOCCFG,            0x1D},
  {CC1101_BSCFG,             0x1C},
  {CC1101_AGCCTRL2,          0xC7},
  {CC1101_AGCCTRL1,          0x00},
  {CC1101_AGCCTRL0,          0xB0},
  {CC1101_WOREVT1,           0x87},
  {CC1101_WOREVT0,           0x6B},
  {CC1101_WORCTRL,           0xFB},
  {CC1101_FREND1,            0xB6},
  {CC1101_FREND0,            0x10},
  {CC1101_FSCAL3,            0xEA},
  {CC1101_FSCAL2,            0x2A},
  {CC1101_FSCAL1,            0x00},
  {CC1101_FSCAL0,            0x1F},
  {CC1101_RCCTRL1,           0x41},
  {CC1101_RCCTRL0,           0x00},
  {CC1101_FSTEST,            0x59},
  {CC1101_PTEST,             0x7F},
  {CC1101_AGCTEST,           0x3F},
  {CC1101_TEST2,             0x88},
  {CC1101_TEST1,             0x31},
  {CC1101_TEST0,             0x09},
  {CC1101_PARTNUM,           0x00},
  {CC1101_VERSION,           0x04},
  {CC1101_FREQEST,           0x00},
  {CC1101_LQI,               0x00},
  {CC1101_RSSI,              0x00},
  {CC1101_MARCSTATE,         0x00},
  {CC1101_WORTIME1,          0x00},
  {CC1101_WORTIME0,          0x00},
  {CC1101_PKTSTATUS,         0x00},
  {CC1101_VCO_VC_DAC,        0x00},
  {CC1101_TXBYTES,           0x00},
  {CC1101_RXBYTES,           0x00},
  {CC1101_RCCTRL1_STATUS,    0x00},
  {CC1101_RCCTRL0_STATUS,    0x00},
};
  **/
//FIFO中的长度值包括 ADDR + DATA + STATE = 1 + DATA + 2 = 数据长度 + 3  
//标准流程
INT8U CC1101( INT8U addr )	
{
    INT8U i, cc1101_chip_status_byte;
    cc1101_spi_cs_low( );
//	cc1101_blocking_delay_us(x);		//等待芯片响应，等待时间<=1us 也可以通过判断cc1101的SO脚是否变低。当SO为低时，表示芯片已经准备好
    cc1101_chip_status_byte = cc1101_spi_transfer( addr | 0X80);	//0X80：读 当向CC1101发送第一个字节时，芯片将同时传出芯片状态字
	if((cc1101_chip_status_byte & 0x80) != 0x00){	//如果芯片状态字的最高位不为零，表示芯片没有准备好，传输失败
	  return 0;
	}
    i = cc1101_spi_transfer( 0xFF );
    cc1101_spi_cs_high( );
    return i;
}


//读取单个控制寄存器的值
INT8U CC1101ReadCtrlReg( INT8U addr )	
{
    INT8U i;
    cc1101_spi_cs_low( );
    cc1101_spi_transfer( addr | 0X80);	//0X80：读
    i = cc1101_spi_transfer( 0xFF );
    cc1101_spi_cs_high( );
    return i;
}

//burst
//读取多个控制寄存器的值
void CC1101ReadMultiCtrlReg( INT8U addr, INT8U *buff, INT8U size )
{
    INT8U i, j;
    cc1101_spi_cs_low( );
    SPI_ExchangeByte( addr | 0X80 | 0X40);	//0X80：读；0X40:突发访问	
    for( i = 0; i < size; i ++ )
    {
        for( j = 0; j < 20; j ++ );			//等待？？
        *( buff + i ) = cc1101_spi_transfer( 0xFF );
    }
    cc1101_spi_cs_high( );
}

void CC1101WriteCtrlReg( INT8U addr, INT8U value )
{
    cc1101_spi_cs_low( );
    cc1101_spi_transfer( addr );
    cc1101_spi_transfer( value );
    cc1101_spi_cs_high( );
}

void CC1101WriteMultiCtrlReg( INT8U addr, INT8U *buff, INT8U size )
{
    INT8U i;
    cc1101_spi_cs_low( );
    cc1101_spi_transfer(addr | 0X40);
    for( i = 0; i < size; i ++ )
    {
        SPI_ExchangeByte( *( buff + i ) );
    }
    cc1101_spi_cs_high( );
}

//读取状态寄存器的值
INT8U CC1101ReadStatusReg( INT8U addr )
{
    INT8U i;
    cc1101_spi_cs_low( );
    cc1101_spi_transfer( addr | 0X80 | 0X40);	//为什么要突发访问？
    i = cc1101_spi_transfer( 0xFF );
    cc1101_spi_cs_high( );
    return i;
}

void CC1101WriteCmd( INT8U command )
{
    cc1101_spi_cs_low( );
    SPI_ExchangeByte( command );
    cc1101_spi_cs_high( );
}

/**************************************************************************/
void GS_CC1101Reset( void )
{
    INT8U x;

    cc1101_spi_cs_high( );
    cc1101_spi_cs_low( );
    cc1101_spi_cs_high( );
    for( x = 0; x < 100; x ++ );	//延时
    CC1101WriteCmd( CC1101_SRES );
}

void GS_CC1101SetAddress( INT8U address, ADDR_MODE AddressMode)
{
    INT8U btmp = CC1101ReadCtrlReg( CC1101_PKTCTRL1 ) & ~0x03;
    CC1101WriteCtrlReg(CC1101_ADDR, address);
    if     ( AddressMode == BROAD_ALL )     {}
    else if( AddressMode == BROAD_NO  )     { btmp |= 0x01; }
    else if( AddressMode == BROAD_0   )     { btmp |= 0x02; }
    else if( AddressMode == BROAD_0AND255 ) { btmp |= 0x03; }   
	//没有配置地址过滤类型
}

void GS_CC1101SetSYNC( INT16U sync )
{
    CC1101WriteCtrlReg(CC1101_SYNC1, 0xFF & ( sync>>8 ) );
    CC1101WriteCtrlReg(CC1101_SYNC0, 0xFF & sync ); 
}

void  GS_CC1101WORInit( void )
{

    CC1101WriteCtrlReg(CC1101_MCSM0,0x18);
    CC1101WriteCtrlReg(CC1101_WORCTRL,0x78); //Wake On Radio Control
    CC1101WriteCtrlReg(CC1101_MCSM2,0x00);
    CC1101WriteCtrlReg(CC1101_WOREVT1,0x8C);
    CC1101WriteCtrlReg(CC1101_WOREVT0,0xA0);
	
	CC1101WriteCmd( CC1101_SWORRST );
}

/**
 * @brief Set the device as TX mode or RX mode
 * @param mode selection
 * @retval None
 */
void GS_CC1101SetTRMode( TRMODE mode )
{
    if( mode == TX_MODE )
    {
 //       CC1101WriteCtrlReg(CC1101_IOCFG0,0x46);

    }
    else if( mode == RX_MODE )
    {

    }
}

void cc1101Delay(uint16_t nCount)
{
    /* Decrement nCount value */
    while (nCount != 0)
    {
        nCount--;
    }
}
void rx_ready(void)
{
  CC1101WriteCmd(CC1101_SIDLE);
  CC1101WriteCmd( CC1101_SFRX );
  CC1101WriteCmd( CC1101_SFTX );	//是否可以省略
  
//  CC1101WriteCtrlReg(CC1101_IOCFG0,0x46);
  CC1101WriteCmd( CC1101_SCAL );
//  cc1101Delay(1000);
  CC1101WriteCmd( CC1101_SRX );
  
}

void rx_send_back(void)
{
    CC1101WriteMultiCtrlReg( CC1101_TXFIFO, "0123456789\r\n", 12 );		//先写FOFI,再开TX
	CC1101WriteCmd( CC1101_STX );
}

/*
 * @brief Set the CC1101 into IDLE mode
 * @param None
 * @retval None
 */ 
void GS_CC1101SetIdle( void )
{
    CC1101WriteCmd(CC1101_SIDLE);
}


/*
 * @brief Flush the TX buffer of CC1101
 * @param None
 * @retval None
 */ 
void GS_CC1101ClrTXBuff( void )
{
  GS_CC1101SetIdle();//MUST BE IDLE MODE
  CC1101WriteCmd( CC1101_SFTX );
  CC1101WriteCmd( CC1101_SCAL );
  CC1101WriteCtrlReg(CC1101_IOCFG0,0x46);
//  CC1101WriteCmd( CC1101_SFSTXON );
}
/**
 * @brief Flush the RX buffer of CC1101
 * @param None
 * @retval None
 */ 
void GS_CC1101ClrRXBuff( void )
{
    GS_CC1101SetIdle();//MUST BE IDLE MODE
    CC1101WriteCmd( CC1101_SFRX );
}



/**
 * @brief Send a packet
 * @param txbuffer, The buffer stores data to be sent
 * @param size, How many bytes should be sent
 * @param mode, Broadcast or address check packet
 * @retval None
 */ 
 


void ready( INT8U *txbuffer, INT8U size, TX_DATA_MODE mode )
{
    INT8U address;
    if( mode == BROADCAST )             { address = 0; }
    else if( mode == ADDRESS_CHECK )    { address = CC1101ReadCtrlReg( CC1101_ADDR ); }
	
/*
    GS_CC1101ClrTXBuff( );
    
    if( ( CC1101ReadCtrlReg( CC1101_PKTCTRL1 ) & ~0x03 ) != 0 )
    {
        CC1101WriteCtrlReg( CC1101_TXFIFO, size + 1 );
        CC1101WriteCtrlReg( CC1101_TXFIFO, address );
    }
    else
    {
        CC1101WriteCtrlReg( CC1101_TXFIFO, size );
    }
*/

	GS_CC1101SetIdle();//MUST BE IDLE MODE
	CC1101WriteCmd( CC1101_SFTX );
	cc1101Delay(1000);
	
    CC1101WriteMultiCtrlReg( CC1101_TXFIFO, txbuffer, size );		//先写FOFI,再开TX

	CC1101WriteCmd( CC1101_SCAL );
}


void GS_CC1101SendPacket( void )
{
	GPIO_SetBits ( GPIOB, GPIO_Pin_2);
	CC1101WriteCmd( CC1101_STX );
	
	GPIO_ResetBits( GPIOB, GPIO_Pin_2 );
    while( GPIO_ReadInputDataBit( GPIOA, GPIO_Pin_2 ) != 0 );
    while( GPIO_ReadInputDataBit( GPIOA, GPIO_Pin_2 ) == 0 );
	
	GPIO_SetBits( GPIOB, GPIO_Pin_2 );
	
//    GS_CC1101ClrTXBuff( );
	GPIO_ResetBits( GPIOB, GPIO_Pin_2 );
}

void CC1101Sleep(void)
{
      CC1101WriteCmd(CC1101_SPWD);
}

/**
 * @brief Get received count of CC1101
 * @param None
 * @retval How many bytes hae been received
 */
INT8U GS_CC1101GetRXCnt( void )
{
    return ( CC1101ReadStatusReg( CC1101_RXBYTES )  & BYTES_IN_RXFIFO );
}


INT8U GS_CC1101RecPacket( INT8U *rxBuffer,  INT8U len)
{
    INT8U status[2];
    INT8U pktLen;
    INT16U x , j = 0;

    if ( GS_CC1101GetRXCnt( ) != 0 )
    {
	  /*
        pktLen = CC1101ReadCtrlReg(CC1101_RXFIFO);           // Read length byte	//FIFO的第一个字节就是FIFO内容长度？
        if( ( CC1101ReadCtrlReg( CC1101_PKTCTRL1 ) & ~0x03 ) != 0 )
        {
            x = CC1101ReadCtrlReg(CC1101_RXFIFO);
        }
        if( pktLen == 0 )           { return 0; }
        else                        { pktLen --; }
		*/
        CC1101ReadMultiCtrlReg(CC1101_RXFIFO, rxBuffer, len); // Pull data
        CC1101ReadMultiCtrlReg(CC1101_RXFIFO, status, 2);   // Read  status bytes

        GS_CC1101ClrRXBuff( );

        if( status[1] & CRC_OK ) {   return pktLen; }
        else                     {   return 0; }
    }
    else   {  return 0; }                               // Error
}


/**
 * @brief Initialize the CC1101, User can modify it
 * @param None
 * @retval None
 */ 
void GS_CC1101Init( void )
{
    volatile INT8U i, j;

    GS_CC1101Reset( );    
    
    for( i = 0; i < 24; i++ )
    {
        CC1101WriteCtrlReg( CC1101InitData[i][0], CC1101InitData[i][1] );
    }

    GS_CC1101SetSYNC( 0x8799 );
//    CC1101WriteCtrlReg(CC1101_MDMCFG1,   0x72); //Modem Configuration	//信道

    CC1101WriteMultiCtrlReg(CC1101_PATABLE, PaTabel, 8 );		//参数寄存器，注意些参数寄存器的方法，

    i = CC1101ReadStatusReg( CC1101_PARTNUM );//for test, must be 0x80
    i = CC1101ReadStatusReg( CC1101_VERSION );//for test, refer to the datasheet
	
	CC1101WriteCmd( CC1101_SCAL );
	CC1101WriteCtrlReg(CC1101_IOCFG0,0x46);
}


/*

------------------------------------THE END-------------------------------------

*/

/*
==========================未经授权，转载需保留此信息============================
Function : Operation for cc1101
Taobao website:http://zeyaotech.taobao.com/
Official website:http://www.ashining.com/
Written by:Lijian
TEL:028-64891123(Sales),028-64891129(Technology),e-mail: service@ashining.com
================================================================================
*/
/*
================================================================================
Description : This module contains the low level operations for CC1101
================================================================================
*/
#ifndef _CC1101_H_
#define _CC1101_H_

#include "mytypedef.h"		//DO NOT modify
#include "CC1101_REG.h"		//DO NOT modify

/*
================================================================================
------------------------------Internal IMPORT functions-------------------------
you must offer the following functions for this module
1. INT8U SPI_ExchangeByte( INT8U input ); //SPI Send and Receive function
2. CC_CSN_LOW( );                        //Pull down the CSN line
3. CC_CSN_HIGH( );                       //Pull up the CSN Line
================================================================================
*/
#include "main.h"     //import SPI_ExchangeByte( );

/*
================================================================================
-----------------------------------macro definitions----------------------------
================================================================================
*/
typedef enum { TX_MODE, RX_MODE }TRMODE;
typedef enum { BROAD_ALL, BROAD_NO, BROAD_0, BROAD_0AND255 }ADDR_MODE;
typedef enum { BROADCAST, ADDRESS_CHECK} TX_DATA_MODE;

/*
================================================================================
-------------------------------------exported APIs------------------------------
================================================================================

*/

void CC1101ReadMultiCtrlReg( INT8U addr, INT8U *buff, INT8U size );
void CC1101WriteCtrlReg( INT8U addr, INT8U value );
/*read a byte from the specified register*/
INT8U CC1101ReadCtrlReg( INT8U addr );

/*Read a status register*/
INT8U CC1101ReadStatusReg( INT8U addr );

/*Set the device as TX mode or RX mode*/
void GS_CC1101SetTRMode( TRMODE mode );

/*Write a command byte to the device*/
void CC1101WriteCmd( INT8U command );

/*Set the CC1101 into IDLE mode*/
void GS_CC1101SetIdle( void );

/*Send a packet*/
void GS_CC1101SendPacket( void );

/*Set the address and address mode of the CC1101*/
void GS_CC1101SetAddress( INT8U address, ADDR_MODE AddressMode);

/*Set the SYNC bytes of the CC1101*/
void GS_CC1101SetSYNC( INT16U sync );

/*Receive a packet*/
INT8U GS_CC1101RecPacket( INT8U *rxBuffer,  INT8U len);

/*Initialize the WOR function of CC1101*/
void  GS_CC1101WORInit( void );

/*Initialize the CC1101, User can modify it*/
void GS_CC1101Init( void );

void CC1101WriteMultiCtrlReg( INT8U addr, INT8U *buff, INT8U size );


#endif // _CC1101_H_
/*
================================================================================
------------------------------------THE END-------------------------------------
================================================================================
*/

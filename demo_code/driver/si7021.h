
#ifndef _SI7021_H
#define _SI7021_H

uint8_t si7021_init(uint8_t accuracy);
uint8_t si7021_start_measure(void);
uint8_t si7021_read_temp_val(uint32_t* temperature);    		//temperatureMilliC
uint8_t si7021_read_humi_val(uint16_t* relativeHumidity);		//100%*100


/***********************
e.g.
power_on();
si7021_hal_delay_ms(50);
si7021_init(12);
si7021_hal_delay_ms(measurementReadyDelayMs);   //??

si7021_start_measure();
si7021_hal_delay_ms(measurementReadyDelayMs);
si7021_read_temp_val();

si7021_start_measure();
si7021_hal_delay_ms(measurementReadyDelayMs);
si7021_read_humi_val();
***********************/

#endif
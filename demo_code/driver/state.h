/*
 * state.h
 *
 *  Created on: 2017��4��24��
 *      Author: zengjie
 */

#ifndef SRC_TRVDRIVERS_STATE_H_
#define SRC_TRVDRIVERS_STATE_H_

/****************************
&key_eco_short_handler,
&key_eco_long_handler,
&key_boost_short_handler,
&key_boost_long_handler,
&key_auto_short_handler,
&key_auto_long_handler,
&key_all_long_handler,

&rotary_encoder_cw_handler,
&rotary_encoder_ccw_handler
****************************/

#include <stdint.h>

typedef void (*STATE_EVENT_HANDLER)(void *);

typedef struct state_struct {
	//   STATE_ID id;
	void (*enter)(void);
	void (*exit)(void);
	STATE_EVENT_HANDLER* event_hendler_table;
	uint32_t event_hendler_table_len;
} STATE_ST;

uint8_t state_jump_to_state(STATE_ST* state);
STATE_ST* state_current_state_get(void);

#endif /* SRC_TRVDRIVERS_STATE_H_ */

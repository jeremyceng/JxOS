#ifndef __HTS221_CONFIG_H
#define __HTS221_CONFIG_H

#include "stdint.h"

void hts221_hal_init(void);
uint8_t	hts221_hal_i2c_write_device_reg(uint8_t addr, uint8_t reg_addr, uint8_t *data, uint8_t len);
uint8_t	hts221_hal_i2c_read_device_reg(uint8_t addr, uint8_t reg_addr, uint8_t *data, uint8_t len);

#endif

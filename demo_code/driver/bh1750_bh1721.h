/**
 *
 * Leedarson
 * All Rights Reserved
 *
 * @file mod_light_bh1750.h
 *
 * @brief Header file for mod_light_bh1750.c.
 *
 *
 * Author: xuexiaofei 
 *
 * Last Changed By: $Author: xuexiaofei $
 * Revision: $Revision: 1.00 $
 * Last Changed: $Date: 2017/03/30 10:18:01 $
 *
 */

#ifndef __MOD_LIGHT_BH1750_H
#define __MOD_LIGHT_BH1750_H

#include "stdint.h"

void bh1750_start_convert( void );
uint16_t bh1750_read_light_lux( void );

/**********************
e.g.
1:bh1750_power_on();		delay_ms(20);
2:bh1750_start_convert();	delay_ms(120);
3:bh1750_read_light_lux();
4:bh1750_power_off();
**********************/

#endif


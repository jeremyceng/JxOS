
#include "hal_uart.h"

void USART_Init(USART_TypeDef* USARTx, USART_InitTypeDef* USART_InitStruct){}
void USART_Cmd(USART_TypeDef* USARTx, FunctionalState NewState){}
void USART_ITConfig(USART_TypeDef* USARTx, uint16_t USART_IT, FunctionalState NewState){}
void USART_SendData(USART_TypeDef* USARTx, uint16_t Data){}
uint16_t USART_ReceiveData(USART_TypeDef* USARTx){}
FlagStatus USART_GetFlagStatus(USART_TypeDef* USARTx, uint16_t USART_FLAG){}


void (*EXTI0_IRQHandler_callback)(void) 	= 0;
void (*TIM0_IRQHandler_callback)(void) 		= 0;
void (*TIM1_IRQHandler_callback)(void) 		= 0;
void (*USART0_IRQHandler_callback)(void) 	= 0;
void (*RTC_IRQHandler_callback)(void) 		= 0;

void EXTI0_IRQHandler(void)
{
//  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
//  {
    /* Clear the  EXTI line 0 pending bit */
//    EXTI_ClearITPendingBit(EXTI_Line0);

        if(EXTI0_IRQHandler_callback != 0){
            EXTI0_IRQHandler_callback();
        }

//  }
}

void TIM0_IRQHandler(void)
{
//	printf("TIM0_IRQHandler\r\n");
//  if(TIM_GetITStatus(TIM0, TIM_IT_CC2) == SET)
//  {
    /* Clear TIM3 Capture compare interrupt pending bit */
 //   TIM_ClearITPendingBit(TIM0, TIM_IT_CC2);
        if(TIM0_IRQHandler_callback != 0){
//			printf("TIM0_IRQHandler\r\n");
            TIM0_IRQHandler_callback();
        }

//  }
}

void TIM1_IRQHandler(void)
{
//  if(TIM_GetITStatus(TIM0, TIM_IT_CC2) == SET)
//  {
    /* Clear TIM3 Capture compare interrupt pending bit */
 //   TIM_ClearITPendingBit(TIM0, TIM_IT_CC2);
        if(TIM1_IRQHandler_callback != 0){
            TIM1_IRQHandler_callback();
        }

//  }
}

void USART0_IRQHandler(void)
{
//  if(USART_GetITStatus(USART0, USART_IT_RXNE) != RESET)
//  {

        if(USART0_IRQHandler_callback != 0){
            USART0_IRQHandler_callback();
        }

//  }
}

void RTC_IRQHandler(void)
{
//  if (RTC_GetITStatus(RTC_IT_SEC) != RESET)
//  {
    /* Clear Interrupt pending bit */
//    RTC_ClearITPendingBit(RTC_FLAG_SEC);

        if(RTC_IRQHandler_callback != 0){
            RTC_IRQHandler_callback();
        }

//  }
}



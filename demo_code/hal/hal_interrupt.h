#ifndef _HAL_ISR_H
#define _HAL_ISR_H

extern void (*EXTI0_IRQHandler_callback)(void);
extern void (*TIM0_IRQHandler_callback)(void);
extern void (*TIM1_IRQHandler_callback)(void);
extern void (*TIM3_IRQHandler_callback)(void);
extern void (*RTC_IRQHandler_callback)(void);


#endif

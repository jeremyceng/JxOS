

	volatile bool_t circle_move_flag;
	volatile uint16_t begin_value;
	volatile uint16_t end_value;

//the "current_value" move to the "begin_value" in the first round
//period_ticks: how many ticks will take to move "begin_value" to "end_value"
void value_move_circle_move_by_tick(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t period_ticks)
{
	uint16_t all_steps;
	if(set_begin_value == set_end_value){
		return;
	}

	if(set_begin_value > set_end_value){
		all_steps = set_begin_value - set_end_value;
	   }
	else{
		all_steps = set_end_value - set_begin_value;
	}
	if(all_steps > period_ticks){
		all_steps = all_steps/period_ticks;
	}
	else{
		all_steps = 1;
	}

	if(period_ticks > 0){
		VALUE_MOVES[value_move_num].begin_value = set_begin_value;
		VALUE_MOVES[value_move_num].end_value = set_end_value;
		VALUE_MOVES[value_move_num].interval_setps = all_steps;
		VALUE_MOVES[value_move_num].circle_move_flag = 1;
		VALUE_MOVES[value_move_num].target_value = set_begin_value;
	}
}

//the "current_value" move to the "begin_value" in the first round
//set_interval_setps: how many steps pre tick
void value_move_circle_move_by_step(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t set_interval_setps)
{
	if(set_begin_value == set_end_value){
		return;
	}
	if(set_interval_setps > 0){
		VALUE_MOVES[value_move_num].begin_value = set_begin_value;
		VALUE_MOVES[value_move_num].end_value = set_end_value;
		VALUE_MOVES[value_move_num].interval_setps = set_interval_setps;
		VALUE_MOVES[value_move_num].circle_move_flag = 1;
		VALUE_MOVES[value_move_num].target_value = set_begin_value;
	}
}


			if(VALUE_MOVES[i].circle_move_flag == 0){
				continue;
			}
			else{
				if(VALUE_MOVES[i].target_value == VALUE_MOVES[i].begin_value){
					VALUE_MOVES[i].target_value = VALUE_MOVES[i].end_value;
				}
				else{
					VALUE_MOVES[i].target_value = VALUE_MOVES[i].begin_value;
				}
			}
		}

#ifndef __SIM_I2C_H__
#define __SIM_I2C_H__

#include "type.h"

void sim_i2c_init(void);
void sim_i2c_reset(void);

void sim_i2c_burst_read(uint8_t slave_id,
		uint16_t read_reg_len,
		uint8_t *read_buf);
void sim_i2c_burst_write(uint8_t slave_id,
		uint16_t write_reg_len,
		uint8_t *write_buf);

void sim_i2c_burst_read_reg(uint8_t slave_id, \
                    uint8_t start_reg_addr, uint16_t read_reg_len, \
                    uint8_t *read_buf);
void sim_i2c_burst_write_reg(uint8_t slave_id, \
                    uint8_t start_reg_addr, uint16_t write_reg_len, \
                    uint8_t *write_buf);

uint16_t sim_i2c_read_16bit_data_lsb(uint8_t slave_id, \
  					uint8_t start_reg_addr);


#endif

#ifndef __PWM_MOVE__H
#define __PWM_MOVE__H

#include "type.h"

void value_move_init(void);
void value_move_tick_handler(void);

//move_ticks: how many ticks will take to move "current_value" to "target_value"
void value_move_to_value_by_tick(uint8_t value_move_num, uint16_t set_target_value, uint16_t move_ticks);

//rate: how many value pre tick
void value_move_to_value_by_rate(uint8_t value_move_num, uint16_t set_target_value, uint16_t rate);

void compute_interval_value_and_interval_tick(uint16_t* interval_setps, uint16_t* interval_ticks,
												uint16_t totle_setps, uint16_t totle_ticks);

void value_move_stop_move(uint8_t value_move_num);
void value_move_init_current_value(uint8_t value_move_num, uint16_t set_current_value);
bool_t value_move_check_active(uint8_t value_move_num);

#endif
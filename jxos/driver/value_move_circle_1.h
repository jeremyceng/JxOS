

//the "current_value" move to the "begin_value" in the first round
//period_ticks: how many ticks will take to move "begin_value" to "end_value"
void value_move_circle_move_by_tick(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t period_ticks);
//the "current_value" move to the "begin_value" in the first round
//set_step_value: how many steps pre tick
void value_move_circle_move_by_step(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t set_step_value);

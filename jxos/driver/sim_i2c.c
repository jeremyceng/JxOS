
#include "type.h"
#include "sim_i2c_config.h"

typedef enum
{
	I2C_ACK  = 0,
	I2C_NACK  = 1
} IC2_ACK_TypeDef;

typedef enum {
	I2C_WRITE_BIT = 0, 
	I2C_RADE_BIT = 1
} IC2_RW_BIT_TypeDef;


#define i2c_delay()		sim_i2c_half_period_time_delay();sim_i2c_half_period_time_delay()

/*****************************************************************************/
//原则：每个步骤的初始状态由上一个步骤准备好，并在结束时为下一个步骤准备好正确的状态
static void i2c_send_start(void) {
	sim_i2c_sda_pin_out_mode();
	
	sim_i2c_sda_pin_low();
	sim_i2c_scl_pin_hight();
	i2c_delay();

	sim_i2c_sda_pin_low();
	sim_i2c_scl_pin_hight();
	i2c_delay();

	sim_i2c_sda_pin_low();
	sim_i2c_scl_pin_hlow();
}

static void i2c_send_byte(uint8_t ch) {
	uint8_t i;

	sim_i2c_sda_pin_out_mode();
	
	i2c_delay();
	i2c_delay();

	for (i = 0; i < 8; i++) {

		sim_i2c_scl_pin_hlow();
		sim_i2c_half_period_time_delay();

		if ((ch << i) & 0x80)	//send bit 1
		{
			sim_i2c_sda_pin_hight();
		} 
		else //send bit 0
		{
			sim_i2c_sda_pin_low();
		}
		sim_i2c_scl_pin_hlow();
		sim_i2c_half_period_time_delay();

		sim_i2c_scl_pin_hight();
		sim_i2c_half_period_time_delay();
		sim_i2c_half_period_time_delay();
		sim_i2c_half_period_time_delay();
	}

	sim_i2c_scl_pin_hlow();
}

static uint8_t i2c_read_byte(void) {
	uint8_t b = 0;
	uint8_t i;

	sim_i2c_sda_pin_in_mode();

	i2c_delay();
	i2c_delay();

	sim_i2c_sda_pin_hight();
	for (i = 0; i < 8; i++) {
		b <<= 1;
		sim_i2c_scl_pin_hlow();
		sim_i2c_half_period_time_delay();
		sim_i2c_half_period_time_delay();
		sim_i2c_half_period_time_delay();

		sim_i2c_scl_pin_hight();
		sim_i2c_half_period_time_delay();
		if (sim_i2c_sda_pin_rec()) {
			b |= 0x01;
		}
		sim_i2c_half_period_time_delay();

	}

	sim_i2c_scl_pin_hlow();
	return b;
}

static void i2c_send_ack(IC2_ACK_TypeDef ACK) {

	sim_i2c_sda_pin_out_mode();
		
	sim_i2c_half_period_time_delay();

	if (ACK == I2C_ACK) {
		sim_i2c_sda_pin_low();
	} else {
		sim_i2c_sda_pin_hight();
	}
	sim_i2c_half_period_time_delay();

	sim_i2c_scl_pin_hight();
	sim_i2c_half_period_time_delay();
	sim_i2c_half_period_time_delay();
	sim_i2c_half_period_time_delay();

	sim_i2c_scl_pin_hlow();
}

static IC2_ACK_TypeDef i2c_check_ack(void) {

	IC2_ACK_TypeDef ack = I2C_NACK;
	
	sim_i2c_sda_pin_in_mode();
	
	sim_i2c_half_period_time_delay();
	sim_i2c_sda_pin_hight();
	sim_i2c_half_period_time_delay();
	sim_i2c_half_period_time_delay();
	sim_i2c_scl_pin_hight();
	sim_i2c_half_period_time_delay();
	if (!sim_i2c_sda_pin_rec()) {
		ack = I2C_ACK;
	} else {
		ack = I2C_NACK;
	}
	sim_i2c_half_period_time_delay();
	
	sim_i2c_scl_pin_hlow();
	return ack;
}

static void i2c_send_stop(void) {

	sim_i2c_sda_pin_out_mode();
		
	i2c_delay();
	i2c_delay();

	sim_i2c_sda_pin_low();
	i2c_delay();
	sim_i2c_scl_pin_hight();
	i2c_delay();
	sim_i2c_sda_pin_hight();
}

static void i2c_send_repeated_start(void) {
	
	sim_i2c_sda_pin_out_mode();
		
	i2c_send_stop();
	i2c_delay();
	i2c_delay();
	i2c_send_start();
}

static uint8_t slave_id_addr_rw_bit(uint8_t slave_id,
		IC2_RW_BIT_TypeDef rw_bit) 
{
	uint8_t ret = 0;

	switch (rw_bit) {
	case I2C_WRITE_BIT:
		ret = slave_id << 1;
		break;
	case I2C_RADE_BIT:
		ret = slave_id << 1;
		ret |= 0x01;
		break;
	default:
		break;
	}
	return ret;
}

/****************************************************************************/
void sim_i2c_reset(void) 
{
	sim_i2c_sda_pin_hight();
	sim_i2c_scl_pin_hight();
	i2c_delay();
}

void sim_i2c_init(void) 
{
	sim_i2c_pin_init();
	sim_i2c_reset();
}

void sim_i2c_burst_read(uint8_t slave_id,
		uint16_t read_reg_len,
		uint8_t *read_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_RADE_BIT));//read
	i2c_check_ack();

	i2c_delay();
	i2c_delay();
	i2c_delay();
	i2c_delay();
	
	for (; read_reg_len > 0; read_reg_len--) {
		*read_buf = i2c_read_byte();	//read reg
		read_buf++;
		if (read_reg_len == 1)	//last reg
				{
			i2c_send_ack(I2C_NACK);
		} else {
			i2c_send_ack(I2C_ACK);
		}
	}

	i2c_send_stop();
}

void sim_i2c_burst_write(uint8_t slave_id,
		uint16_t write_reg_len,
		uint8_t *write_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	for (; write_reg_len > 0; write_reg_len--) {
		i2c_send_byte(*write_buf);	//read reg
		write_buf++;
		i2c_check_ack();
	}

	i2c_send_stop();
}

void sim_i2c_burst_read_reg(uint8_t slave_id,
		uint8_t start_reg_addr, uint16_t read_reg_len,
		uint8_t *read_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	i2c_send_byte(start_reg_addr);	//send reg addr
	i2c_check_ack();

	i2c_send_repeated_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_RADE_BIT));//read
	i2c_check_ack();

	for (; read_reg_len > 0; read_reg_len--) {
		*read_buf = i2c_read_byte();	//read reg
		read_buf++;
		if (read_reg_len == 1){	//last reg
			i2c_send_ack(I2C_NACK);
		} else {
			i2c_send_ack(I2C_ACK);
		}
	}

	i2c_send_stop();
}

void sim_i2c_burst_write_reg(uint8_t slave_id,
		uint8_t start_reg_addr, uint16_t write_reg_len,
		uint8_t *write_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	i2c_send_byte(start_reg_addr);	//send reg addr
	i2c_check_ack();

	for (; write_reg_len > 0; write_reg_len--) {
		i2c_send_byte(*write_buf);	//write reg
		write_buf++;
		i2c_check_ack();
	}

	i2c_send_stop();
}

uint16_t sim_i2c_read_16bit_data_lsb(uint8_t slave_id, uint8_t start_reg_addr) 
{
	uint8_t buf[2] = { 0 };
	uint16_t ret = 0;

	sim_i2c_burst_read_reg(slave_id, start_reg_addr, 2, buf);

	ret = buf[1];
	ret <<= 8;
	ret |= buf[0];

	return ret;
}

/*
 bool    i2c1_send_byte_with_check(uint8_t ch)
 {
 u8  i;
 for(i = 0; i < 8; i++)
 {
 if((ch << i) & 0x80)
 {
 OP_I2C1_SDA_H();
 i2c_delay();
 if(!IN_I2C1_SDA())
 return FALSE;

 OP_I2C1_SCL_H();
 i2c_delay();
 OP_I2C1_SCL_L();
 i2c_delay();
 }
 else
 {
 OP_I2C1_SDA_L();
 i2c_delay();
 if(IN_I2C1_SDA())
 return FALSE;

 OP_I2C1_SCL_H();
 i2c_delay();
 OP_I2C1_SCL_L();
 i2c_delay();
 }
 }
 return  TRUE;
 }
 */
#ifndef __BLINK_H
#define __BLINK_H

#include "type.h"

//A whole blink sycle is: on -> duty_ration -> off -> period-duty_ration -> ..

void blink_init(void);

void blink_on(uint16_t blink_num);
void blink_off(uint16_t blink_num);
void blink_toggle(uint16_t blink_num);
uint8_t blink_get_state(uint16_t blink_num);
void blink_set(uint16_t blink_num, 
				uint16_t set_period, uint16_t set_duty_ration);
void blink_stop(uint16_t blink_num);
void blink_start(uint16_t blink_num, 
				uint8_t set_blink_times, uint8_t set_finish_state); //blink_times = 0xff blink forever
uint8_t blink_is_blink(uint16_t blink_num);
uint8_t blink_check_blink_all(void);

void blink_handler(void);

#endif

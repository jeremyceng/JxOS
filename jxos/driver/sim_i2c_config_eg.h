#ifndef _SIM_I2C_CONFIG_H
#define _SIM_I2C_CONFIG_H

#define sim_i2c_sda_pin_hight()	
#define sim_i2c_sda_pin_low()	
#define sim_i2c_sda_pin_rec()	

#define sim_i2c_scl_pin_hight()
#define sim_i2c_scl_pin_hlow()

#define sim_i2c_sda_pin_out_mode()
#define sim_i2c_sda_pin_in_mode()

#define sim_i2c_half_period_time_delay()
#define sim_i2c_pin_init()

#endif
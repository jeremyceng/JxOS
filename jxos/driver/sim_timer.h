
#ifndef __SIM_TIEMR__H
#define __SIM_TIEMR__H

#include "type.h"

void sim_timer_init(void);
void sim_timer_tick_hander(uint16_t ticks);

uint8_t sim_timer_check_running(uint16_t sim_timer_num);
uint8_t sim_timer_check_overtime(uint16_t sim_timer_num);

void sim_timer_start(uint16_t sim_timer_num);
void sim_timer_stop(uint16_t sim_timer_num);
void sim_timer_restart(uint16_t sim_timer_num);
void sim_timer_set_timeout(uint16_t sim_timer_num, uint16_t timeout);

#endif
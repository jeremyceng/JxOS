
#ifndef __VALUE_TABLE__H
#define __VALUE_TABLE__H

#include "type.h"

void value_table_init(void);
void value_table_set_value(uint16_t value_num, uint16_t set_value);
void value_table_get_value(uint16_t value_num, uint16_t* get_value);

#endif
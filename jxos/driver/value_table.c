

#include "type.h"
#include "value_table_config.h"

#ifndef VALUE_TABLE_MAX
#error "VALUE_TABLE_MAX NOT HAVE A CONFIG VALUE"
#endif

// #define id			    0
// #define value		    1
// #define offset_max		3

//static uint16_t VALUES[VALUE_TABLE_MAX*offset_max];

static uint16_t VALUES[VALUE_TABLE_MAX];


void value_table_init(void)
{
	volatile uint8_t i;

    for(i = 0; i < VALUE_TABLE_MAX; i++) {
        VALUES[i] = 0;
	}
}

void value_table_set_value(uint16_t value_num, uint16_t set_value)
{
    if(value_num < VALUE_TABLE_MAX){
        if(VALUES[value_num] != set_value){
            VALUES[value_num] = set_value;
            value_table_value_changed_event(value_num, set_value);
        }
    }
}

void value_table_get_value(uint16_t value_num, uint16_t* get_value)
{
    if(value_num < VALUE_TABLE_MAX){
        *get_value = VALUES[value_num];
    }
}


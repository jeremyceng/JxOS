#include "jxos_config.h"
#include "jxos_registry.h"
#include "string.h"

#if (JXOS_REGISTRY_ENABLE == 1)

#if (JXOS_TASK_ENABLE == 1)
#define TASK_MAX_SET TASK_MAX
#else
#define TASK_MAX_SET 0
#endif 

#if (JXOS_MSG_ENABLE == 1)
#define MSG_QUEUE_MAX_SET MSG_QUEUE_MAX
#else
#define MSG_QUEUE_MAX_SET 0
#endif

#if (JXOS_MESSAGE_PIPE_ENABLE == 1)
#define MESSAGE_PIPE_MAX_SET MESSAGE_PIPE_MAX
#else
#define MESSAGE_PIPE_MAX_SET 0
#endif

#if (JXOS_BULLETIN_BOARD_ENABLE == 1)
#define BULLETIN_BOARD_MAX_SET BULLETIN_BOARD_MAX
#else
#define BULLETIN_BOARD_MAX_SET 0
#endif

#if (JXOS_MAIL_BOX_ENABLE == 1)
#define MAIL_BOX_MAX_SET MAIL_BOX_MAX
#else
#define MAIL_BOX_MAX_SET 0
#endif

#define REGISTRY_MAX	TASK_MAX_SET + \
						MESSAGE_PIPE_MAX_SET + \
						BULLETIN_BOARD_MAX_SET + \
						MAIL_BOX_MAX_SET +\
						MSG_QUEUE_MAX_SET

typedef struct
{
    const char* name;
	void* handle;
} JXOS_REGISTRY_STRUCT;

static JXOS_REGISTRY_STRUCT  jxos_registry_list[REGISTRY_MAX];
static uint8_t jxos_registry_count = 0;

void* jxos_registry_find(const char* name)
{
	void* p = 0;
	uint8_t len1,len2,i;

//	printf("====> find: %s \r\n", name);
	if(name != 0){
        len1 = strlen(name);
		for(i = 0; i < jxos_registry_count; i++){
			len2 = strlen(jxos_registry_list[i].name);
//			printf("----> racorde: %s\r\n", jxos_registry_list[i].name);
			if((len1 == len2)&&
			(memcmp(jxos_registry_list[i].name, name, len1) == 0)){
				p = jxos_registry_list[i].handle;
				break;
			}
		}
	}
	return p;
}

uint8_t jxos_registry_register(const char* name, void* handle)
{
	if((name != 0)&&(handle != 0)
        &&(jxos_registry_count <= REGISTRY_MAX)
        &&(jxos_registry_find(name) == 0)){
		jxos_registry_list[jxos_registry_count].name = name;
		jxos_registry_list[jxos_registry_count].handle = handle;
		jxos_registry_count++;
		return 1;
	}
	else{
		return 0;
	}
}

#endif

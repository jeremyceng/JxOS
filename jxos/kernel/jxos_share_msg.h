#include "jxos_config.h"
#include "type.h"




typedef void* JXOS_SHARE_MSG_SENDER_HANDLE;
typedef void* JXOS_SHARE_MSG_RECEIVER_HANDLE;


typedef struct
{
	FIFO_ST  buff;
    uint8_t item_size;
}SHARE_MSG_RECEIVER_STRUCT

typedef struct
{
	SHARE_MSG_RECEIVER_STRUCT*  receiver_table;
	uint8_t receiver_table_count;
	uint8_t receiver_table_max;
    uint8_t item_size;
}SHARE_MSG_SENDER_STRUCT

void jxos_share_msg_init(void);


JXOS_SHARE_MSG_SENDER_HANDLE jxos_share_msg_sender_create(uint8_t receiver_max, uint8_t msg_item_size,
						const char* name);
JXOS_SHARE_MSG_SENDER_HANDLE jxos_share_msg_sender_get_handle(const char* name);

JXOS_SHARE_MSG_RECEIVER_HANDLE jxos_share_msg_receiver_create(uint8_t* receive_buff, uint16_t receive_buff_len
						const char* name);
JXOS_SHARE_MSG_RECEIVER_HANDLE jxos_share_msg_receiver_get_handle(const char* name);

bool_t jxos_share_msg_connect(JXOS_SHARE_MSG_SENDER_HANDLE sender, JXOS_SHARE_MSG_RECEIVER_HANDLE receiver);
bool_t jxos_share_msg_disconnect(JXOS_SHARE_MSG_SENDER_HANDLE sender, JXOS_SHARE_MSG_RECEIVER_HANDLE receiver);

bool_t jxos_share_msg_send(JXOS_SHARE_MSG_HANDLE sender, const uint8_t* msg);

bool_t jxos_share_msg_receive(JXOS_SHARE_MSG_RECEIVER_HANDLE receiver, uint8_t* msg_item_buffer);


//e.g.
//--------------sys-----------------------
jxos_share_msg_init();

//--------------app1-----------------------
JXOS_SHARE_MSG_SENDER_HANDLE test_share_msg_sender;
test_share_msg_sender = jxos_share_msg_sender_create(4, 2, "test_sender");

//--------------app2-----------------------
JXOS_SHARE_MSG_HANDLE test_share_msg_receiver;
uint8_t test_share_msg_receiver_buff[8];
test_share_msg_receiver = jxos_share_msg_receiver_create(test_share_msg_receiver_buff, 8, "test_receive");
jxos_share_msg_receiver_connect(test_share_msg_sender, test_share_msg_receiver);

//--------------app3-----------------------
JXOS_SHARE_MSG_HANDLE test_share_msg_receiver_2;
uint8_t test_share_msg_receiver_buff_2[8];
test_share_msg_receiver_2 = jxos_share_msg_receiver_create(test_share_msg_receiver_buff_2, 8, "test_receive");
jxos_share_msg_receiver_connect(test_share_msg_sender, test_share_msg_receiver_2);

//--------------app1-----------------------
jxos_share_msg_send(test_share_msg_sender, "message");

//--------------app2-----------------------
uint8_t msg_item_buffer
jxos_msg_receive(test_share_msg_receiver, msg_item_buffer);

//--------------app3-----------------------
uint8_t msg_item_buffer_2
jxos_msg_receive(test_share_msg_receiver_2, msg_item_buffer_2);

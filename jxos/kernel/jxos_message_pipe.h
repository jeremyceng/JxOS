
#ifndef __JXOS_MESSAGE_PIPE_H
#define __JXOS_MESSAGE_PIPE_H

#include "jxos_config.h"
#include "type.h"

#if (JXOS_MESSAGE_PIPE_ENABLE == 1)

typedef void* JXOS_MESSAGE_PIPE_HANDLE;

void jxos_message_pipe_init(void);
uint8_t jxos_message_pipe_check_empty_all(void);

JXOS_MESSAGE_PIPE_HANDLE jxos_message_pipe_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,
						char* message_pipe_name); //size+data+index, data > 0; data_size > 2

uint8_t jxos_message_pipe_send(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_message_pipe_receive(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_message_pipe_check_empty(JXOS_MESSAGE_PIPE_HANDLE message_pipe);		//err:0xff; true:1; false:0;
uint8_t jxos_message_pipe_check_full(JXOS_MESSAGE_PIPE_HANDLE message_pipe);		//err:0xff; true:1; false:0;
uint8_t jxos_message_pipe_clean_up(JXOS_MESSAGE_PIPE_HANDLE message_pipe,
								char* sender_name_or_receiver_name);
uint8_t jxos_message_pipe_sender_name_register(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name);
uint8_t jxos_message_pipe_receiver_name_register(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name);
uint8_t jxos_message_pipe_sender_name_cancel(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name);
uint8_t jxos_message_pipe_receiver_name_cancel(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MESSAGE_PIPE_HANDLE jxos_message_pipe_get_handle(char* message_pipe_name);
#endif

#endif

#endif // __JXOS_MESSAGE_PIPE_H

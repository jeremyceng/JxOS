
#ifndef __JXOS_BULLETIN_BOARD_H
#define __JXOS_BULLETIN_BOARD_H

#include "jxos_config.h"
#include "type.h"

#if (JXOS_BULLETIN_BOARD_ENABLE == 1)

typedef void* JXOS_BULLETIN_BOARD_HANDLE;

void jxos_bulletin_board_init(void);
uint8_t jxos_bulletin_board_check_empty_all(void);

JXOS_BULLETIN_BOARD_HANDLE jxos_bulletin_board_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,
						char* bulletin_board_name);

uint8_t jxos_bulletin_board_send(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_bulletin_board_receive(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t bulletin_serial_num, uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_bulletin_board_receive_latest(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_bulletin_board_check_empty(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);		//err:0xff; true:1; false:0;
uint8_t jxos_bulletin_board_check_full(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);		//err:0xff; true:1; false:0;
uint8_t jxos_bulletin_board_check_num(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);		//return 0XFF when err
uint8_t jxos_bulletin_board_del_earliest(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							char* sender_name);
uint8_t jxos_bulletin_board_clean_up(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							char* sender_name);
uint8_t jxos_bulletin_board_sender_name_register(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name);
uint8_t jxos_bulletin_board_sender_name_cancel(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name);

#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_BULLETIN_BOARD_HANDLE jxos_bulletin_board_get_handle(char* bulletin_board_name);
#endif

#if (JXOS_BULLETIN_BOARD_INDEX_ENABLE == 1)
//index: 0~255
uint8_t jxos_bulletin_board_read_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t bulletin_serial_num, uint8_t* index);
uint8_t jxos_bulletin_board_get_next_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, uint8_t* local_index);
uint8_t jxos_bulletin_board_send_with_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_bulletin_board_receive_by_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t index, uint8_t* receive_buffer, uint8_t* receive_data_len);
#endif

#endif

#endif // __JXOS_BULLETIN_BOARD_H

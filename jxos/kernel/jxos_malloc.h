
#ifndef __JXOS_MALLOC_H
#define __JXOS_MALLOC_H

#include "type.h"

typedef struct
{
	uint8_t* mem_pool;
	uint16_t len;
	uint16_t offset;
} JXOS_MALLOC_MEM_POOL_STRUCT;

uint8_t jxos_malloc_set_mem_pool(JXOS_MALLOC_MEM_POOL_STRUCT* mem_pool);
void *jxos_malloc(uint8_t malloc_size);

#endif

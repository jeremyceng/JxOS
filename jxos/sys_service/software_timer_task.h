
#ifndef _SOFTWARE_TIMER_TASK_H
#define _SOFTWARE_TIMER_TASK_H

#include "type.h"

void sys_software_timer_task_init(void);
void sys_software_timer_task(void);


#endif


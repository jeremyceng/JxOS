
#ifndef _POWER_MGR_TASK_H
#define _POWER_MGR_TASK_H

#include "type.h"

void sys_power_mgr_task_init(void);

uint8_t sys_power_mgr_task_keep_wake_mark_get_id(void);
void sys_power_mgr_task_keep_wake_mark_set(uint8_t mark_id);
void sys_power_mgr_task_keep_wake_mark_reset(uint8_t mark_id);
void sys_power_mgr_task_interrupt_happened_falg(void);

#endif

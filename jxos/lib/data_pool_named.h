
#ifndef __DATA_POOL_NAMED_H
#define __DATA_POOL_NAMED_H

#include "string.h"
#include "../lib/data_pool.h"

#define NAME_LEN 6

typedef struct
{
	DATA_POOL_STRUCT data_pool;
    uint8_t writer_name[NAME_LEN];
    uint8_t reader_name[NAME_LEN];
    uint8_t write_enable;
} DATA_POOL_NAMED_STRUCT;

uint8_t data_pool_named_init(DATA_POOL_NAMED_STRUCT* data_pool_named,
							uint8_t* buffer, uint16_t buffer_len, uint8_t data_size);
uint8_t data_pool_named_read_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name,
								uint16_t data_serial_num, uint8_t* read_buffer, uint8_t* read_data_len); //data_serial_num:0~254
uint8_t data_pool_named_write_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name,
                                uint8_t* write_data, uint8_t write_data_len);  //write_data_len <= data_size - 1
uint8_t data_pool_named_del_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name);
uint8_t data_pool_named_check_empty(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name); //err:0xff; true:1; false:0;
uint8_t data_pool_named_check_full(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name);  //err:0xff; true:1; false:0;
uint16_t data_pool_named_check_data_num(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name); //return:0~0xfffe; err:0xffff; 0->empty
uint8_t data_pool_named_clr_all(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name);
uint8_t data_pool_named_write_enable(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name);
uint8_t data_pool_named_write_disable(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name);
uint8_t data_pool_named_writer_name_register(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name);
uint8_t data_pool_named_reader_name_register(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name);
uint8_t data_pool_named_writer_name_cancel(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name_to_cancel);
uint8_t data_pool_named_reader_name_cancel(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name_to_cancel);

#endif
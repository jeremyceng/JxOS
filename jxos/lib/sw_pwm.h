
#ifndef _SW_PWM_H_
#define _SW_PWM_H_

#include "sw_timer.h"

typedef struct{ 
 uint8_t b7	:1;
 uint8_t b6	:1;
 uint8_t b5	:1;
 uint8_t b4	:1;
 uint8_t b3	:1;
 uint8_t b2	:1;
 uint8_t current_level :1;
 uint8_t idle_level	:1;
}SW_PWM_STATE_STRUCT;	

typedef struct {
    SW_TIMER_STRUCT sw_timer;
	uint16_t compare_1;
	uint16_t compare_2;
    void (*set_high_callback)(void);
    void (*set_low_callback)(void);
    void (*compare_1_callback)(void);
    void (*compare_2_callback)(void);
	SW_PWM_STATE_STRUCT state;
} SW_PWM_STRUCT;

void sw_pwm_set_compare_1(SW_PWM_STRUCT* sw_pwm, uint16_t compare_1);
void sw_pwm_set_compare_2(SW_PWM_STRUCT* sw_pwm, uint16_t compare_2);
void sw_pwm_set_idle_level(SW_PWM_STRUCT* sw_pwm, uint8_t idle_level);
void sw_pwm_set_period(SW_PWM_STRUCT* sw_pwm, uint16_t period);
uint16_t sw_pwm_get_period(SW_PWM_STRUCT* sw_pwm);
void sw_pwm_reset_counter(SW_PWM_STRUCT* sw_pwm);
void sw_pwm_enable(SW_PWM_STRUCT* sw_pwm);
void sw_pwm_disable(SW_PWM_STRUCT* sw_pwm);
uint8_t sw_pwm_check_enable(SW_PWM_STRUCT* sw_pwm);
void sw_pwm_reset_timeout(SW_PWM_STRUCT* sw_pwm);
uint8_t sw_pwm_check_timeout(SW_PWM_STRUCT* sw_pwm);
void sw_pwm_tick_handler(SW_PWM_STRUCT* sw_pwm, uint16_t ticks);
void sw_pwm_init(SW_PWM_STRUCT* sw_pwm, uint16_t period, uint16_t compare_1, uint16_t compare_2, uint8_t idle_level,
					void (*period_timeout_callback_handler)(void* p),
					void (*set_high_callback_handler)(void),
					void (*set_low_callback_handler)(void),
					void (*compare_1_callback_handler)(void),
					void (*compare_2_callback_handler)(void));

typedef struct
{
	uint8_t sw_pwm_num;
	uint8_t sw_pwm_pointer;
	SW_PWM_STRUCT* sw_pwm;
} SW_PWM_GROUP_STRUCT;

void sw_pwm_group_tick_handler(SW_PWM_GROUP_STRUCT* sw_pwm_group, uint16_t ticks);
void sw_pwm_group_init(SW_PWM_GROUP_STRUCT* sw_pwm_group,
                               uint8_t sw_pwm_group_sw_pwm_num,
                               SW_PWM_STRUCT* sw_pwm_group_space);
void sw_pwm_init(SW_PWM_STRUCT* sw_pwm, uint16_t period, uint16_t compare_1, uint16_t compare_2, uint8_t idle_level,
							void (*period_timeout_callback_handler)(void* p),
							void (*set_high_callback_handler)(void),
							void (*set_low_callback_handler)(void),
							void (*compare_1_callback_handler)(void),
							void (*compare_2_callback_handler)(void));

#endif
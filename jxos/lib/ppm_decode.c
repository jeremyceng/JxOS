
#include "lib/type.h"


#define DECODE_BIT_0			0
#define DECODE_BIT_1			1
#define DECODE_BIT_SYNC			2
#define DECODE_BIT_END			3
#define DECODE_BIT_ERROR		4

/*
#define SYNC_PULSE_TIME_TICK		40UL
#define SYNC_IDLE_TIME_TICK			40UL

#define BIT0_PULSE_TIME_TICK		20UL	
#define BIT0_IDLE_TIME_TICK			20UL

#define BIT1_PULSE_TIME_TICK		10UL
#define BIT1_IDLE_TIME_TICK			10UL

#define END_PULSE_TIME_TICK			80UL
#define END_IDLE_TIME_TICK			5UL

#define ALLOW_ERR_PERCENT			30UL
*/

typedef struct {
	uint16_t SYNC_PULSE_TIME_TICK;
	uint16_t SYNC_IDLE_TIME_TICK;
	
	uint16_t BIT0_PULSE_TIME_TICK;	
	uint16_t BIT0_IDLE_TIME_TICK;
	
	uint16_t BIT1_PULSE_TIME_TICK;
	uint16_t BIT1_IDLE_TIME_TICK;
	
	uint16_t END_PULSE_TIME_TICK;
	uint16_t END_IDLE_TIME_TICK;
	
	uint16_t ALLOW_ERR_PERCENT;
} DECODE_STRUCT;


uint8_t decode_bit_pwm(DECODE_STRUCT* decode_st, uint16_t pulse_tick_time, uint16_t idle_tick_time)
{
	if((pulse_tick_time <= decode_st->SYNC_PULSE_TIME_TICK + ((decode_st->SYNC_PULSE_TIME_TICK*decode_st->ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= decode_st->SYNC_PULSE_TIME_TICK - ((decode_st->SYNC_PULSE_TIME_TICK*decode_st->ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= decode_st->SYNC_IDLE_TIME_TICK + ((decode_st->SYNC_IDLE_TIME_TICK*decode_st->ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= decode_st->SYNC_IDLE_TIME_TICK - ((decode_st->SYNC_IDLE_TIME_TICK*decode_st->ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_SYNC;
	}
	
	if((pulse_tick_time <= BIT0_PULSE_TIME_TICK + ((BIT0_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= BIT0_PULSE_TIME_TICK - ((BIT0_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= BIT0_IDLE_TIME_TICK + ((BIT0_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= BIT0_IDLE_TIME_TICK - ((BIT0_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_0;
	}

	if((pulse_tick_time <= BIT1_PULSE_TIME_TICK + ((BIT1_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= BIT1_PULSE_TIME_TICK - ((BIT1_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= BIT1_IDLE_TIME_TICK + ((BIT1_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= BIT1_IDLE_TIME_TICK - ((BIT1_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_1;
	}
	
	if((pulse_tick_time <= END_PULSE_TIME_TICK + ((END_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= END_PULSE_TIME_TICK - ((END_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= END_IDLE_TIME_TICK + ((END_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= END_IDLE_TIME_TICK - ((END_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_END;
	}
	
	return DECODE_BIT_ERROR;
}

/***
uint8_t decode_bit_pwm(uint16_t pulse_tick_time, uint16_t idle_tick_time)
{
	
	if((pulse_tick_time <= SYNC_PULSE_TIME_TICK + ((SYNC_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= SYNC_PULSE_TIME_TICK - ((SYNC_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= SYNC_IDLE_TIME_TICK + ((SYNC_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= SYNC_IDLE_TIME_TICK - ((SYNC_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_SYNC;
	}
	
	if((pulse_tick_time <= BIT0_PULSE_TIME_TICK + ((BIT0_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= BIT0_PULSE_TIME_TICK - ((BIT0_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= BIT0_IDLE_TIME_TICK + ((BIT0_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= BIT0_IDLE_TIME_TICK - ((BIT0_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_0;
	}

	if((pulse_tick_time <= BIT1_PULSE_TIME_TICK + ((BIT1_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= BIT1_PULSE_TIME_TICK - ((BIT1_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= BIT1_IDLE_TIME_TICK + ((BIT1_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= BIT1_IDLE_TIME_TICK - ((BIT1_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_1;
	}
	
	if((pulse_tick_time <= END_PULSE_TIME_TICK + ((END_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(pulse_tick_time >= END_PULSE_TIME_TICK - ((END_PULSE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time <= END_IDLE_TIME_TICK + ((END_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))
	&&(idle_tick_time >= END_IDLE_TIME_TICK - ((END_IDLE_TIME_TICK*ALLOW_ERR_PERCENT)/100))){
		return DECODE_BIT_END;
	}
}
***/

void bsp_rf_receive_tick_handler(void)		//10us
{
   	c temp_pin_level ;
	uint8_t edge;
	uint16_t bit_time;

	if(bsp_rf_receive_finish_receive_falg == 1){
		return;
	}
	
	temp_pin_level = bsp_rf_receive_pin_hal_read_level();
	edge = EDGE_NONE;
	if(temp_pin_level == 0){
		if(low_count <= L_TIME_MAX){
			low_count++;
		}
		if(last_pin_level  == 1){
			edge = EDGE_FALL;
		}
	}
	else{
		if(hight_count <= H_TIME_MAX){
			hight_count++;
		}
		if(last_pin_level == 0){
			edge = EDGE_RISE;
		}
	}
	last_pin_level = temp_pin_level;

	if(edge == EDGE_FALL){
		if((sync_rec_ok_falg == 1)
			&&(rf_receive_buff_p != 0)
			&&(hight_count >= END_H_TIME - ((END_H_TIME*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
//			bit_state = RECEIVE_END;
			sync_rec_ok_falg = 0;
			bsp_rf_receive_buff_len = rf_receive_buff_p;
			rf_receive_buff_p = 0;
			bsp_rf_receive_finish_receive_falg = 1;
			bsp_rf_receive_state = BSP_RF_REC_STATE_FINISH;
			if(bsp_rf_receive_ok_call_back != 0){
				bsp_rf_receive_ok_call_back();
			}
		}
	}
	else if(edge == EDGE_RISE){
		bit_time = low_count + hight_count;
		if((bit_time >= SYNC_L_TIME + SYNC_H_TIME - (((SYNC_L_TIME + SYNC_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= SYNC_L_TIME + SYNC_H_TIME + (((SYNC_L_TIME + SYNC_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec sync head
//			bit_state = RECEIVE_SYNC;
			bit_i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 1;
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_SYNC;
			if(bsp_rf_receive_ing_call_back != 0){
				bsp_rf_receive_ing_call_back();
			}
		}
		else if((bit_time >= BIT0_L_TIME + BIT0_H_TIME - (((BIT0_L_TIME + BIT0_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= BIT0_L_TIME + BIT0_H_TIME + (((BIT0_L_TIME + BIT0_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec bit 0
//			bit_state = RECEIVE_BIT0;
			if(sync_rec_ok_falg == 1){
				bit_i++;
			}
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_DATA;
//			if(bsp_rf_receive_ing_call_back != 0){
//				bsp_rf_receive_ing_call_back();
//			}
		}
		else if((bit_time >= BIT1_L_TIME + BIT1_H_TIME - (((BIT1_L_TIME + BIT1_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= BIT1_L_TIME + BIT1_H_TIME + (((BIT1_L_TIME + BIT1_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec bit 1
//			bit_state = RECEIVE_BIT1;
			if(sync_rec_ok_falg == 1){
				receive_byte |= 0x01<<bit_i; //the 8 bit cpu was allowed move data in16 bit only, if receive_byte is more then 16 bit, there will be a error happened
				bit_i++;
			}
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_DATA;
//			if(bsp_rf_receive_ing_call_back != 0){
//				bsp_rf_receive_ing_call_back();
//			}
		}
		else{
			bit_i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 0;
			bsp_rf_receive_state = BSP_RF_REC_STATE_WAITTING;
//			if(bsp_rf_receive_wait_call_back != 0){
//				bsp_rf_receive_wait_call_back();
//			}
		}
		hight_count = 0;
		low_count = 0;
/**************************************************************
//optimize 
		if(bit_state == RECEIVE_SYNC){
			i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 1;
		}
		else if((sync_rec_ok_falg == 1)
			&&(bit_state == RECEIVE_BIT0)){
			i++;
		}
		else if((sync_rec_ok_falg == 1)
			&&(bit_state == RECEIVE_BIT1)){
			receive_byte |= 0x01<<i;
			i++;
		}
		else{
			i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 0;
		}
***************************************************************/
		if(bit_i == 8){
			if(rf_receive_buff_p < DATA_FLOW_MAX_LEN){
				bsp_rf_receive_buff[rf_receive_buff_p] = receive_byte;
				rf_receive_buff_p++;
			}
			bit_i = 0;
			receive_byte = 0;
		}
	}
}

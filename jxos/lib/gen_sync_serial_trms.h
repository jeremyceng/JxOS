

#ifndef _GSST_H_
#define _GSST_H_


#include "lmexxx_conf.h"


/****** General synchronous serial transmission *******/

typedef struct{ 
U8 b7			:1;
U8 b6			:1;
U8 b5			:1;
U8 b4			:1;
U8 mst_slv 		:1;	//0:master; 1:slave
U8 cpha 		:1;
U8 cpol 		:1;		
U8 lsb_msb 		:1;	//0:LSB; 1:MSB;
}GSST_CONFIG_STRUCT;	

struct __GSST_STRUCT__{
	U8 bpt;				//how many bit per transfer
	void (*delay)(void);
	void (*clk_set)(U8 clk_level);
	U8 (*clk_get)(void);
	void (*dat_set)(U8 dat_level);
	U8 (*dat_get)(void);
	U32 (*transmit)(struct __GSST_STRUCT__* gsst, U32 trm_data);
	GSST_CONFIG_STRUCT config;
};
typedef struct __GSST_STRUCT__ GSST_STRUCT;

U32 gsst_transmit_master(GSST_STRUCT* gsst, U32 send_data);
U32 gsst_transmit_slave(GSST_STRUCT* gsst, U32 send_data);

#endif



#ifndef _SW_TIMERS_MNG_H_
#define _SW_TIMERS_MNG_H_

#include "PLCP_sw_timer.h"


SW_TIMER_STRUCT* sw_timers_mng_new(void);
U8 sw_timers_mng_del(SW_TIMER_STRUCT* sw_timer);
void sw_timers_mng_tick_handler(U32 ticks);
void sw_timers_mng_init(void);


#endif

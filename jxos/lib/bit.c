
#include "type.h"

#define set_bit(byte, bit)			byte |= (1<<bit)
#define reset_bit(byte, bit)		byte &= ~(1<<bit)
#define get_bit(byte, bit)			((byte & (1<<bit))>>bit)

static uint32_t bit_32;

uint32_t set_bit_32(uint32_t dat_32, uint8_t bits)
{
	bit_32 = 1;
	bit_32 <<= bits;
	dat_32 |= bit_32;
	return dat_32;
}

uint32_t reset_bit_32(uint32_t dat_32, uint8_t bits)
{
	bit_32 = 1;
	bit_32 <<= bits;
	dat_32 &= ~bit_32;
	return dat_32;
}

uint32_t toggle_bit_32(uint32_t dat_32, uint8_t bits)
{
	bit_32 = 1;
	bit_32 <<= bits;
	dat_32 ^= bit_32;
	return dat_32;
}

uint32_t get_bit_32(uint32_t dat_32, uint8_t bits)
{
	bit_32 = 1;
	bit_32 <<= bits;
	dat_32 &= bit_32;
	dat_32 >>= bits;
	return dat_32;
}
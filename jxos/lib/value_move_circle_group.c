

#include "value_move_circle_group.h"
#include "string.h"

void value_move_circle_group_tick_handler(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group)
{
	uint8_t i;
	for (i = 0; i < value_move_circle_group->value_move_circle_count; i++) {
			value_move_circle_tick_handler(&(value_move_circle_group->value_move_circle[i]));
	}
}

uint8_t value_move_circle_group_check_active(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group)
{
	uint8_t i;
	for (i = 0; i < value_move_circle_group->value_move_circle_count; i++) {
			if(value_move_circle_check_active(&(value_move_circle_group->value_move_circle[i])) == 1){
				return 1;
			}
	}
	return 0;
}

void value_move_circle_group_init(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group,
						uint8_t value_move_circle_group_value_move_num,
						VALUE_MOVE_CIRCLE_STRUCT* value_move_space)
{
	memset(value_move_space, 0, sizeof(VALUE_MOVE_CIRCLE_STRUCT)*value_move_circle_group_value_move_num);
	value_move_circle_group->value_move_circle_count = 0;
	value_move_circle_group->value_move_circle_num_max = value_move_circle_group_value_move_num;
	value_move_circle_group->value_move_circle = value_move_space;
}


VALUE_MOVE_CIRCLE_STRUCT* value_move_circle_new(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group, VALUE_MOVE_CIRCLE_STRUCT* new_value_move)
{
	VALUE_MOVE_CIRCLE_STRUCT* value_move_circle = 0;
	if(value_move_circle_group->value_move_circle_count < value_move_circle_group->value_move_circle_num_max){
		value_move_circle = &(value_move_circle_group->value_move_circle[value_move_circle_group->value_move_circle_count]);
		memcpy((uint8_t*)value_move_circle, (uint8_t*)new_value_move, sizeof(VALUE_MOVE_CIRCLE_STRUCT));
		value_move_circle_group->value_move_circle_count++;
		return value_move_circle;
	}
	else{
		return 0;
	}
}


/**********************
e.g.
#include "lib\value_move_circle_group.h"

#define VALUE_MOVE_CIRCLE_EVENT_NUM_MAX        8
static VALUE_MOVE_CIRCLE_GROUP_STRUCT value_group;
static VALUE_MOVE_CIRCLE_STRUCT values[VALUE_MOVE_CIRCLE_EVENT_NUM_MAX];

static uint8_t obj_to_mun(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
    uint8_t i;
    for(i = 0; i < VALUE_MOVE_CIRCLE_EVENT_NUM_MAX; i++){
        if(value_move_circle == &(values[i])){
            return i;
        }
    }
    return 0xff;
}

static void value_changed(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
    uint8_t v_num = obj_to_mun(value_move_circle);
    printf("value_move_circle %d changed to %d\r\n", v_num, value_move_circle_get_current_value(value_move_circle));
}

void main(void)
{
    uint8_t i;
    VALUE_MOVE_STRUCT* value_move_p;
    VALUE_MOVE_CIRCLE_STRUCT* value_move_circle_p;
    VALUE_MOVE_CIRCLE_STRUCT value_move_circle_n;

    value_move_circle_n.value_move.current_value = 0;
    value_move_circle_n.value_move.target_value = 0;
    value_move_circle_n.value_move.current_value_changed_event_callbakc = value_changed;

    value_move_circle_n.begin_value = 0;
    value_move_circle_n.end_value = 0;

    value_move_circle_group_init(&value_group, VALUE_MOVE_CIRCLE_EVENT_NUM_MAX, values);

    for(i = 0; i < VALUE_MOVE_CIRCLE_EVENT_NUM_MAX; i++){
        value_move_circle_new(&value_group, &value_move_circle_n);
    }

    value_move_circle_p = &(values[0]);
    value_move_circle_init_current_value(value_move_circle_p, 0);
    value_move_circle_move_by_tick(value_move_circle_p, 10, 16, 13);

    while(1){
    	value_move_circle_group_tick_handler(&value_group);
        delay(1000);
    }
}

*****/






#include "times.h"

uint8_t time_chack_legal(TIME_STRUCT time)
{
	if(time.hour > 23){
		return 0;
	}
	if(time.min > 59){
		return 0;
	}
	if(time.sec > 59){
		return 0;
	}
	
	return 1;
}

/*
void time_print(TIME_STRUCT time)
{
	printf_string("time: ");
	printf_byte_decimal(time.hour);
	printf_char(':');
	printf_byte_decimal(time.min);
	printf_char(':');
	printf_byte_decimal(time.sec);
	printf_string("\r\n");
}
*/

uint8_t time_init(TIME_STRUCT* time,\
				uint8_t hour, uint8_t min, uint8_t sec)
{
	TIME_STRUCT temp_time;
		
	temp_time.hour = hour;
	temp_time.min = min;
	temp_time.sec = sec;
	
	if(0 != time_chack_legal(temp_time)){
		*time = temp_time;	
		return 1;
	}
	else{
		return 0;
	}	
}

static uint8_t time_add_hours(TIME_STRUCT* time, uint8_t hour)
{
	uint8_t over_day_flag = 0;
		
	if(hour > 23){
		return 0;	
	}	
	if(0 == time_chack_legal(*time)){
		return 0;
	}
	
	time->hour += hour;
	if(time->hour > 23){
		time->hour -= 24;
		over_day_flag = 1;
	}
	else{
		over_day_flag = 2;
	}
	
	return over_day_flag;
}

static uint8_t time_add_mins(TIME_STRUCT* time, uint8_t min)
{
	uint8_t over_day_flag = 0;
	
	if(min > 59){
		return 0;	
	}		
	if(0 == time_chack_legal(*time)){
		return 0;
	}
	
	time->min += min;
	if(time->min > 59){
		time->min -= 60;
		over_day_flag = time_add_hours(time, 1);
	}
	
	return over_day_flag;
}

static uint8_t time_add_secs(TIME_STRUCT* time,uint8_t sec)
{
	uint8_t over_day_flag = 0;
	
	if(sec > 59){
		return 0;	
	}		
	if(0 == time_chack_legal(*time)){
		return 0;
	}
	
	time->sec += sec;
	if(time->sec > 59){
		time->sec -= 60;
		over_day_flag = time_add_mins(time, 1);
	}
	
	return over_day_flag;	
}

uint8_t time_add_one_secs(TIME_STRUCT* time)
{
	uint8_t over_day_flag = 0;
		
	if(0 == time_chack_legal(*time)){
		return 0;
	}
	
	time->sec += 1;
	if(time->sec > 59){
		time->sec -= 60;
		over_day_flag = time_add_mins(time, 1);
	}
	
	return over_day_flag;	
}
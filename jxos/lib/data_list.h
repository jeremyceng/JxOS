
#ifndef __DATA_LIST_H
#define __DATA_LIST_H

#include "type.h"

typedef struct
{
	uint8_t* data_list_buffer;
    uint16_t data_list_len;
    uint8_t data_size;
} DATA_LIST_STRUCT;

//data_serial_num == sequence num
uint8_t data_list_init(DATA_LIST_STRUCT* data_list,
					uint8_t* buffer, uint16_t buffer_len, uint8_t data_size);	//buffer_len = data_count_max*(data_size+1)
uint8_t* data_list_get_data_piont(DATA_LIST_STRUCT* data_list, uint16_t data_serial_num);	//data_serial_num:0~0xfffe
uint8_t data_list_read_data(DATA_LIST_STRUCT* data_list,
						uint16_t data_serial_num, uint8_t* read_buffer, uint8_t* read_data_len);	//data_serial_num:0~0xfffe
uint8_t data_list_write_data(DATA_LIST_STRUCT* data_list,
						uint16_t data_serial_num, uint8_t* write_data, uint8_t write_data_len);	//data_serial_num:0~0xfffe, write_data_len <= data_size - 1
uint8_t data_list_del_data(DATA_LIST_STRUCT* data_list, uint16_t data_serial_num);	//data_serial_num:0~0xfffe
uint8_t data_list_check_free_data(DATA_LIST_STRUCT* data_list, uint16_t data_serial_num);	//data_serial_num:0~0xfffe
uint16_t data_list_find_free_data(DATA_LIST_STRUCT* data_list);
uint16_t data_list_find_data(DATA_LIST_STRUCT* data_list,
						uint8_t compare_index, uint8_t* compare_data, uint8_t compare_data_len);
uint8_t data_list_check_full(DATA_LIST_STRUCT* data_list);
/************************************************************************/
void data_list_find_data_continue_init(DATA_LIST_STRUCT* data_list);
uint16_t data_list_find_data_continue(DATA_LIST_STRUCT* data_list,
						uint8_t compare_index, uint8_t* compare_data, uint8_t compare_data_len);
/************************************************************************/

#endif


#ifndef __DATA_POOL_H
#define __DATA_POOL_H

#include "type.h"

typedef struct
{
	uint8_t* data_pool_buffer;
    uint16_t data_pool_len;
    uint8_t data_size;
    uint16_t add_p;
    uint16_t del_p;
} DATA_POOL_STRUCT;

uint8_t data_pool_init(DATA_POOL_STRUCT* data_pool,
					uint8_t* buffer, uint16_t buffer_len, uint8_t data_size);	//size+data, data > 0; data_size > 1
uint8_t* data_pool_get_data_piont(DATA_POOL_STRUCT* data_pool, uint16_t data_serial_num);	//data_serial_num:0~0xfffe
uint8_t data_pool_read_data(DATA_POOL_STRUCT* data_pool,
					uint16_t data_serial_num, uint8_t* read_buffer, uint8_t* read_data_len);	//data_serial_num:0~0xfffe
uint8_t data_pool_write_data(DATA_POOL_STRUCT* data_pool,
					uint8_t* write_data, uint8_t write_data_len); //write_data_len <= data_size-1
uint8_t data_pool_del_data(DATA_POOL_STRUCT* data_pool);
uint8_t data_pool_check_empty(DATA_POOL_STRUCT* data_pool);     //err:0xff; true:1; false:0;
uint8_t data_pool_check_full(DATA_POOL_STRUCT* data_pool);      //err:0xff; true:1; false:0;
uint16_t data_pool_check_data_num(DATA_POOL_STRUCT* data_pool);	//return:0~0xfffe; err:0xffff; 0:empty
uint8_t data_pool_clr_all(DATA_POOL_STRUCT* data_pool);


#endif


#ifndef __VALUE_MOVE__H
#define __VALUE_MOVE__H

#include "type.h"

typedef struct __VALUE_MOVE_STRUCT__ VALUE_MOVE_STRUCT;
struct __VALUE_MOVE_STRUCT__{ 
	/************************************************/
	/******************attribute*********************/
	//config_attribute

	//public_attribute	
	uint16_t current_value;

	//private_attribute
	uint16_t target_value;
	uint16_t interval_value;
	uint16_t interval_ticks;
	uint16_t interval_tick_counter;

	/************************************************/
	/******************action************************/
	//private_action

	//public_action
	//void value_move_to_value_by_tick(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t move_ticks);
	//void value_move_to_value_by_step(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t set_step_value);
	//...
	//void value_move_tick_handler(VALUE_MOVE_STRUCT* value_move);
	
	/************************************************/
	/******************callback**********************/
	//config_callbakc

	//event_callback
	void (*current_value_changed_event_callbakc)(VALUE_MOVE_STRUCT* value_move);
};   

//move_ticks: how many ticks will take to move "current_value" to "target_value"
void value_move_to_value_by_tick(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t move_ticks);

//set_step_value: how many steps to move pre tick
void value_move_to_value_by_step(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t set_step_value);

void compute_interval_value_and_interval_tick(uint16_t* interval_value, uint16_t* interval_ticks,
												uint16_t totle_setps, uint16_t totle_ticks);
void value_move_stop_move(VALUE_MOVE_STRUCT* value_move);
void value_move_init_current_value(VALUE_MOVE_STRUCT* value_move, uint16_t set_current_value);
bool_t value_move_check_active(VALUE_MOVE_STRUCT* value_move);

void value_move_tick_handler(VALUE_MOVE_STRUCT* value_move);

#endif


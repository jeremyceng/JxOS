
#ifndef __BUTTON_GROUP__H
#define __BUTTON_GROUP__H

#include "button.h"

typedef struct
{
	uint8_t button_num_max;
	uint8_t button_num;
	BUTTON_STRUCT* button;
} BUTTON_GROUP_STRUCT;

void button_group_scan_tick_handler(BUTTON_GROUP_STRUCT* button_group, uint16_t ticks);
void button_group_init(BUTTON_GROUP_STRUCT* button_group,
						uint8_t button_group_button_num,
						BUTTON_STRUCT* button_space);
BUTTON_STRUCT* button_new(BUTTON_GROUP_STRUCT* button_group, BUTTON_STRUCT* new_button);

// BUTTON_STRUCT* button_new(BUTTON_GROUP_STRUCT* button_group,
// 							uint8_t jitter_tick,
// 							uint8_t long_press_tick,
// 							uint8_t long_press_repeat_tick,
// 							uint8_t (*hal_read_button_state_callbakc_handler)(BUTTON_STRUCT* button),
// 							void (*press_callback_handler)(BUTTON_STRUCT* button),
// 							void (*release_callback_handler)(BUTTON_STRUCT* button),
// 							void (*long_press_callback_handler)(BUTTON_STRUCT* button));


#endif


#include <stdint.h>
#include <time.h>

#include "rtc_time.h"

static struct tm rtc_start_time = {
	RTC_START_TIME_SEC,				/* Seconds: 0-59 (K&R says 0-61?) */
	RTC_START_TIME_MIN,				/* Minutes: 0-59 */
	RTC_START_TIME_HOUR,			/* Hours since midnight: 0-23 */
	RTC_START_TIME_MDAY,			/* Day of the month: 1-31 */
	(RTC_START_TIME_MON - 1),		/* Months *since* january: 0-11 */
	(RTC_START_TIME_YEAR - 1900),	/* Years since 1900 */
	0,								/* Days since Sunday (0-6) */
	0,								/* Days since Jan. 1: 0-365 */
	0								/* +1 Daylight Savings Time, 0 No DST*/
};
static time_t rtc_time_counter = 0;
static struct tm rtc_time_localtime;

void rtc_time_tick(void)	//in sec
{
	rtc_time_counter++;
}

struct tm rtc_time_get_time(void)
{
	time_t ttime = rtc_time_counter;
	rtc_time_localtime = *(localtime(&ttime));
	return rtc_time_localtime;
}

void rtc_time_set_time(struct tm set_time)
{
	time_t ttime;
	ttime = mktime(&set_time);
//	__disable_irq();
	rtc_time_counter = ttime;
//	__enable_irq();
}

void rtc_time_init(void)
{
	rtc_time_set_time(rtc_start_time);
}


//e.g.
//struct tm ttime;
//ttime = rtc_time_get_time();
//printf( "\r%d:%d:%d %d:%d:%d",\
//		(ttime.tm_year+1900),\
//		(ttime.tm_mon+1),\
//		ttime.tm_mday,\
//		ttime.tm_hour,\
//		ttime.tm_min,\
//		ttime.tm_sec);


#ifndef __LED_H
#define __LED_H

#include "sw_pwm.h"

typedef struct {
	SW_PWM_STRUCT sw_pwm;
	uint8_t blink_times;
	uint8_t state;
} LED_STRUCT;

void led_blink_stop(LED_STRUCT* led);
void led_on(LED_STRUCT* led);
void led_off(LED_STRUCT* led);
void led_toggle(LED_STRUCT* led);
uint8_t led_get_state(LED_STRUCT* led);
void led_blink_start(LED_STRUCT* led, uint16_t period_time, uint16_t on_time, uint8_t blink_times);

typedef struct
{
	uint8_t led_num;
	uint8_t led_pointer;
	LED_STRUCT* led;
} LED_GROUP_STRUCT;

void led_group_tick_handler(LED_GROUP_STRUCT* led_group, uint16_t ticks);
void led_group_init(LED_GROUP_STRUCT* led_group,
						uint8_t led_group_led_num,
						LED_STRUCT* led_space);
LED_STRUCT* led_new(LED_GROUP_STRUCT* led_group,
						void (*led_on_callback)(void),  void (*led_off_callback)(void));


#endif

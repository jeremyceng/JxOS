

#include "type.h"
#include "bit_mark.h"

typedef struct
{
	uint8_t* key_list;
    uint8_t key_list_len;		//2 <= composite_key_list_len <= 255
	uint8_t	counter;
} COMPOSITE_KEY_STRUCT;


uint8_t composite_key_init(COMPOSITE_KEY_STRUCT* composite_key,
					uint8_t* composite_key_list, uint8_t composite_key_list_len);
//uint8_t composite_key_struct_check_err(COMPOSITE_KEY_STRUCT* composite_key);
uint8_t composite_key_press_handler(COMPOSITE_KEY_STRUCT* composite_key,
									BIT_MARK_STRUCT* pressing_key_mark);
uint8_t composite_key_press_handler_in_order(COMPOSITE_KEY_STRUCT* composite_key,
											BIT_MARK_STRUCT* pressing_key_mark,
											uint8_t press_key_num);


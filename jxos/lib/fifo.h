#ifndef __FIFO__H
#define __FIFO__H


#include "type.h"

typedef struct {
    uint16_t head;            //0 ~ len-1
    uint16_t tail;            //0 ~ len-1
    uint16_t len;            //0 ~ uint16_t MAX(65535)
    uint8_t* buff;

    uint8_t head_out_of_range: 1;
    uint8_t tail_out_of_range: 1;
}FIFO_ST;

//typedef struct {
//    uint16_t head;            //0 ~ len-1
//    uint16_t tail;            //0 ~ len-1
//    uint16_t len;            //0 ~ uint16_t MAX(65535)
//    uint8_t* buff;
//
//    uint8_t head_out_of_range;
//    uint8_t tail_out_of_range;
//}FIFO_ST;

uint8_t fifo_check_error(FIFO_ST* fifo);
uint16_t fifo_get_free_len(FIFO_ST *fifo);
uint16_t fifo_get_used_len(FIFO_ST *fifo);
void fifo_reset(FIFO_ST *fifo);
void fifo_write(FIFO_ST *fifo, uint8_t dat);
void fifo_read(FIFO_ST *fifo, uint8_t* dat);

//The following functions are security functions, thay are including parameter detection
uint8_t fifo_write_data(FIFO_ST* fifo, uint16_t data_len , uint8_t* in_data);
uint8_t fifo_read_data(FIFO_ST* fifo, uint16_t data_len, uint8_t* out_data);
uint8_t fifo_init(FIFO_ST *fifo, uint16_t buff_space_len, uint8_t* buff_space);


#endif




#include "communicater.h"


void communicater_tick_handler(COMMUNICATER_STRUCT* cmt, uint16_t ticks)
{
    retransmiter_tick_handler(&(cmt->retransmiter), ticks);
}

void communicater_match_handler(COMMUNICATER_STRUCT* cmt, uint8_t* response_match_info, uint8_t response_match_info_len)
{
    if(cmt->retransmiter.state == RETRANSMITER_STATE_REPEATING){
        if(matcher_match(&(cmt->matcher), response_match_info, response_match_info_len) == true){
            retransmiter_stop_repeat(&(cmt->retransmiter));
        }
    }
}

void communicater_send(COMMUNICATER_STRUCT* cmt, uint8_t* send_msg, uint8_t send_msg_len,
                       uint8_t repeat_max, uint16_t cycle_tick, void (*send_msg_callback)(uint8_t* send_msg, uint8_t send_msg_len))
{
    cmt->retransmiter.msg = send_msg;
    cmt->retransmiter.msg_len = send_msg_len;
    cmt->retransmiter.repeat_max_config = repeat_max;
    cmt->retransmiter.repeat_cycle_tick_config = cycle_tick;
    cmt->retransmiter.send_msg_config_callback = send_msg_callback;
    retransmiter_send(&(cmt->retransmiter));
}

void communicater_set_response_macth_info(COMMUNICATER_STRUCT* cmt, uint8_t* response_match_info, uint8_t response_match_info_len)
{
    cmt->matcher.match_info_config = response_match_info;
    cmt->matcher.match_info_len_config = response_match_info_len;
    cmt->matcher.state = MATCHER_STATE_NO;
}

uint8_t communicater_wait_response(COMMUNICATER_STRUCT* cmt)
{
    return cmt->retransmiter.state;
}

#include<Windows.h>
bool_t communicater_wait_response_blocked(COMMUNICATER_STRUCT* cmt)
{
    while(cmt->retransmiter.state == RETRANSMITER_STATE_REPEATING){
        Sleep(100);
    }
    if(cmt->retransmiter.state == RETRANSMITER_STATE_OVERTIME){
        return false;
    }
    else{
        return true;
    }
}


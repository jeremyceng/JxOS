
#ifndef __COMMUNICATER_H__
#define __COMMUNICATER_H__

#include "retransmiter.h"
#include "matcher.h"

#define COMMUNICATER_RESPONSE_STATE_REPEATING           0
#define COMMUNICATER_RESPONSE_STATE_GET_RSP             1
#define COMMUNICATER_RESPONSE_STATE_OVERTIME            2

typedef struct __COMMUNICATER_STRUCT__ COMMUNICATER_STRUCT;
struct __COMMUNICATER_STRUCT__{
    RETRANSMITER_STRUCT retransmiter;
    MATCHER_STRUCT matcher;
};

void communicater_tick_handler(COMMUNICATER_STRUCT* cmt, uint16_t ticks);
void communicater_match_handler(COMMUNICATER_STRUCT* cmt, uint8_t* response_match_info, uint8_t response_match_info_len);

void communicater_send(COMMUNICATER_STRUCT* cmt, uint8_t* send_msg, uint8_t send_msg_len,
                       uint8_t repeat_max, uint16_t cycle_tick, void (*send_msg_callback)(uint8_t* send_msg, uint8_t send_msg_len));
void communicater_set_response_macth_info(COMMUNICATER_STRUCT* cmt, uint8_t* response_match_info, uint8_t response_match_info_len);
bool_t communicater_wait_response(COMMUNICATER_STRUCT* cmt);
bool_t communicater_wait_response_blocked(COMMUNICATER_STRUCT* cmt);

#endif

#include "ring_buff.h"
/********************************************************************************
文  件：ringbuff.c
文  件：ringbuff.c
作  者:    JeremyCeng
版  本:    V1.0.5
时  间:    2017-06-20
说  明:    环形缓存区类
历  史: V1.0.0 2014-11-03:    初版
        V1.0.1 2015-04-28:    缓存区大小由宏定义改为可变
                            缓存区结构体内增加缓存区大小属性 len
                            缓存区由 s8 型改为可以在头文件中宏定义的 RINGBUFF_OBG_TYPE 型
        V1.0.2 2015-05-20:    在缓存区结构体内增加函数指针（方法），使缓存区成为类。
                            将缓存区初始化函数改为缓存区对象的构造函数。
                            修复读出写入时出现指针越界的BUG。
        V1.0.3 2015-07-18:    在读写缓存区过程中加入打开关闭buf读写相关的中断功能，
                            可以防止读写过程被中断打断，以保护缓冲区
        V1.0.4 2016-02-23:    去除每次读写过程都检测缓冲区错误，提高读写效率，
                            去除打开关闭buf读写相关的中断功能。
        V1.0.5 2017-06-20:    增加环形缓冲区的宏定义版本，
                            去除缓存区结构体中 free 项，防止在读操作和写操作中修改同一元素。
********************************************************************************/
//参考 巧夺天工的kfifo @ linux
/********************************************
//#ifndef NULL
#define NULL                (void*)0
//#endif

#define s8    char
#define s16 short int
#define s32    int
#define s64    long long

#define u8    unsigned char
#define u16 unsigned short int
#define u32    unsigned int
#define u64    unsigned long long

#define RINGBUFF_EMPTY        0xff
#define RINGBUFF_FULL        0xff
#define RINGBUFF_ERROR        0
#define RINGBUFF_OK            1

#define RINGBUFF_OBG_TYPE s8

typedef struct _ringbuff_mgr_ RINGBUFF_MGR;
struct _ringbuff_mgr_{
    u16 head;            //0 ~ len-1
    u16 tail;            //0 ~ len-1
    u16 len;            //0 ~ u16 MAX(65535)
    u8 head_out_of_range;
    u8 tail_out_of_range;
    RINGBUFF_OBG_TYPE* buff;

    s8 (*read)(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* data);
    s8 (*write)(RINGBUFF_MGR *buff,RINGBUFF_OBG_TYPE data);

    u16 (*get_len)(RINGBUFF_MGR *buff);
    u16 (*get_free)(RINGBUFF_MGR *buff);

    s8 (*check_haed_obj)(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* haed_obj);
    s8 (*check_tail_obj)(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* tail_obj);

    s8 (*write_data)(RINGBUFF_MGR *buff, u16 data_len ,RINGBUFF_OBG_TYPE* in_data);
    s8 (*read_data)(RINGBUFF_MGR* buff, u16 data_len, RINGBUFF_OBG_TYPE* out_data);

    s8 (*write_str)(RINGBUFF_MGR *buff, s8 *in_str);
    s8 (*read_str)(RINGBUFF_MGR *buff, s8 *out_str);
};
***********************************************/
/***********************************************
//2017-06-20
// define the ring buffer size
#define RING_BUF_MAXSIZE               (128)
#define RING_BUF_MASK                  (RING_BUF_MAXSIZE-1ul)//1ul

// Below are define the basic operation of the ring buffer
#define RING_BUF_RESET(RingBuf)             (RingBuf.rdIdx = RingBuf.wrIdx = 0)
#define RING_BUF_WR(RingBuf, dataIn)        (RingBuf.data[RING_BUF_MASK &RingBuf.wrIdx++] = (dataIn))
#define RING_BUF_RD(RingBuf)                (RingBuf.data[RING_BUF_MASK &RingBuf.rdIdx++])
#define RING_BUF_EMPTY_CHECK(RingBuf)       (RingBuf.rdIdx == RingBuf.wrIdx)
#define RING_BUF_FULL_CHECK(RingBuf)        (RingBuf.rdIdx == RingBuf.wrIdx+1)
#define RING_BUF_COUNT(RingBuf)             (RING_BUF_MASK & (RingBuf.wrIdx - RingBuf.rdIdx))

typedef struct __RING_BUF_T {
  unsigned char data[RING_BUF_MAXSIZE];
  unsigned wrIdx;
  unsigned rdIdx;
} RING_BUF_T;

e.g.
RING_BUF_T LeuartRingBuf;

if (RING_BUF_FULL_CHECK(LeuartRingBuf) == 0) {
    RING_BUF_WR(LeuartRingBuf, RxData);
}
***********************************************/
/********************************************************************************
功能描述:    检测环形缓存区是否出错
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    RINGBUFF_ERROR;RINGBUFF_OK;
日    期:     2018-1-31
备注说明:
********************************************************************************/
u8 ringbuff_check_error(RINGBUFF_MGR *buff)
{
    if((buff->tail >= buff->len)||
        (buff->tail >= buff->len)||
		(buff->buff == 0)||
		((buff->head_out_of_range != buff->tail_out_of_range)&&(buff->head_out_of_range != ~buff->tail_out_of_range))){
        return RINGBUFF_ERROR;
    }
    else{
        return RINGBUFF_OK;
    }
}

/********************************************************************************
功能描述:    查询环形缓存区总长度
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区总长度;RINGBUFF_ERROR;
日    期:    2018-1-31
备注说明:    头指针、尾指针、剩余空间不变化
********************************************************************************
u16 ringbuff_check_buff_long(RINGBUFF_MGR *buff)
{
	return buff->len;
}

/********************************************************************************
功能描述:    查询环形缓存区剩余空间
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区剩余空间大小;RINGBUFF_ERROR;
日    期:    2018-1-31
备注说明:    头指针、尾指针、剩余空间不变化
********************************************************************************/
u16 ringbuff_check_free_space(RINGBUFF_MGR *buff)
{
    u16 free = 0;
	if(buff->tail >= buff->head){
		free = buff->len - (buff->tail - buff->head);
	}
	else{
		//free = buff->len - (buff->len + buff->tail) - buff->head;
		free = buff->head - buff->tail;
	}

	if((free == buff->len)&&
	   (buff->head_out_of_range != buff->tail_out_of_range)){
	   free = 0;
	}

	return free;
}

/********************************************************************************
功能描述:    查询环形缓存区已用空间
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区已用空间大小;RINGBUFF_ERROR;
日    期:     2018-1-31
备注说明:    头指针、尾指针、剩余空间不变化
********************************************************************************/
u16 ringbuff_check_used_space(RINGBUFF_MGR *buff)
{
	return  (buff->len - ringbuff_check_free_space(buff));
}

/********************************************************************************
功能描述:    查询环形缓存区头元素的值
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区头元素的值;RINGBUFF_ERROR;
日    期:    2018-1-31
备注说明:    头指针、尾指针、剩余空间不变化
********************************************************************************
RINGBUFF_OBG_TYPE ringbuff_check_haed_obj(RINGBUFF_MGR *buff)
{
	return buff->buff[buff->head];
}

/********************************************************************************
功能描述:    查询环形缓存区尾元素的值
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区尾元素的值;RINGBUFF_ERROR;
日    期:    2018-1-31
备注说明:    头指针、尾指针、剩余空间不变化
********************************************************************************
RINGBUFF_OBG_TYPE ringbuff_check_tail_obj(RINGBUFF_MGR *buff)
{
	return buff->buff[buff->tail];
}

/********************************************************************************
功能描述:    将一个字节写入到环形缓存区尾部
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针    data：要写入的值
输出参数:
返 回 值:    RINGBUFF_OK;RINGBUFF_ERROR;RINGBUFF_FULL;
日    期:    2018-1-31
备注说明:    尾指针将自动变化
********************************************************************************/
static void ___write(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE dat)
{                              //not full
    buff->buff[buff->tail] = dat;

    if((buff->tail+1) >= buff->len){    //buff->tail > buff->len：指针越界
        buff->tail_out_of_range = ~buff->tail_out_of_range;
        buff->tail = 0;
    }
	else{
    	buff->tail++;
	}
}
/*
u8 ringbuff_write(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE dat)
{
    if((buff->tail == buff->head)&&
       (buff->tail_out_of_range != buff->head_out_of_range)){
        return RINGBUFF_ERROR;
    }
    else{                                //not full
		buff->buff[buff->tail] = dat;
		if((buff->tail+1) >= buff->len){    //buff->tail > buff->len：指针越界
			buff->tail_out_of_range = ~buff->tail_out_of_range;
			buff->tail = 0;
		}
		else{
			buff->tail++;
		}
        return RINGBUFF_OK;
    }
}

/********************************************************************************
功能描述:    环形缓存区头部读出一个字节
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:
返 回 值:    环形缓存区头元素的值;RINGBUFF_ERROR;RINGBUFF_EMPTY;
日    期:     2017-06-20
备注说明:    头指针将自动变化
********************************************************************************/
static void ___read(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* dat)
{                               //not empty
    *dat = buff->buff[buff->head];

    if((buff->head+1) >= buff->len){    //buff->head > buff->len：指针越界
        buff->head_out_of_range = ~buff->head_out_of_range;
        buff->head = 0;
    }
	else{
    	buff->head++;	
	}
}
/*
u8 ringbuff_read(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* dat)
{
    if((buff->tail == buff->head)&&
       (buff->tail_out_of_range == buff->head_out_of_range)){
        return RINGBUFF_ERROR;
    }
    else{                               //not empty
		*dat = buff->buff[buff->head];
		if((buff->head+1) >= buff->len){    //buff->head > buff->len：指针越界
			buff->head_out_of_range = ~buff->head_out_of_range;
			buff->head = 0;
		}
		else{
			buff->head++;	
		}
        return RINGBUFF_OK;
    }
}

/********************************************************************************
功能描述:    将一个数据流写入到环形缓存区尾部
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针    in_data：要写入的数据流指针
            data_len：要写入的数据流长度
输出参数:
返 回 值:    RINGBUFF_OK;RINGBUFF_FULL;RINGBUFF_ERROR;
日    期:     2016-02-23
备注说明:    尾指针、剩余空间将自动变化
            如果要写入的数据长度大于缓存区剩余空间长度（空间不够），返回RINGBUFF_ERROR
********************************************************************************/
u8 ringbuff_write_data(RINGBUFF_MGR *buff, u16 data_len , RINGBUFF_OBG_TYPE* in_data)
{
	u16 free = ringbuff_check_free_space(buff);
    if(free == 0){
        return RINGBUFF_ERROR;
    }
    else if(free < data_len){
        return RINGBUFF_ERROR;
    }
    else{
        for(; data_len > 0; in_data++){
            ___write(buff, *in_data);
            data_len--;
        }
        return RINGBUFF_OK;
    }
}

/********************************************************************************
功能描述:    环形缓存区头部读出一个数据流
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针 data_len：要读出的数据流长度
输出参数:    out_data：读出字符串的指针
返 回 值:    RINGBUFF_ERROR;RINGBUFF_EMPTY;RINGBUFF_OK;
日    期:     2016-02-23
备注说明:    头指针、剩余空间将自动变化;
            如果要读出的数据长度大于缓存区内部数据长度（数据不够），返回RINGBUFF_ERROR
********************************************************************************/
u8 ringbuff_read_data(RINGBUFF_MGR* buff, u16 data_len, RINGBUFF_OBG_TYPE* out_data)
{
    u16 free = ringbuff_check_free_space(buff);
    if(buff->len == free){
        return RINGBUFF_ERROR;
    }
    else if(buff->len - free < data_len){
        return RINGBUFF_ERROR;
    }
    else{
        for(; data_len > 0; out_data++){
            ___read(buff, out_data);
            data_len--;
        }
        return RINGBUFF_OK;
    }
}

/********************************************************************************
功能描述:    检测一个字符串的长度
作    者:
输入参数:    str：字符串指针
输出参数:
返 回 值:    字符串的长度值
日    期:    2014-11-03
备注说明:    返回的长度值不包括字符串尾部的结束符'\0'
********************************************************************************
static u16 strlen(const s8* str)
{
    const s8 *char_ptr;
    const unsigned long *longword_ptr;
    register unsigned long longword, magic_bits;

    for (char_ptr = str; ((unsigned long)char_ptr
        & (sizeof(unsigned long) - 1)) != 0;
        ++char_ptr) {
        if (*char_ptr == '\0')
            return char_ptr - str;
    }

    longword_ptr = (unsigned long*)char_ptr;

    magic_bits = 0x7efefeffL;

    while (1) {
        longword = *longword_ptr++;

        if ((((longword + magic_bits) ^ ~longword) & ~magic_bits) != 0) {
            const s8 *cp = (const s8*)(longword_ptr - 1);

            if (cp[0] == 0)
                return cp - str;
            if (cp[1] == 0)
                return cp - str + 1;
            if (cp[2] == 0)
                return cp - str + 2;
            if (cp[3] == 0)
                return cp - str + 3;
        }
    }
}

/********************************************************************************
功能描述:    检测一个字符串
作    者:    JeremyCeng
输入参数:    str：字符串指针 max_len：字符串允许的最大长度值
输出参数:
返 回 值:    字符串的长度值;-1(字符串不合法)
日    期:    2014-11-03
备注说明:    返回的长度值不包括字符串尾部的结束符'\0';
            字符串不合法的情况有:
                1.字符串中有非法(不属于ASCII字符集)字符
                2.字符串长度超过允许的最大长度值
********************************************************************************
static int str_check(s8* str,u16 max_len)
{
   s8 *char_ptr;
   int i = 0;
   char_ptr = str;
    while(1){
        if(0 == *char_ptr){
            break;
        }
        else if((*char_ptr > 0)&&(*char_ptr < 128)){
            i++;
            char_ptr++;
            if(i >= max_len){
                i = -1;
                break;
            }
        }
        else{
            i = -1;
            break;
        }
    }
    return i;
}
/********************************************************************************
功能描述:    将一个字符串写入到环形缓存区尾部
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针    in_str：要写入的字符串指针
输出参数:
返 回 值:    RINGBUFF_OK;RINGBUFF_ERROR;
日    期:     2014-11-03
备注说明:    尾指针、剩余空间将自动变化
            将对待写入的字符串进行合法性检查
            字符串不合法的情况有:
                1.字符串中有非法(不属于ASCII字符集)字符
                2.字符串长度超过允许的最大长度值
********************************************************************************
u8 string_in_buff(RINGBUFF_MGR* buff, s8* in_str)
{
    u16 free = ringbuff_check_free_space(buff);

	if(-1 == str_check(in_str, free)){
		return RINGBUFF_ERROR;
	}
	else{
		for(;*in_str != '\0';in_str++){
			___write(buff,(RINGBUFF_OBG_TYPE)*in_str);
		}
		___write(buff,'\0');
		return RINGBUFF_OK;
	}
}

/********************************************************************************
功能描述:    环形缓存区头部读出一个字符串
作    者:    JeremyCeng
输入参数:    buff：环形缓存区结构体指针
输出参数:    out_str：读出字符串的指针
返 回 值:    RINGBUFF_ERROR;RINGBUFF_EMPTY;RINGBUFF_OK;
日    期:     2014-11-03
备注说明:    头指针、剩余空间将自动变化;
            当循环环形缓存区中有非法(不属于ASCII字符集)字符时，不对缓存区进行读操作，返回RINGBUFF_ERROR
            当循环环形缓存区没有字符串结束符'\0'时，不对缓存区进行读操作，返回RINGBUFF_ERROR
********************************************************************************
u8 string_out_buff(RINGBUFF_MGR* buff, s8* out_str)
{
    u16 free = ringbuff_check_free_space(buff);

	if(-1 == str_check(&(buff->buff[buff->head]), (buff->len - free))){
		return RINGBUFF_ERROR;
	}
	else{
		if(buff->len == free){
			return RINGBUFF_ERROR;
		}
		else{
			while(1){
				___read(buff, (RINGBUFF_OBG_TYPE*)out_str);
				if('\0' == *out_str){
					break;
				}
				out_str++;
			}
			return RINGBUFF_OK;
		}
	}
}

/********************************************************************************
功能描述:    环形缓存区构造函数
作    者:    JeremyCeng
输入参数:    buff_long_init：设置缓存区空间大小
            buff_init：缓存区空间
            int_disable：关闭与buf读写相关的中断函数指针，收入为0时，该功能无效
             int_enable：打开与buf读写相关的中断函数指针，收入为0时，该功能无效
输出参数:    buff：环形缓存区结构体指针
返 回 值:    RINGBUFF_ERROR;RINGBUFF_OK;
日    期:     2016-02-23
备注说明:
********************************************************************************/
u8 RingBuff(RINGBUFF_MGR *buff, u16 buff_long_init, RINGBUFF_OBG_TYPE* buff_init)
{
    if((buff == NULL)||(buff_long_init == 0)||(buff_init == NULL)) {
        return RINGBUFF_ERROR;
    }

//    buff->read = ringbuff_read;
//    buff->write = ringbuff_write;
//    buff->get_len = ringbuff_check_buff_long;
//    buff->get_free = ringbuff_check_free_space;
//    buff->read_data = ringbuff_read_data;
//    buff->write_data = ringbuff_write_data;

    buff->buff = buff_init;
    buff->len = buff_long_init;

    buff->head = 0;
    buff->tail = 0;
    buff->head_out_of_range = 0;
    buff->tail_out_of_range = 0;

    return ringbuff_check_error(buff);
}

void ringbuff_reset(RINGBUFF_MGR *buff)
{
    buff->head = 0;
    buff->tail = 0;
    buff->head_out_of_range = 0;
    buff->tail_out_of_range = 0;
}

#if 0
/**E.G.**/
int main(void)
{
    int i;
    RINGBUFF_MGR buff;
    s8 buff_init[128] = {0};

    int buff_free = 0;
    RINGBUFF_OBG_TYPE buff_boj_write;
    RINGBUFF_OBG_TYPE buff_boj_read;
    s8 buff_data_write[] = "hello!!";
    RINGBUFF_OBG_TYPE buff_data_read[10] ={0};

    RingBuff(&buff,10,buff_init);

    buff_free = buff.get_free(&buff);
    printf("buff free:%d\r\n",buff_free);

    /*****************************************************/
    for(i = 0; i < 20; i++){
        printf("\r\nwrite&read 1 byte test...\r\n");
        buff_boj_write = 'a';
        printf("ringbuff_write,in: %c\r\n",buff_boj_write);
        buff.write(&buff,buff_boj_write);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);

        buff.read(&buff, &buff_boj_read);
        printf("ringbuff_read,out: %c\r\n",buff_boj_read);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);
    }
    /*****************************************************/

        printf("\r\nwrite&read data test...\r\n");
        printf("ringbuff_write_data,in: %s\r\n",buff_data_write);
        buff.write_data(&buff, 8, buff_data_write);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);

        if(0 == buff.read_data(&buff, 9, buff_data_read)){
            printf("err\n");
        }
        buff.read_data(&buff, 8, buff_data_read);
        printf("ringbuff_read_data,out: %s\r\n",buff_data_read);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);

    /*****************************************************/

    for(i = 0; i < 20; i++){
        printf("\r\nwrite&read string test...\r\n");
        printf("string_in_buff,in: %s\r\n",buff_data_write);
        string_in_buff(&buff, buff_data_write);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);

        string_out_buff(&buff, buff_data_read);
        printf("string_out_buff,out: %s\r\n",buff_data_read);
        buff_free = buff.get_free(&buff);
        printf("buff free:%d\r\n",buff_free);
    }


    return 0;
}
#endif

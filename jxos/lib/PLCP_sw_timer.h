

#ifndef _SW_TIMER_H_
#define _SW_TIMER_H_

#include "lmexxx_conf.h"

typedef struct{ 
 U8 b7	:1;
 U8 b6	:1;
 U8 b5	:1;
 U8 b4	:1;
 U8 b3	:1;
 U8 cyclic 	:1;
 U8 timeout :1;
 U8 enable	:1;
}SW_TIMER_STATE_STRUCT;	

struct __SW_TIMER_STRUCT__{
	U32 interval;
	U32 counter;
	void* param;
	void (*timeout_callback)(struct __SW_TIMER_STRUCT__* this_timer, void* this_param);
	SW_TIMER_STATE_STRUCT state;
};
typedef struct __SW_TIMER_STRUCT__ SW_TIMER_STRUCT;

void sw_timer_tick_handler(SW_TIMER_STRUCT* sw_timer, U32 ticks);


#endif

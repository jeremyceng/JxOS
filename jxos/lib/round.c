

#include "type.h"

uint32_t __round(uint32_t number, uint8_t rounding_down_digits)
{
    uint32_t temp;
    uint32_t ret;

    if(number > 0XFFFFFFFF){
        number = 0XFFFFFFFF;
    }
    if(rounding_down_digits > 9){
        return 0XFFFFFFFF;
    }
    if(rounding_down_digits == 0){
        return number;
    }

    temp = 1;
    for(; rounding_down_digits > 0 ; rounding_down_digits--){
        temp *= 10;
    }

    ret = number%temp;
    if(ret > temp/2){           //end number more then 5, rounding up;
        ret = number/temp + 1;
    }
    else if(ret < temp/2){      //end number less then 5, rounding down;
        ret = number/temp;
    }
    else{						//end number equal to 5;
        ret = number/temp;
        if(ret%2 == 1){         //result is an odd number, rounding up;
            ret += 1;
        }
    }

    return ret;
}
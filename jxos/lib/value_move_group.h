
#ifndef __VALUE_MOVE_GROUP__H
#define __VALUE_MOVE_GROUP__H

#include "value_move.h"

typedef struct
{
	uint8_t value_move_num_max;
	uint8_t value_move_count;
	VALUE_MOVE_STRUCT* value_move;
} VALUE_MOVE_GROUP_STRUCT;

void value_move_group_tick_handler(VALUE_MOVE_GROUP_STRUCT* value_move_group);
uint8_t value_move_group_check_active(VALUE_MOVE_GROUP_STRUCT* value_move_group);
void value_move_group_init(VALUE_MOVE_GROUP_STRUCT* value_move_group,
						uint8_t value_move_group_value_move_num,
						VALUE_MOVE_STRUCT* value_move_space);
VALUE_MOVE_STRUCT* value_move_new(VALUE_MOVE_GROUP_STRUCT* value_move_group, VALUE_MOVE_STRUCT* new_value_move);

#endif
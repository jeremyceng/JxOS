

#include "value_move.h"

void value_move_stop_move(VALUE_MOVE_STRUCT* value_move)
{
	if(value_move == 0){
		return;
	}
	value_move->target_value = value_move->current_value;
}

void value_move_init_current_value(VALUE_MOVE_STRUCT* value_move, uint16_t set_current_value)
{
	if(value_move == 0){
		return;
	}
	value_move->target_value = set_current_value;
	value_move->current_value = set_current_value;
}

bool_t value_move_check_active(VALUE_MOVE_STRUCT* value_move)
{
	if(value_move == 0){
		return false;
	}
	if(value_move->current_value == value_move->target_value){
		return false;
	}
	else{
		return true;
	}
}

void compute_interval_value_and_interval_tick(uint16_t* interval_value, uint16_t* interval_ticks,
												uint16_t totle_value, uint16_t totle_ticks)
{
	if((totle_value == 0)||(totle_ticks == 0)||
	(interval_value == 0)||(interval_ticks == 0)){
		return;
	}
	if(totle_value > totle_ticks){
		*interval_value = totle_value/totle_ticks;
		*interval_ticks = 1;
	}
	else{
		*interval_ticks = totle_ticks/totle_value;
		*interval_value = 1;
	}
}

//move_ticks: how many ticks will take to move "current_value" to "target_value"
void value_move_to_value_by_tick(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t move_ticks)
{
	uint16_t totle_value = 0;

	value_move_stop_move(value_move);

	if(value_move == 0){
		return;
	}
	if(value_move->current_value == set_target_value){
		return;
	}
	if(move_ticks > 0){
		if(value_move->current_value > set_target_value){
			totle_value = value_move->current_value - set_target_value;
		}
		else if(value_move->current_value < set_target_value){
			totle_value = set_target_value - value_move->current_value;
		}
		compute_interval_value_and_interval_tick(&(value_move->interval_value), &(value_move->interval_ticks),
													totle_value, move_ticks);
		value_move->target_value = set_target_value;
		value_move->interval_tick_counter = 0;
	}
	else{
		value_move->interval_value = 0;
		value_move->interval_ticks = 0;
		value_move->target_value = set_target_value;
		value_move->interval_tick_counter = 0;

		value_move->current_value = set_target_value;
		if(value_move->current_value_changed_event_callbakc){
			value_move->current_value_changed_event_callbakc(value_move);
		}
	}
}

//rate: how many value pre tick
void value_move_to_value_by_rate(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t rate)
{
	value_move_stop_move(value_move);

	if(value_move == 0){
		return;
	}
	if(value_move->current_value == set_target_value){
		return;
	}
	if(rate > 0){
		if(value_move->current_value > set_target_value){
			if(rate > (value_move->current_value - set_target_value)){
				rate = value_move->current_value - set_target_value;
			}
		}
		else if(value_move->current_value < set_target_value){
			if(rate > (set_target_value - value_move->current_value)){
				rate = set_target_value - value_move->current_value;
			}
		}

		value_move->interval_value = rate;
		value_move->interval_ticks = 1;
		value_move->target_value = set_target_value;
		value_move->interval_tick_counter = 0;
	}
}

void value_move_tick_handler(VALUE_MOVE_STRUCT* value_move)
{
	if(value_move == 0){
		return;
	}
	if(value_move_check_active(value_move) == 0){
		return;
	}
	if(value_move->interval_value == 0){
		value_move_stop_move(value_move);
		return;
	}
	if(value_move->interval_ticks == 0){
		value_move_stop_move(value_move);
		return;
	}

	value_move->interval_tick_counter++;
	if(value_move->interval_tick_counter < value_move->interval_ticks){
		return;
	}
	else{
		value_move->interval_tick_counter = 0;
	}

	if(value_move->current_value > value_move->target_value){
		if(value_move->current_value - value_move->target_value > value_move->interval_value){
			value_move->current_value -= value_move->interval_value;
		}
		else{
			value_move->current_value = value_move->target_value;
		}
		if(value_move->current_value_changed_event_callbakc){
			value_move->current_value_changed_event_callbakc(value_move);
		}
	}
	else if(value_move->current_value < value_move->target_value){
		if(value_move->target_value - value_move->current_value > value_move->interval_value){
			value_move->current_value += value_move->interval_value;
		}
		else{
			value_move->current_value = value_move->target_value;
		}
		if(value_move->current_value_changed_event_callbakc){
			value_move->current_value_changed_event_callbakc(value_move);
		}
	}
}


#ifndef _BIT_H
#define _BIT_H

#include "type.h"

//#include "stdbool.h"
//setbit
//clearbit
//testbit

#define set_bit(byte, bits)			byte |= (1<<bits)
#define reset_bit(byte, bits)		byte &= ~(1<<bits)
#define get_bit(byte, bits)			((byte & (1<<bits))>>bits)

uint32_t set_bit_32(uint32_t dat_32, uint8_t bits);
uint32_t reset_bit_32(uint32_t dat_32, uint8_t bits);
uint32_t toggle_bit_32(uint32_t dat_32, uint8_t bits);
uint32_t get_bit_32(uint32_t dat_32, uint8_t bits);

#endif

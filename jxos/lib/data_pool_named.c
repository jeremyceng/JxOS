
#include "string.h"
#include "../lib/data_pool_named.h"

//if writer_name not registered or writer_name same as registered writer_name; return 1;
static uint8_t data_pool_named_check_writer_name(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name)
{
	uint8_t i;
	if(writer_name == 0){
		return 0;
	}

    for(i = 0; i < NAME_LEN; i++){
		if(data_pool_named->writer_name[i] != 0){
			break;
		}
    }
    if(i != NAME_LEN){	//writer_name was registered
		if(memcmp(&(data_pool_named->writer_name[0]), writer_name, NAME_LEN) != 0){
			return 0;
    	}
    }
    //writer_name was NOT registered
    return 1;
}

static uint8_t data_pool_named_check_reader_name(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name)
{
	uint8_t i;
	if(reader_name == 0){
		return 0;
	}

    for(i = 0; i < NAME_LEN; i++){
		if(data_pool_named->reader_name[i] != 0){
			break;
		}
    }
    if(i != NAME_LEN){	//reader_name was registered
		if(memcmp(&(data_pool_named->reader_name[0]), reader_name, NAME_LEN) != 0){
			return 0;
		}
    }
    return 1;
}

uint8_t data_pool_named_init(DATA_POOL_NAMED_STRUCT* data_pool_named,
					uint8_t* buffer, uint16_t buffer_len, uint8_t data_size)
{
	if(data_pool_named == 0){
		return 0;
	}
	memset(&(data_pool_named->writer_name[0]), 0, NAME_LEN);
	memset(&(data_pool_named->reader_name[0]), 0, NAME_LEN);
	data_pool_named->write_enable = 1;
	return data_pool_init(&(data_pool_named->data_pool), buffer, buffer_len, data_size);
}

uint8_t data_pool_named_read_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name,
								uint16_t data_serial_num, uint8_t* read_buffer, uint8_t* read_data_len)
{
	if(data_pool_named == 0){
		return 0;
	}

	if((reader_name != 0)&&(data_pool_named_check_reader_name(data_pool_named, reader_name) == 0)){
		return 0;
	}

    return data_pool_read_data(&(data_pool_named->data_pool), data_serial_num, read_buffer, read_data_len);
}

uint8_t data_pool_named_write_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name,
                                uint8_t* write_data, uint8_t write_data_len)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(data_pool_named->write_enable == 0){
		return 0;
	}

	if((writer_name != 0)&&(data_pool_named_check_writer_name(data_pool_named, writer_name) == 0)){
		return 0;
	}

    return data_pool_write_data(&(data_pool_named->data_pool), write_data, write_data_len);
}

uint8_t data_pool_named_del_data(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(data_pool_named->write_enable == 0){
		return 0;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_del_last_data_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_del_last_data_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_del_last_data_lab;
	}
	return 0;

data_pool_named_del_last_data_lab:
    return data_pool_del_data(&(data_pool_named->data_pool));
}

uint8_t data_pool_named_check_empty(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0xff;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_check_data_num_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	return 0xff;

data_pool_named_check_data_num_lab:
    return data_pool_check_empty(&(data_pool_named->data_pool));
}

uint8_t data_pool_named_check_full(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0xff;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_check_data_num_lab;
	}

	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	return 0xff;

data_pool_named_check_data_num_lab:
    return data_pool_check_full(&(data_pool_named->data_pool));
}

uint16_t data_pool_named_check_data_num(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0xffff;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_check_data_num_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_check_data_num_lab;
	}
	return 0xffff;

data_pool_named_check_data_num_lab:
    return data_pool_check_data_num(&(data_pool_named->data_pool));
}

uint8_t data_pool_named_clr_all(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(data_pool_named->write_enable == 0){
		return 0;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_clr_all_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_clr_all_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_clr_all_lab;
	}
	return 0;

data_pool_named_clr_all_lab:
    return data_pool_clr_all(&(data_pool_named->data_pool));
}

uint8_t data_pool_named_write_enable(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_write_enable_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_write_enable_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_write_enable_lab;
	}
	return 0;

data_pool_named_write_enable_lab:
    data_pool_named->write_enable = 1;
    return 1;
}

uint8_t data_pool_named_write_disable(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name, char* reader_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if((reader_name == 0)||(writer_name == 0)){
		goto data_pool_named_write_disable_lab;
	}
	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 1){
		goto data_pool_named_write_disable_lab;
	}
	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 1){
		goto data_pool_named_write_disable_lab;
	}
	return 0;

data_pool_named_write_disable_lab:
    data_pool_named->write_enable = 0;
    return 1;
}

uint8_t data_pool_named_writer_name_register(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(writer_name == 0){
		return 0;
	}

	if(data_pool_named_check_writer_name(data_pool_named, writer_name) == 0){
		return 0;
	}

    memcpy(&(data_pool_named->writer_name[0]), writer_name, NAME_LEN);
    return 1;
}

uint8_t data_pool_named_reader_name_register(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(reader_name == 0){
		return 0;
	}

	if(data_pool_named_check_reader_name(data_pool_named, reader_name) == 0){
		return 0;
	}

    memcpy(&(data_pool_named->reader_name[0]), reader_name, NAME_LEN);
    return 1;
}

uint8_t data_pool_named_writer_name_cancel(DATA_POOL_NAMED_STRUCT* data_pool_named, char* writer_name_to_cancel)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(writer_name_to_cancel == 0){
		return 0;
	}

	if(data_pool_named_check_writer_name(data_pool_named, writer_name_to_cancel) == 0){
		return 0;
	}

	memset(&(data_pool_named->writer_name[0]), 0, NAME_LEN);
    return 1;
}

uint8_t data_pool_named_reader_name_cancel(DATA_POOL_NAMED_STRUCT* data_pool_named, char* reader_name_to_cancel)
{
	if(data_pool_named == 0){
		return 0;
	}

	if(reader_name_to_cancel == 0){
		return 0;
	}

	if(data_pool_named_check_reader_name(data_pool_named, reader_name_to_cancel) == 0){
		return 0;
	}

	memset(&(data_pool_named->reader_name[0]), 0, NAME_LEN);
    return 1;
}



#ifndef STRING_H

#include "type.h"

uint8_t memcmp(const void *str1, const void *str2, uint32_t n);
void memcpy(void *dest, const void *src, uint32_t n);
void memset(void *str, uint8_t c, uint32_t n);
uint32_t strlen(const char *str);

#endif



#include "value_move_circle.h"

void value_move_circle_stop_move(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
	value_move_stop_move(&(value_move_circle->value_move));
	value_move_circle->begin_value = value_move_circle->end_value;
}

void value_move_circle_init_current_value(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_current_value)
{
	value_move_init_current_value(&(value_move_circle->value_move), set_current_value);
}

uint16_t value_move_circle_get_current_value(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
	return (value_move_circle->value_move.current_value);
}

uint8_t value_move_circle_check_active(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
	if((value_move_check_active(&(value_move_circle->value_move)) == 0)
	&&(value_move_circle->begin_value == value_move_circle->end_value)){
		return 0;
	}
	else{
		return 1;
	}
}

//the "current_value" move to the "begin_value" in the first round
//period_ticks: how many ticks will take to move "begin_value" to "end_value"
void value_move_circle_move_by_tick(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_begin_value, uint16_t set_end_value, uint16_t period_ticks)
{
	uint16_t totle_setps;

	value_move_circle_stop_move(value_move_circle);

	if(set_begin_value == set_end_value){
		return;
	}

	if(period_ticks == 0){
		return;
	}

	if(set_begin_value > set_end_value){
		totle_setps = set_begin_value - set_end_value;
	   }
	else{
		totle_setps = set_end_value - set_begin_value;
	}

	compute_interval_value_and_interval_tick(&(value_move_circle->value_move.interval_setps), &(value_move_circle->value_move.interval_ticks),
											totle_setps, period_ticks);
	value_move_circle->value_move.target_value = set_begin_value;
	value_move_circle->value_move.interval_tick_counter = 0;

	value_move_circle->begin_value = set_begin_value;
	value_move_circle->end_value = set_end_value;
}

//the "current_value" move to the "begin_value" in the first round
//set_step_value: how many steps pre tick
void value_move_circle_move_by_step(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_begin_value, uint16_t set_end_value, uint16_t set_step_value)
{
	uint16_t totle_setps;

	value_move_circle_stop_move(value_move_circle);

	if(set_begin_value == set_end_value){
		return;
	}

	if(set_step_value == 0){
		return;
	}

	if(set_begin_value > set_end_value){
		totle_setps = set_begin_value - set_end_value;
	   }
	else{
		totle_setps = set_end_value - set_begin_value;
	}

	if(totle_setps > set_step_value){
		value_move_circle->value_move.interval_setps = set_step_value;
	}
	else{
		value_move_circle->value_move.interval_setps = totle_setps;
	}
	value_move_circle->value_move.interval_ticks = 1;
    value_move_circle->value_move.target_value = set_begin_value;
    value_move_circle->value_move.interval_tick_counter = 0;

	value_move_circle->begin_value = set_begin_value;
	value_move_circle->end_value = set_end_value;
}

void value_move_circle_tick_handler(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{

	if(value_move_circle_check_active(value_move_circle) == 0){
		return;
	}

	if(value_move_check_active(&(value_move_circle->value_move)) == 0){
		if(value_move_circle->value_move.target_value == value_move_circle->begin_value){
			value_move_circle->value_move.target_value = value_move_circle->end_value;
		}
		else{
			value_move_circle->value_move.target_value = value_move_circle->begin_value;
		}
	}

	value_move_tick_handler(&(value_move_circle->value_move));
}


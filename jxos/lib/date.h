#ifndef _DATE_H_
#define _DATE_H_

#include "type.h"

typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t weekday;
} DATE_STRUCT;

void date_print(DATE_STRUCT date);
uint8_t date_init(DATE_STRUCT* date,\
				uint16_t year, uint8_t month, uint8_t day);
uint8_t date_add_one_day(DATE_STRUCT* date);


#endif

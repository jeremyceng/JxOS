
/********************************************************************************
��  ����ringbuff.h
��  ��:    JeremyCeng
��  ��:    V1.0.5
ʱ  ��:    2017-06-20
˵  ��:    ���λ�������
��  ʷ: V1.0.0 2014-11-03:    ����
        V1.0.1 2015-04-28:    ��������С�ɺ궨���Ϊ�ɱ�
                            �������ṹ�������ӻ�������С���� len
                            �������� s8 �͸�Ϊ������ͷ�ļ��к궨��� RINGBUFF_OBG_TYPE ��
        V1.0.2 2015-05-20:    �ڻ������ṹ�������Ӻ���ָ�루��������ʹ��������Ϊ�ࡣ
                            ����������ʼ��������Ϊ����������Ĺ��캯����
                            �޸�����д��ʱ����ָ��Խ���BUG��
        V1.0.3 2015-07-18:    �ڶ�д�����������м���򿪹ر�buf��д��ص��жϹ��ܣ�
                            ���Է�ֹ��д���̱��жϴ�ϣ��Ա���������
        V1.0.4 2016-02-23:    ȥ��ÿ�ζ�д���̶���⻺����������߶�дЧ�ʣ�
                            ȥ���򿪹ر�buf��д��ص��жϹ��ܡ�
        V1.0.5 2017-06-20:    ���ӻ��λ������ĺ궨��汾��
                            ȥ���������ṹ���� free ���ֹ�ڶ�������д�������޸�ͬһԪ�ء�
********************************************************************************/
#ifndef __RINGBUFF_H
#define __RINGBUFF_H

#ifndef NULL
#define NULL                (void*)0
#endif

#define s8    	char
#define s16 	int
#define s32    	long int
#define s64    	long long

#define u8    	unsigned char
#define u16 	unsigned int
#define u32    	long unsigned int
#define u64    	unsigned long long

#define RINGBUFF_ERROR       0
#define RINGBUFF_OK          1

#define RINGBUFF_OBG_TYPE u8

typedef struct _ringbuff_mgr_ RINGBUFF_MGR;
struct _ringbuff_mgr_{
    u16 head;            //0 ~ len-1
    u16 tail;            //0 ~ len-1
    u16 len;            //0 ~ u16 MAX(65535)
    u8 head_out_of_range;
    u8 tail_out_of_range;
    RINGBUFF_OBG_TYPE* buff;
};


u8 ringbuff_check_error(RINGBUFF_MGR *buff);
u16 ringbuff_check_buff_long(RINGBUFF_MGR *buff);
u16 ringbuff_check_free_space(RINGBUFF_MGR *buff);
u16 ringbuff_check_used_space(RINGBUFF_MGR *buff);

RINGBUFF_OBG_TYPE ringbuff_check_haed_obj(RINGBUFF_MGR *buff);
RINGBUFF_OBG_TYPE ringbuff_check_tail_obj(RINGBUFF_MGR *buff);

u8 ringbuff_write(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE dat);
u8 ringbuff_read(RINGBUFF_MGR *buff, RINGBUFF_OBG_TYPE* dat);

u8 ringbuff_write_data(RINGBUFF_MGR *buff, u16 data_len , RINGBUFF_OBG_TYPE* in_data);
u8 ringbuff_read_data(RINGBUFF_MGR* buff, u16 data_len, RINGBUFF_OBG_TYPE* out_data);

u8 string_in_buff(RINGBUFF_MGR* buff, s8* in_str);
u8 string_out_buff(RINGBUFF_MGR* buff, s8* out_str);

u8 RingBuff(RINGBUFF_MGR *buff, u16 buff_long_init, RINGBUFF_OBG_TYPE* buff_init);
void ringbuff_reset(RINGBUFF_MGR *buff);

#endif



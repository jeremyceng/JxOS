
/*
typedef struct {
	uint32_t port;
	uint32_t pin;
} ROTARY_ENCODER_GPIO_DEF_STRUCT;

*/
#include "../lib/type.h"
#include "../hal/hal_gpio.h"

//PC10 -> POWER
//PC11 -> A
//PC12 -> B

void bsp_rotary_encoder_hal_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_SetBits(GPIOC, GPIO_Pin_10);
}

uint8_t bsp_rotary_encoder_hal_read_pin_a_level(void)
{
	return GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_11);
}

uint8_t bsp_rotary_encoder_hal_read_pin_b_level(void)
{
	return GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12);
}

void bsp_rotary_encoder_delay_ms(uint16_t num)
{
}


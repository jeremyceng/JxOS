
#include <bsp_ht1621_config.h>

//#define ht1621_cs_pin_hight()	PIN_LCD_CS = 1
//#define ht1621_cs_pin_low()		PIN_LCD_CS = 0
//
//#define ht1621_wr_pin_hight()	PIN_LCD_WR = 1
//#define ht1621_wr_pin_low()		PIN_LCD_WR = 0
//
////#define ht1621_rd_pin_hight()
////#define ht1621_rd_pin_low()
//
//#define ht1621_data_pin_hight()	PIN_LCD_DATA = 1
//#define ht1621_data_pin_low()	PIN_LCD_DATA = 0
////#define ht1621_data_read()
//
//void delay_10us(volatile uint32_t nCount); //#define ht1621_delay_10us	delay_10us

void bsp_ht1621_pin_init(void){}

void bsp_ht1621_cs_pin_hight(void){}
void bsp_ht1621_cs_pin_low(void){}

void bsp_ht1621_wr_pin_hight(void){}
void bsp_ht1621_wr_pin_low(void){}

void bsp_ht1621_rd_pin_hight(void){}
void bsp_ht1621_rd_pin_low(void){}

void bsp_ht1621_data_pin_hight(void){}
void bsp_ht1621_data_pin_low(void){}
uint8_t bsp_ht1621_data_pin_rec(void){}

void bsp_ht1621_delay_10us(volatile uint32_t n){}

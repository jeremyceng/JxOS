
#ifndef __BSP_KEY_BASE_ON_ADC_CONFIG__H
#define __BSP_KEY_BASE_ON_ADC_CONFIG__H

#include "../lib/type.h"

#define BSP_KEY_BASE_ON_ADC_FILT_VOLTAGE_MV	100
#define BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN 2

#define BSP_KEY_BASE_ON_ADC_LONG_PRESS_COUNT 		50

//单位 MV
#define KEY_NA_VOL      2000
#define KEY_0_VOL       0
#define KEY_1_VOL       344
#define KEY_2_VOL       677
#define KEY_3_VOL       1015
#define KEY_4_VOL       1376
#define KEY_5_VOL       1739
#define KEY_VOL_ERR     100

void bsp_key_base_on_adc_hal_init(void);
int32_t bsp_key_base_on_adc_hal_read_voltage_mv(void);

#endif




#ifndef __ROTARY_ENCODER_CONGIF_H
#define __ROTARY_ENCODER_CONGIF_H

#include "../lib/type.h"

#define ROTARY_ENCODER_NUM_MAX 		        1


void bsp_rotary_encoder_hal_init(void);
uint8_t bsp_rotary_encoder_hal_read_pin_a_level(uint32_t num);
uint8_t bsp_rotary_encoder_hal_read_pin_b_level(uint32_t num);

void bsp_rotary_encoder_delay_ms(uint16_t num);

#endif



#include <bsp_key_base_on_adc_config.h>
#include "../hal/hal_adc.h"
#include "../hal/hal_gpio.h"

void bsp_key_base_on_adc_hal_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    ADC_InitStructure.ADC_Mode = 0;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_Init(ADC0 ,&ADC_InitStructure);

	ADC_Cmd(ADC0,ENABLE);

    ADC_StartCalibration(ADC0);
	while(ADC_GetCalibrationStatus(ADC0));

	ADC_SoftwareStartConvCmd(ADC0, ENABLE);
}

int32_t bsp_key_base_on_adc_hal_read_voltage_mv(void)
{
    uint16_t AdcValue;

	while(ADC_GetSoftwareStartConvStatus(ADC0));
	AdcValue = ADC_GetConversionValue(ADC0);
//	AdcValue = ConvertValueToVolts(AdcValue)

    return (int32_t)AdcValue;
}



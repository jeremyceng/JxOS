
#ifndef __MOD_LIGHT_BH1750_H
#define __MOD_LIGHT_BH1750_H

#include "stdint.h"

void bh1750_hal_init(void);
uint8_t	bh1750_hal_i2c_write_device(uint8_t addr, uint8_t *data, uint8_t len);
uint8_t	bh1750_hal_i2c_read_device(uint8_t addr, uint8_t *data, uint8_t len);

#endif


#ifndef _HT1621_CONFIG_H_
#define _HT1621_CONFIG_H_

#include "../lib/type.h"

//BIAS 1/2 0b100 0010ab00 X 
//LCD 1/2偏压选项
//ab = 00 -> 2 个公共口
//ab = 01 -> 3 个公共口
//ab = 10 -> 4 个公共口
//BIAS 1/3 0b100 0010ab01 X 
//LCD 1/3偏压选项
//ab = 00 -> 2 个公共口
//ab = 01 -> 3 个公共口
//ab = 10 -> 4 个公共口

//TNORMAL 10011100011X C 普通模式
#define BIAS_D12_B13   		0x28
#define BIAS_D13_B14   		0x29		//0b100 00101001 X  1/3duty 4com
#define SYS_DIS 			0X00		//0b100 00000000 X  关振系统荡器和LCD偏压发生器
#define SYS_EN  			0x01		//0b100 00000001 X 	打开系统振荡器
#define LCD_OFF 			0x02		//0b100 00000010 X  关LCD偏压
#define LCD_ON  			0x03		//0b100 00000011 X  打开LCD偏压
#define WDT_DIS  			0X05		//0b100 00000101 X  禁止看门狗
#define TONE_OFF  			0X08		//0b100 00001000 X 关闭声音输出
#define TONE_ON 			0X09		//0b100 00001001 X  打开声音输出
#define XTAL_32k   			0x14		//0b100 00010100 X 外部接时钟
#define RC_256k  			0x18		//0b100 00011000 X  内部时钟

void bsp_ht1621_pin_init(void);

void bsp_ht1621_cs_pin_hight(void);
void bsp_ht1621_cs_pin_low(void);

void bsp_ht1621_wr_pin_hight(void);
void bsp_ht1621_wr_pin_low(void);

void bsp_ht1621_rd_pin_hight(void);
void bsp_ht1621_rd_pin_low(void);

void bsp_ht1621_data_pin_hight(void);
void bsp_ht1621_data_pin_low(void);
uint8_t bsp_ht1621_data_pin_rec(void);

void bsp_ht1621_delay_10us(volatile uint32_t n);

#endif











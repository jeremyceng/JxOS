
#ifndef _APP_LAYER_CONFIG_H
#define _APP_LAYER_CONFIG_H

#include "application_layer_service.h"

/*
帧结构:
service serila mun		：此帧在 master 端所有运行的 service 列表中的序号（端口号）
3 bit: 111XXXXX (0~7)

service id				：此 service 的 id （服务的类型）
5 bit: XXX11111 (0~37)

service cmd				：此 service 的 cmd （服务的命令）
4 bit: 1111XXXX (0~15)

service cmd param len	：此 cmd 的 参数的长度 (字节)
4 bit: XXXX1111 (0~15)

service cmd param		：此 cmd 的 参数
n byte (Big-Endian)
*/

#define BUNDLING_TABLE_MAX						10

//SERVICE_CONFIG_TABLE < 0XF0
#define SERVICE_CONFIG_TABLE_INIT \
	{SERVICE_ONOFF_ID,	SERVICE_SLAVE} \

#define SERVICE_ONOFF_MASTER_ENABLE 		0
#define SERVICE_ONOFF_SLAVE_ENABLE 			1

#define SERVICE_TEMPERATURE_MASTER_ENABLE 	0
#define SERVICE_TEMPERATURE_SLAVE_ENABLE 	0

#endif

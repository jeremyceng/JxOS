#ifndef __std_app_LED_BLINK_TASK_H
#define __std_app_LED_BLINK_TASK_H

#include "type.h"

void std_app_led_blink_task_init(void);
void std_app_led_blink_task(void);

#endif

#include "jxos_public.h"

#if ((JXOS_STD_APP_BUTTON_SCAN_TASK_ENABLE == 1)&&(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1))

#include "driver/button.h"

static swtime_type button_scan_swt;
extern void std_app_button_scan_init_callback(void);

#if (BUTTON_SCAN_LOW_POWER_CONSUMPTION_ENABLE == 1)
static JXOS_EVENT_HANDLE button_press_event;
void std_app_button_task_button_press_interrupt_handler(void)
{
	jxos_event_set(button_press_event);
}
#endif

#if (JXOS_Compiler_optimization_1 == 1)
	#if (JXOS_TASK_ENABLE == 1)
	static void button_scan_task(void)
	#else
	void std_app_button_scan_task(void)
	#endif
#else
static void button_scan_task(uint8_t task_id, void * parameter)
#endif
{
#if (BUTTON_SCAN_LOW_POWER_CONSUMPTION_ENABLE == 1)
	if(jxos_event_wait(button_press_event) == 1){
		sys_svc_software_timer_restart(button_scan_swt);
		button_scan_handler();
		if(button_all_release() == 1){
			sys_svc_software_timer_stop(button_scan_swt);
		}
	}
#endif

	if(sys_svc_software_timer_check_overtime(button_scan_swt) == 1){
		button_scan_handler();
#if (BUTTON_SCAN_LOW_POWER_CONSUMPTION_ENABLE == 1)
		if(button_all_release() == 1){
			sys_svc_software_timer_stop(button_scan_swt);
		}
#endif
	}
}

/******************************/
void std_app_button_scan_task_init(void)
{
	button_init();
	std_app_button_scan_init_callback();

#if (JXOS_TASK_ENABLE == 1)
	jxos_task_create(button_scan_task, "bt_scan", 0);
#endif

	button_scan_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(button_scan_swt, BUTTON_SCAN_SCAN_TICK_TIME_MS);
#if (BUTTON_SCAN_LOW_POWER_CONSUMPTION_ENABLE != 1)
		sys_svc_software_timer_restart(button_scan_swt);
#endif
}

#endif


//#if (JXOS_MSG_ENABLE == 1)&&(BUTTON_SCAN_MSG_BUFF_LEN != 0)
//static uint8_t button_msg_buff[BUTTON_SCAN_MSG_BUFF_LEN*sizeof(STD_APP_BUTTON_SCAN_MSG_STRUCT)];
//JXOS_MSG_HANDLE std_app_button_scan_msg;
//#else
//uint8_t std_app_button_scan_msg_button_num;
//uint8_t std_app_button_scan_msg_button_event;
//#endif


//static void send_button_msg(uint8_t button_num, uint8_t button_event)
//{
//#if (JXOS_MSG_ENABLE == 1)&&(BUTTON_SCAN_MSG_BUFF_LEN != 0)
//	STD_APP_BUTTON_SCAN_MSG_STRUCT msg_itme;
//	msg_itme.button_num = button_num;
//	msg_itme.button_event = button_event;
//	jxos_msg_send(std_app_button_scan_msg, &msg_itme);
//#else
//	std_app_button_scan_msg_button_num = button_num;
//	std_app_button_scan_msg_button_event = button_event;
//#endif
//}

//void std_app_button_scan_task_button_press_event_callback(uint8_t button_num)
//{
//	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_PRESS);
//}

//void std_app_button_scan_task_button_release_event_callback(uint8_t button_num)
//{
//	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_RELEASE);
//}

//void std_app_button_scan_task_button_long_press_event_callback(uint8_t button_num)
//{
//	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS);
//}

//void std_app_button_scan_task_button_long_press_repeat_event_callback(uint8_t button_num)
//{
//	send_button_msg(button_num, STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS_REPEAT);
//}

//#if (JXOS_MSG_ENABLE == 1)&&(BUTTON_SCAN_MSG_BUFF_LEN != 0)
//    std_app_button_scan_msg = jxos_msg_create(button_msg_buff, BUTTON_SCAN_MSG_BUFF_LEN*sizeof(STD_APP_BUTTON_SCAN_MSG_STRUCT),
//											sizeof(STD_APP_BUTTON_SCAN_MSG_STRUCT), "std_app_bt_scan");
//#else
//	std_app_button_scan_msg_button_num = 0xff;
//	std_app_button_scan_msg_button_event = 0xff;
//#endif
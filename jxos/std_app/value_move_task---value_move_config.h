

#include "jxos_config.h"

#define VALUE_MOVE_NUM_MAX 		VALUE_MOVE_TASK_VALUE_MOVE_NUM_MAX

extern void std_app_value_move_value_changed_event_callback(uint8_t value_move_num, uint16_t changed_value);

#define value_move_value_changed_event(value_move_num, changed_value)    std_app_value_move_value_changed_event_callback(value_move_num, changed_value);

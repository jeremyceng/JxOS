#include "jxos_public.h"

#if ((JXOS_STD_APP_LED_BLINK_TASK_ENABLE == 1)&&(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1))

#include "driver/blink.h"

static swtime_type led_blink_swt;
extern void std_app_led_blink_init_callback(void);

#if (JXOS_Compiler_optimization_1 == 1)
	#if (JXOS_TASK_ENABLE == 1)
	static void led_blink_task(void)
	#else
	void std_app_led_blink_task(void)
	#endif
#else
static void led_blink_task(uint8_t task_id, void * parameter)
#endif
{
	if(sys_svc_software_timer_check_overtime(led_blink_swt) == 1){
		blink_handler();
	}
}

void std_app_led_blink_task_init(void)
{
	blink_init();

	std_app_led_blink_init_callback();

#if (JXOS_TASK_ENABLE == 1)
	jxos_task_create(led_blink_task, "led_blk", 0);
#endif

	led_blink_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(led_blink_swt, LED_BLINK_BLINK_TICK_TIME_MS);
	sys_svc_software_timer_restart(led_blink_swt);
}

/******************************************************/
void std_app_led_blink_off(uint8_t led_id)
{
	blink_off(led_id);
}

void std_app_led_blink_on(uint8_t led_id)
{
	blink_on(led_id);
}

void std_app_led_blink_set(uint8_t led_id,
				uint16_t set_period, uint16_t set_duty_ration)
{
	blink_set(led_id, set_period, set_duty_ration);
}

//blink_times = 0xff blink forever
void std_app_led_blink_start(uint8_t led_id,
				uint8_t set_blink_times, uint8_t set_finish_state)
{
	blink_start(led_id, set_blink_times, set_finish_state);
}

void std_app_led_blink_stop(uint8_t led_id)
{
	blink_stop(led_id);
}

#endif


//#if (JXOS_MSG_ENABLE == 1)&&(LED_BLINK_MSG_BUFF_LEN != 0)
//static uint8_t led_msg_buff[LED_BLINK_MSG_BUFF_LEN*sizeof(STD_APP_LED_BLINK_MSG_STRUCT)];
//JXOS_MSG_HANDLE std_app_led_blink_msg;
//#else
//	uint8_t std_app_led_blink_msg_led_num;
//	uint8_t std_app_led_blink_msg_led_cmd;
//	uint16_t std_app_led_blink_msg_blink_period;
//	uint16_t std_app_led_blink_msg_blink_duty;
//	uint8_t std_app_led_blink_msg_blink_times;
//	uint8_t std_app_led_blink_msg_blink_finish_state;
//#endif

//#if (JXOS_MSG_ENABLE == 1)&&(LED_BLINK_MSG_BUFF_LEN != 0)
//	STD_APP_LED_BLINK_MSG_STRUCT temp_msg_item;
//	if(jxos_msg_receive(std_app_led_blink_msg, &temp_msg_item)){
//		if(temp_msg_item.led_id < LED_BLINK_LED_NUM_MAX){
//			switch (temp_msg_item.led_cmd)
//			{
//			case STD_APP_LED_BLINK_CMD_OFF:
//				blink_off(temp_msg_item.led_id);
//				break;
//			case STD_APP_LED_BLINK_CMD_ON:
//				blink_on(temp_msg_item.led_id);
//				break;
//			case STD_APP_LED_BLINK_CMD_BLINK:
//				if(!blink_is_blink(temp_msg_item.led_id)){
//					blink_start(temp_msg_item.led_id, temp_msg_item.blink_period/LED_BLINK_BLINK_TICK_TIME_MS,
//								temp_msg_item.blink_duty/LED_BLINK_BLINK_TICK_TIME_MS, temp_msg_item.blink_times,
//								temp_msg_item.blink_finish_state);
//				}
//				break;
//			case STD_APP_LED_BLINK_CMD_STOP:
//				blink_stop(temp_msg_item.led_id);
//				break;
//			default:
//				break;
//			}
//		}
//	}
//#else
//	if(std_app_led_blink_msg_led_num < LED_BLINK_LED_NUM_MAX){
//		switch (std_app_led_blink_msg_led_cmd)
//		{
//		case STD_APP_LED_BLINK_CMD_OFF:
//			blink_off(std_app_led_blink_msg_led_num);
//			break;
//		case STD_APP_LED_BLINK_CMD_ON:
//			blink_on(std_app_led_blink_msg_led_num);
//			break;
//		case STD_APP_LED_BLINK_CMD_BLINK:
//			blink_start(std_app_led_blink_msg_led_num, std_app_led_blink_msg_blink_period/LED_BLINK_BLINK_TICK_TIME_MS,
//						std_app_led_blink_msg_blink_duty/LED_BLINK_BLINK_TICK_TIME_MS, std_app_led_blink_msg_blink_times,
//						std_app_led_blink_msg_blink_finish_state);
//			break;
//		case STD_APP_LED_BLINK_CMD_STOP:
//			blink_stop(std_app_led_blink_msg_led_num);
//			break;
//		default:
//			break;
//		}
//		std_app_led_blink_msg_led_num = 0xff;
//	}
//#endif

//#define STD_APP_LED_BLINK_CMD_OFF				0
//#define STD_APP_LED_BLINK_CMD_ON				1
//#define STD_APP_LED_BLINK_CMD_BLINK				2
//#define STD_APP_LED_BLINK_CMD_STOP				4

//#define STD_APP_LED_BLINK_CMD_FINISH_STATE_OFF	0
//#define STD_APP_LED_BLINK_CMD_FINISH_STATE_ON	1
//	#if (JXOS_MSG_ENABLE == 1)
//	typedef struct {
//		uint8_t led_id;
//		uint8_t led_cmd;
//		uint16_t blink_period;		//unit: 1ms
//		uint16_t blink_duty;		//unit: 1ms
//		uint8_t blink_times;
//		uint8_t blink_finish_state;
//	} STD_APP_LED_BLINK_MSG_STRUCT;
//	extern JXOS_MSG_HANDLE std_app_led_blink_msg;
//	//output msg name: "std_app_led_blk"
//	#else
//	extern uint8_t std_app_led_blink_msg_led_num;
//	extern uint8_t std_app_led_blink_msg_led_cmd;
//	extern uint16_t std_app_led_blink_msg_blink_period;	//unit: 1ms
//	extern uint16_t std_app_led_blink_msg_blink_duty;	//unit: 1ms
//	extern uint8_t std_app_led_blink_msg_blink_times;
//	extern uint8_t std_app_led_blink_msg_blink_finish_state;
//	#endif
//#endif

//#if (JXOS_MSG_ENABLE == 1)&&(LED_BLINK_MSG_BUFF_LEN != 0)
//	std_app_led_blink_msg = jxos_msg_create(led_msg_buff, LED_BLINK_MSG_BUFF_LEN*sizeof(STD_APP_LED_BLINK_MSG_STRUCT),
//											sizeof(STD_APP_LED_BLINK_MSG_STRUCT), "std_app_led_blk");
//#else
//	std_app_led_blink_msg_led_num = 0xff;
//	std_app_led_blink_msg_led_cmd = 0xff;
//	std_app_led_blink_msg_blink_period = 0xff;
//	std_app_led_blink_msg_blink_duty = 0xff;
//	std_app_led_blink_msg_blink_times = 0xff;
//	std_app_led_blink_msg_blink_finish_state = 0xff;
//#endif
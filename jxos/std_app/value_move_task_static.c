
#include "jxos_public.h"

#if ((JXOS_STD_APP_VALUE_MOVE_TASK_ENABLE == 1)&&(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1))
#include "driver/value_move.h"

#if (VALUE_MOVE_TICK_TIME_MS == 0)
#error "VALUE_MOVE_TICK_TIME_MS can t be 0"
#endif

static swtime_type value_move_swt;
extern void std_app_value_move_init_callback(void);

#if (JXOS_Compiler_optimization_1 == 1)
#if (JXOS_TASK_ENABLE == 1)
static void value_move_task(void)
#else
void std_app_value_move_task(void)
#endif
#else
static void value_move_task(uint8_t task_id, void * parameter)
#endif
{
	if(sys_svc_software_timer_check_overtime(value_move_swt) == 1){
		value_move_tick_handler();
	}
}

void std_app_value_move_task_init(void)
{
	value_move_init();

	std_app_value_move_init_callback();

#if (JXOS_TASK_ENABLE == 1)
	jxos_task_create(value_move_task, "v_move", 0);
#endif

	value_move_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(value_move_swt, VALUE_MOVE_TICK_TIME_MS);
	sys_svc_software_timer_restart(value_move_swt);

}

/******************************************************/
void std_app_value_move_set_current_value(uint8_t value_id, uint16_t current_value)
{
	value_move_init_current_value(value_id, current_value);
}

void std_app_value_move_move_by_time(uint8_t value_id, uint16_t target_val, uint16_t move_time_ms)
{
	value_move_to_value_by_tick(value_id, target_val, move_time_ms/VALUE_MOVE_TICK_TIME_MS);
}

void std_app_value_move_move_by_rate(uint8_t value_id, uint16_t target_val, uint16_t rate)
{
	value_move_to_value_by_rate(value_id, target_val, rate);
}

////move to the "max_val" in the first round
//void std_app_value_move_task_circle_move(uint8_t value_id, uint16_t max_val, uint16_t min_val, uint16_t circle_time_ms)
//{
//	value_move_circle_move_by_tick(value_id, max_val, min_val, circle_time_ms/VALUE_MOVE_TICK_TIME_MS);
//}

////move to the "max_val" in the first round
//void std_app_value_move_task_circle_move_by_step(uint8_t value_id, uint16_t max_val, uint16_t min_val, uint16_t steps)
//{
//	value_move_circle_move_by_step(value_id, max_val, min_val, steps);
//}

void std_app_value_move_stop(uint8_t value_id)
{
	value_move_stop_move(value_id);
}

#endif


//#if (JXOS_MSG_ENABLE == 1)&&(VALUE_MOVE_TASK_MSG_BUFF_LEN != 0)
//static uint8_t value_move_msg_buff[VALUE_MOVE_TASK_MSG_BUFF_LEN*sizeof(STD_APP_VALUE_MOVE_MSG_STRUCT)];
//JXOS_MSG_HANDLE std_app_value_move_msg;
//#else
//	uint8_t std_app_value_move_msg_value_move_num;
//	uint16_t std_app_value_move_msg_value_move_cmd;
//	uint16_t std_app_value_move_msg_target_value;
//	uint16_t std_app_value_move_msg_current_value;
//	uint16_t std_app_value_move_msg_move_ticks;
//	uint16_t std_app_value_move_msg_rate;
//#endif

//#if (JXOS_MSG_ENABLE == 1)&&(VALUE_MOVE_TASK_MSG_BUFF_LEN != 0)
//#else
//	if(std_app_value_move_msg_value_move_num < VALUE_MOVE_TASK_VALUE_MOVE_NUM_MAX){
//		switch (std_app_value_move_msg_value_move_cmd)
//		{
//		case STD_APP_VALUE_MOVE_CMD_STOP:
//			value_move_stop_move(std_app_value_move_msg_value_move_num);
//			break;
//		case STD_APP_VALUE_MOVE_CMD_INIT_CURRENT_VAL:
//			value_move_init_current_value(std_app_value_move_msg_value_move_num,
//										std_app_value_move_msg_current_value);
//			break;
//		case STD_APP_VALUE_MOVE_CMD_INIT_MOVE_TICK:
//			value_move_to_value_by_tick(std_app_value_move_msg_value_move_num,
//										std_app_value_move_msg_target_value,
//										std_app_value_move_msg_move_ticks);
//			break;
//		case STD_APP_VALUE_MOVE_CMD_INIT_MOVE_RATE:
//			value_move_to_value_by_rate(std_app_value_move_msg_value_move_num,
//										std_app_value_move_msg_target_value,
//										std_app_value_move_msg_rate);
//			break;
//		default:
//			break;
//		}
//		std_app_value_move_msg_value_move_cmd = 0xff;
//	}
//#endif

//#if (JXOS_MSG_ENABLE == 1)&&(VALUE_MOVE_TASK_MSG_BUFF_LEN != 0)
//	std_app_value_move_msg = jxos_msg_create(value_move_msg_buff, VALUE_MOVE_TASK_MSG_BUFF_LEN*sizeof(STD_APP_VALUE_MOVE_MSG_STRUCT),
//											sizeof(STD_APP_VALUE_MOVE_MSG_STRUCT), "std_app_v_move");
//#else
//	std_app_value_move_msg_value_move_num = 0xff;
//	std_app_value_move_msg_value_move_cmd = 0xff;
//	std_app_value_move_msg_target_value = 0xff;
//	std_app_value_move_msg_current_value = 0xff;
//	std_app_value_move_msg_move_ticks = 0xff;
//	std_app_value_move_msg_rate = 0xff;
//#endif

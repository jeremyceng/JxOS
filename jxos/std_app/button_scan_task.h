
#ifndef __std_app_BUTTON_SCAN_TASK_H
#define __std_app_BUTTON_SCAN_TASK_H

#include "type.h"

void std_app_button_scan_task_init(void);
void std_app_button_scan_task(void);

#endif

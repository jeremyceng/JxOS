
// void jxos_hal_init(void) （需要在系统运行前初始化的硬件）
// void jxos_user_task_init(void) （用户任务初始化）
// void jxos_prepare_to_run(void) （需要在系统即将运行前执行的动作）
// 如果 jxos_config.h 配置文件中不启用 task 功能 （JXOS_TASK_ENABLE == 0），
// 则还需要在 jxos_init_config.c 文件中实现 void jxos_user_task_run(void) （用户任务的执行函数）

//os
extern void jxos_user_init_callback(void);
extern void jxos_user_task_init_callback(void);
extern void jxos_prepare_to_run_callback(void);
extern void jxos_user_task_run_callback(void);

//sys_service
extern void sys_svc_software_timer_init_callback(void);

extern void (*sys_power_mgr_task_hal_init_callback)(void);
extern void (*sys_power_mgr_task_prepare_to_sleep_callback)(void);
extern void (*sys_power_mgr_task_go_to_sleep_callback)(void);
extern void (*sys_power_mgr_task_recover_to_wake_callback)(void);

//std_app
extern void std_app_led_blink_init_callback(void);
extern void std_app_led_blink_blink_on_callback(uint8_t blink_num);
extern void std_app_led_blink_blink_off_callback(uint8_t blink_num);

extern void std_app_button_scan_init_callback(void);
extern void std_app_button_scan_get_pin_level_callback(uint8_t button_num);
extern void std_app_button_scan_task_button_press_event_callback(uint8_t button_num);
extern void std_app_button_scan_task_button_release_event_callback(uint8_t button_num);
extern void std_app_button_scan_task_button_long_press_event_callback(uint8_t button_num);
extern void std_app_button_scan_task_button_long_press_repeat_event_callback(uint8_t button_num);

extern void std_app_value_move_init_callback(void);
extern void std_app_value_move_value_changed_event_callback(uint8_t value_move_num, uint16_t changed_value);




#define uint16_t    unsigned int
#define uint8_t    unsigned char

#define BATT_TYPE_LI_ION    0


#define BATT_TYPE   BATT_TYPE_LI_ION

#if(BATT_TYPE == BATT_TYPE_LI_ION)
uint16_t batt_voltage_percent_table[21] = {
    330,
    355,
    368,
    376,
    379,
    380,
    381,
    382,
    383,
    384,
    385,
    387,
    389,
    391,
    393,
    396,
    399,
    402,
    406,
    412,
    418
};
#endif

uint8_t batt_voltage_to_percent(uint16_t voltage)
{
    uint8_t percent = 0;
    uint8_t table_len = sizeof(batt_voltage_percent_table)/sizeof(uint16_t);
    uint8_t i = 0;

    uint16_t temp_1 = 0;
    uint16_t temp_2 = 0;
    uint16_t temp_3 = 0;

    for(; i <= table_len; i++){
        if(voltage <= batt_voltage_percent_table[i]){
            break;
        }
    }

    if(i == 0){
        percent = 0;
    }
    else if(i < table_len){

//        temp_1 = voltage - batt_voltage_percent_table[i-1];
//        temp_2 = batt_voltage_percent_table[i] - batt_voltage_percent_table[i-1];
//        temp_3 = temp_1*5/temp_2;
//        percent = (100/(table_len-1))*(i-1) + temp_3;
//        printf("temp_1 %d\r\n",temp_1);
//        printf("temp_2 %d\r\n",temp_2);
//        printf("temp_3 %d\r\n",temp_3);

        percent = (100/(table_len-1))*(i-1) +\
        ((voltage - batt_voltage_percent_table[i-1])*5)/(batt_voltage_percent_table[i] - batt_voltage_percent_table[i-1]);
    }
    else{
        percent = 100;
    }

    return percent;
}

uint8_t batt_percent_level(uint8_t percent, uint8_t level_max)
{
    uint8_t level = 0;
    level = (percent*level_max)/100;
}

void batt_level_dis(uint8_t level)
{
    printf("batt_level_dis:%d\r\n", level);
}

uint8_t batt_level = 0; //0~4;

static uint8_t dis_p = 0;
void batt_chage_show_init(void)
{
    dis_p = batt_level;
}

void batt_chage_show(void)
{
    if(dis_p > 4){
        dis_p = batt_level;
    }
    batt_level_dis(dis_p);
    dis_p++;
}
//充电后量背光or灭背光？？
//亮两米后熄灭


int main()
{
    int a = 0x1234;
    uint8_t percent = 0;
//    printf("%x\r\n",a/256);
//
//    printf("%x\r\n",a%256);
//
//    printf("%x\r\n",a>>8);
//
//    printf("%x\r\n",a&0xff);

//    printf("p %d\r\n", batt_voltage_to_percent(334));

//    for(a = 330; a <= 420; a++){
//        percent = batt_voltage_to_percent(a);
//        printf("v:%d  p:%d l:%d\r\n", a, percent, batt_percent_level(percent, 4)+1);
//    }

    if(/*开始充电*/){
        batt_chage_show_init();
        bl_on();
        bl_count = 0;
        while(1){
            if(/*250ms*/){
                batt_level = batt_level_get();
                batt_chage_show();

                if(bl_count < 8){
                    bl_count++;
                    if(bl_count == 8){
                        bl_off();
                    }
                }
            }
            if(/*充电结束*/){
                break;
            }
        }
        batt_level_dis(batt_level);
    }


    return    0;
}

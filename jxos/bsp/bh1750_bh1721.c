
#include <bh1750_bh1721_config.h>

#define LIGHT_USED_BH1750 1
#ifdef LIGHT_USED_BH1750
#define ZW_BH1750_APP 1

#ifdef   ZW_BH1750_APP
#define ZW_DEBUG_BH1750_SEND_BYTE(data) ZW_DEBUG_SEND_BYTE(data)
#define ZW_DEBUG_BH1750_SEND_STR(STR) ZW_DEBUG_SEND_STR(STR)
#define ZW_DEBUG_BH1750_SEND_NUM(data)  ZW_DEBUG_SEND_NUM(data)
#define ZW_DEBUG_BH1750_SEND_WORD_NUM(data) ZW_DEBUG_SEND_WORD_NUM(data)
#define ZW_DEBUG_BH1750_SEND_NL()  ZW_DEBUG_SEND_NL()
#else
#define ZW_DEBUG_SENSORPIR_SEND_BYTE(data)
#define ZW_DEBUG_SENSORPIR_SEND_STR(STR)
#define ZW_DEBUG_SENSORPIR_SEND_NUM(data)
#define ZW_DEBUG_SENSORPIR_SEND_WORD_NUM(data)
#define ZW_DEBUG_SENSORPIR_SEND_NL()
#endif

#define	  _1750_I2C_ADDR   0x23


//-CMD-----------------------------------------------------
#define _1750_CMD_POWER_OFF    (0x00)
#define _1750_CMD_POWER_ON     (0x01)
#define _1750_CMD_POWER_RESET  (0x03)
//------------------------------------
#define _1750_CMD_H_MUL        (0x10)
#define _1750_CMD_H_MUL_2      (0x11)
#define _1750_CMD_L_MUL        (0x13)
//------------------------------------
#define _1750_CMD_H_ONCE       (0x20)
#define _1750_CMD_H_ONCE_2     (0x21)
#define _1750_CMD_L_ONCE       (0x23)
//------------------------------------
#define _1750_CMD_SET_T_H      (0x40) // 01000 xxx
#define _1750_CMD_SET_T_L      (0x60) // 011 xxxxx


//------DPI:--------------------------------------
#define _1750_H_DPI  (1) 
#define _1750_H2_DPI (0.5) 
#define _1750_L_DPI  (4) 

//-----measuring time-----------------------------------
#define _1750_H_MEASURING_MS  (120) 
#define _1750_H2_MEASURING_MS (120) 
#define _1750_L_MEASURING_MS  (16) 
//------------------------------------
#define _1750_H_MEASURING_MAX_MS  (180) 
#define _1750_H2_MEASURING_MAX_MS (180) 
#define _1750_L_MEASURING_MAX_MS  (24) 


//==============================================================

//static uint16_t _lightLux = 0;
//
//static uint8_t  LightCount=0x00;
//
//uint8_t    LX_State=0x00;
//
//typedef struct _TEMP_HUMI
//{
//  uint8_t  measureFlag;
//  uint8_t  lightiState;
//}TEMP_LIGHT;
//
//
//
//TEMP_LIGHT   Temp_Light={0x00,0x00};
//
//enum
//{
//  LIGHT_IDLE,
//  LIGHT_START,
//  LIGHT_CONVERSION,
//  LIGHT_END
//}TEMP_LIGTH;
//
//enum
//{
//   meaIdle, 
//   meaStart,
//   meaReady,
//   meaCovent,
//   meaSucced,
//   meaFail
//}TEMP__LIGTH_STATE;
//
//static uint8_t LightMeaState=LIGHT_IDLE;

//==============================================================

static void _1750_TxCmd( uint8_t command )
{
	uint8_t cmd = command;
	bh1750_hal_i2c_write_device(_1750_I2C_ADDR, &cmd, 1);
//	halI2cWriteBytes(_1750_I2C_ADDR<<1, &cmd, 1);
}

//----------------------------------------------------------
static void _1750_init( void )
{
	_1750_TxCmd( _1750_CMD_POWER_ON );
}


static void _1750_powerOn( void )
{
	_1750_TxCmd( _1750_CMD_POWER_ON );
}

static void _1750_powerOff( void )
{
	_1750_TxCmd( _1750_CMD_POWER_OFF );
}

//----------------------------------------------------------
void bh1750_init(void)
{
	bh1750_hal_init();
}

void bh1750_start_convert( void ) //wait 500ms to read data after start convert
{
	_1750_powerOn();
	_1750_TxCmd( _1750_CMD_H_ONCE ); 
}

uint16_t bh1750_read_light_lux( void )
{
/*************************************************************
    Single_Write_BH1750(0x01);   // power on
    Single_Write_BH1750(0x10);   // H- resolution mode

     delay_nms(180);

    Multiple_Read_BH1750();       //read data to buf

    dis_data=BUF[0];
    dis_data=(dis_data<<8)+BUF[1];
    
    temp=(float)dis_data/1.2;
*************************************************************/	

	uint8_t buf[2] = {0};
	uint32_t value = 0;
	uint8_t temp;
	
	bh1750_hal_i2c_read_device(_1750_I2C_ADDR, buf, 2);
	value += buf[0];
	value <<= 8;
	value += buf[1];

//	halI2cReadBytes(_1750_I2C_ADDR<<1, buf, 2);
//   value= (uint32_t)((uint32_t)buf[0] << 8 ) + buf[1];
	// start-----Divided by 1.2, half adjust
	/***
	value *= 12;
	temp = 	value % 10;
	value /= 10;
	if( value > 0 )
	{
		if(temp > 4)
			value++;
	}
	_1750_powerOff();
//	_lightLux = (uint16_t) value;
	***/
	return (uint16_t)value;
}

//uint16_t modLightValGet( void )
//{
//    Temp_Light.lightiState=MEASURE_STATUS_IDLE;
//	return _lightLux;    
//}

//code const void (code * StartMeasureLight_p)(void) = &StartMeasureLight;
//void StartMeasureLight(void)
//{
//  Temp_Light.measureFlag=0x01;
//
//}

//code const uint8_t (code * GetMeasureLight_p)(void) = &GetMeasureLight;
//
//uint8_t GetMeasureLight(void)
//{
//
//    return Temp_Light.measureFlag;
//}

//code const void (code * StartMeasureLightProcess_p)(void) = &StartMeasureLightProcess;


//code const void (code * StartMeasureLightProcess_p)(void) = &StartMeasureLightProcess;


//void StartMeasureLightProcess(void)
//{
//   if(Temp_Light.measureFlag==0x00)
//     return;
//   switch(LightMeaState)
//   {
//       case LIGHT_IDLE:
//            ZW_DEBUG_BH1750_SEND_STR("LIGHT_IDLE \r\n");
//            LightMeaState=LIGHT_START;
//			Temp_Light.lightiState=MEASURE_STATUS_RUNING;
//            break;
//       case LIGHT_START:
//	   	    Temp_Light.lightiState=MEASURE_STATUS_RUNING;
//            LightMeaState=LIGHT_CONVERSION;
//            modLightStartConvert();
//            break;
//       case LIGHT_CONVERSION:
//            LightCount++;
//			Temp_Light.lightiState=MEASURE_STATUS_RUNING;
//            if(LightCount>5)
//            {
//                LightMeaState=LIGHT_END;
//                modLightConvertMul();
//                LightCount=0x00;
//                ZW_DEBUG_BH1750_SEND_STR("LIGHT_Covent OK ");
//            }
//            break;
//       case LIGHT_END: 
//	   		Temp_Light.measureFlag=0x00;
//			Temp_Light.lightiState=MEASURE_STATUS_DONE;
//            LightMeaState=LIGHT_IDLE;
//            LX_State|=0x01;
//            ZW_DEBUG_BH1750_SEND_NUM(_lightLux>>8);
//            ZW_DEBUG_BH1750_SEND_NUM(_lightLux);
//            ZW_DEBUG_BH1750_SEND_NL();
//            break;
//   }
//}

/*
code const uint8_t (code * GetLXMeasureStatus_p)(void) = &GetLXMeasureStatus;
 uint8_t GetLXMeasureStatus(void)
{

        return Temp_Light.lightiState;
}
*/

#endif // #ifdef LIGHT_USED_BH1750


#include "DAC8562.h"


#define LD_CHANNEL         0
#define TEC_CHANNEL         1

void DAC8562_WriteCmd(unsigned long int u32data)
{
    unsigned char i;
    SPI_SCK = 0;
    delay(2);
    SPI_NSS2 = 0;
//    SPI_SCK = 0;
    delay(2);
    for(i=0;i<24;i++)
    {
      if((u32data&0x800000)==0x800000) 
          SPI_MOSI = 1;
      else         
          SPI_MOSI = 0;
      delay(2);      //??????  ????????
      SPI_SCK = 1;//???????
      delay(2);//CLK????????? ??????? ?????spi?????
      SPI_SCK = 0; //?????????????????????
      u32data = u32data<<1;//????????????
    }
    SPI_SCK = 1;
    SPI_NSS2 = 1;
}

//ch=0,DAC-A  ch=1,DAC-B
//v=dac/65536*2.5*gain
void DAC8562_SetDACData(unsigned char ch,unsigned short dac)
{
	unsigned long int cmd = 0;
    cmd = ((unsigned long)3<<19)|((unsigned long)ch<<16)|(dac<<0);
    DAC8562_WriteCmd(cmd);
}

void DAC8562_Init(void)
{
    u16 TEC_DAC = 0;
	unsigned long int cmd = 0;
	
    cmd = ((unsigned long)4<<19)|((unsigned long)0<<16)|(3<<0);
    DAC8562_WriteCmd(cmd); //power up DAC-A and DAC-B
    cmd = ((unsigned long)6<<19)|((unsigned long)0<<16)|(3<<0);
    DAC8562_WriteCmd(cmd); //LDAC pin inactive for DAC-A and DAC-B
	
    TEC_DAC = 0.0001*TemperatureCompensateCoef_TEC.a*board_temperature*board_temperature +
                         0.01*TemperatureCompensateCoef_TEC.b*board_temperature + TemperatureCompensateCoef_TEC.c;
    DAC8562_SetDACData(TEC_CHANNEL,TEC_DAC);
    DAC8562_SetDACData(LD_CHANNEL,0);
	
    cmd = ((unsigned long)7<<19)|((unsigned long)0<<16)|(1<<0);
    DAC8562_WriteCmd(cmd); //Enable internal reference and reset DACs to gain = 2
    cmd = ((unsigned long)0<<19)|((unsigned long)2<<16)|(3<<0);
    DAC8562_WriteCmd(cmd); //Enable internal reference and reset DACs to gain = 1
}











































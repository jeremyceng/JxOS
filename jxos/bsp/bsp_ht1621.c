#include <bsp_ht1621_config.h>

static void ht1621_pin_reset(void)
{
	bsp_ht1621_cs_pin_hight();
	bsp_ht1621_wr_pin_hight();
	bsp_ht1621_rd_pin_hight();
	bsp_ht1621_data_pin_hight();
}

static void ht1621_send_data(uint8_t dat, uint8_t data_len)
{
	for(; data_len > 0; data_len--){
		bsp_ht1621_wr_pin_low();			//写入时钟拉低

		if((dat&0x80) == 0)				//取出最高位 判断
			bsp_ht1621_data_pin_low();
		else
			bsp_ht1621_data_pin_hight();

		dat <<= 1;						//数据移位
		bsp_ht1621_delay_10us(1);

		bsp_ht1621_wr_pin_hight();			//写入时钟拉高
		bsp_ht1621_delay_10us(1);
	}
}

static void ht1621_rec_data(uint8_t* dat, uint8_t data_len)
{
	uint8_t temp;

	bsp_ht1621_data_pin_hight();			//释放引脚电平
	for(; data_len > 0; data_len--){
		bsp_ht1621_rd_pin_low();			//写入时钟拉低
		bsp_ht1621_delay_10us(1);

		bsp_ht1621_rd_pin_hight();
		*dat <<= 1;              		//数据移位
		temp =  bsp_ht1621_data_pin_rec(); 	//读取引脚电平
		*dat |= temp;
		bsp_ht1621_delay_10us(1); 			//写入时钟拉高
	}
}

//命令格式
//100					0000001 	X
//标志码 100表示命令     命令值       最后一位为无效值
void bsp_ht1621_write_cmd(uint8_t cmd)
{
	ht1621_pin_reset();				//控制端口复位

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_low();			//片选拉低
	bsp_ht1621_delay_10us(1);

	ht1621_send_data(0x80, 3);		//写入标志码“100”

	ht1621_send_data(cmd, 8);		//9 位command 命令
	ht1621_send_data(0x00, 1);		//最后一位为无效值

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_hight();			//片选拉高
	bsp_ht1621_delay_10us(1);

	ht1621_pin_reset();				//控制端口复位
}

//数据格式
//101					a5 a4 a3 a2 a1 a0	d0 d1 d2 d3
//标志码 101表示数据     6位地址              4位数据
//32个地址 00000 ~ 11111
//只需要5位
//在1621B中，32个地址用了6位，其中最高位a5无效
void bsp_ht1621_write_data(uint8_t addr, uint8_t dat)
{
	ht1621_pin_reset();				//控制端口复位

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_low();			//片选拉低
	bsp_ht1621_delay_10us(1);

	ht1621_send_data(0xa0, 3);		//写入标志码“101”

	ht1621_send_data(addr<<2, 6);
	ht1621_send_data(dat, 4);

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_hight();			//片选拉高
	bsp_ht1621_delay_10us(1);

	ht1621_pin_reset();				//控制端口复位
}

void bsp_ht1621_read_data(uint8_t addr, uint8_t* dat)
{
	ht1621_pin_reset();				//控制端口复位

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_low();			//片选拉低
	bsp_ht1621_delay_10us(1);

	ht1621_send_data(0xc0, 3);		//写入标志码“110”

	ht1621_send_data(addr<<2, 6);
	ht1621_rec_data(dat, 4);

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_hight();			//片选拉高
	bsp_ht1621_delay_10us(1);

	ht1621_pin_reset();				//控制端口复位
}

void bsp_ht1621_write_data_series(uint8_t addr, uint8_t *dat, uint8_t dat_len)
{
	ht1621_pin_reset();				//控制端口复位

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_low();			//片选拉低
	bsp_ht1621_delay_10us(1);

	ht1621_send_data(0xa0, 3);		//写入标志码“101”

	ht1621_send_data(addr<<2, 6);
    for (; dat_len > 0; dat_len--){
        ht1621_send_data(*dat, 8); 	//写入数据
        dat++;
    }

	bsp_ht1621_delay_10us(1);
	bsp_ht1621_cs_pin_hight();			//片选拉高
	bsp_ht1621_delay_10us(1);

	ht1621_pin_reset();				//控制端口复位
}

void bsp_ht1621_init(void)
{
	//控制引脚初始化
	bsp_ht1621_pin_init();

	//delay 100ms
	bsp_ht1621_delay_10us(10000);

	//禁止看门狗
    bsp_ht1621_write_cmd(WDT_DIS);

	//打开片内RC振荡器
	bsp_ht1621_write_cmd(SYS_EN);

	//系统时钟源设为片内RC振荡器
	bsp_ht1621_write_cmd(RC_256k);

	//1/3偏压,4个公共口
	bsp_ht1621_write_cmd(BIAS_D13_B14);

	//打开LCD偏压发生器
	bsp_ht1621_write_cmd(LCD_ON);
}

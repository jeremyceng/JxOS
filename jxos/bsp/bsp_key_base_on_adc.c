
#include <bsp_key_base_on_adc_config.h>

#define BSP_KEY_BASE_ON_ADC_NA_KEY_NUM 		0XFE
#define BSP_KEY_BASE_ON_ADC_ERR_KEY_NUM 	0XFF

void (*bsp_key_base_on_adc_press_callback)(uint8_t key_num);
void (*bsp_key_base_on_adc_release_callback)(uint8_t key_num);
void (*bsp_key_base_on_adc_long_press_callback)(uint8_t key_num);

static int32_t key_average_buffer[BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN];
static uint8_t average_buffer_counter = 0;

static int32_t last_voltage  = 0;

static uint16_t long_press_count = 0;
static uint8_t last_key_num = 0;

void bsp_key_base_on_adc_scan(void)
{
    uint8_t i = 0;
	int32_t voltage = 0;
	int32_t temp_voltage = 0;

	uint8_t key_num = BSP_KEY_BASE_ON_ADC_NA_KEY_NUM;

	voltage = bsp_key_base_on_adc_hal_read_voltage_mv();
    if(voltage > last_voltage)
    {
        temp_voltage = voltage - last_voltage;
    }
    else
    {
        temp_voltage = last_voltage - voltage;
    }
    last_voltage = voltage;
    if(temp_voltage <= BSP_KEY_BASE_ON_ADC_FILT_VOLTAGE_MV)
    {
        key_average_buffer[average_buffer_counter] = voltage;
        average_buffer_counter++;
        if(average_buffer_counter >= BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN)
        {
            average_buffer_counter = 0;
            for (i = 0; i < BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN; i++ )
            {
                temp_voltage += key_average_buffer[i];
            }
//            err_voltage >>= 2;
			temp_voltage /= BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN;
			/**********************************************************/
			//key state handle
			if((temp_voltage <= KEY_0_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_0_VOL - KEY_VOL_ERR))
			{
				key_num = 0;
			}
			if((temp_voltage <= KEY_1_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_1_VOL - KEY_VOL_ERR))
			{
				key_num = 1;
			}
			else if((temp_voltage <= KEY_2_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_2_VOL - KEY_VOL_ERR))
			{
				key_num = 2;
			}
			else if((temp_voltage <= KEY_3_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_3_VOL - KEY_VOL_ERR))
			{
				key_num = 3;
			}
			else if((temp_voltage <= KEY_4_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_4_VOL - KEY_VOL_ERR))
			{
				key_num = 4;
			}
			else if((temp_voltage <= KEY_5_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_5_VOL - KEY_VOL_ERR))
			{
				key_num = 5;
			}
			else if((temp_voltage <= KEY_NA_VOL + KEY_VOL_ERR)
					&& (temp_voltage >= KEY_NA_VOL - KEY_VOL_ERR))
			{
				key_num = BSP_KEY_BASE_ON_ADC_NA_KEY_NUM;
			}
			else
			{
				key_num = BSP_KEY_BASE_ON_ADC_ERR_KEY_NUM;
			}

			if(key_num != last_key_num)
			{
				long_press_count = 0;
				if(key_num != BSP_KEY_BASE_ON_ADC_NA_KEY_NUM)
				{
					if(bsp_key_base_on_adc_press_callback != 0)
					  bsp_key_base_on_adc_press_callback(key_num);
				}
				else
				{
					if(bsp_key_base_on_adc_release_callback != 0)
						bsp_key_base_on_adc_release_callback(last_key_num);
				}
				last_key_num = key_num;
			}
			else
			{
				if(long_press_count < BSP_KEY_BASE_ON_ADC_LONG_PRESS_COUNT)
				{
					long_press_count++;
				}
				else
				{
					long_press_count = 0;
					if(key_num != BSP_KEY_BASE_ON_ADC_NA_KEY_NUM){
						if(bsp_key_base_on_adc_long_press_callback != 0)
							bsp_key_base_on_adc_long_press_callback(key_num);
					}
				}
			}
			/**********************************************************/
        }
    }
    else
    {
        average_buffer_counter = 0;
    }
}

void bsp_key_base_on_adc_init(void)
{
	average_buffer_counter = 0;
	last_voltage  = 0;
	long_press_count = 0;
	last_key_num = 0;

	bsp_key_base_on_adc_press_callback = 0;
	bsp_key_base_on_adc_release_callback = 0;
	bsp_key_base_on_adc_long_press_callback = 0;

    bsp_key_base_on_adc_hal_init();
}



#ifndef __BSP_KEY_BASE_ON_ADC__H
#define __BSP_KEY_BASE_ON_ADC__H

#define BSP_KEY_BASE_ON_ADC_FILT_VOLTAGE_MV	100
#define BSP_KEY_BASE_ON_ADC_FILT_AVERAGE_BUFFER_LEN 4

#define BSP_KEY_BASE_ON_ADC_LONG_PRESS_COUNT 		50

extern void (*bsp_key_base_on_adc_press_callback)(uint8_t key_num);
extern void (*bsp_key_base_on_adc_release_callback)(uint8_t key_num);
extern void (*bsp_key_base_on_adc_long_press_callback)(uint8_t key_num);

void bsp_key_base_on_adc_scan(void);
void bsp_key_base_on_adc_init(void);

#endif


#include <bsp_rotary_encoder_config.h>

void (*bsp_rotary_encoder_clockwise_callback)(uint32_t num);
void (*bsp_rotary_encoder_counter_clockwise_callback)(uint32_t num);

//��ת������
//CW:  Clockwise			˳ʱ��
//CCW: Counter Clockwise 	��ʱ��

typedef struct {
	uint8_t last_pin_a_level;
	uint8_t last_pin_b_level;
} ROTARY_ENCODER_STRUCT;
static ROTARY_ENCODER_STRUCT RES[ROTARY_ENCODER_NUM_MAX];

static void rotary_encoder_struct_init(void) {
    uint32_t i;
    for(i = 0; i < ROTARY_ENCODER_NUM_MAX; i++) {
        RES[i].last_pin_a_level = 0;
        RES[i].last_pin_b_level = 0;
	}
}

void bsp_rotary_encoder_pin_a_edge_handler(uint32_t num)
{
	uint8_t TEMPA = 0;
	uint8_t TEMPB = 0;

    bsp_rotary_encoder_delay_ms(1);

    TEMPA = bsp_rotary_encoder_hal_read_pin_a_level(num);
    TEMPB = bsp_rotary_encoder_hal_read_pin_b_level(num);

    if((TEMPA == RES[num].last_pin_a_level)||(TEMPB == RES[num].last_pin_b_level)){
			goto err;
    }

    if(TEMPA == 1){
        if(TEMPB == 1){
			if(bsp_rotary_encoder_clockwise_callback != 0)
				bsp_rotary_encoder_clockwise_callback(num);
        }
        else{
			if(bsp_rotary_encoder_counter_clockwise_callback != 0)
				bsp_rotary_encoder_counter_clockwise_callback(num);
        }
    }
    else{
        if(TEMPB == 1){
			if(bsp_rotary_encoder_counter_clockwise_callback != 0)
				bsp_rotary_encoder_counter_clockwise_callback(num);
        }
        else{
			if(bsp_rotary_encoder_clockwise_callback != 0)
				bsp_rotary_encoder_clockwise_callback(num);
        }
    }
err:
    RES[num].last_pin_a_level = TEMPA;
    RES[num].last_pin_b_level = TEMPB;
}

void bsp_rotary_encoder_init(void)
{
    rotary_encoder_struct_init();
    bsp_rotary_encoder_hal_init();
	bsp_rotary_encoder_clockwise_callback = 0;
	bsp_rotary_encoder_counter_clockwise_callback = 0;
}




/***************************************
服务原语
N层						N+1层
-----------------------------
	<-----------request			//N+1 对 N 请求
	confirm----------->			//N 
	...................
	...................
	indication-------->
	<----------response
-----------------------------

PHY 从字节到瓦特

概念：
频带，发射功率，接收灵敏度，信道数，信道带宽，速率，调制方式，校验

功能：
开启关闭无线收发器，信道选择，数据传输，
能量检测，链路质量检测，信道空闲评估，载波侦听，信号强度指示

属性：（常量/变量）
当前信道，信道列表，发射功率，最大载荷长度，数据流间隔时间

数据流结构

接口：

---------------------------------------
DLK (MAC/LLC) 相邻节点之间的数据传输
概念：
MAC（媒体访问控制） LLC（逻辑链路控制） 网络拓扑 设备类型 帧 设备地址 回复 重传 碰撞避免 仲裁 节点 校验 关联
广播 单播 组播 PPP FDDI 以太网 PPPOE
链路：一个节点到相邻节点的一段物理线路，中间没有其他节点

LLC 向上层提供服务，与介质，拓扑无关
MAC 面向传输介质，与介质，拓扑相关

功能：
LLC 上层数据分包与重组，分包顺序控制，分包序号管理
	面向连接的服务：	1建立连接 2数据传输+ACK 3断开连接
	确认无连接的服务：	1.数据传输+ACK
	无确认无连接的服务：1.数据传输
MAC 数据组帧解帧，差错控制，链路管理，应答/重传，流量控制，防碰撞机制，
	建立关联，维护关联，解除关联，数据传输，介质管理，节点类型管理

属性 节点地址，网络地址，节点类型，最大数据长度

帧结构

接口

***************************************/

/***************************************
JSNET PHY
定义:
频带 		433Mmhz
发射功率	--
接收灵敏度	--
信道数		--
信道带宽	--
速率		--
调制方式 	OOK
校验		--

属性:
当前信道 	--
信道列表	--
发射功率	--
最大载荷长度64bit
隔时间		100us

数据流结构: SYNC PAYLOAD END
SYNC		ON 80us		OFF 80us
BIT1		
BIT0
END

接口:
request_tx_enable()	
request_tx_disable()
request_rx_enable()
request_rx_disable()

request_data_send()
confirm_data_send()
indication_data_receive()
***************************************/

/***************************************
JSNET DLK MAC/LLC
定义:
设备类型	完全节点，精简节点
网络数		3(不能为0)
节点数		32
广播地址	11111
中心节点地址00000
帧序号范围	0-7
最大载荷	16
回复超时	100ms
重发次数	3
帧间隔		100ms
校验		异或


属性:
节点地址
网络地址
节点类型
帧类型

帧结构: 
帧头2bit	口令3bit	帧类型1bit	地址类型1bit	回复标志1bit
10						CMD0;DAT1	长1;短0			Y1;N0	

目标节点地址 
长2BYTE;短1BYTE	

源节点地址 
长2BYTE;短1BYTE		

载荷长度5bi 帧序号3bit	
0-31		0-7		

载荷 

校验1byte																
前面所有字节

接口:
request_receive_enable()	
request_receive_disable()

request_device_type_set()
request_device_type_get()

request_reset()
confirm_reset()

request_scan()
confirm_scan()

request_new()
confirm_new()

request_associate()
confirm_associate()
indication_associate()

request_disassociate()
confirm_disassociate()
indication_disassociate()

request_data_send()
confirm_data_send()
indication_data_receive()

request_data_poll()
confirm_data_poll()
indication_data_poll()

***************************************/


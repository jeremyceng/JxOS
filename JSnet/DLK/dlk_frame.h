
#ifndef _JSNET_DLK_FRAME_H
#define _JSNET_DLK_FRAME_H

#include "lib/type.h"

#define FRAME_UNENCRYPTED		0
#define FRAME_ENCRYPTED			1

#define FRAME_NO_ACK			0
#define FRAME_NEED_ACK			1

#define FRAME_SHORT_ADDR		0
#define FRAME_LONG_ADDR			1

#define FRAME_COUNT_MAX			0XFF	//0-255

#define FRAME_PAYLOAD_MAX_LEN	31		//0-255
	
#define FRAME_ADDR_BROADCAST	0XFF

#define FRAME_CONTROL_BYTE_LEN	3
#define FRAME_SHORT_ADDR_LEN	1
#define FRAME_LONG_ADDR_LEN		2
#define FRAME_VERF_BYTE_LEN		1
#define FRAME_LEN_MAX			(FRAME_CONTROL_BYTE_LEN+\
								(FRAME_LONG_ADDR_LEN*2)+\
								FRAME_PAYLOAD_MAX_LEN+\
								FRAME_VERF_BYTE_LEN)

typedef struct
{
	uint8_t encrypted			:1;
	uint8_t need_ack			:1;
	uint8_t target_addr_type	:1;
	uint8_t source_addr_type	:1;
	uint8_t frame_type			:4;
} FRAME_HEAD_BYTE_STRUCT;

typedef struct
{
	FRAME_HEAD_BYTE_STRUCT head_byte;
	uint8_t	frame_count;
	uint8_t	playload_len;
	uint8_t verf;
} FRAME_STRUCT;

uint8_t deframe(uint8_t* input_frame, uint8_t input_frame_len, 
			FRAME_STRUCT* output_frame_struct,
			uint8_t* output_frame_playload, 
			uint8_t* output_target_node_addr,
			uint8_t* output_source_node_addr);

uint8_t framing(FRAME_STRUCT* input_frame_struct,
			uint8_t* input_frame_playload, 
			uint8_t* input_target_node_addr,
			uint8_t* input_source_node_addr,
			uint8_t* out_frame, uint8_t* out_frame_len);

uint8_t deframe_is_encrypted(uint8_t* input_frame);
uint8_t deframe_get_frame_count(uint8_t* input_frame);

uint8_t addr_type_to_addr_len(uint8_t addr_type);	//return 0xff ->err
uint8_t addr_len_to_addr_type(uint8_t addr_len);	//return 0xff ->err

#endif


#ifndef _JSNET_DLK_SEND_RECEIVE_H
#define _JSNET_DLK_SEND_RECEIVE_H

void dlk_send_pause(void);
void dlk_send_continue(void);
void dlk_send_stop_repeat(void);
uint8_t dlk_send_is_repeatting(void);
uint8_t* dlk_send_buff_get(uint8_t* dlk_send_buffer_len);
uint8_t dlk_send(uint8_t* frame, uint8_t frame_len,
				 uint8_t wait_prepare,	//wait for the other side rx ready, enabled when send a response
				 uint8_t wait_respons,	//wait for the other side ack, enabled when need ack
				 uint8_t repeat);		//enabled when need repeat
void dlk_send_handler(void);
void dlk_send_init(void);

uint8_t dlk_receive_callbck_register(void (*dlk_receive_callbck)
									 (uint8_t* receive_data, uint8_t data_len));
void dlk_receive_init(void);

#endif

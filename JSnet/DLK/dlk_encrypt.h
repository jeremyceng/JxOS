
#ifndef _JSNET_DLK_ENCRYPT_H
#define _JSNET_DLK_ENCRYPT_H

#include "lib/type.h"

void encrypt(uint8_t* en_data, uint8_t* data_len);
uint8_t decrypt(uint8_t* de_data, uint8_t* data_len);

#endif

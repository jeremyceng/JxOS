
#include "stdio.h"

#include "jxos_public.h"
#include "../PHY/jsnet_phy.h"
#include "jsnet_dlk.h"
#include "dlk_primitives.h"
#include "dlk_send_receive.h"
#include "dlk_dongle.h"

/***************************************************************/
//dlk task
static uint8_t dlk_task_id;
static void dlk_task(uint8_t task_id, void * parameter)
{
#if(JSNET_DLK_DEVICE_TYPE != JSNET_DLK_DEVICE_TYPE_DONGLE)
	dlk_send_handler();
	dlk_primitives_handler();
#endif
}

void dlk_task_init(void)
{
	jxos_task_create(dlk_task, "dlk_task", 0);

#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_DONGLE)
	dongle_init();
#else
	dlk_parameter_init();
	dlk_send_init();
	dlk_receive_init();
	dlk_primitives_init();
#endif
}

/**********************************
#include "dlk_cmd.h"
static void key_release_handler(void)
{
	led_task_led_blink_set(20, 10, 10);

#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
	jsnet_dlk_request_new_pan(); 	//ok
//	jsnet_dlk_request_scan();		//ok
//	jsnet_dlk_request_data_send("1122", 4, 3);
//	jsnet_dlk_request_data_poll(0);
#endif
}

uint8_t debug_msg[5];
void jsnet_dlk_confirm_new_pan_callback(uint8_t pan_id)
{
	debug_msg[0] = 0xFa;
	debug_msg[1] = pan_id;
	debug_msg[2] = pan_id;
	debug_msg[3] = 0xFa;
	jsnet_dlk_request_discoverable();		//ok
	jsnet_dlk_request_data_send(debug_msg, 4, 3);
}

void jsnet_dlk_confirm_scan_callback(uint8_t* pan_id_list, uint8_t pan_id_list_count)
{
	/*
	debug_msg[0] = 0xFA;
	debug_msg[1] = pan_id_list_count;
	debug_msg[2] = pan_id_list[0];
	debug_msg[3] = pan_id_list[1];
	debug_msg[4] = 0xFA;
	*/
//	if(pan_id_list_count > 0){
//		jsnet_dlk_parameter_set_local_verf_code(0x2244);				//ok
//		jsnet_dlk_request_associate(pan_id_list[0],pan_id_list[1]);		//ok
//	}
//}
//
//void jsnet_dlk_indication_data_poll_callback(uint8_t source_node_addr)
//{
//	jsnet_dlk_request_data_send("5566", 4, source_node_addr);
//}
//
//
//void test_init(void)
//{
//	key_task_key_release_callback_reg(key_release_handler);
//
//	jsnet_dlk_confirm_scan = jsnet_dlk_confirm_scan_callback;		//ok
//#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
//	jsnet_dlk_confirm_new_pan = jsnet_dlk_confirm_new_pan_callback;	//ok
//	jsnet_dlk_indication_data_poll = jsnet_dlk_indication_data_poll_callback;
//#endif
//}


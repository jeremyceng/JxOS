
#include "hal.h"
#include "bsp/bsp_rf_driver.h"

void (*exint0_ISR_callback)(void) = 0;
void exint0_ISR() interrupt 0       //INT0�ж����
{
	if(exint0_ISR_callback != 0){
		exint0_ISR_callback();
	}
}

void (*WakeUp_Timer_ISR_callback)(void) = 0;
void WakeUp_Timer_ISR (void)   interrupt 17     //ISR for self wake-up timer
{
	clr_WKTF;
	if(WakeUp_Timer_ISR_callback != 0){
		WakeUp_Timer_ISR_callback();
	}
}

extern uint8_t software_timer_task_tick_handler_flag;
//void (*Timer0_ISR_callback)(void) = 0;
void Timer0_ISR (void) interrupt 1              //interrupt address is 0x000B
{  
//	if(Timer0_ISR_callback != 0){
//		Timer0_ISR_callback();
//	}
	static uint8_t sys_tick  = 0;
//	P17 = 1;
    bsp_rf_send_tick_handler();		//1.44us
    bsp_rf_receive_tick_handler();	//7.12us
//	P17 = 0;
	sys_tick++;
	if(sys_tick > 125){	//5ms
		sys_tick = 0;
		software_timer_task_tick_handler_flag = 1;
	}	
}

void (*Timer3_ISR_callback)(void) = 0;
void Timer3_ISR (void) interrupt 16 
{
    clr_TF3;
	if(Timer3_ISR_callback != 0){
		Timer3_ISR_callback();
	}
}

/**
 * FUNCTION_PURPOSE: serial interrupt, echo received data.
 * FUNCTION_INPUTS: P0.7(RXD) serial input
 * FUNCTION_OUTPUTS: P0.6(TXD) serial output
 */
extern uint8_t uart_rx_isr_rec_data[];
extern uint8_t uart_rx_isr_rec_data_len;
extern uint8_t uart_rx_isr_handler_flag;
extern uint8_t uart_tx_isr_handler_flag;
//void (*SerialPort0_RX_ISR_callback)(char byte) = 0;
//void (*SerialPort0_TX_ISR_callback)(void) = 0;
void SerialPort0_ISR(void) interrupt 4 
{
//	char byte;
    if (RI==1) 
    {                                       /* if reception occur */
        clr_RI;                             /* clear reception flag for next reception */
//		if(SerialPort0_RX_ISR_callback != 0){
//			byte = SBUF;
			//SerialPort0_RX_ISR_callback(byte);
			uart_rx_isr_rec_data[uart_rx_isr_rec_data_len] = SBUF;
			uart_rx_isr_rec_data_len++;
			uart_rx_isr_handler_flag = 1;
//		}
    }
    if(TI==1)
    {
        clr_TI;                             /* if emission occur */
//		SerialPort0_TX_ISR_callback();
		uart_tx_isr_handler_flag = 1;
    }
}

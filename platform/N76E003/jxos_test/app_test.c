

#include <kernel/jxos_task.h>
#include <kernel/jxos_msg.h>
#include <kernel/jxos_event.h>

#include <hal/hal_exti.h>
#include <hal/hal_interrupt.h"

#include <sys_service/software_timer_task.h>

static swtime_type app_test_swt = 0;

uint8_t test_msg[32];
JXOS_MSG_HANDLE msg;
JXOS_EVENT_HANDLE test_event;

static void app_test_task(uint8_t task_id, void * parameter)
{
    if(jxos_event_wait(test_event) == 1){
//		printf("app_test_task send msg!!\r\n");
        jxos_msg_send(msg, "isr_evt");
    }
    if(sys_svc_software_timer_check_overtime(app_test_swt) == 1){
//		printf("app_test_task send msg!!\r\n");
        jxos_msg_send(msg, "t_tick!");
    }

}

static void app_test_exti_handler(void)
{
    jxos_event_set(test_event);
}

void app_test_task_init(void)
{
//	EXTI_InitTypeDef EXTI_InitStruct;
//	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
//    EXTI_Init(&EXTI_InitStruct);
//    EXTI0_IRQHandler_callback = app_test_exti_handler;

	jxos_task_create(app_test_task, "test", 0);
    msg = jxos_msg_create(test_msg, 32, 8, "test_msg");
    test_event =jxos_event_create();

	app_test_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(app_test_swt, 100);
	sys_svc_software_timer_start(app_test_swt);
}


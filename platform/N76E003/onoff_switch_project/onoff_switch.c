
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"
#include "bsp/bsp_rf_driver.h"

/*********************************************
1.实现 HAL
2.配置 BSP
3.配置 SRTnet COMM LAYE
4.配置 SRTnet APP LAYE
5.实现用户逻辑(key_task, led_task, onoff_switch)
6.配置 JXOS
6.配置 sys service
*********************************************/

/*********************************************
短按 toggel
双击 off
长按（5s）bunding
*********************************************/

#define LOW_BATT_VOLTAGE		2500
#define NORMAL_BATT_VOLTAGE		3000

uint16_t band_v;
uint16_t band_ad;
unsigned long int vdd = NORMAL_BATT_VOLTAGE;
uint8_t LOW_BATT_FLAG = 0;

uint8_t software_timer_task_tick_handler_flag = 0;
uint8_t std_app_key_task_key_press_interrupt_handler_falg = 0;
extern uint8_t power_mgr_interrupt_happened;
		
static JXOS_MSG_HANDLE key_msg;
static swtime_type rf_send_swt;
static swtime_type batt_state_swt;
static swtime_type low_batt_led_swt;
static uint8_t onoff_switch_keep_wake_id;

void led_on(void);
void led_off(void);
void led_init(void);
void rf_tx_enable(void);
void rf_tx_disable(void);

void onoff_switch_short_press(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_short_press\r\n");
	srtnet_app_layer_service_id_onoff_send_cmd_toggle(key_num);
}

void onoff_switch_long_press(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_long_press\r\n");
	srtnet_app_layer_service_id_binding_send_cmd_bind(key_num);
}

void onoff_switch_double_click(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_double_click\r\n");
	srtnet_app_layer_service_id_onoff_send_cmd_off(key_num);
}

static void onoff_switch_task(uint8_t task_id, void * parameter)
{
	static uint8_t adc_read_count = 0;
	static uint8_t led_blink = 0;
	uint8_t msg_item;
	uint8_t key_num;
	uint8_t key_msg_type;
	if(software_timer_task_tick_handler_flag == 1){
		software_timer_task_tick_handler_flag = 0;
		sys_svc_software_timer_tick_handler();
	}
	if(std_app_key_task_key_press_interrupt_handler_falg == 1){
		std_app_key_task_key_press_interrupt_handler_falg = 0;
		power_mgr_interrupt_happened = 0;
		std_app_key_task_key_press_interrupt_handler();
	}
	
	if(jxos_msg_receive(key_msg, &msg_item) == 1){
		key_msg_type = msg_item&0x0f;
		key_num = msg_item>>4;

		switch(key_msg_type){
		case 0:
			onoff_switch_long_press(key_num);
			break;
		case 1:
			onoff_switch_short_press(key_num);
			break;
		case 2:
			onoff_switch_double_click(key_num);
			break;
		default:
			key_num = 0xff;
			break;
		}
		
		if(key_num != 0xff){
			sys_svc_software_timer_restart(rf_send_swt);
			sys_power_mgr_task_keep_wake_mark_set(onoff_switch_keep_wake_id);
			led_on();
		}
	}

	if(sys_svc_software_timer_check_overtime(rf_send_swt) == 1){
		sys_svc_software_timer_stop(rf_send_swt);
		led_off();
		
		if(0 == sys_svc_software_timer_check_running(batt_state_swt)){
			adc_read_count = 0;
			sys_svc_software_timer_set_time(batt_state_swt, 10);
			sys_svc_software_timer_restart(batt_state_swt);
			set_BODEN;		//need to set boden to enable the Band-gap
			adc_enable();
		}
	}

	if(sys_svc_software_timer_check_overtime(batt_state_swt) == 1){
		band_ad = adc_read(8);
		if(adc_read_count <= 3){
			adc_read_count++;
			sys_svc_software_timer_set_time(batt_state_swt, 10);
			sys_svc_software_timer_restart(batt_state_swt);
		}
		else{
			sys_power_mgr_task_keep_wake_mark_reset(onoff_switch_keep_wake_id);
			adc_read_count = 0;
			vdd = 0xfff;
			vdd = vdd*band_v;
			vdd = vdd/band_ad;
			if(vdd >= NORMAL_BATT_VOLTAGE){
				LOW_BATT_FLAG = 0;
			}
			else if(vdd <= LOW_BATT_VOLTAGE){
				LOW_BATT_FLAG = 1;
			}
			//else{
				//LOW_BATT_FLAG not change;
			//}
			
			if(LOW_BATT_FLAG == 0){
				adc_disable();		//when adc anbled the current is about 400us, so adc must be disable befor goto sleep
				clr_BODEN;			//bod must be disable befor goto sleep
				sys_svc_software_timer_stop(batt_state_swt);
			}
			else{
				sys_svc_software_timer_set_time(batt_state_swt, 60000);
				sys_svc_software_timer_restart(batt_state_swt);

				if(0 == sys_svc_software_timer_check_running(low_batt_led_swt)){
					led_blink = 1;
					led_on();
					sys_svc_software_timer_set_time(low_batt_led_swt, 50);
					sys_svc_software_timer_restart(low_batt_led_swt);
				}
			}
		}
	}

	if(sys_svc_software_timer_check_overtime(low_batt_led_swt) == 1){
		if(LOW_BATT_FLAG == 0){
			sys_svc_software_timer_stop(low_batt_led_swt);
			led_off();
		}
		else{
			if(led_blink == 0){
				led_blink = 1;
				led_on();
				sys_svc_software_timer_set_time(low_batt_led_swt, 50);
				sys_svc_software_timer_restart(low_batt_led_swt);
			}
			else{
				led_blink = 0;
				led_off();
				sys_svc_software_timer_set_time(low_batt_led_swt, 1950);
				sys_svc_software_timer_restart(low_batt_led_swt);
			}
		}
	}
}

void  onoff_switch_task_init(void)
{
	band_v = bandgap_volt_read();
	
	led_init();
	led_off();
	
	srtnet_comm_layer_transmitter_device_request_local_addr_write(0xADD1);
	srtnet_comm_layer_request_local_frame_head_write(0xF1);
	srtnet_comm_layer_transmitter_device_enable_callback = rf_tx_enable;
	srtnet_comm_layer_transmitter_device_disable_callback = rf_tx_disable;	

	rf_send_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(rf_send_swt, 600);
	batt_state_swt = sys_svc_software_timer_new();
	low_batt_led_swt = sys_svc_software_timer_new();

	onoff_switch_keep_wake_id = sys_power_mgr_task_keep_wake_mark_get_id();

	jxos_task_create(onoff_switch_task, "onoff_switch", 0);

    key_msg = jxos_msg_get_handle("std_app_key_msg");
}





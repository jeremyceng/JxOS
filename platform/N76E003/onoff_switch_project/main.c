
#include "N76E003.h"
#include "Common.h"
#include "SFR_Macro.h"
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"
#include "lib/bit.h"
#include "hal.h"

/****************************
//PIN DEFINE

//P00   rf tx power
//P01   
//P02   rf tx data  ICPCK
//P03   
//P04   
//P05   led
//P06
//P07

//P10
//P11
//P12
//P13	key 0
//P14	key 1
//P15	key 2
//P16   ICPDA
//P17	key 3

//P20   hw reset

//P30

void sleep_test(void)
{
	clr_BODEN;
	
	gpio_mode_config(0, 0, GPIO_MODE_PP);		//rf tx power
	gpio_mode_config(0, 1, GPIO_MODE_PP);		//rf rx power
	gpio_mode_config(0, 2, GPIO_MODE_PP);		//rf tx	data
	gpio_mode_config(0, 3, GPIO_MODE_BD);
	gpio_mode_config(0, 4, GPIO_MODE_BD);
	gpio_mode_config(0, 5, GPIO_MODE_PP);		//led
	gpio_mode_config(0, 6, GPIO_MODE_BD);
	gpio_mode_config(0, 7, GPIO_MODE_BD);

	gpio_mode_config(1, 0, GPIO_MODE_BD);
	gpio_mode_config(1, 1, GPIO_MODE_BD);
	gpio_mode_config(1, 2, GPIO_MODE_BD);
	gpio_mode_config(1, 3, GPIO_MODE_BD);		//key 0
	gpio_mode_config(1, 4, GPIO_MODE_BD);  		//key 1
	gpio_mode_config(1, 5, GPIO_MODE_BD);		//key 2
	gpio_mode_config(1, 6, GPIO_MODE_BD);
	gpio_mode_config(1, 7, GPIO_MODE_BD);  		//key 3

//	gpio_mode_config(2, 0, GPIO_MODE_BD);

	gpio_mode_config(3, 0, GPIO_MODE_BD);		//key 4

	P00 = 0;
	P01 = 0;
	P02 = 0;
	P03 = 1;
	P04 = 1;
	P05 = 0;
	P06 = 1;
	P07 = 1;

	P10 = 1;
	P11 = 1;
	P12 = 1;
	P13 = 1;
	P14 = 1;
	P15 = 1;
	P16 = 1;
	P17 = 1;

//	P20 = 1;

	P30 = 1;
	
	tim0_stop();
	adc_disable();
	IT0 = 0;
	EX0 = 0;
	
	sleep();
}
****************************/

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
//	gpio_mode_config(0, 0, GPIO_MODE_PP);		//rf tx power
	gpio_mode_config(0, 1, GPIO_MODE_PP);		//rf rx power
//	gpio_mode_config(0, 2, GPIO_MODE_PP);		//rf tx	data
	gpio_mode_config(0, 3, GPIO_MODE_BD);
	gpio_mode_config(0, 4, GPIO_MODE_BD);
//	gpio_mode_config(0, 5, GPIO_MODE_PP);		//led
	gpio_mode_config(0, 6, GPIO_MODE_BD);
	gpio_mode_config(0, 7, GPIO_MODE_BD);

	gpio_mode_config(1, 0, GPIO_MODE_BD);
	gpio_mode_config(1, 1, GPIO_MODE_BD);
	gpio_mode_config(1, 2, GPIO_MODE_BD);
//	gpio_mode_config(1, 3, GPIO_MODE_BD);		//key 0
//	gpio_mode_config(1, 4, GPIO_MODE_BD);  		//key 1
//	gpio_mode_config(1, 5, GPIO_MODE_BD);		//key 2
	gpio_mode_config(1, 6, GPIO_MODE_BD);
//	gpio_mode_config(1, 7, GPIO_MODE_BD);  		//key 3

//	gpio_mode_config(2, 0, GPIO_MODE_BD);

	gpio_mode_config(3, 0, GPIO_MODE_BD);

//	P00 = 0;
	P01 = 0;
//	P02 = 0;
	P03 = 1;
	P04 = 1;
//	P05 = 0;
	P06 = 1;
	P07 = 1;

	P10 = 1;
	P11 = 1;
	P12 = 1;
//	P13 = 1;
//	P14 = 1;
//	P15 = 1;
	P16 = 1;
//	P17 = 1;

//	P20 = 1;

	P30 = 1;

	set_EA;
}

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
	InitialUART0_Timer1(9600);
	clr_ES;           	//disable UART interrupt
	clr_REN;			//disable uart0 rx
	set_TI;
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	uint8_t ret = TI;
	return ret;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
	clr_TI;
    SBUF = byte;
}
#endif

static uint8_t power_mgr_sleep_reg_copy = 0;
void sys_power_mgr_task_hal_init_callback_handler(void)
{
	power_mgr_sleep_reg_copy = PCON;
}

void sys_power_mgr_task_prepare_to_sleep_callback_handler(void)
{
	power_mgr_sleep_reg_copy = PCON;
	power_mgr_sleep_reg_copy |= SET_BIT1;
}

void sys_power_mgr_task_go_to_sleep_callback_handler(void)
{
	PCON = power_mgr_sleep_reg_copy;
}

void sys_power_mgr_task_recover_to_wake_callback_handler(void)
{
}

/********************************************************************************
f        dev     f/dev             bit           1/(f/dev)       (1/(f/dev))*(2^bit)
                                                 dev/f           (dev*(2^bit))/f
-------------------------------------------------------------------------------
10k      1       10k                8            0.1ms            25.6
10k      4       ...                8            0.4ms            102.4
10k      16      ...                8            1.6ms            409.6
10k      64      ...                8            6.4ms            1638.4
10k      256     ...                8            25.6ms           6553.6
10k      512     ...                8            51.2ms           13107.2
10k      1024    ...                8            102.4ms          26214.4
10k      2048    ...                8            204.8ms          52428.8
********************************************************************************/
void sys_software_timer_task_hal_init_callback_handler(void)
{
	clr_WKPS2; clr_WKPS1; clr_WKPS0;	//dev = 1
	RWK = 256 - 50;		//5ms
	set_EWKT;													// enable WKT interrupt
}

void sys_software_timer_task_hal_start_callback_handler(void)
{
	set_WKTR; 												// Wake-up timer run
}

void sys_software_timer_task_hal_stop_callback_handler(void)
{
	clr_WKTR;
}

/********************************************************************	
uint16_t sys_software_timer_task_hal_set_next_interrupt_time_in_ms_callback_handler(uint16_t next_interrupt_time_in_ms)
{
	unsigned long int temp;

	if(next_interrupt_time_in_ms > 52428){
		next_interrupt_time_in_ms = 52428;
	}

	temp = next_interrupt_time_in_ms;
	if(next_interrupt_time_in_ms <= 102){				//1~102
		clr_WKPS2; clr_WKPS1; set_WKPS0;	//dev = 4
		// RWK 1 = 0.4ms --> RWK = next_interrupt_time_in_ms/0.4
		temp = ((temp*10)>>2);
		RWK = 256 - (uint8_t)temp;
		next_interrupt_time_in_ms = (uint8_t)temp+1;
		next_interrupt_time_in_ms = (next_interrupt_time_in_ms<<2)/10;
	}
	else if(next_interrupt_time_in_ms <= 26214){		//103~26214
		set_WKPS2; set_WKPS1; clr_WKPS0;	//dev = 1024
		// RWK 1 = 102.4ms --> RWK = next_interrupt_time_in_ms/102.4
		temp = (temp*10)>>10;
		RWK = 256 - (uint8_t)temp;
		next_interrupt_time_in_ms = (uint8_t)temp;
		next_interrupt_time_in_ms = (next_interrupt_time_in_ms<<10)/10;
	}
	else{												//26214~52428
		set_WKPS2; set_WKPS1; set_WKPS0;	//dev = 2048
		// RWK 1 = 204.8ms --> RWK = next_interrupt_time_in_ms/204.8
		temp = (temp*10)>>11;
		RWK = 256 - (uint8_t)temp;
		next_interrupt_time_in_ms = (uint8_t)temp;
		next_interrupt_time_in_ms = (next_interrupt_time_in_ms<<11)/10;
	}

	set_WKTR; 												// Wake-up timer run
	if(next_interrupt_time_in_ms != 0){
		return next_interrupt_time_in_ms;
	}
	else{
		return 1;
	}
}
************************************************************************/

void std_app_key_task_hal_init_callback_handler(void)
{
	gpio_mode_config(1, 3, GPIO_MODE_BD);
	P13 = 1;

	gpio_mode_config(1, 4, GPIO_MODE_BD);
	P14 = 1;

	gpio_mode_config(1, 5, GPIO_MODE_BD);
	P15 = 1;

	gpio_mode_config(1, 7, GPIO_MODE_BD);
	P17 = 1;

	gpio_mode_config(3, 0, GPIO_MODE_BD);
	P30 = 1;

	gpio_int_config(1, 3, 0, 0);
	gpio_int_config(1, 4, 0, 0);
	gpio_int_config(1, 5, 0, 0);
	gpio_int_config(1, 7, 0, 0);
	gpio_int_enable();
	PIF = 0x00;                             //clear interrupt flag
	
	IT0 = 1;
	EX0 = 1;
}

uint8_t std_app_key_task_hal_read_pin_level_callback_handler(uint8_t key_mun)
{
	uint8_t ret;
	switch(key_mun){
		case 0:
		ret = P13;
		break;
		
		case 1:
		ret = P14;
		break;
		
		case 2:
		ret = P15;
		break;
		
		case 3:
		ret = P17;
		break;	
		
		case 4:
		ret = P30;
		break;
		
		default:
		break;
	}
	return ret;
}

void srtnet_comm_layer_task_hal_init_callback_handler(void)
{
	timer0_init_40us();
	gpio_mode_config(0, 0, GPIO_MODE_PP);		//rf_power_init
	P00 = 0;
}

/*****************************************************************************/
void led_init(void)
{
	gpio_mode_config(0, 5, GPIO_MODE_PP);
}

void led_on(void)
{
	P05 = 1;
}

void led_off(void)
{
	P05 = 0;
}

void rf_tx_enable(void)
{
	P00 = 1;
	set_TR0;                                    //Timer0 run
}

void rf_tx_disable(void)
{
	P00 = 0;
	clr_TR0;                                    //Timer0 stop
}

void wkt_stop(void)
{
	clr_WKTR;
}

void wkt_start(void)
{
	set_WKTR;
}

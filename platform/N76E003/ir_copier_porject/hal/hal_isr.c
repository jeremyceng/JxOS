
#include "hal.h"
#include "jxos_config.h"
extern uint8_t power_mgr_interrupt_happened;

//void (*exint0_ISR_callback)(void) = 0;
void exint0_ISR() interrupt 0 
{
//	if(exint0_ISR_callback != 0){
//		exint0_ISR_callback();
//	}
#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
	power_mgr_interrupt_happened = 1;
#endif
}

extern uint8_t software_timer_task_tick_handler_flag;
//void (*WakeUp_Timer_ISR_callback)(void) = 0;
void WakeUp_Timer_ISR (void)   interrupt 17     //ISR for self wake-up timer
{
	clr_WKTF;
//	if(WakeUp_Timer_ISR_callback != 0){
//		WakeUp_Timer_ISR_callback();
//	}
#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
	software_timer_task_tick_handler_flag = 1;
#endif
#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
	power_mgr_interrupt_happened = 1;
#endif
}

static uint16_t tick = 0;
void bsp_key_scan_handler(void);
void ir_rx_tick_handler(void);
void ir_tx_tick_handler(void);
//void (*Timer0_ISR_callback)(void) = 0;
void Timer0_ISR (void) interrupt 1              //interrupt address is 0x000B
{  
//	if(Timer0_ISR_callback != 0){
//		Timer0_ISR_callback();
//	}
	ir_rx_tick_handler();
	ir_tx_tick_handler();
	
	tick++;
	if(tick > (25000/40)){
		tick = 0;
		bsp_key_scan_handler();
	}
}

//void (*Timer3_ISR_callback)(void) = 0;
void Timer3_ISR (void) interrupt 16 
{
//    clr_TF3;
//	if(Timer3_ISR_callback != 0){
//		Timer3_ISR_callback();
//	}
}

/**
 * FUNCTION_PURPOSE: serial interrupt, echo received data.
 * FUNCTION_INPUTS: P0.7(RXD) serial input
 * FUNCTION_OUTPUTS: P0.6(TXD) serial output
 */
//void (*SerialPort0_RX_ISR_callback)(char byte) = 0;
void SerialPort0_ISR(void) interrupt 4 
{
//	char byte;
//    if (RI==1) 
//    {                                       /* if reception occur */
//        clr_RI;                             /* clear reception flag for next reception */
//		if(SerialPort0_RX_ISR_callback != 0){
//			byte = SBUF;
//			SerialPort0_RX_ISR_callback(byte);
//		}
//    }
//    if(TI==1)
//    {
//        clr_TI;                             /* if emission occur */
//    }
}

//GPIO
void PinInterrupt_ISR (void) interrupt 7
{
//	if(PIF == 0x01)
//	{
		PIF = 0x00;                             //clear interrupt flag
//	}
#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
	power_mgr_interrupt_happened = 1;
#endif
}
#ifndef __KEY_CONGIF_H
#define __KEY_CONGIF_H

#include "lib/type.h"

#define KEY_NUM_MAX 		        4

#define KEY_PRESS_LEVEL				0

#define KEY_JITTER_TIME					0
#define KEY_LONG_PRESS_TIME 		    20	//KEY_TASK_SCAN_TIME 50ms
#define KEY_LONG_PRESS_REPEAT_TIME 		20

#endif

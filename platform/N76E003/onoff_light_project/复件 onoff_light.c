
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"

#define onoff_light_state_inactive_bind		0
#define onoff_light_state_active_bind		1
#define onoff_light_state_activing			2

static uint8_t rec_toggle_cmd_count = 0;
static uint8_t onoff_light_state = 0;

static swtime_type onoff_light_active_bind_swt;
static JXOS_MSG_HANDLE led_msg_h;
static JXOS_MSG_HANDLE button_msg_h;

uint8_t software_timer_task_tick_handler_flag = 0;

static void led_msg_send_on(uint8_t led_num)
{
	LED_MSG_STRUCT msg;
	msg.led_num = led_num;
	msg.blink_times = 0;
	msg.period = 0;
	msg.on_time = 0;
	msg.on_off = 1;
	jxos_msg_send(led_msg_h, &msg);
}
static void led_msg_send_off(uint8_t led_num)
{
	LED_MSG_STRUCT msg;
	msg.led_num = led_num;
	msg.blink_times = 0;
	msg.period = 0;
	msg.on_time = 0;
	msg.on_off = 0;
	jxos_msg_send(led_msg_h, &msg);
}
static void led_msg_send_led_toggle(uint8_t led_num)
{
	LED_MSG_STRUCT msg;
	msg.led_num = led_num;
	msg.blink_times = 0;
	msg.period = 0;
	msg.on_time = 0;
	msg.on_off = 2;
	jxos_msg_send(led_msg_h, &msg);
}
static void led_msg_send_start_blink(uint8_t led_num, uint8_t p_time, uint8_t on_time, uint8_t blink_time)
{
	LED_MSG_STRUCT msg;
	msg.led_num = led_num;
	msg.blink_times = blink_time;
	msg.period = p_time;
	msg.on_time = on_time;
	msg.on_off = 0xff;
	jxos_msg_send(led_msg_h, &msg);
}
static void led_msg_send_stop_blink(uint8_t led_num)
{
	LED_MSG_STRUCT msg;
	msg.led_num = led_num;
	msg.blink_times = 0;
	msg.period = 0;
	msg.on_time = 0;
	msg.on_off = 0xff;
	jxos_msg_send(led_msg_h, &msg);
}
static void button_short_press(uint8_t key_num)
{
	led_msg_send_led_toggle(key_num);
}
static void button_long_press(uint8_t key_num)
{
	if(onoff_light_state != onoff_light_state_active_bind){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(key_num);
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 5000);
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
		led_msg_send_start_blink(key_num, 8, 4, 5);
	}
	else{
		onoff_light_state = onoff_light_state_inactive_bind;
		srtnet_app_layer_service_no_active_service();
		srtnet_app_layer_binding_table_clear(key_num);
		sys_svc_software_timer_stop(onoff_light_active_bind_swt);
	}
}

static void onoff_light_task(uint8_t task_id, void * parameter)
{
	BUTTON_MSG_STRUCT msg;

	if(software_timer_task_tick_handler_flag == 1){
		software_timer_task_tick_handler_flag = 0;
		sys_svc_software_timer_tick_handler();
	}

	if(jxos_msg_receive(button_msg_h, &msg) == 1){
		switch(msg.multi_click_count){
		case 0:
			button_long_press(msg.button_num);
			break;
		case 1:
			button_short_press(msg.button_num);
			break;
		default:
			break;
		}
	}

	if(sys_svc_software_timer_check_overtime(onoff_light_active_bind_swt) == 1){
		sys_svc_software_timer_stop(onoff_light_active_bind_swt);
		if(SERVICE_CONFIG_TABLE_LEN == 1){
			if(onoff_light_state == onoff_light_state_activing){
				if(rec_toggle_cmd_count == 5){							//rec toggle cmd 5 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
					led_msg_send_start_blink(0, 4, 2, 3);
				}
				else if(rec_toggle_cmd_count == 8){					//rec toggle cmd 10 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_binding_table_clear(0);								//clr bind table
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
					led_msg_send_start_blink(0, 4, 2, 3);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
				}
				rec_toggle_cmd_count = 0;
			}
			else{
				if(srtnet_app_layer_binding_table_is_empty() == 1){
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();
				}
			}
		}
		else if(SERVICE_CONFIG_TABLE_LEN > 1){
			onoff_light_state = onoff_light_state_inactive_bind;
			srtnet_app_layer_service_no_active_service();
		}
	}
}


static void srtnet_app_layer_service_id_onoff_rec_cmd_off_callback_handler(uint8_t service_seri_num)
{
	led_msg_send_off(service_seri_num);
}
static void srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback_handler(uint8_t service_seri_num)
{
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_activing;
		srtnet_app_layer_service_set_active_service(0);
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 2000);			//rec toggle cmd in 2s
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
		rec_toggle_cmd_count++;
	}
	led_msg_send_led_toggle(service_seri_num);
}
static void srtnet_app_layer_service_id_binding_rec_cmd_bind_callback_handler(SRTNET_BINDING_STRUCT* p)
{
	led_msg_send_start_blink(p->slave_service_serila_mun, 5, 1, 5);
	onoff_light_state = onoff_light_state_inactive_bind;
	srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
}

void  onoff_light_task_init(void)
{
	uint8_t i;

	onoff_light_active_bind_swt = sys_svc_software_timer_new();
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000);
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
	}

	led_msg_h = jxos_msg_get_handle("std_app_led_msg");
	button_msg_h = jxos_msg_get_handle("std_app_button_msg");

	srtnet_app_layer_service_id_binding_rec_cmd_bind_callback = srtnet_app_layer_service_id_binding_rec_cmd_bind_callback_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback = srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_off_callback	= srtnet_app_layer_service_id_onoff_rec_cmd_off_callback_handler;

	srtnet_comm_layer_request_local_frame_head_write(0xF1);
	srtnet_comm_layer_receiver_device_keep_enable(1);

	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(0);							//active service num 0
	}
	else if(SERVICE_CONFIG_TABLE_LEN > 1){
		onoff_light_state = onoff_light_state_inactive_bind;
		srtnet_app_layer_service_no_active_service();
	}

	for(i = 0; i < LED_TASK_LED_NUM_MAX; i++){
		led_msg_send_on(i);
	}

	jxos_task_create(onoff_light_task, "onoff_light", 0);
}



#include "N76E003.h"
#include "Common.h"
#include "SFR_Macro.h"
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"
#include "lib/bit.h"
#include "hal.h"

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
	wdt_init();
	gpio_mode_config(0, 1, GPIO_MODE_PP);
	P01 = 1;
	set_EA;
	set_TR0;                                    //Timer0 run
}

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
	InitialUART0_Timer1(9600);
	clr_ES;           	//disable UART interrupt
	clr_REN;			//disable uart0 rx
	set_TI;
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	uint8_t ret = TI;
	return ret;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
	clr_TI;
    SBUF = byte;
}
#endif

void sys_software_timer_task_hal_init_callback_handler(void)
{
	timer0_init_40us();
}

/*
void std_app_led_task_hal_init_callback_handler(void)
{
	gpio_mode_config(0, 5, GPIO_MODE_PP);	
}

void std_app_led_task_hal_led_on_callback_handler(void)
{
	P05 = 1;
}

void std_app_led_task_hal_led_off_callback_handler(void)
{
	P05 = 0;	
}

void std_app_button_task_hal_init_callback_handler(void)
{
	gpio_mode_config(3, 0, GPIO_MODE_BD);
	P30 = 1;
}

#define PRESS_LEVEL 0
uint8_t std_app_button_task_hal_read_button_state_callback_handler(uint8_t num)
{
	uint8_t ret;
	switch(num){
		case 0:
		ret = (P30 == PRESS_LEVEL)?0:1;
		break;
		
		default:
		break;
	}
	return ret;
}
*/
#define store_data_base_addr		0x4780
uint8_t srtnet_app_layer_binding_table_store_callback_handler(uint8_t* binding_table, uint8_t binding_table_len)
{	
	uint8_t i;

	flash_erase_page_aprom(store_data_base_addr);
	for(i = 0; i < binding_table_len; i++){
		flash_write_byte_aprom((store_data_base_addr+i), binding_table[i]);
	}

	return 1;
}

uint8_t srtnet_app_layer_binding_table_recover_callback_handler(uint8_t* binding_table, uint8_t binding_table_len)
{
	uint8_t i;
	for(i = 0; i < binding_table_len; i++){
		binding_table[i] = flash_read_byte((store_data_base_addr+i));
	}
	return 1;
}


void led_init(void)
{
	gpio_mode_config(1, 0, GPIO_MODE_PP);
	gpio_mode_config(1, 1, GPIO_MODE_PP);
}

void led_on(uint8_t led_num)
{
	switch(led_num){
		case 0:
		P10 = 0;
		break;

		case 1:
		P11 = 0;
		break;
			
		default:
		break;
	}
}

void led_off(uint8_t led_num)
{
	switch(led_num){
		case 0:
		P10 = 1;
		break;
		
		case 1:
		P11 = 1;
		break;
		
		default:
		break;
	}
}

void led_state_store(uint8_t led_state_mark)
{
	flash_write_byte_aprom((store_data_base_addr+64), led_state_mark);
}

uint8_t led_state_recover(void)
{
	uint8_t led_state_mark = flash_read_byte((store_data_base_addr+64));
	return led_state_mark;
}

void key_init(void)
{
	gpio_mode_config(1, 4, GPIO_MODE_BD);
	P14 = 1;
	gpio_mode_config(1, 5, GPIO_MODE_BD);
	P15 = 1;
}

uint8_t key_read_pin_level(uint8_t num)
{
	uint8_t ret = 1;
	switch(num){
		case 0:
		ret = P14;
		break;

		case 1:
		ret = P15;
		break;

		default:
		break;
	}
	return ret;
}

void Timer0_ISR_callback_handler()
{
	static uint8_t sys_tick  = 0;

	srtnet_comm_layer_tick_handler();
	sys_tick++;
	if(sys_tick > 125){	//5ms
		sys_tick = 0;
	sys_svc_software_timer_tick_handler();
	}
}

void feed_watchdog(void)
{
	wdt_feed_dog();
}



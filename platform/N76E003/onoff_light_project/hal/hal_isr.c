
#include "hal.h"

void (*exint0_ISR_callback)(void) = 0;
void exint0_ISR() interrupt 0       //INT0�ж����
{
	if(exint0_ISR_callback != 0){
		exint0_ISR_callback();
	}
}

void (*WakeUp_Timer_ISR_callback)(void) = 0;
void WakeUp_Timer_ISR (void)   interrupt 17     //ISR for self wake-up timer
{
	clr_WKTF;
	if(WakeUp_Timer_ISR_callback != 0){
		WakeUp_Timer_ISR_callback();
	}
}

#include "bsp/bsp_rf_driver.h"
extern uint8_t software_timer_task_tick_handler_flag;
//void (*Timer0_ISR_callback)(void) = 0;
void Timer0_ISR (void) interrupt 1              //interrupt address is 0x000B
{  
//	if(Timer0_ISR_callback != 0){
//		Timer0_ISR_callback();
//	}
	static uint8_t sys_tick  = 0;
	
	bsp_rf_receive_tick_handler();
	sys_tick++;
	if(sys_tick > 125){	//5ms
		sys_tick = 0;
		software_timer_task_tick_handler_flag = 1;
	}
}

void (*Timer3_ISR_callback)(void) = 0;
void Timer3_ISR (void) interrupt 16 
{
    clr_TF3;
	if(Timer3_ISR_callback != 0){
		Timer3_ISR_callback();
	}
}

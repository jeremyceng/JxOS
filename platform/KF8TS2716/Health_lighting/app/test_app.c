

#include "jxos_public.h"


static swtime_type test_swt;
void test_app(void)
{
	if(sys_svc_software_timer_check_overtime(test_swt) == 1){
		printf_string("----------------test_app\r\n");
	}
}

void test_app_init(void)
{
	uint8_t i;
	test_swt = sys_svc_software_timer_new();
	i = (uint8_t)test_swt;
	printf_byte_hex(i);
	printf_string(" test_swt\r\n");
	sys_svc_software_timer_set_time(test_swt, 1000);
	sys_svc_software_timer_restart(test_swt);
}

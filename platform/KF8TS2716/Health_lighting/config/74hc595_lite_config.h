
#include <KF8TS2716.h>

void delay_595(void);

#define hc_74hc595_sclr_hihgt_config()      //in put data enable
#define hc_74hc595_sclr_low_config()        //in put data set to 0

#define hc_74hc595_sck_hihgt_config()       P2LR5 = 1   //in put data shift a bit (Qh->move out, Qg->Qh ... Qa->Qb, new bit->Qa)
#define hc_74hc595_sck_low_config()         P2LR5 = 0   //in put data lock

#define hc_74hc595_rck_hihgt_config()       P2LR4 = 1   //out put data reload
#define hc_74hc595_rck_low_config()         P2LR4 = 0   //out put data lock

#define hc_74hc595_oe_hihgt_config()        //out put disable
#define hc_74hc595_oe_low_config()          //out put enable

#define hc_74hc595_si_hihgt_config()        P2LR3 = 1	//in put bit 1
#define hc_74hc595_si_low_config()          P2LR3 = 0	//in put bit 0

#define hc_74hc595_delay_config()           delay_595()

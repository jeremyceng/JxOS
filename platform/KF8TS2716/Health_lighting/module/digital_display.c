

#include <KF8TS2716.h>
#include "type.h"


#define _digital_init()      LEDOMS0=0x00;\
                            LEDOMS1=0x00;\
                            LEDODS0=0xFF;\
                            LEDODS1=0xFF;\
                            LEDLUM=0x01;\
                            LEDDATA2=0X00;\
                            LEDDATA1=0X00;\
                            LEDDATA0=0X00;\
                            LEDCTL=0x88;\
                            TR30 = 0;\
                            TR31 = 0;\
                            TR32 = 0;\
                            TR33 = 0;\
                            TR34 = 0;\
                            TR35 = 0;\
                            TR36 = 0;\
                            TR00 = 0;\
                            TR01 = 0;\
                            TR03 = 0;\
                            LEDEN = 0;

#define _digital_1(n)        LEDDATA2 = n
#define _digital_10(n)       LEDDATA1 = n                       
#define _digital_100(n)      LEDDATA0 = n     

static uint8_t __get_unit(uint16_t decimal , uint8_t unit)
{
    uint8_t ret;
    uint16_t num = decimal ;

    if((decimal  < 10000)&&(unit == 4)){
        return 10;
    }
    ret = 0;
    while (num >= 10000)
    {
        num -= 10000;
        ret++;
    }
    if(unit == 4){
        return ret;
    }

    if((decimal  < 1000)&&(unit == 3)){
        return 10;
    }
    ret = 0;
    while (num >= 1000)
    {
        num -= 1000;
        ret++;
    }
    if(unit == 3){
        return ret;
    }

    if((decimal  < 100)&&(unit == 2)){
        return 10;
    }
    ret = 0;
    while (num >= 100)
    {
        num -= 100;
        ret++;
    }
    if(unit == 2){
        return ret;
    }

    if((decimal  < 10)&&(unit == 1)){
        return 10;
    }
    ret = 0;
    while (num >= 10)
    {
        num -= 10;
        ret++;
    }
    if(unit == 1){
        return ret;
    }

    // if((decimal  < 1)&&(unit == 0)){
    //     return 10;
    // }
    ret = 0;
    while (num >= 1)
    {
        num -= 1;
        ret++;
    }
    if(unit == 0){
        return ret;
    }
    return 10;
}

static uint8_t get_code(uint8_t n)
{
	if(n == 0){
		n = 0X82;
	}
	else if(n == 1){
		n = 0XDB;
	}
	else if(n == 2){
		n = 0XA1;
	}
	else if(n == 3){
		n = 0X91;
	}
	else if(n == 4){
		n = 0XD8;
	}
	else if(n == 5){
		n = 0X94;
	}
	else if(n == 6){
		n = 0X84;
	}
	else if(n == 7){
		n = 0XD3;
	}
	else if(n == 8){
		n = 0X00;
	}
	else if(n == 9){
		n = 0X90;
	}
	else if(n == 'A'){
		n = 0XC0;
	}
	else if(n == 'F'){
		n = 0XE4;
	}
	else if(n == 'H'){
		n = 0XC8;
	}

	else{
		n = 0xFF;
	}
    return n;
}

void digital_display_init(void)
{
    _digital_init();
    _digital_1(0xff);
    _digital_10(0xff);
    _digital_100(0xff);
}

void digital_display_single(uint8_t bit, uint8_t dis_data)
{
	uint8_t n;
	n = get_code(dis_data);

    if(bit == 0){
        _digital_1(n);
    }
    else if(bit == 1){
        _digital_10(n);
    }
    else if(bit == 2){
	    _digital_100(n);
    }
}

void digital_display_show(uint16_t num)
{
	uint8_t n;
	n = __get_unit(num, 2);
	n = get_code(n);
	_digital_100(n);

	n = __get_unit(num, 1);
	n = get_code(n);
	_digital_10(n);

	n = __get_unit(num, 0);
	n = get_code(n);
	_digital_1(n);
}

void digital_display_set_lum(uint8_t lum) //0 ~ 7
{
    uint8_t temp;
    if(lum == 0){
        LEDEN = 0;
    }
    else{
        LEDEN = 1;
        lum--;
        lum = 7 - lum;
        temp = LEDLUM;
        temp = temp&0xf8;
        temp += lum;
        LEDLUM = temp;
    }
}


#ifndef __LED__H
#define __LED__H

#include <KF8TS2716.h>
#include "type.h"

#define led_init()		P0LR4 = 0;P0LR5 = 0;P2LR2 = 0;TR04 = 0;TR05 = 0;TR22 = 0

#define led_b_on()		P0LR4 = 1
#define led_b_off()		P0LR4 = 0

#define led_r_on()		P0LR5 = 1
#define led_r_off()		P0LR5 = 0

#define led_g_on()		P2LR2 = 1
#define led_g_off()		P2LR2 = 0

void pwm_led_init(void);

void pwm_led_b_set(uint8_t duty);
void pwm_led_r_set(uint8_t duty);
void pwm_led_g_set(uint8_t duty);

#endif


#include "sc92f_conf.h"
#include "jxos_public.h"

/*******************************************************************/
void touch_handler_task_init(void);
void event_tick_task_init(void);

void _my_fputc(char ch)
{
	ch = ch;
	//USCI0_UART_SendData8(ch);
	//while(!USCI0_GetFlagStatus(USCI0_UART_FLAG_TI));
	//USCI0_ClearFlag(USCI0_UART_FLAG_TI);
}

void jxos_user_init_callback(void)
{
	GPIO_WriteLow(GPIO0,GPIO_PIN_0);		//light
	GPIO_WriteLow(GPIO0,GPIO_PIN_1);		//uv light
	GPIO_WriteLow(GPIO0,GPIO_PIN_3);		//fan hight
	GPIO_WriteLow(GPIO0,GPIO_PIN_2);		//fan low
	GPIO_WriteHigh(GPIO0,GPIO_PIN_4);		//led fan hight
	GPIO_WriteHigh(GPIO0,GPIO_PIN_5);		//led fan low
	GPIO_WriteHigh(GPIO2,GPIO_PIN_0);		//led time
	GPIO_WriteHigh(GPIO2,GPIO_PIN_1);		//led time 
	GPIO_WriteHigh(GPIO2,GPIO_PIN_4);		//led time 
	GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//led time 

	GPIO_Init(GPIO0,GPIO_PIN_0,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0,GPIO_PIN_1,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0,GPIO_PIN_3,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0,GPIO_PIN_2,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0,GPIO_PIN_4,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0,GPIO_PIN_5,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2,GPIO_PIN_0,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2,GPIO_PIN_1,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2,GPIO_PIN_4,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2,GPIO_PIN_5,GPIO_MODE_OUT_PP);
	
	SC92L8532_NIO_Init();

	//GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_IN_PU);
	//USCI0_ITConfig(DISABLE,LOW);
	//USCI0_UART_Init(32000000, 521000, USCI0_UART_Mode_10B,USCI0_UART_RX_DISABLE);

	PWM_Init(PWM_PRESSEL_FHRC_D1,32000);	//32MHz / 32000 = 1KHz
	/*PWM波形无反相*/PWM_PolarityConfig(PWM0, PWM_POLARITY_NON_INVERT);
	/*PWM0 独立模式*/PWM_IndependentModeConfig(PWM0, 0);
	PWM_ITConfig(DISABLE,LOW);
	PWM_Aligned_Mode_Select(PWM0_Edge_Aligned_Mode);
	PWM_FaultDetectionConfigEX(PWM0_Type,DISABLE);
	PWM_OutputStateConfig(PWM0, PWM_OUTPUTSTATE_ENABLE);
	PWM_CmdEX(PWM0_Type,ENABLE);
}

void jxos_user_task_init_callback(void)
{
	touch_handler_task_init();
	event_tick_task_init();
}

void jxos_prepare_to_run_callback(void)
{
	EA = 1;
	printf_string("jxos_prepare_to_run_callback...\r\n");
}


/*******************************************************************/
void sys_svc_software_timer_init_callback(void)
{
	//-----------------------------
	//1 tick = 1/FREQ sec; 
	//1 sec = FREQ tick;
	//0.0005 sce = FREQ*0.0005 tick
	//-----------------------------
	//FREQ = Fsys/12; Fsys = 32000000;
	//0.0005 sce = (32000000/12)*0.0005 tick = 13333
	//-----------------------------
	TMOD |= 0x01;			   
	TL0 = (65536 - 13333)%256;   
	TH0 = (65536 - 13333)/256;
	TR0 = 0;
	ET0 = 1;
	TR0 = 1;
}


/*******************************************************************/
#define BUTTON_TIME		 	0
#define BUTTON_FAN 			1
#define BUTTON_ONOFF 		2

extern unsigned long exKeyValueFlag;
static bool_t onoff_longpress_flag = false;

void off_handler(void);
void light_ctrl_handler(void);
void time_ctrl_handler(void);
void fan_ctrl_handler(void);

void std_app_button_scan_init_callback(void)
{
	onoff_longpress_flag = false;
}

bool_t std_app_button_scan_get_pin_level_callback(uint8_t button_num)
{
	uint8_t ret = 1;
	switch (button_num)
	{
	case BUTTON_TIME:
		ret = exKeyValueFlag>>14;
		ret = ret&0x01;
		break;
	case BUTTON_FAN:
		ret = exKeyValueFlag>>15;
		ret = ret&0x01;
		break;
	case BUTTON_ONOFF:
		ret = exKeyValueFlag>>22;
		ret = ret&0x01;
		break;
	default:
		break;
	}

	return ret;
}

void std_app_button_scan_task_button_press_event_callback(uint8_t button_num)
{
	button_num = button_num;
}

void std_app_button_scan_task_button_release_event_callback(uint8_t button_num)
{
	switch (button_num){
	case BUTTON_TIME:
		time_ctrl_handler();
		break;
	case BUTTON_FAN:
		fan_ctrl_handler();
		break;
	case BUTTON_ONOFF:
		if(onoff_longpress_flag == false){
			light_ctrl_handler();
		}
		onoff_longpress_flag = false;
		break;
	default:
		break;
	}
}

void std_app_button_scan_task_button_long_press_event_callback(uint8_t button_num)
{
	switch (button_num){
	case BUTTON_ONOFF:
		off_handler();
		onoff_longpress_flag = true;
		break;
	default:
		break;
	}	
}

void std_app_button_scan_task_button_long_press_repeat_event_callback(uint8_t button_num)
{
	button_num = button_num;
}


/*******************************************************************/
void std_app_value_move_value_changed_event_callback(uint8_t value_move_num, uint16_t changed_value)
{
	if(value_move_num == 0){
		PWM_IndependentModeConfig(PWM0, changed_value);
		//printf_16bit_hex(changed_value);
		//printf_string(" changed_value\r\n");
	}
}

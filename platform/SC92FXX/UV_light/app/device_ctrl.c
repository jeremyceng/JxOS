
#include "sc92f_conf.h"

#include "jxos_public.h"

void light_level_ctrl(uint8_t light_level)
{
	switch (light_level){
	case 0:
		std_app_value_move_move_by_time(0, 0, 1000);
		//PWM_IndependentModeConfig(PWM0, 0);		//light off	
		break;
	case 1:
		std_app_value_move_move_by_time(0, 32000/4, 1000);
		//PWM_IndependentModeConfig(PWM0, 32000/4);		
		break;
	case 2:
		std_app_value_move_move_by_time(0, 32000/2, 1000);
		//PWM_IndependentModeConfig(PWM0, 32000/2);	
		break;
	case 3:
		std_app_value_move_move_by_time(0, (32000/4)*3, 1000);
		//PWM_IndependentModeConfig(PWM0, (32000/4)*3);	
		break;
	case 4:
		std_app_value_move_move_by_time(0, 32000, 1000);
		//PWM_IndependentModeConfig(PWM0, 32000);	
		break;
	default:
		break;
	}
}

void fan_level_ctrl(uint8_t fan_level)
{
	switch (fan_level){
	case 0:
		GPIO_WriteLow(GPIO0,GPIO_PIN_3);		//fan hight		0
		GPIO_WriteLow(GPIO0,GPIO_PIN_2);		//fan low		0

		GPIO_WriteHigh(GPIO0,GPIO_PIN_4);		//led fan hight off
		GPIO_WriteHigh(GPIO0,GPIO_PIN_5);		//led fan low	off

		GPIO_WriteLow(GPIO0,GPIO_PIN_1);		//uv light		off
		break;

	case 1:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_3);		//fan hight		1
		GPIO_WriteHigh(GPIO0,GPIO_PIN_2);		//fan low		1

		GPIO_WriteHigh(GPIO0,GPIO_PIN_4);		//led fan hight off
		GPIO_WriteLow(GPIO0,GPIO_PIN_5);		//led fan low	on

		GPIO_WriteHigh(GPIO0,GPIO_PIN_1);		//uv light		on
		break;

	case 2:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_3);		//fan hight		1
		GPIO_WriteLow(GPIO0,GPIO_PIN_2);		//fan low		0

		GPIO_WriteLow(GPIO0,GPIO_PIN_4);		//led fan hight on
		GPIO_WriteLow(GPIO0,GPIO_PIN_5);		//led fan low	on

		GPIO_WriteHigh(GPIO0,GPIO_PIN_1);		//uv light		on
		break;

	default:
		break;
	}
}

void time_led_ctrl(uint16_t time)
{
	if(time == 0){
		printf_string("time_led_ctrl 0\r\n");
		GPIO_WriteHigh(GPIO2,GPIO_PIN_0);		//led time
		GPIO_WriteHigh(GPIO2,GPIO_PIN_1);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_4);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//led time 
	}else if(time <= 15*60){
		printf_string("time_led_ctrl 15\r\n");
		GPIO_WriteLow(GPIO2,GPIO_PIN_0);		//led time
		GPIO_WriteHigh(GPIO2,GPIO_PIN_1);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_4);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//led time 
	}else if(time <= 30*60){
		printf_string("time_led_ctrl 30\r\n");
		GPIO_WriteLow(GPIO2,GPIO_PIN_0);		//led time
		GPIO_WriteLow(GPIO2,GPIO_PIN_1);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_4);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//led time 
	}else if(time <= 45*60){
		printf_string("time_led_ctrl 45\r\n");
		GPIO_WriteLow(GPIO2,GPIO_PIN_0);		//led time
		GPIO_WriteLow(GPIO2,GPIO_PIN_1);		//led time 
		GPIO_WriteLow(GPIO2,GPIO_PIN_4);		//led time 
		GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//led time 
	}else if(time <= 60*60){
		printf_string("time_led_ctrl 60\r\n");
		GPIO_WriteLow(GPIO2,GPIO_PIN_0);		//led time
		GPIO_WriteLow(GPIO2,GPIO_PIN_1);		//led time 
		GPIO_WriteLow(GPIO2,GPIO_PIN_4);		//led time 
		GPIO_WriteLow(GPIO2,GPIO_PIN_5);		//led time 
	}
	else{
		printf_string("time_led_ctrl else\r\n");
	}
}
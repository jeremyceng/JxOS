#include "jxos_public.h"
#include "..\Drivers\TKDriver\C\TKDriver.h"
#include "sc92f_conf.h"

unsigned long exKeyValueFlag = 0;

static void touch_handler_task(void)
{
    if(SOCAPI_TouchKeyStatus & 0x80)
    {
        /*<UserCodeStart>*//*<SinOne-Tag><22>*/
        SOCAPI_TouchKeyStatus &= 0x7f;
        /*<UserCodeEnd>*//*<SinOne-Tag><22>*/
        /*<UserCodeStart>*//*<SinOne-Tag><18>*/
        exKeyValueFlag = TouchKeyScan();
        /*<UserCodeEnd>*//*<SinOne-Tag><18>*/
        /*<UserCodeStart>*//*<SinOne-Tag><36>*/
        TouchKeyRestart();
        /*<UserCodeEnd>*//*<SinOne-Tag><36>*/
    }
}

void touch_handler_task_init(void)
{
    TouchKeyInit();
    exKeyValueFlag = 0;
	jxos_task_create(touch_handler_task, "app1", 0);
}


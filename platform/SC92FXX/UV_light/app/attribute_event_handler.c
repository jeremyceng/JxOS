
#include "driver/value_table.h"
#include "jxos_public.h"


void light_level_ctrl(uint8_t light_level);
void fan_level_ctrl(uint8_t fan_level);
void time_led_ctrl(uint16_t time);

//event -> attributer -> hardware

#define ATTR_TIME_COUNTER		0
#define ATTR_LIGHT_LEVEL		1
#define ATTR_FAN_LEVE		   	2
#define ATTR_RUN_STATE			3

#define STATE_OFF		0
#define STATE_ON		1

void light_attribute_changed_handler(uint16_t attribute_id, uint16_t changed_attribute)
{
	uint16_t temp_time_counter;
	uint16_t temp_light_level;
	uint16_t temp_fan_level;
	uint16_t temp_run_state;

	value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);
	value_table_get_value(ATTR_LIGHT_LEVEL, &temp_light_level);
	value_table_get_value(ATTR_FAN_LEVE, &temp_fan_level);
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

	printf_string("-----------------------\r\n");
	printf_16bit_hex(attribute_id);
	printf_string(" attribute_id\r\n");
	printf_16bit_hex(changed_attribute);
	printf_string(" changed_attribute\r\n");
	printf_string("-----------------------\r\n");

	switch (attribute_id){
	case ATTR_TIME_COUNTER:
		time_led_ctrl(changed_attribute);
		break;

	case ATTR_LIGHT_LEVEL:
		light_level_ctrl(changed_attribute);
		break;

	case ATTR_FAN_LEVE:
		fan_level_ctrl(changed_attribute);
		break;

	case ATTR_RUN_STATE:
		switch (changed_attribute){
		case STATE_OFF:
			light_level_ctrl(0);
			fan_level_ctrl(0);
			time_led_ctrl(0);
			break;

		case STATE_ON:
			light_level_ctrl(temp_light_level);
			fan_level_ctrl(temp_fan_level);
			time_led_ctrl(temp_time_counter);
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}
}

/*****************************************************************************/
void light_ctrl_handler(void)
{
	uint16_t temp_run_state;
	uint16_t temp_light_level;
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);
	value_table_get_value(ATTR_LIGHT_LEVEL, &temp_light_level);

	switch (temp_run_state){
	case STATE_OFF:
		value_table_set_value(ATTR_RUN_STATE, STATE_ON);
		value_table_set_value(ATTR_LIGHT_LEVEL, 1);
		value_table_set_value(ATTR_FAN_LEVE, 1);
		value_table_set_value(ATTR_TIME_COUNTER, 60*60);
		break;

	case STATE_ON:
		temp_light_level++;
		if(temp_light_level > 4){
			temp_light_level = 0;
		}
		value_table_set_value(ATTR_LIGHT_LEVEL, temp_light_level);
		break;

	default:
		break;
	}
}

void off_handler(void)
{
	uint16_t temp_run_state;
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

	if(temp_run_state != STATE_OFF){
		value_table_set_value(ATTR_RUN_STATE, STATE_OFF);
		value_table_set_value(ATTR_LIGHT_LEVEL, 0);
		value_table_set_value(ATTR_FAN_LEVE, 0);
		value_table_set_value(ATTR_TIME_COUNTER, 0);
	}
}

void time_ctrl_handler(void)
{
	uint16_t temp_time_counter;
	uint16_t temp_run_state;

	value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

	if(temp_run_state != STATE_OFF){
		temp_time_counter += 15*60;
		if(temp_time_counter > 60*60){
			temp_time_counter = 15*60;
		}
		value_table_set_value(ATTR_TIME_COUNTER, temp_time_counter);
	}
}

void fan_ctrl_handler(void)
{
	uint16_t temp_fan_level;
	uint16_t temp_run_state;

	value_table_get_value(ATTR_FAN_LEVE, &temp_fan_level);
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

	if(temp_run_state != STATE_OFF){
		temp_fan_level++;
		if(temp_fan_level > 2){
			temp_fan_level = 1;
		}
		value_table_set_value(ATTR_FAN_LEVE, temp_fan_level);
	}
}

static void timer_tick_handler(void)
{
	uint16_t temp_run_state;
	uint16_t temp_time_counter;
	value_table_get_value(ATTR_RUN_STATE, &temp_run_state);
	value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);

	if(temp_run_state != STATE_OFF){
		temp_time_counter--;
		value_table_set_value(ATTR_TIME_COUNTER, temp_time_counter);
		if(temp_time_counter == 0){
			value_table_set_value(ATTR_RUN_STATE, STATE_OFF);
			value_table_set_value(ATTR_LIGHT_LEVEL, 0);
			value_table_set_value(ATTR_FAN_LEVE, 0);
		}
	}
}

/*****************************************************************************/
static swtime_type event_tick_swt;
static void event_tick_task(void)
{
	if(sys_svc_software_timer_check_overtime(event_tick_swt)){
		timer_tick_handler();
	}
}

void event_tick_task_init(void)
{
	value_table_init();
	value_table_set_value(ATTR_RUN_STATE, STATE_OFF);
	value_table_set_value(ATTR_LIGHT_LEVEL, 0);
	value_table_set_value(ATTR_FAN_LEVE, 0);
	value_table_set_value(ATTR_TIME_COUNTER, 0);

	std_app_value_move_set_current_value(0, 0);

	jxos_task_create(event_tick_task, "e", 0);

	event_tick_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(event_tick_swt, 1000);	//1sec
	sys_svc_software_timer_restart(event_tick_swt);
}

//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_adc.h
//	作者		:
//	模块功能	: ADC固件库函数头文件
//  最后更正日期: 2022/01/24
// 	版本		: V1.100013
//  说明        :
//*************************************************************

#ifndef _sc92f_ADC_H_
#define _sc92f_ADC_H_

//头文件引用
#include "sc92f.h"
//说明:ADC参考电源枚举定义在sc92f_option.h，使用ADC请注意将该文件导入
#include "sc92f_option.h"

#if !defined(SC92F827X) && !defined(SC92F837X)

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2) || defined(SC92FWxx)
typedef enum
{
  ADC_PRESSEL_FOSC_D6  = (uint8_t)0x02, //预分频 fADC = fHRC/6
  ADC_PRESSEL_FOSC_D12 = (uint8_t)0x01  //预分频 fADC = fHRC/12
}ADC_PresSel_TypeDef;
#elif defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)\
	|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
typedef enum
{
  ADC_PRESSEL_FHRC_D32 = (uint8_t)0x00, //预分频 fADC = fHRC/32
  ADC_PRESSEL_FHRC_D24 = (uint8_t)0x01, //预分频 fADC = fHRC/24
  ADC_PRESSEL_FHRC_D16 = (uint8_t)0x02, //预分频 fADC = fHRC/16
  ADC_PRESSEL_FHRC_D12 = (uint8_t)0x03, //预分频 fADC = fHRC/12
  ADC_PRESSEL_FHRC_D8 = (uint8_t)0x04,  //预分频 fADC = fHRC/8
  ADC_PRESSEL_FHRC_D6 = (uint8_t)0x05,  //预分频 fADC = fHRC/6
  ADC_PRESSEL_FHRC_D4 = (uint8_t)0x06,  //预分频 fADC = fHRC/4
  ADC_PRESSEL_FHRC_D3 = (uint8_t)0x07   //预分频 fADC = fHRC/3
} ADC_PresSel_TypeDef;
#elif defined (SC92F742x) || defined (SC92F730x) || defined (SC92F725X) || defined (SC92F735X) || defined (SC92F732X) || defined (SC92F7490)\
	|| defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
typedef enum
{
  ADC_PRESSEL_2_MHz = (uint8_t)0x00,  //预分频 fADC = 2MHz
  ADC_PRESSEL_333_kHz = (uint8_t)0x20 //预分频 fADC = 333kHz
} ADC_PresSel_TypeDef;
#elif defined(SC92F848x) || defined(SC92F748x)
typedef enum
{
  ADC_PRESSEL_FSYS_D16 = (uint8_t)0x00, //预分频 fADC = fSYS/16
  ADC_PRESSEL_FSYS_D12 = (uint8_t)0x01, //预分频 fADC = fSYS/12
  ADC_PRESSEL_FSYS_D8 = (uint8_t)0x02,  //预分频 fADC = fSYS/8
  ADC_PRESSEL_FSYS_D6 = (uint8_t)0x03,  //预分频 fADC = fSYS/6
  ADC_PRESSEL_FSYS_D4 = (uint8_t)0x04,  //预分频 fADC = fSYS/4
  ADC_PRESSEL_FSYS_D3 = (uint8_t)0x05,  //预分频 fADC = fSYS/3
  ADC_PRESSEL_FSYS_D2 = (uint8_t)0x06,  //预分频 fADC = fSYS/2
  ADC_PRESSEL_FSYS_D1 = (uint8_t)0x07   //预分频 fADC = fSYS/1
} ADC_PresSel_TypeDef;
#elif defined(SC92F859x) || defined(SC92F759x)
typedef enum
{
  ADC_PRESSEL_FSYS_D16 = (uint8_t)0x00, //预分频 fADC = fSYS/16
  ADC_PRESSEL_FSYS_D12 = (uint8_t)0x01, //预分频 fADC = fSYS/12
  ADC_PRESSEL_FSYS_D6 = (uint8_t)0x02,  //预分频 fADC = fSYS/6
  ADC_PRESSEL_FSYS_D4 = (uint8_t)0x03,  //预分频 fADC = fSYS/4
} ADC_PresSel_TypeDef;
#elif defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  ADC_PRESSEL_3CLOCK = (uint8_t)0x10,  //采样时间为3个系统时钟
  ADC_PRESSEL_6CLOCK = (uint8_t)0x14,  //采样时间为6个系统时钟
  ADC_PRESSEL_16CLOCK = (uint8_t)0x18, //采样时间为16个系统时钟
  ADC_PRESSEL_32CLOCK = (uint8_t)0x1c  //采样时间为32个系统时钟
} ADC_PresSel_TypeDef;
#endif

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
|| defined(SC92FWxx) || defined(SC92F859x)  || defined (SC92F759x)
typedef enum
{
  ADC_Cycle_6Cycle = (uint8_t)0x00, //ADC采样时间为6个ADC采样时钟周期
  ADC_Cycle_36Cycle = (uint8_t)0x04 //ADC采样时间为36个ADC采样时钟周期
} ADC_Cycle_TypeDef;
#elif defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined (SC92F83Ax) || defined (SC92F73Ax)\
	|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F7003) || defined(SC92F8003) || defined (SC92F740x) || defined (SC92F848x) || defined (SC92F748x) 
typedef enum
{
  ADC_Cycle_6Cycle = (uint8_t)0x00, //ADC采样时间为6个ADC采样时钟周期
  ADC_Cycle_36Cycle = (uint8_t)0x08 //ADC采样时间为36个ADC采样时钟周期
} ADC_Cycle_TypeDef;
#elif defined (SC92F742x) || defined (SC92F730x) || defined (SC92F725X) || defined (SC92F735X) || defined (SC92F732X)\
|| defined (SC92F7490) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  ADC_Cycle_Null = (uint8_t)0x00, 
} ADC_Cycle_TypeDef;
#endif

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
		|| defined(SC92FWxx) || defined(SC92F859x) || defined(SC92F759x)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_2 = (uint8_t)0x02,     //选择AIN2做AD输入口
  ADC_CHANNEL_3 = (uint8_t)0x03,     //选择AIN3做AD输入口
  ADC_CHANNEL_4 = (uint8_t)0x04,     //选择AIN4做AD输入口
  ADC_CHANNEL_5 = (uint8_t)0x05,     //选择AIN5做AD输入口
  ADC_CHANNEL_6 = (uint8_t)0x06,     //选择AIN6做AD输入口
  ADC_CHANNEL_7 = (uint8_t)0x07,     //选择AIN7做AD输入口
  ADC_CHANNEL_8 = (uint8_t)0x08,     //选择AIN8做AD输入口
  ADC_CHANNEL_9 = (uint8_t)0x09,     //选择AIN9做AD输入口
  ADC_CHANNEL_10 = (uint8_t)0x0A,    //选择AIN10做AD输入口
  ADC_CHANNEL_11 = (uint8_t)0x0B,    //选择AIN11做AD输入口
  ADC_CHANNEL_12 = (uint8_t)0x0C,    //选择AIN12做AD输入口
  ADC_CHANNEL_13 = (uint8_t)0x0D,    //选择AIN13做AD输入口
  ADC_CHANNEL_14 = (uint8_t)0x0E,    //选择AIN14做AD输入口
  ADC_CHANNEL_15 = (uint8_t)0x0F,    //选择AIN15做AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x1f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#elif defined(SC92F7003) || defined(SC92F8003) || defined(SC92F740x)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_2 = (uint8_t)0x02,     //选择AIN2做AD输入口
  ADC_CHANNEL_3 = (uint8_t)0x03,     //选择AIN3做AD输入口
  ADC_CHANNEL_4 = (uint8_t)0x04,     //选择AIN4做AD输入口
  ADC_CHANNEL_5 = (uint8_t)0x05,     //选择AIN5做AD输入口
  ADC_CHANNEL_6 = (uint8_t)0x06,     //选择AIN6做AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x1f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#elif defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F83Ax)\
|| defined(SC92F73Ax) || defined(SC92F84Ax) || defined(SC92F74Ax) || defined(SC92F742x) || defined(SC92F725X)\
|| defined(SC92F735X) || defined(SC92F732X) || defined(SC92F848x) || defined(SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_2 = (uint8_t)0x02,     //选择AIN2做AD输入口
  ADC_CHANNEL_3 = (uint8_t)0x03,     //选择AIN3做AD输入口
  ADC_CHANNEL_4 = (uint8_t)0x04,     //选择AIN4做AD输入口
  ADC_CHANNEL_5 = (uint8_t)0x05,     //选择AIN5做AD输入口
  ADC_CHANNEL_6 = (uint8_t)0x06,     //选择AIN6做AD输入口
  ADC_CHANNEL_7 = (uint8_t)0x07,     //选择AIN7做AD输入口
  ADC_CHANNEL_8 = (uint8_t)0x08,     //选择AIN8做AD输入口
  ADC_CHANNEL_9 = (uint8_t)0x09,     //选择AIN9做AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x1f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#elif defined(SC92F730x)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_6 = (uint8_t)0x06,     //选择AIN6做AD输入口
  ADC_CHANNEL_7 = (uint8_t)0x07,     //选择AIN7做AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x1f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#elif defined(SC92F7490)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x1f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#elif defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
typedef enum
{
  ADC_CHANNEL_0 = (uint8_t)0x00,     //选择AIN0做AD输入口
  ADC_CHANNEL_1 = (uint8_t)0x01,     //选择AIN1做AD输入口
  ADC_CHANNEL_2 = (uint8_t)0x02,     //选择AIN2做AD输入口
  ADC_CHANNEL_3 = (uint8_t)0x03,     //选择AIN3做AD输入口
  ADC_CHANNEL_4 = (uint8_t)0x04,     //选择AIN4做AD输入口
  ADC_CHANNEL_5 = (uint8_t)0x05,     //选择AIN5做AD输入口
  ADC_CHANNEL_6 = (uint8_t)0x06,     //选择AIN6做AD输入口
  ADC_CHANNEL_7 = (uint8_t)0x07,     //选择AIN7做AD输入口
  ADC_CHANNEL_8 = (uint8_t)0x08,     //选择AIN8做AD输入口
  ADC_CHANNEL_9 = (uint8_t)0x09,     //选择AIN9做AD输入口
  ADC_CHANNEL_9_PGA = (uint8_t)0x19, //选择AIN9做PGA输入口
  ADC_CHANNEL_Temp = (uint8_t)0x0e,  //选择内部温度传感器做为AD输入口
  ADC_CHANNEL_VDD_D4 = (uint8_t)0x0f //选择内部1/4VDD做AD输入口
} ADC_Channel_TypeDef;
#endif

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
		|| defined(SC92FWxx) || defined(SC92F859x) || defined(SC92F759x)
typedef enum
{
  ADC_EAIN_0 = (uint16_t)0x0001,  //选择AIN0
  ADC_EAIN_1 = (uint16_t)0x0002,  //选择AIN1
  ADC_EAIN_2 = (uint16_t)0x0004,  //选择AIN2
  ADC_EAIN_3 = (uint16_t)0x0008,  //选择AIN3
  ADC_EAIN_4 = (uint16_t)0x0010,  //选择AIN4
  ADC_EAIN_5 = (uint16_t)0x0020,  //选择AIN5
  ADC_EAIN_6 = (uint16_t)0x0040,  //选择AIN6
  ADC_EAIN_7 = (uint16_t)0x0080,  //选择AIN7
  ADC_EAIN_8 = (uint16_t)0x0100,  //选择AIN8
  ADC_EAIN_9 = (uint16_t)0x0200,  //选择AIN9
  ADC_EAIN_10 = (uint16_t)0x0400, //选择AIN10
  ADC_EAIN_11 = (uint16_t)0x0800, //选择AIN11
  ADC_EAIN_12 = (uint16_t)0x1000, //选择AIN12
  ADC_EAIN_13 = (uint16_t)0x2000, //选择AIN13
  ADC_EAIN_14 = (uint16_t)0x4000, //选择AIN14
  ADC_EAIN_15 = (uint16_t)0x8000  //选择AIN15
} ADC_EAIN_TypeDef;
#elif defined(SC92F7003) || defined(SC92F8003) || defined(SC92F740x)
typedef enum
{
  ADC_EAIN_0 = (uint16_t)0x0001, //选择AIN0
  ADC_EAIN_1 = (uint16_t)0x0002, //选择AIN1
  ADC_EAIN_2 = (uint16_t)0x0004, //选择AIN2
  ADC_EAIN_3 = (uint16_t)0x0008, //选择AIN3
  ADC_EAIN_4 = (uint16_t)0x0010, //选择AIN4
  ADC_EAIN_5 = (uint16_t)0x0020, //选择AIN5
  ADC_EAIN_6 = (uint16_t)0x0040  //选择AIN6
} ADC_EAIN_TypeDef;
#elif defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F83Ax)\
|| defined(SC92F73Ax) || defined(SC92F84Ax) || defined(SC92F74Ax) || defined(SC92F742x) || defined(SC92F725X)\
|| defined(SC92F735X) || defined(SC92F732X) || defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)\
|| defined(SC92F848x) || defined(SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  ADC_EAIN_0 = (uint16_t)0x0001, //选择AIN0
  ADC_EAIN_1 = (uint16_t)0x0002, //选择AIN1
  ADC_EAIN_2 = (uint16_t)0x0004, //选择AIN2
  ADC_EAIN_3 = (uint16_t)0x0008, //选择AIN3
  ADC_EAIN_4 = (uint16_t)0x0010, //选择AIN4
  ADC_EAIN_5 = (uint16_t)0x0020, //选择AIN5
  ADC_EAIN_6 = (uint16_t)0x0040, //选择AIN6
  ADC_EAIN_7 = (uint16_t)0x0080, //选择AIN7
  ADC_EAIN_8 = (uint16_t)0x0100, //选择AIN8
  ADC_EAIN_9 = (uint16_t)0x0200, //选择AIN9
} ADC_EAIN_TypeDef;
#elif defined(SC92F730x)
typedef enum
{
  ADC_EAIN_0 = (uint16_t)0x0001, //选择AIN0
  ADC_EAIN_1 = (uint16_t)0x0002, //选择AIN1
  ADC_EAIN_6 = (uint16_t)0x0040, //选择AIN6
  ADC_EAIN_7 = (uint16_t)0x0080, //选择AIN7
} ADC_EAIN_TypeDef;
#elif defined(SC92F7490)
typedef enum
{
  ADC_EAIN_0 = (uint16_t)0x0001, //选择AIN0
  ADC_EAIN_1 = (uint16_t)0x0002, //选择AIN1
} ADC_EAIN_TypeDef;
#endif

#if defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
typedef enum
{
  ADC_PGACOM_0V = 0x00,   //共模电压为0V
  ADC_PGACOM_1_2V = 0x40, //共模电压为1.2V
} ADC_PGACOM_TypeDef;

typedef enum
{
  ADC_PGAGAN_NonInvert20 = 0x00,  //同相增益为20，反相增益为19
  ADC_PGAGAN_NonInvert100 = 0x20, //同相增益为100，反相增益为99
} ADC_PGAGAN_TypeDef;

typedef enum
{
  ADC_PGAIPT_NonInvert = 0x00, //同相输入
  ADC_PGAIPT_Invert = 0x10,    //反相增输入
} ADC_PGAIPT_TypeDef;

void ADC_TSCmd(FunctionalState NewState);
void ADC_CHOPConfig(PriorityStatus NewState);
uint16_t ADC_Get_TS_StandardData(void);
float ADC_GetTSValue(void);

void ADC_PGAConfig(ADC_PGACOM_TypeDef ADC_PGACOM, ADC_PGAGAN_TypeDef ADC_PGAGAN, ADC_PGAIPT_TypeDef ADC_PGAIPT);
void ADC_PGACmd(PriorityStatus NewState);

#endif

void ADC_DeInit(void);
void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection,ADC_Cycle_TypeDef ADC_Cycle);
void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel,
                       FunctionalState NewState);
void ADC_Cmd(FunctionalState NewState);
void ADC_StartConversion(void);
uint16_t ADC_GetConversionValue(void);
void ADC_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority);
FlagStatus ADC_GetFlagStatus(void);
void ADC_ClearFlag(void);
void ADC_EAINConfig(uint16_t ADC_EAIN_Select,
                    FunctionalState NewState);
void ADC_VrefConfig(ADC_Vref_TypeDef ADC_Vref);
#endif

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
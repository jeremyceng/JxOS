//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_pwm.h
//	作者		:
//	模块功能	: PWM固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2021/02/23
// 	版本		: V1.10004
//  说明    :
//*************************************************************

#ifndef _sc92f_PWM_H_
#define	_sc92f_PWM_H_

#include "sc92f.h"
#if !defined (SC92F7490)
#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
		|| defined(SC92F859x) || defined(SC92F759x)
typedef enum
{
  PWM_PRESSEL_FHRC_D1  = (uint8_t)0x00,	   //PWM预分频为Fhrc/1
  PWM_PRESSEL_FHRC_D2  = (uint8_t)0x10,	   //PWM预分频为Fhrc/2
  PWM_PRESSEL_FHRC_D4  = (uint8_t)0x20,	   //PWM预分频为Fhrc/4
  PWM_PRESSEL_FHRC_D8  = (uint8_t)0x30	   //PWM预分频为Fhrc/8
} PWM_PresSel_TypeDef;

typedef enum
{
  PWM40 = (uint8_t)0x01,   //PWM输出通道选择:PWM40
  PWM41 = (uint8_t)0x02,   //PWM输出通道选择:PWM41
  PWM42 = (uint8_t)0x04,   //PWM输出通道选择:PWM42
  PWM43 = (uint8_t)0x08,   //PWM输出通道选择:PWM43
  PWM50 = (uint8_t)0x10,   //PWM输出通道选择:PWM50
  PWM51 = (uint8_t)0x20,   //PWM输出通道选择:PWM51
  PWM52 = (uint8_t)0x40,   //PWM输出通道选择:PWM52
  PWM53 = (uint8_t)0x80	   //PWM输出通道选择:PWM53
} PWM_OutputPin_TypeDef;
#elif defined (SC92F742x) || defined (SC92F730x) || defined (SC92F827X) || defined (SC92F837X)  || defined (SC92F725X) || defined(SC92F735X)
typedef enum
{
  PWM_PRESSEL_FSYS_D1  = (uint8_t)0x00,		//PWM预分频为Fsys/1
  PWM_PRESSEL_FSYS_D2  = (uint8_t)0x01,		//PWM预分频为Fsys/2
  PWM_PRESSEL_FSYS_D4  = (uint8_t)0x02,		//PWM预分频为Fsys/4
  PWM_PRESSEL_FSYS_D8  = (uint8_t)0x03,		//PWM预分频为Fsys/8
  PWM_PRESSEL_FSYS_D32 = (uint8_t)0x04,		//PWM预分频为Fsys/32
  PWM_PRESSEL_FSYS_D64 = (uint8_t)0x05,		//PWM预分频为Fsys/64
  PWM_PRESSEL_FSYS_D128 = (uint8_t)0x06,		//PWM预分频为Fsys/128
  PWM_PRESSEL_FSYS_D256 = (uint8_t)0x07		//PWM预分频为Fsys/256
} PWM_PresSel_TypeDef;

#if defined (SC92F827X)
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
} PWM_OutputPin_TypeDef;
#elif defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
	PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
} PWM_OutputPin_TypeDef;
#elif defined (SC92F837X)
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
	PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
	PWM3 = (uint8_t)0x08,						//PWM输出通道选择:PWM3
} PWM_OutputPin_TypeDef;
#elif defined (SC92F730x)
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
	PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
	PWM4 = (uint8_t)0x10,						//PWM输出通道选择:PWM4
	PWM5 = (uint8_t)0x20						//PWM输出通道选择:PWM5
} PWM_OutputPin_TypeDef;
#else
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
	PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
	PWM3 = (uint8_t)0x08,						//PWM输出通道选择:PWM3
	PWM4 = (uint8_t)0x10,						//PWM输出通道选择:PWM4
	PWM5 = (uint8_t)0x20						//PWM输出通道选择:PWM5
} PWM_OutputPin_TypeDef;
#endif

#elif defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F848x) || defined (SC92F748x) 
typedef enum
{
  PWM_PRESSEL_FOSC_D1  = (uint8_t)0x00,		//PWM预分频为Fosc/1
  PWM_PRESSEL_FOSC_D2  = (uint8_t)0x01,		//PWM预分频为Fosc/2
  PWM_PRESSEL_FOSC_D8  = (uint8_t)0x02,		//PWM预分频为Fosc/8
  PWM_PRESSEL_FOSC_D32 = (uint8_t)0x03		//PWM预分频为Fosc/32
} PWM_PresSel_TypeDef;
typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
  PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
  PWM3 = (uint8_t)0x08,						//PWM输出通道选择:PWM3
  PWM4 = (uint8_t)0x10,						//PWM输出通道选择:PWM4
  PWM5 = (uint8_t)0x20,						//PWM输出通道选择:PWM5
} PWM_OutputPin_TypeDef;

typedef enum
{
  PWM0PWM3 = (uint8_t)0x00,					//PWM互补模式通道选择:PWM0、PWM3
  PWM1PWM4 = (uint8_t)0x01,					//PWM互补模式通道选择:PWM1、PWM4
  PWM2PWM5 = (uint8_t)0x02					//PWM互补模式通道选择:PWM2、PWM5
} PWM_ComplementaryOutputPin_TypeDef;
#elif defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
typedef enum
{
  PWM_PRESSEL_FOSC_D1  = (uint8_t)0x00,		//PWM预分频为Fosc/1
  PWM_PRESSEL_FOSC_D2  = (uint8_t)0x01,		//PWM预分频为Fosc/2
  PWM_PRESSEL_FOSC_D8  = (uint8_t)0x02,		//PWM预分频为Fosc/8
  PWM_PRESSEL_FOSC_D32 = (uint8_t)0x03		//PWM预分频为Fosc/32
} PWM_PresSel_TypeDef;
typedef enum
{
  PWM2_OutputPin_P26       = ((uint8_t)0x00),		//PWM2输出口选择P26
  PWM2_OutputPin_P14       = ((uint8_t)0x04)		//PWM2输出口选择P14
} PWM2_OutputPin_TypeDef;

typedef enum
{
  PWM5_OutputPin_P12       = ((uint8_t)0x00),		//PWM5输出口选择P12
  PWM5_OutputPin_P21       = ((uint8_t)0x08)		//PWM5输出口选择P21
} PWM5_OutputPin_TypeDef;

typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
  PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
  PWM3 = (uint8_t)0x08,						//PWM输出通道选择:PWM3
  PWM4 = (uint8_t)0x10,						//PWM输出通道选择:PWM4
  PWM5 = (uint8_t)0x20,						//PWM输出通道选择:PWM5
  PWM6 = (uint8_t)0x40,						//PWM输出通道选择:PWM6
} PWM_OutputPin_TypeDef;

typedef enum
{
  PWM0PWM3 = (uint8_t)0x00,					//PWM互补模式通道选择:PWM0、PWM3
  PWM1PWM4 = (uint8_t)0x01,					//PWM互补模式通道选择:PWM1、PWM4
  PWM2PWM5 = (uint8_t)0x02					//PWM互补模式通道选择:PWM2、PWM5
} PWM_ComplementaryOutputPin_TypeDef;
#elif defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) 
typedef enum
{
  PWM_PRESSEL_FSYS_D1  = (uint8_t)0x00,		//PWM预分频为Fsys/1
  PWM_PRESSEL_FSYS_D2  = (uint8_t)0x01,		//PWM预分频为Fsys/2
  PWM_PRESSEL_FSYS_D4  = (uint8_t)0x02,		//PWM预分频为Fsys/4
  PWM_PRESSEL_FSYS_D8  = (uint8_t)0x03,		//PWM预分频为Fsys/8
  PWM_PRESSEL_FSYS_D32 = (uint8_t)0x04,		//PWM预分频为Fsys/32
  PWM_PRESSEL_FSYS_D64 = (uint8_t)0x05,		//PWM预分频为Fsys/64
  PWM_PRESSEL_FSYS_D128 = (uint8_t)0x06,		//PWM预分频为Fsys/128
  PWM_PRESSEL_FSYS_D256 = (uint8_t)0x07		//PWM预分频为Fsys/256
} PWM_PresSel_TypeDef;

typedef enum
{
  PWM0 = (uint8_t)0x01,						//PWM输出通道选择:PWM0
  PWM1 = (uint8_t)0x02,						//PWM输出通道选择:PWM1
  PWM2 = (uint8_t)0x04,						//PWM输出通道选择:PWM2
} PWM_OutputPin_TypeDef;

typedef enum
{
  PWM0_OutputPin_P00       = ((uint8_t)0x00),		//PWM2输出口选择P26
  PWM0_OutputPin_P25       = ((uint8_t)0x01)		//PWM2输出口选择P14
} PWM0_OutputPin_TypeDef;

typedef enum
{
  PWM1_OutputPin_P01       = ((uint8_t)0x00),		//PWM5输出口选择P12
  PWM1_OutputPin_P26      = ((uint8_t)0x02)		//PWM5输出口选择P21
} PWM1_OutputPin_TypeDef;

typedef enum
{
  PWM2_OutputPin_P02       = ((uint8_t)0x00),		//PWM2输出口选择P26
  PWM2_OutputPin_P27       = ((uint8_t)0x04)		//PWM2输出口选择P14
} PWM2_OutputPin_TypeDef;

//duty微调控制，以四个PWM2周期为一个循环:
typedef enum
{
  PWM_DutyMode0      = ((uint8_t)0x00),		//00:四个PWM2输出的duty均为PDT2设定值；(D、D、D、D)
  PWM_DutyMode1      = ((uint8_t)0x01),		//01:第一个PWM2输出的duty为PDT2设定值加1，其余三个PWM2输出的duty均为PDT2设定值；(D+1、D、D、D)
  PWM_DutyMode2      = ((uint8_t)0x02),   //10:第一和第二个PWM2输出的duty为PDT2设定值加1，其余二个PWM2输出的duty均为PDT2设定值；(D+1、D+1、D、D)
  PWM_DutyMode3      = ((uint8_t)0x03),	  //11:第一、第二和第三个PWM2输出的duty为PDT2设定值加1，第四个PWM2输出的Duty为PDT2设定值； (D+1、D+1、D+1、D)
} PWM_DutyMode_TypeDef;
#elif defined (SC92FWxx)
typedef enum
{
  PWM0_PRESSEL_FHRC_D1  = (uint8_t)0x00,	  //PWM0预分频为Fhrc/1
  PWM0_PRESSEL_FHRC_D2  = (uint8_t)0x10,	  //PWM0预分频为Fhrc/2
  PWM0_PRESSEL_FHRC_D4  = (uint8_t)0x20,	  //PWM0预分频为Fhrc/4
  PWM0_PRESSEL_FHRC_D8  = (uint8_t)0x30,	  //PWM0预分频为Fhrc/8
  PWM1_PRESSEL_FHRC_D1  = (uint8_t)0x01,    //PWM1预分频为Fhrc/1
  PWM1_PRESSEL_FHRC_D2  = (uint8_t)0x11,    //PWM1预分频为Fhrc/2
  PWM1_PRESSEL_FHRC_D4  = (uint8_t)0x21,    //PWM1预分频为Fhrc/4
  PWM1_PRESSEL_FHRC_D8  = (uint8_t)0x31,    //PWM1预分频为Fhrc/8
} PWM_PresSel_TypeDef;

typedef enum
{
  PWM000 = (uint8_t)0x00,   //PWM输出通道选择: PWM000
  PWM001 = (uint8_t)0x02,   //PWM输出通道选择: PWM001
  PWM002 = (uint8_t)0x04,   //PWM输出通道选择: PWM002
  PWM003 = (uint8_t)0x06,   //PWM输出通道选择: PWM003
  PWM004 = (uint8_t)0x08,   //PWM输出通道选择: PWM004
  PWM005 = (uint8_t)0x0A,   //PWM输出通道选择: PWM005
  PWM006 = (uint8_t)0x0C,   //PWM输出通道选择: PWM006
  PWM007 = (uint8_t)0x0E,   //PWM输出通道选择: PWM007
  PWM010 = (uint8_t)0x10,   //PWM输出通道选择: PWM010
  PWM011 = (uint8_t)0x12,   //PWM输出通道选择: PWM011
  PWM012 = (uint8_t)0x14,   //PWM输出通道选择: PWM012
  PWM013 = (uint8_t)0x16,   //PWM输出通道选择: PWM013
  PWM014 = (uint8_t)0x18,   //PWM输出通道选择: PWM014
  PWM015 = (uint8_t)0x1A,   //PWM输出通道选择: PWM015
  PWM016 = (uint8_t)0x1C,   //PWM输出通道选择: PWM016
  PWM017 = (uint8_t)0x1E,   //PWM输出通道选择: PWM017
  PWM020 = (uint8_t)0x20,   //PWM输出通道选择: PWM020
  PWM021 = (uint8_t)0x22,   //PWM输出通道选择: PWM021
  PWM022 = (uint8_t)0x24,   //PWM输出通道选择: PWM022
  PWM023 = (uint8_t)0x26,   //PWM输出通道选择: PWM023
  PWM024 = (uint8_t)0x28,   //PWM输出通道选择: PWM024
  PWM025 = (uint8_t)0x2A,   //PWM输出通道选择: PWM025
  PWM026 = (uint8_t)0x2C,   //PWM输出通道选择: PWM026
  PWM027 = (uint8_t)0x2E,   //PWM输出通道选择: PWM027
  PWM030 = (uint8_t)0x30,   //PWM输出通道选择: PWM030
  PWM031 = (uint8_t)0x32,   //PWM输出通道选择: PWM031
  PWM032 = (uint8_t)0x34,   //PWM输出通道选择: PWM032
  PWM033 = (uint8_t)0x36,   //PWM输出通道选择: PWM033
  PWM034 = (uint8_t)0x38,   //PWM输出通道选择: PWM034
  PWM035 = (uint8_t)0x3A,   //PWM输出通道选择: PWM025
  PWM036 = (uint8_t)0x3C,   //PWM输出通道选择: PWM026
  PWM037 = (uint8_t)0x3E,   //PWM输出通道选择: PWM027
  PWM140 = (uint8_t)0x40,   //PWM输出通道选择: PWM140
  PWM141 = (uint8_t)0x42,   //PWM输出通道选择: PWM141
  PWM142 = (uint8_t)0x44,   //PWM输出通道选择: PWM142
  PWM143 = (uint8_t)0x46,   //PWM输出通道选择: PWM153
  PWM150 = (uint8_t)0x48,   //PWM输出通道选择: PWM150
  PWM151 = (uint8_t)0x4A,   //PWM输出通道选择: PWM151
  PWM152 = (uint8_t)0x4C,   //PWM输出通道选择: PWM152
  PWM153 = (uint8_t)0x4E,   //PWM输出通道选择: PWM153
} PWM_OutputPin_TypeDef;

typedef enum
{
  PWM0_Type  = (uint8_t)0x00,	   //PWM预分频为Fhrc/1
  PWM1_Type  = (uint8_t)0x01,	   //PWM预分频为Fhrc/2
} PWM_Type_TypeDef;

#elif defined (SC92L853x) || defined (SC92L753x)
//PWM预分频选择
typedef enum
{
  PWM_PRESSEL_FHRC_D1 = (uint8_t)0x00, //PWM0预分频为Fhrc/1
  PWM_PRESSEL_FHRC_D2 = (uint8_t)0x10, //PWM0预分频为Fhrc/2
  PWM_PRESSEL_FHRC_D4 = (uint8_t)0x20, //PWM0预分频为Fhrc/4
  PWM_PRESSEL_FHRC_D8 = (uint8_t)0x30, //PWM0预分频为Fhrc/8
} PWM_PresSel_TypeDef;

//PWM输出通道选择
typedef enum
{
  PWM0 = (uint8_t)0x00, //PWM输出通道选择: PWM00
  PWM1 = (uint8_t)0x02, //PWM输出通道选择: PWM01
  PWM2 = (uint8_t)0x04, //PWM输出通道选择: PWM02
  PWM3 = (uint8_t)0x06, //PWM输出通道选择: PWM03
  PWM4 = (uint8_t)0x08, //PWM输出通道选择: PWM04
  PWM5 = (uint8_t)0x0A, //PWM输出通道选择: PWM05
  PWM6 = (uint8_t)0x0C, //PWM输出通道选择: PWM06
  PWM7 = (uint8_t)0x0E, //PWM输出通道选择: PWM07
} PWM_OutputPin_TypeDef;

//PWM对齐模式
typedef enum
{
  PWM0_Edge_Aligned_Mode = (uint8_t)0x00,     //PWM边沿对齐模式
  PWM0_Center_Alignment_Mode = (uint8_t)0x01, //PWM中间对齐模式
} PWM_Aligned_Mode_TypeDef;

//PWM故障检测模式
typedef enum
{
  PWM0_Latch_Mode = ((uint8_t)0x00),    //PWM故障检测模式:锁存模式
  PWM0_Immediate_Mode = ((uint8_t)0x20) //PWM故障检测模式:立即模式
} PWM_FaultDetectionMode_TypeDef;

//PWM故障检测电平选择
typedef enum
{
  PWM0_FaultDetectionVoltage_Low = ((uint8_t)0x00), //PWM故障检测低电平选择
  PWM0_FaultDetectionVoltage_high = ((uint8_t)0x10) //PWM故障检测高电平选择
} PWM_FaultDetectionVoltageSelect_TypeDef;

//PWM故障检测输入信号滤波时间
typedef enum
{
  PWM0_WaveFilteringTime_0us = ((uint8_t)0x00), //PWM故障检测输入信号滤波时间0us
  PWM0_WaveFilteringTime_1us = ((uint8_t)0x01), //PWM故障检测输入信号滤波时间1us
  PWM0_WaveFilteringTime_4us = ((uint8_t)0x02), //PWM故障检测输入信号滤波时间4us
  PWM0_WaveFilteringTime_16us = ((uint8_t)0x03) //PWM故障检测输入信号滤波时间16us
} PWM_FaultDetectionWaveFilteringTime_TypeDef;

//PWM互补模式通道选择
typedef enum
{
  PWM00PWM01 = (uint8_t)0x00, //PWM互补模式通道选择:PWM00、PWM01
  PWM02PWM03 = (uint8_t)0x04, //PWM互补模式通道选择:PWM02、PWM03
  PWM04PWM05 = (uint8_t)0x08, //PWM互补模式通道选择:PWM04、PWM05
  PWM06PWM07 = (uint8_t)0x0C  //PWM互补模式通道选择:PWM06、PWM07
} PWM_ComplementaryOutputPin_TypeDef;

//PWM源选择
typedef enum
{
  PWM0_Type = (uint8_t)0x00, //操作PWM0
} PWM_Type_TypeDef;
#endif

typedef enum
{
  PWM_OUTPUTSTATE_DISABLE	= ((uint8_t)0x00),	//该PIN脚作为GPIO
  PWM_OUTPUTSTATE_ENABLE	= ((uint8_t)0x01)	//该PIN脚作为PWM
} PWM_OutputState_TypeDef;

typedef enum
{
  PWM_POLARITY_NON_INVERT	= ((uint8_t)0x00),	//PWM输出不反向`
  PWM_POLARITY_INVERT		= ((uint8_t)0x01)	//PWM输出反向
} PWM_Polarity_TypeDef;

void PWM_DeInit(void);
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period);
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState);
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity);
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin,
                               uint16_t PWM_DutyCycle);
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority);

#if defined (SC92FWxx) || defined (SC92L853x) || defined (SC92L753x)
void PWM_CmdEX(PWM_Type_TypeDef PWM_Type,
               FunctionalState NewState);
FlagStatus PWM_GetFlagStatusEX(PWM_Type_TypeDef PWM_Type);
void PWM_ClearFlagEX(PWM_Type_TypeDef PWM_Type);
void PWM_IndependentModeConfigEX(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle,
                                 PWM_OutputState_TypeDef PWM_OutputState);
void PWM_Aligned_Mode_Select(PWM_Aligned_Mode_TypeDef PWM_Aligned_Mode);
void PWM_ITConfigEX(PWM_Type_TypeDef PWM_Type, FunctionalState NewState, PriorityStatus Priority);
FlagStatus PWM_GetFaultDetectionFlagStatusEX(PWM_Type_TypeDef PWM_Type);
void PWM_ClearFaultDetectionFlagEX(PWM_Type_TypeDef PWM_Type);
void PWM_FaultDetectionModeConfigEX(PWM_Type_TypeDef PWM_Type,
                                    PWM_FaultDetectionMode_TypeDef FaultDetectionMode,
                                    PWM_FaultDetectionVoltageSelect_TypeDef FaultDetectionVoltageSelect,
                                    PWM_FaultDetectionWaveFilteringTime_TypeDef FaultDetectionWaveFilteringTime);
void PWM_FaultDetectionConfigEX(PWM_Type_TypeDef PWM_Type, FunctionalState NewState);
#else
void PWM_Cmd(FunctionalState NewState);
FlagStatus PWM_GetFlagStatus(void);
void PWM_ClearFlag(void);
#endif

/* 复用通道以及死区配置函数 */
#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax)|| defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x) || defined (SC92F848x) || defined (SC92F748x)  || defined (SC92F848x) || defined (SC92F748x)
void PWM_ComplementaryModeConfig( PWM_ComplementaryOutputPin_TypeDef
                                  PWM_ComplementaryOutputPin,
                                  uint16_t PWM_DutyCycle);
void PWM_DeadTimeConfig(uint8_t PWM012_RisingDeadTime, uint8_t PWM345_fallingDeadTime);
#endif
#if defined (SC92L853x) || defined (SC92L753x)
void PWM_DeadTimeConfigEX(PWM_Type_TypeDef PWM_Type, uint8_t PWM_RisingDeadTime, uint8_t PWM_FallingDeadTime);
#endif
/* PWM输出通道选择 */
#if defined (SC92F8003) || defined (SC92F740x) || defined (SC92F7003)
  void PWM_PWM2Selection(PWM2_OutputPin_TypeDef PWM2_OutputPin);
  void PWM_PWM5Selection(PWM5_OutputPin_TypeDef PWM5_OutputPin);
#endif
#if defined (SC92F732X) ||  defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
  void PWM_PWM0Selection(PWM0_OutputPin_TypeDef PWM0_OutputPin);
  void PWM_PWM1Selection(PWM1_OutputPin_TypeDef PWM1_OutputPin);
  void PWM_PWM2Selection(PWM2_OutputPin_TypeDef PWM2_OutputPin);
  void PMM_DutyModeSelection(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_DutyMode_TypeDef PWM_DutyMode);
#endif

#endif

#endif
/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
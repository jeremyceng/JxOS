//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_timer2.h
//	作者		:
//	模块功能	: TIMER2固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2020/9/14
// 	版本		: V1.10002
//  说明        :该文件仅适用于SC92F系列芯片
//*************************************************************

#ifndef _sc92f_TIMER2_H_
#define	_sc92f_TIMER2_H_

#include "sc92f.h"

typedef enum
{
  TIM2_PRESSEL_FSYS_D12  = ((uint8_t)0x00), //TIMER2计数源来自系统时钟12分频
  TIM2_PRESSEL_FSYS_D1   = ((uint8_t)0x01)  //TIMER2计数源来自系统时钟
} TIM2_PresSel_TypeDef;

#if defined (SC92F730x) || defined (SC92F827X) || defined (SC92F837X) || defined (SC92F725X) || defined (SC92F735X)

typedef enum
{
  TIM2_MODE_TIMER   = ((uint8_t)0x01), //TIMER2做定时器
} TIM2_CountMode_TypeDef;

typedef enum
{
  TIM2_COUNTDIRECTION_UP       = ((uint8_t)0x00),  //向上计数模式
} TIM2_CountDirection_TypeDef;

typedef enum
{
  TIM2_FLAG_TF2   = (uint8_t)0x80, 	   //中断标志位TF2，计数溢出
} TIM2_Flag_TypeDef;

typedef enum
{
  TIM2_WORK_MODE1   = ((uint8_t)0x00),		//TIMER2选择工作模式1
} TIM2_WorkMode_TypeDef;

#else

typedef enum
{
  TIM2_MODE_TIMER   = ((uint8_t)0x01), //TIMER2做定时器
  TIM2_MODE_COUNTER = ((uint8_t)0x02)  //TIMER2做计数器
} TIM2_CountMode_TypeDef;

typedef enum
{
  TIM2_COUNTDIRECTION_UP  		 = ((uint8_t)0x00),  //向上计数模式
  TIM2_COUNTDIRECTION_DOWN_UP  = ((uint8_t)0x10)   //向上/向下计数模式
} TIM2_CountDirection_TypeDef;

typedef enum
{
  TIM2_FLAG_TF2   = (uint8_t)0x80, 	   //中断标志位TF2，计数溢出
  TIM2_FLAG_EXF2  = (uint8_t)0x40 	   //中断标志位EXF2，外部触发
} TIM2_Flag_TypeDef;

typedef enum
{
  TIM2_WORK_MODE0   = ((uint8_t)0x00),		//TIMER2选择工作模式0
  TIM2_WORK_MODE1   = ((uint8_t)0x01),		//TIMER2选择工作模式1
  TIM2_WORK_MODE3   = ((uint8_t)0x03),		//TIMER2选择工作模式3
} TIM2_WorkMode_TypeDef;

#endif

void TIM2_DeInit();
void TIM2_PrescalerSelection(TIM2_PresSel_TypeDef TIM2_PrescalerSelection);
void TIM2_TimeBaseInit(TIM2_PresSel_TypeDef TIM2_PrescalerSelection,
                       TIM2_CountMode_TypeDef TIM2_CountMode,
                       TIM2_CountDirection_TypeDef TIM2_CountDirection);
void TIM2_WorkMode0Config(uint16_t TIM2_SetCounter);
void TIM2_WorkMode1Config(uint16_t TIM2_SetCounter);
void TIM2_WorkMode3Config(uint16_t TIM2_SetCounter);
void TIM2_WorkModeConfig(TIM2_WorkMode_TypeDef TIM2_WorkMode, uint16_t TIM2_SetCounter);
void TIM2_SetEXEN2(FunctionalState NewState);
void TIM2_Cmd(FunctionalState NewState);
void TIM2_ITConfig(FunctionalState NewState, PriorityStatus Priority);
FlagStatus TIM2_GetFlagStatus(TIM2_Flag_TypeDef TIM2_Flag);
void TIM2_ClearFlag(TIM2_Flag_TypeDef TIM2_Flag);

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_gpio.h
//	作者		:
//	模块功能	: GPIO固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2021/08/20
// 	版本		: V1.10001
//  说明        :
//*************************************************************

#ifndef _sc92f_GPIO_H_
#define	_sc92f_GPIO_H_

#include "sc92f.h"

#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB) || defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined(SC92FWxx)|| defined(SC92F859x) || defined(SC92F759x)
typedef enum
{

  GPIO0 = (uint8_t)0x00,	   //P0
  GPIO1 = (uint8_t)0x01,	   //P1
  GPIO2 = (uint8_t)0x02,	   //P2
  GPIO3 = (uint8_t)0x03,	   //P3
  GPIO4 = (uint8_t)0x04,	   //P4
  GPIO5 = (uint8_t)0x05	     //P5
}GPIO_TypeDef;
#elif defined (SC92F730x ) || defined (SC92F725X) || defined (SC92F735X) || defined (SC92F8003) || defined (SC92F740x) || defined (SC92F827X) || defined (SC92F837X) || defined (SC92F7003)
typedef enum
{
  GPIO0 = (uint8_t)0x00,	   //P0
  GPIO1 = (uint8_t)0x01,	   //P1
  GPIO2 = (uint8_t)0x02,	   //P2
}GPIO_TypeDef;
#else
typedef enum
{
  GPIO0 = (uint8_t)0x00,	   //P0
  GPIO1 = (uint8_t)0x01,	   //P1
  GPIO2 = (uint8_t)0x02,	   //P2
  GPIO5 = (uint8_t)0x05	     //P5
}GPIO_TypeDef;
#endif


typedef enum
{
  GPIO_MODE_IN_HI  = (uint8_t)0x00, //高阻输入模式
  GPIO_MODE_IN_PU  = (uint8_t)0x01, //带上拉的输入模式
  GPIO_MODE_OUT_PP = (uint8_t)0x02  //强推挽输出模式
} GPIO_Mode_TypeDef;

typedef enum
{
  GPIO_PIN_0    = ((uint8_t)0x01),  //IO管脚Px0
  GPIO_PIN_1    = ((uint8_t)0x02),  //IO管脚Px1
  GPIO_PIN_2    = ((uint8_t)0x04),  //IO管脚Px2
  GPIO_PIN_3    = ((uint8_t)0x08),  //IO管脚Px3
  GPIO_PIN_4    = ((uint8_t)0x10),  //IO管脚Px4
  GPIO_PIN_5    = ((uint8_t)0x20),  //IO管脚Px5
  GPIO_PIN_6    = ((uint8_t)0x40),  //IO管脚Px6
  GPIO_PIN_7    = ((uint8_t)0x80),  //IO管脚Px7
  GPIO_PIN_LNIB = ((uint8_t)0x0F),  //IO管脚Px0~3
  GPIO_PIN_HNIB = ((uint8_t)0xF0),  //IO管脚Px4~7
  GPIO_PIN_ALL  = ((uint8_t)0xFF)   //IO管脚Px0~7
} GPIO_Pin_TypeDef;

typedef enum
{
  IOH_Grade_0    = ((uint8_t)0x00),  //IOH等级0
  IOH_Grade_1    = ((uint8_t)0x01),  //IOH等级1
  IOH_Grade_2    = ((uint8_t)0x02),  //IOH等级2
  IOH_Grade_3    = ((uint8_t)0x03),  //IOH等级3
} GPIO_IOH_Grade_TypeDef;

void GPIO_IOH_Config(GPIO_TypeDef GPIOx, GPIO_Pin_TypeDef PortPins,GPIO_IOH_Grade_TypeDef GPIO_IOH_Grade);

void GPIO_DeInit(void);
void GPIO_Init(GPIO_TypeDef GPIOx,
               uint8_t PortPins, GPIO_Mode_TypeDef GPIO_Mode);
void GPIO_Write(GPIO_TypeDef GPIOx,
                uint8_t PortVal);
void GPIO_WriteHigh(GPIO_TypeDef GPIOx,
                    uint8_t PortPins);
void GPIO_WriteLow(GPIO_TypeDef GPIOx,
                   uint8_t PortPins);
uint8_t GPIO_ReadPort(GPIO_TypeDef GPIOx);
BitStatus GPIO_ReadPin(GPIO_TypeDef GPIOx,
                     uint8_t PortPins);

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/

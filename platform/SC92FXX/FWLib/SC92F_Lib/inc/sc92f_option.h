//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_option.h
//	作者		:
//	模块功能	: Customer Option寄存器配置头文件
//	局部函数列表:
//  最后更正日期: 2022/01/24
// 	版本		: V1.0007
//  说明        :
//*************************************************************

#ifndef _sc92f_OPTION_H_
#define	_sc92f_OPTION_H_

#include "sc92f.h"

#if defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  SYSCLK_PRESSEL_FOSC_D1  = (uint8_t)0x00,	//预分频 Fsys = Fosc/1
  SYSCLK_PRESSEL_FOSC_D2  = (uint8_t)0x10,	//预分频 Fsys = Fosc/2
  SYSCLK_PRESSEL_FOSC_D4  = (uint8_t)0x20,	//预分频 Fsys = Fosc/4
  SYSCLK_PRESSEL_FOSC_D8  = (uint8_t)0x30 	//预分频 Fsys = Fosc/8
} SYSCLK_PresSel_TypeDef;
#else
typedef enum
{
  SYSCLK_PRESSEL_FOSC_D1  = (uint8_t)0x00,	//预分频 Fsys = Fosc/1
  SYSCLK_PRESSEL_FOSC_D2  = (uint8_t)0x10,	//预分频 Fsys = Fosc/2
  SYSCLK_PRESSEL_FOSC_D4  = (uint8_t)0x20,	//预分频 Fsys = Fosc/4
  SYSCLK_PRESSEL_FOSC_D12 = (uint8_t)0x30 	//预分频 Fsys = Fosc/12
} SYSCLK_PresSel_TypeDef;
#endif

#if defined (SC92F859x) || defined (SC92F759x)||defined (SC92F848x) || defined (SC92F748x)
typedef enum
{
  LVR_INVALID	= (uint8_t)0x04,	//LVR无效
  LVR_1_9V    = (uint8_t)0x00,	//LVR 1.9V复位
  LVR_2_9V    = (uint8_t)0x01,	//LVR 2.9V复位
  LVR_3_7V    = (uint8_t)0x02,	//LVR 3.7V复位
  LVR_4_3V    = (uint8_t)0x03 	//LVR 4.3V复位
} LVR_Config_TypeDef;
#elif defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  LVR_INVALID	= (uint8_t)0x04,	//LVR无效
  LVR_1_7V    = (uint8_t)0x00,	//LVR 1.7V复位
  LVR_2_7V    = (uint8_t)0x01,	//LVR 2.7V复位
  LVR_3_7V    = (uint8_t)0x02,	//LVR 3.7V复位
  LVR_4_3V    = (uint8_t)0x03 	//LVR 4.3V复位
} LVR_Config_TypeDef;
#else
typedef enum
{
  LVR_INVALID	= (uint8_t)0x04,	//LVR无效
  LVR_2_3V    = (uint8_t)0x00,	//LVR 2.3V复位
  LVR_2_9V    = (uint8_t)0x01,	//LVR 2.9V复位
  LVR_3_7V    = (uint8_t)0x02,	//LVR 3.7V复位
  LVR_4_3V    = (uint8_t)0x03 	//LVR 4.3V复位
} LVR_Config_TypeDef;
#endif

#if defined (SC92F848x) || defined (SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  IAP_OPERATERANGE_ONLY_EEPROM 		    = (uint8_t)0x00,	//只允许EEPROM进行IAP操作
  IAP_OPERATERANGE__LAST_1K_CODEREGION  = (uint8_t)0x04,	//允许ROM最后1k和EEPROM进行IAP操作
  IAP_OPERATERANGE__LAST_2K_CODEREGION  = (uint8_t)0x08,	//允许ROM最后2k和EEPROM进行IAP操作
  IAP_OPERATERANGE__ALL_CODEREGION 	    = (uint8_t)0x0c		//允许ROM和EEPROM所有区域进行IAP操作
} IAP_OperateRange_TypeDef;
#else
typedef enum
{
  IAP_OPERATERANGE_ONLY_EEPROM 		    = (uint8_t)0x00,	//只允许EEPROM进行IAP操作
  IAP_OPERATERANGE__LAST_0_5K_CODEREGION  = (uint8_t)0x04,	//允许ROM最后0.5k和EEPROM进行IAP操作
  IAP_OPERATERANGE__LAST_1K_CODEREGION    = (uint8_t)0x08,	//允许ROM最后1k和EEPROM进行IAP操作
  IAP_OPERATERANGE__ALL_CODEREGION 	    = (uint8_t)0x0c		//允许ROM和EEPROM所有区域进行IAP操作
} IAP_OperateRange_TypeDef;
#endif


#if defined (SC92F859x) || defined (SC92F759x) || defined (SC92F848x) || defined (SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
  ADC_VREF_VDD  = 0x00,					//选择VDD做ADC参考电压
  ADC_VREF_1_024V = 0x40,   		//选择内部1.024V做ADC参考电压
  ADC_VREF_2_4V = 0x80,   			//选择内部2.4V做ADC参考电压
  ADC_VREF_2_048V = 0xC0,				//选择内部2.048V做ADC参考电压
} ADC_Vref_TypeDef;
#else

typedef enum
{
  ADC_VREF_VDD  = 0x00,					//选择VDD做ADC参考电压
  ADC_VREF_2_4V = 0x80,   			//选择内部2.4V做ADC参考电压
} ADC_Vref_TypeDef;
#endif

#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax)\
	|| defined (SC92F73Ax) || defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F740x)\
	|| defined (SC92F848x) || defined (SC92F748x) || defined (SC92F8003) || defined (SC92F7003)
typedef enum
{
  XTIPLL_HIGHER_THAN_12M  = (uint8_t)0x40,	//外接晶振振荡频率大于等于12M
  XTIPLL_UNDER_12M        = (uint8_t)0x00 	//外接晶振振荡频率小于12M
} XTIPLL_Range_TypeDef;
#endif

void OPTION_WDT_Cmd(FunctionalState NewState);

void OPTION_SYSCLK_Init(SYSCLK_PresSel_TypeDef
                        SYSCLK_PresSel);
#if !defined(SC92F848x) && !defined(SC92F748x) && !defined(SC92F859x) && !defined (SC92F759x)  && !defined(SC92L853x) && !defined (SC92L753x)
void OPTION_RST_PIN_Cmd(FunctionalState NewState);
#endif
void OPTION_LVR_Init(LVR_Config_TypeDef
                     LVR_Config);
void OPTION_ADC_VrefConfig(ADC_Vref_TypeDef ADC_Vref);
void OPTION_IAP_SetOperateRange(
  IAP_OperateRange_TypeDef IAP_OperateRange);

#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax)\
	|| defined (SC92F73Ax) || defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F740x)\
	|| defined (SC92F8003) || defined (SC92F7003)
void OPTION_XTIPLL_SetRange(XTIPLL_Range_TypeDef
                            XTIPLL_Range);
#endif

#if !defined(SC92F848x) && !defined(SC92F748x) 
void OPTION_XTIPLL_Cmd(FunctionalState NewState);
#endif

#if defined (SC92F742x)||defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax) \
		||defined (SC92F74Ax_2)||defined (SC92F84Ax_2)||defined (SC92F844xB)||defined (SC92F744xB) \
		||defined (SC92F859x) || defined (SC92F759x) ||defined (SC92F848x) || defined (SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
void OPTION_JTG_Cmd(FunctionalState NewState);
#endif


#endif
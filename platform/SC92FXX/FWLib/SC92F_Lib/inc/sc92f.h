//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f.h
//	作者		:
//	模块功能	: 赛元SC92系列系统头文件
//	局部函数列表:
//  最后更正日期: 2022/03/11
// 	版本		: V1.10026
//  说明        ：本文件仅适用于赛元92F系列单片机
//*************************************************************

#ifndef _sc92f_H
#define _sc92f_H

#ifdef SC92F854x
  #include "SC92F854x_C.H"
#endif

#ifdef SC92F754x
  #include "SC92F754x_C.H"
#endif

#ifdef SC92F844xB
  #include "SC92F844xB_C.H"
#endif

#ifdef SC92F744xB
  #include "SC92F744xB_C.H"
#endif

#ifdef SC92F846xB
  #include "SC92F846xB_C.H"
#endif

#ifdef SC92F746xB
  #include "SC92F746xB_C.H"
#endif

#ifdef SC92F836xB
  #include "SC92F836xB_C.H"
#endif

#ifdef SC92F736xB
  #include "SC92F736xB_C.H"
#endif

#ifdef SC92F742x
  #include "SC92F742x_C.H"
#endif

#ifdef SC92F7003
  #include "SC92F7003_C.H"
#endif

#ifdef SC92F8003
  #include "SC92F8003_C.H"
#endif

#ifdef SC92F740x
  #include "SC92F740x_C.H"
#endif

#ifdef SC92F74Ax
  #include "SC92F74Ax_C.H"
#endif

#ifdef SC92F84Ax
  #include "SC92F84Ax_C.H"
#endif

#ifdef SC92F73Ax
  #include "SC92F73Ax_C.H"
#endif

#ifdef SC92F83Ax
  #include "SC92F83Ax_C.H"
#endif

#ifdef SC92F74Ax_2
  #include "SC92F74Ax_2_C.H"
#endif

#ifdef SC92F84Ax_2
  #include "SC92F84Ax_2_C.H"
#endif

#ifdef SC92F730x
  #include "SC92F730x_C.h"
#endif

#ifdef SC92F827X
  #include "SC92F827X_C.h"
#endif

#ifdef SC92F837X
  #include "SC92F837X_C.H"
#endif

#ifdef SC92F7490
  #include "SC92F7490_C.H"
#endif

#ifdef SC92F725X
  #include "SC92F725X_C.h"
#endif

#ifdef SC92F735X
  #include "SC92F735X_C.h"
#endif

#ifdef SC92F732X
  #include "SC92F732X_C.H"
#endif

#ifdef SC92FWxx
  #include "SC92FWxx_C.H"
#endif

#ifdef SC93F833x
  #include "SC93F833x_C.H"
#endif

#ifdef SC93F843x
  #include "SC93F843x_C.H"
#endif

#ifdef SC93F743x
  #include "SC93F743x_C.H"
#endif

#ifdef SC92F848x
  #include "SC92F848x_C.H"
#endif

#ifdef SC92F748x
  #include "SC92F748x_C.H"
#endif

#ifdef SC92F859x
	#include "SC92F859x_C.H"
#endif

#ifdef SC92F759x
	#include "SC92F759x_C.H"
#endif

#ifdef SC92L853x
	#include "SC92L853x_C.H"
#endif

#ifdef SC92L753x
	#include "SC92L753x_C.H"
#endif

#define enableInterrupts()    EA=1	 /** 开启总中断	 **/
#define disableInterrupts()   EA=0	 /** 关闭总中断	 **/

#define     __I     volatile const   /*!< defines 'read only' permissions     */
#define     __O     volatile         /*!< defines 'write only' permissions    */
#define     __IO    volatile         /*!< defines 'read / write' permissions  */

/*!< Signed integer types  */
typedef   signed char     int8_t;
typedef   signed short    int16_t;
typedef   signed long     int32_t;

/*!< Unsigned integer types  */
typedef unsigned char     uint8_t;
typedef unsigned short    uint16_t;
typedef unsigned long     uint32_t;

typedef int32_t  s32;
typedef int16_t  s16;
typedef int8_t   s8;

typedef uint32_t  u32;
typedef uint16_t  u16;
typedef uint8_t   u8;


typedef enum {FALSE = 0, TRUE = !FALSE} bool;

typedef enum {RESET = 0, SET = !RESET} FlagStatus,
ITStatus, BitStatus;

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;

typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;

typedef enum {LOW = 0, HIGH = !LOW} PriorityStatus;

#endif
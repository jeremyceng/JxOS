//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92F_lpd.h
//	作者		:
//	模块功能	: LPD固件库函数头文件
//  最后更正日期: 2022/01/19
// 	版本		: V1.10000
//  说明    :该文件仅适用于SC92F系列芯片
//*************************************************************

#ifndef _sc92f_LPD_H_
#define _sc92f_LPD_H_

#include "sc92f.h"

typedef enum
{
  LPD_VTRIP_1_85V = (uint8_t)0x00, //LPD门限电压阈值为1.85V
  LPD_VTRIP_2_05V = (uint8_t)0x01, //LPD门限电压阈值为2.05V
  LPD_VTRIP_2_25V = (uint8_t)0x02, //LPD门限电压阈值为2.25V
  LPD_VTRIP_2_45V = (uint8_t)0x03, //LPD门限电压阈值为2.45V
  LPD_VTRIP_2_85V = (uint8_t)0x04, //LPD门限电压阈值为2.85V
  LPD_VTRIP_3_45V = (uint8_t)0x05, //LPD门限电压阈值为3.45V
  LPD_VTRIP_3_85V = (uint8_t)0x06, //LPD门限电压阈值为3.85V
  LPD_VTRIP_4_45V = (uint8_t)0x07, //LPD门限电压阈值为4.45V
} LPD_Vtrip_TypeDef;

typedef enum
{
  LPD_FLAG_LPDIF = (uint8_t)0x40, //LPD中断请求标志
  LPD_FLAG_LPDOF = (uint8_t)0x80, //LPD状态标志位
} LPD_Flag_TypeDef;

void LPD_DeInit(void);
void LPD_VtripConfig(LPD_Vtrip_TypeDef LPD_Vtrip);
void LPD_ITConfig(FunctionalState NewState, PriorityStatus Priority);
void LPD_Cmd(FunctionalState NewState);
FlagStatus LPD_GetFlagStatus(LPD_Vtrip_TypeDef LPD_Flag);
void LPD_ClearFlag(LPD_Vtrip_TypeDef LPD_Flag);

#endif
//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_uart0.h
//	作者		:
//	模块功能	: UART0固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2022/01/24
// 	版本		: V1.10002
//  说明        :本文件仅适用于赛元92F/93F/92L系列单片机
//*************************************************************
#ifndef _sc92f_UART0_H_
#define	_sc92f_UART0_H_

#include "sc92f.h"
#if !defined (SC92F742x) && !defined (SC92F827X) && !defined (SC92F837X)

#define  UART0_BaudRate_FsysDIV12    0X00	 //仅模式0可用，串行端口在系统时钟的1/12下运行
#define  UART0_BaudRate_FsysDIV4	 0X01	 //仅模式0可用，串行端口在系统时钟的1/4下运行
#define  UART0_BaudRate_FsysDIV64   0X00 //仅模式1可用，串行端口在系统时钟的1/64下运行
#define  UART0_BaudRate_FsysDIV32   0X01 //仅模式1可用，串行端口在系统时钟的1/32下运行
#if defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
typedef enum
{
  UART0_PinSelection_P15P16 = (uint8_t)0x00, //UART0引脚为P15，P16
  UART0_PinSelection_P15 = (uint8_t)0x00, //UART0引脚为P15，P16，无RX
  UART0_PinSelection_P11P20 = (uint8_t)0x10,  //UART0引脚为P11，P20
  UART0_PinSelection_P20 = (uint8_t)0x10,  //UART0引脚为P20，无RX
} UART0_PinSelection_TypeDef;
#endif

#if  defined(SC92F725X) || defined(SC92F735X)|| defined (SC92F730x ) || defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)     
typedef enum
{
  UART0_CLOCK_TIMER1 = (uint8_t)0X02,	          //TIMER1做波特率发生器
  UART0_CLOCK_TIMER1_FreqMcl2 = (uint8_t)0X82, //TIMER1做波特率发生器,且二倍频
  UART0_CLOCK_TIMER1_DIV6 = (uint8_t)0X80,     //TIMER1做波特率发生器,且6分频
  UART0_CLOCK_TIMER1_DIV12 = (uint8_t)0X00,   //TIMER1做波特率发生器,且12分频
  UART0_CLOCK_TIMER2 = (uint8_t)0X34,	        //TIMER2做波特率发生器
  UART0_CLOCK_TIMER2_DIV12 = (uint8_t)0X30,		//定时器2 12分频，模式1和3通用
}UART0_Clock_Typedef;
#elif defined (SC92F848x) || defined (SC92F748x) || defined(SC92F859x) || defined(SC92F759x) || defined(SC92L853x) || defined(SC92L753x)
typedef enum
{
	UART0_CLOCK_TIMER1 = (uint8_t)0X00,	//TIMER1做波特率发生器
  UART0_CLOCK_TIMER2 = (uint8_t)0X30,	//TIMER2做波特率发生器
	UART0_CLOCK_TIMER1_DIV16 = (uint8_t)0X80,	//TIMER1做波特率发生器
  UART0_CLOCK_TIMER2_DIV16 = (uint8_t)0XB0,	//TIMER2做波特率发生器
}UART0_Clock_Typedef;
#else
typedef enum
{
  //模式0和3的定时器选择
  UART0_CLOCK_TIMER1 = (uint8_t)0X00,	//TIMER1做波特率发生器
  UART0_CLOCK_TIMER2 = (uint8_t)0X30,	//TIMER2做波特率发生器
}UART0_Clock_Typedef;	 
#endif

#if defined (SC92F730x) || defined (SC92F725X) || defined (SC92F735X) || defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
typedef enum
{
  UART0_Mode_8B  = 0X00,		//UART工作模式:8位半双工
  UART0_Mode_10B = 0X40,		//UART工作模式:10位全双工
  UART0_Mode_11B = 0XC0,  	//UART工作模式:11位全双工   
  UART0_Mode_11B_BaudRateFix = 80//UART工作模式:11位全双工,波特率固定
}UART0_Mode_Typedef;
#else
typedef enum
{
  UART0_Mode_8B  = 0X00,		//UART工作模式:8位半双工
  UART0_Mode_10B = 0X40,		//UART工作模式:10位全双工
  UART0_Mode_11B = 0XC0,  	//UART工作模式:11位全双工
}UART0_Mode_Typedef;
#endif

typedef enum
{
  UART0_RX_ENABLE  = 0x10,   //允许接收数据
  UART0_RX_DISABLE = 0x00	   //不允许接收数据
} UART0_RX_Typedef;

typedef enum
{
  UART0_FLAG_RI = 0X01,		//接收中断标志位RI
  UART0_FLAG_TI = 0X02	  	//发送中断标志位TI
} UART0_Flag_Typedef;

void UART0_DeInit(void);
void UART0_Init(uint32_t Uart0Fsys,
                uint32_t BaudRate, UART0_Mode_Typedef Mode,
                UART0_Clock_Typedef ClockMode,
                UART0_RX_Typedef RxMode);
void UART0_SendData8(uint8_t Data);
uint8_t UART0_ReceiveData8(void);
void UART0_SendData9(uint16_t Data);
uint16_t UART0_ReceiveData9(void);
void UART0_ITConfig(FunctionalState NewState,
                    PriorityStatus Priority);
FlagStatus UART0_GetFlagStatus(UART0_Flag_Typedef
                               UART0_Flag);
void UART0_ClearFlag(UART0_Flag_Typedef
                     UART0_Flag);
#if defined (SC92F8003) || defined (SC92F740x)  || defined (SC92F7003)
void UART0_PinSelection(UART0_PinSelection_TypeDef
                        PinSeletion);
#endif

#endif

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/

//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_iap.h
//	作者		:
//	模块功能	: IAP固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2022/01/24
// 	版本		: V1.10004
//  说明        :本文件仅适用于赛元92F/93F/92L系列单片机
//*************************************************************

#ifndef _sc92f_IAP_H_
#define	_sc92f_IAP_H_

#include "sc92f.h"
#include "intrins.h"

typedef enum
{
  IAP_MEMTYPE_ROM       = (uint8_t)0x00,    //IAP操作区域为ROM
  IAP_MEMTYPE_IFB       = (uint8_t)0x01,    //IAP操作区域为IFB
  IAP_MEMTYPE_EEPROM    = (uint8_t)0x02     //IAP操作区域为EEPROM
} IAP_MemType_TypeDef;


#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
|| defined(SC92F7003) || defined(SC92F8003) || defined(SC92F740x)
typedef enum
{
  IAP_HOLDTIME_1500US   = (uint8_t)0x08, //设定CPU Hold Time为1.5MS
  IAP_HOLDTIME_3000US   = (uint8_t)0x04, //设定CPU Hold Time为3MS
  IAP_HOLDTIME_6000US	  = (uint8_t)0x00  //设定CPU Hold Time为6MS
}IAP_HoldTime_TypeDef;
#elif defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)\
	|| defined (SC92F84Ax) || defined (SC92F74Ax)|| defined (SC92F742x) || defined (SC92F730x) || defined (SC92F827X) || defined (SC92F837X) \
	|| defined (SC92F725X) || defined (SC92F735X) || defined (SC92F732X) || defined (SC92F7490) || defined (SC93F833x) || defined (SC93F843x)\
	|| defined (SC93F733x) || defined (SC93F743x)
typedef enum
{
  IAP_HOLDTIME_4MS   = (uint8_t)0x00,		//设定CPU Hold Time为4MS
  IAP_HOLDTIME_2MS   = (uint8_t)0x04,		//设定CPU Hold Time为2MS
  IAP_HOLDTIME_1MS   = (uint8_t)0x08		//设定CPU Hold Time为1MS
}IAP_HoldTime_TypeDef;
#elif defined (SC92FWxx)
typedef enum
{  
	IAP_HOLDTIME_3000US   = (uint8_t)0x00,		//设定CPU Hold Time为4MS
  IAP_HOLDTIME_1500US   = (uint8_t)0x04,		//设定CPU Hold Time为2MS
  IAP_HOLDTIME_1000US   = (uint8_t)0x08		//设定CPU Hold Time为1MS
}IAP_HoldTime_TypeDef;
#elif defined(SC92F848x) || defined(SC92F748x) || defined(SC92F859x) || defined(SC92F759x) || defined (SC92L853x) || defined (SC92L753x)
typedef enum
{
	IAP_HOLDTIME_Null = (uint8_t)0x00			//高速Flash，不需要Hold Time
}IAP_HoldTime_TypeDef;

typedef enum
{
  IAP_BTLDType_APPROM = (uint8_t)0x00, //MCU复位后从APROM复位
  IAP_BTLDType_LDROM = (uint8_t)0x01,  //MCU复位后从LDROM复位
} IAP_BTLDType_TypeDef;


#endif


void IAP_DeInit(void);
void IAP_SetHoldTime(IAP_HoldTime_TypeDef IAP_HoldTime);
void IAP_ProgramByte(uint16_t Address,
                     uint8_t Data, IAP_MemType_TypeDef IAP_MemType,
                     uint8_t WriteTimeLimit);
uint8_t IAP_ReadByte(uint16_t Address,
                     IAP_MemType_TypeDef IAP_MemType);

#if defined(SC92F848x) || defined(SC92F748x) || defined(SC92F859x) || defined(SC92F759x) || defined (SC92L853x) || defined (SC92L753x)
void IAP_SectorErase(IAP_MemType_TypeDef IAP_MemType, uint32_t IAP_SectorEraseAddress,
                     uint8_t WriteTimeLimit);
void IAP_BootLoaderControl(IAP_BTLDType_TypeDef IAP_BTLDType);
#endif

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
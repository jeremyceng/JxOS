//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_conf.h
//	作者		:
//	模块功能	: sc92f配置头文件
//  最后更正日期: 2022/01/24
// 	版本		: V1.10002
//  说明        ：本文件仅适用于赛元92F系列单片机
//*************************************************************

#ifndef _sc92f_CONF_H_
#define _sc92f_CONF_H_

#include "sc92f_gpio.h"
#include "sc92f_timer0.h"
#include "sc92f_timer1.h"
#include "sc92f_timer2.h"
#include "sc92f_timer3.h"
#include "sc92f_timer4.h"
#include "sc92f_adc.h"
#include "sc92f_btm.h"
#include "sc92f_pwm.h"
#include "sc92f_int.h"
#include "sc92f_uart0.h"
#include "sc92f_ssi.h"
#include "sc92f_chksum.h"
#include "sc92f_iap.h"
#include "sc92f_option.h"
#include "sc92f_wdt.h"
#include "sc92f_pwr.h"
#include "sc92f_ddic.h"
#include "sc92f_acmp.h"
#include "sc92f_mdu.h"
#include "sc92f_CRC.h"
#include "sc92f_usci0.h"
#include "sc92f_usci1.h"
#include "sc92f_usci2.h"
#include "sc92f_lpd.h"

#endif
//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_ssi.h
//	作者		:
//	模块功能	: SSI固件库函数头文件
//	局部函数列表:
//  最后更正日期: 2020/08/20
// 	版本		: V1.10001
//  说明        :
//*************************************************************

#ifndef _sc92f_SSI_H_
#define	_sc92f_SSI_H_

#include "sc92f.h"

#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)||defined  (SC92F84Ax) || defined (SC92F74Ax)\
		|| defined (SC92F83Ax) || defined (SC92F73Ax) || defined (SC92F7003) || defined(SC92F8003) || defined (SC92F740x) || defined (SC92F827X)\
		|| defined (SC92F837X) || defined (SC92FWxx) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) || defined (SC92F848x) || defined (SC92F748x)\ 
		|| defined (SC92F859x) || defined (SC92F859x)
typedef enum
{
  SPI_FIRSTBIT_MSB = (uint8_t)0x00, 			   //MSB优先发送
  SPI_FIRSTBIT_LSB = (uint8_t)0x04  			   //LSB优先发送
} SPI_FirstBit_TypeDef;

typedef enum
{
  SPI_BAUDRATEPRESCALER_4   = (uint8_t)0x00, //SPI时钟速率为系统时钟除以4
  SPI_BAUDRATEPRESCALER_8   = (uint8_t)0x01, //SPI时钟速率为系统时钟除以8
  SPI_BAUDRATEPRESCALER_16  = (uint8_t)0x02, //SPI时钟速率为系统时钟除以16
  SPI_BAUDRATEPRESCALER_32  = (uint8_t)0x03, //SPI时钟速率为系统时钟除以32
  SPI_BAUDRATEPRESCALER_64  = (uint8_t)0x04, //SPI时钟速率为系统时钟除以64
  SPI_BAUDRATEPRESCALER_128 = (uint8_t)0x05, //SPI时钟速率为系统时钟除以128
  SPI_BAUDRATEPRESCALER_256 = (uint8_t)0x06, //SPI时钟速率为系统时钟除以256
  SPI_BAUDRATEPRESCALER_512 = (uint8_t)0x07  //SPI时钟速率为系统时钟除以512
} SPI_BaudRatePrescaler_TypeDef;

typedef enum
{
  SPI_MODE_MASTER = (uint8_t)0x20, //SPI为主设备
  SPI_MODE_SLAVE  = (uint8_t)0x00  //SPI为从设备
} SPI_Mode_TypeDef;

typedef enum
{
  SPI_CLOCKPOLARITY_LOW  = (uint8_t)0x00, //SCK在空闲状态下为低电平
  SPI_CLOCKPOLARITY_HIGH = (uint8_t)0x10  //SCK在空闲状态下为高电平
} SPI_ClockPolarity_TypeDef;

typedef enum
{
  SPI_CLOCKPHASE_1EDGE = (uint8_t)0x00, //SCK的第一沿采集数据
  SPI_CLOCKPHASE_2EDGE = (uint8_t)0x08  //SCK的第二沿采集数据
} SPI_ClockPhase_TypeDef;

typedef enum
	{
  //作为从机
  TWI_SlaveBusy = 0x00,
  TWI_SlaveReceivedaAddress = 0x01,
  TWI_SlaveReceivedaData = 0x02,
  TWI_SlaveSendData = 0x03,
  TWI_SlaveReceivedaUACK = 0x04,
  TWI_SlaveDisableACK = 0x05,
  TWI_SlaveAddressError = 0x06,
  
}SSI_TWIState_TypeDef;

typedef enum
{
  SPI_TXE_DISINT = (uint8_t)0x00, //TXE为1时不允许发送中断
  SPI_TXE_ENINT  = (uint8_t)0x01  //TXE为1时允许发送中断
} SPI_TXE_INT_TypeDef;

typedef enum
{
  UART1_Mode_10B  = 0X00,//UART1设置为10位模式
  UART1_Mode_11B  = 0X80 //UART1设置为11位模式
} UART1_Mode_TypeDef;

typedef enum
{
  UART1_RX_ENABLE  = 0X10,//UART1允许接收
  UART1_RX_DISABLE = 0X00 //UART1禁止接收
} UART1_RX_TypeDef;

typedef enum
{
  SPI_FLAG_SPIF    = (uint8_t)0x80, //SPI数据传送标志位SPIF
  SPI_FLAG_WCOL    = (uint8_t)0x50, //SPI写入冲突标志位WCOL
  SPI_FLAG_TXE     = (uint8_t)0x08, //SPI发送缓存器空标志TXE
  TWI_FLAG_TWIF    = (uint8_t)0x40,	//TWI中断标志位TWIF
  TWI_FLAG_GCA	   = (uint8_t)0x10,	//TWI通用地址响应标志位GCA
  UART1_FLAG_TI	   = (uint8_t)0x02,	//UART1发送中断标志位TI
  UART1_FLAG_RI	   = (uint8_t)0x01	//UART1接收中断标志位RI
} SSI_Flag_TypeDef;

#if defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
typedef enum
{
  SSI_PinSelection_P10P27P26 = (uint8_t)0x00, 		   //SSI共用引脚为P10，P27，P26
  SSI_PinSelection_P21P22P23 = (uint8_t)0x20, 		   //SSI共用引脚为P21，P22，P23
  SSI_PinSelection_URATP27 = (uint8_t)0x00,  		   //SSIUART引脚为P27,无RX
  SSI_PinSelection_URATP22 = (uint8_t)0x20  		   //SSIUART引脚为P22,无RX
} SSI_PinSelection_TypeDef;
void SSI_PinSelection(SSI_PinSelection_TypeDef
                      PinSeletion);
#endif

void SSI_DeInit(void);
void SSI_SPI_Init(SPI_FirstBit_TypeDef FirstBit,
                  SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,
                  SPI_Mode_TypeDef Mode,
                  SPI_ClockPolarity_TypeDef ClockPolarity,
                  SPI_ClockPhase_TypeDef ClockPhase,
                  SPI_TXE_INT_TypeDef SPI_TXE_INT);
void SSI_SPI_Cmd(FunctionalState NewState);
void SSI_SPI_SendData(uint8_t Data);
uint8_t SSI_SPI_ReceiveData(void);
void SSI_TWI_Init(uint8_t TWI_Address);
void SSI_TWI_AcknowledgeConfig(FunctionalState NewState);
void SSI_TWI_GeneralCallCmd(FunctionalState NewState);
FlagStatus SSI_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState);
FlagStatus SSI_GetFlagStatus(SSI_Flag_TypeDef SSI_FLAG);
void SSI_TWI_Cmd(FunctionalState NewState);
void SSI_TWI_SendData(uint8_t Data);
uint8_t SSI_TWI_ReceiveData(void);
void SSI_UART1_Init(uint32_t UART1Fsys,
                    uint32_t BaudRate, UART1_Mode_TypeDef Mode,
                    UART1_RX_TypeDef RxMode);
void SSI_UART1_SendData8(uint8_t Data);
uint8_t SSI_UART1_ReceiveData8(void);
void SSI_UART1_SendData9(uint16_t Data);
uint16_t SSI_UART1_ReceiveData9(void);
void SSI_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority);
void SSI_ClearFlag(SSI_Flag_TypeDef SSI_FLAG);
#endif

#if defined (SC92F742x) || defined (SC92F7490)
typedef enum
{
  SPI_FIRSTBIT_MSB = (uint8_t)0x00,			//MSB优先发送
  SPI_FIRSTBIT_LSB = (uint8_t)0x04			//LSB优先发送
} SPI_FirstBit_TypeDef;

typedef enum
{
  SPI_BAUDRATEPRESCALER_1   = (uint8_t)0x00,	//SPI时钟速率为Fsys/1
  SPI_BAUDRATEPRESCALER_2   = (uint8_t)0x01,	//SPI时钟速率为Fsys/2
  SPI_BAUDRATEPRESCALER_4   = (uint8_t)0x02,	//SPI时钟速率为Fsys/4
  SPI_BAUDRATEPRESCALER_8   = (uint8_t)0x03,	//SPI时钟速率为Fsys/8
  SPI_BAUDRATEPRESCALER_16  = (uint8_t)0x04,	//SPI时钟速率为Fsys/16
  SPI_BAUDRATEPRESCALER_32  = (uint8_t)0x05,	//SPI时钟速率为Fsys/32
  SPI_BAUDRATEPRESCALER_64  = (uint8_t)0x06,	//SPI时钟速率为Fsys/64
  SPI_BAUDRATEPRESCALER_128 = (uint8_t)0x07 	//SPI时钟速率为Fsys/128
} SPI_BaudRatePrescaler_TypeDef;

typedef enum
{
  SPI_MODE_MASTER = (uint8_t)0x20,			//SPI为主设备
  SPI_MODE_SLAVE  = (uint8_t)0x00				//SPI为从设备
} SPI_Mode_TypeDef;

typedef enum
{
  SPI_CLOCKPOLARITY_LOW  = (uint8_t)0x00,		//SCK在空闲状态下为低电平
  SPI_CLOCKPOLARITY_HIGH = (uint8_t)0x10  	//SCK在空闲状态下为高电平
} SPI_ClockPolarity_TypeDef;

typedef enum
{
  SPI_CLOCKPHASE_1EDGE = (uint8_t)0x00,		//SCK的第一沿采集数据
  SPI_CLOCKPHASE_2EDGE = (uint8_t)0x08		//SCK的第二沿采集数据
} SPI_ClockPhase_TypeDef;

typedef enum
{
  SPI_TXE_DISINT = (uint8_t)0x00, 			//TXE为0时不允许发送中断
  SPI_TXE_ENINT  = (uint8_t)0x01  			//TXE为1时允许发送中断
} SPI_TXE_INT_TypeDef;

typedef enum
{
  UART_Mode_10B  = 0X00,						//UART设置为10位模式
  UART_Mode_11B  = 0X80						//UART设置为11位模式
} UART_Mode_TypeDef;

typedef enum
{
  UART_RX_ENABLE  = 0X10,						//UART允许接收
  UART_RX_DISABLE = 0X00						//UART禁止接收
} UART_RX_TypeDef;

typedef enum{
  //作为从机
  TWI_SlaveBusy = 0x00,
  TWI_SlaveReceivedaAddress = 0x01,
  TWI_SlaveReceivedaData = 0x02,
  TWI_SlaveSendData = 0x03,
  TWI_SlaveReceivedaUACK = 0x04,
  TWI_SlaveDisableACK = 0x05,
  TWI_SlaveAddressError = 0x06,
  
}SSI_TWIState_TypeDef;

typedef enum
{
  SPI_FLAG_SPIF    = (uint8_t)0x80,				//SPI数据传送标志位SPIF
  SPI_FLAG_WCOL    = (uint8_t)0x50, 			//SPI写入冲突标志位WCOL
  SPI_FLAG_TXE     = (uint8_t)0x08,  			//SPI发送缓存器空标志TXE
  TWI_FLAG_TWIF    = (uint8_t)0x40,				//TWI中断标志位TWIF
  TWI_FLAG_GCA	   = (uint8_t)0x10,				//TWI通用地址响应标志位GCA
  UART_FLAG_TI	   = (uint8_t)0x02,				//UART发送中断标志位TI
  UART_FLAG_RI	   = (uint8_t)0x01				//UART接收中断标志位RI
} SSI_Flag_TypeDef;

void SSI0_DeInit(void);
void SSI0_SPI_Init(SPI_FirstBit_TypeDef FirstBit,
                   SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,
                   SPI_Mode_TypeDef Mode,
                   SPI_ClockPolarity_TypeDef ClockPolarity,
                   SPI_ClockPhase_TypeDef ClockPhase,
                   SPI_TXE_INT_TypeDef SPI_TXE_INT);
void SSI0_SPI_Cmd(FunctionalState NewState);
void SSI0_SPI_SendData(uint8_t Data);
uint8_t SSI0_SPI_ReceiveData(void);
void SSI0_TWI_Init(uint8_t TWI_Address);
void SSI0_TWI_AcknowledgeConfig(FunctionalState NewState);
void SSI0_TWI_GeneralCallCmd(FunctionalState NewState);
FlagStatus SSI0_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState);
void SSI0_TWI_Cmd(FunctionalState NewState);
void SSI0_TWI_SendData(uint8_t Data);
uint8_t SSI0_TWI_ReceiveData(void);
void SSI0_UART_Init(uint32_t UARTFsys,
                    uint32_t BaudRate, UART_Mode_TypeDef Mode,
                    UART_RX_TypeDef RxMode);
void SSI0_UART_SendData8(uint8_t Data);
uint8_t SSI0_UART_ReceiveData8(void);
void SSI0_UART_SendData9(uint16_t Data);
uint16_t SSI0_UART_ReceiveData9(void);
void SSI0_ITConfig(FunctionalState NewState,
                   PriorityStatus Priority);
FlagStatus SSI0_GetFlagStatus(SSI_Flag_TypeDef SSI_FLAG);
void SSI0_ClearFlag(SSI_Flag_TypeDef SSI_FLAG);

void SSI1_DeInit(void);
void SSI1_SPI_Init(SPI_FirstBit_TypeDef FirstBit,
                   SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,
                   SPI_Mode_TypeDef Mode,
                   SPI_ClockPolarity_TypeDef ClockPolarity,
                   SPI_ClockPhase_TypeDef ClockPhase,
                   SPI_TXE_INT_TypeDef SPI_TXE_INT);
void SSI1_SPI_Cmd(FunctionalState NewState);
void SSI1_SPI_SendData(uint8_t Data);
uint8_t SSI1_SPI_ReceiveData(void);
void SSI1_TWI_Init(uint8_t TWI_Address);
void SSI1_TWI_AcknowledgeConfig(FunctionalState NewState);
void SSI1_TWI_GeneralCallCmd(FunctionalState NewState);
FlagStatus SSI1_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState);
void SSI1_TWI_Cmd(FunctionalState NewState);
void SSI1_TWI_SendData(uint8_t Data);
uint8_t SSI1_TWI_ReceiveData(void);
void SSI1_UART_Init(uint32_t UARTFsys,
                    uint32_t BaudRate, UART_Mode_TypeDef Mode,
                    UART_RX_TypeDef RxMode);
void SSI1_UART_SendData8(uint8_t Data);
uint8_t SSI1_UART_ReceiveData8(void);
void SSI1_UART_SendData9(uint16_t Data);
uint16_t SSI1_UART_ReceiveData9(void);
void SSI1_ITConfig(FunctionalState NewState,
                   PriorityStatus Priority);
FlagStatus SSI1_GetFlagStatus(SSI_Flag_TypeDef
                              SSI_FLAG);
void SSI1_ClearFlag(SSI_Flag_TypeDef SSI_FLAG);
#endif

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
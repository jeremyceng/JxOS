//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_chksum.h
//	作者		:
//	模块功能	: CHKSUM固件库函数头文件
//  最后更正日期: 2021/8/20
// 	版本		: V1.10000
//  说明        :
//*************************************************************

#ifndef _sc92f_CHKSUM_H_
#define	_sc92f_CHKSUM_H_

#include "sc92f.h"

#if defined(SC92F7003) || defined(SC92F8003) || defined(SC92F736xB) || defined(SC92F836xB) || defined(SC92F740x) || defined(SC92F742x)\
		|| defined(SC92F73Ax) || defined(SC92F83Ax) || defined(SC92F744xB) || defined(SC92F844xB) || defined(SC92F746xB) || defined(SC92F846xB)\
		|| defined(SC92F748x) || defined(SC92F848x) || defined(SC92F74Ax) || defined(SC92F84Ax) || defined(SC92F74Ax_2) || defined(SC92F84Ax_2)\
		|| defined(SC92F754x)	|| defined (SC92F854x) || defined (SC92F759x) || defined(SC92F859x) || defined (SC92F7490) || defined(SC92FWxx)\
		|| defined(SC92F827X) || defined(SC92F847X)
  void CHKSUM_DeInit(void);
  void CHKSUM_StartOperation(void);
  uint16_t CHKSUM_GetCheckValue(void);
#endif

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
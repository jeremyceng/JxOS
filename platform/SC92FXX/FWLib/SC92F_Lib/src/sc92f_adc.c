//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_adc.c
//	作者	:
//	模块功能	: ADC固件库函数C文件
//  最后更正日期: 2021/09/18
// 	版本	: V1.10005
//  说明	:
//*************************************************************

#include "sc92f_adc.h"

#if !defined(SC92F827X) && !defined(SC92F837X)
/**************************************************
*函数名称:void ADC_DeInit(void)
*函数功能:ADC相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void ADC_DeInit(void)
{
  ADCCON = 0x00;
  ADCCFG0 = 0X00;
  ADCCFG1 = 0X00;
#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
	|| defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F84Ax) || defined(SC92F74Ax)\
	|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92F848x) || defined(SC92F748x) || defined (SC92F859x) || defined (SC92F759x)\
  || defined (SC92L853x) || defined (SC92L753x)	
  ADCCFG2 = 0X00;
#endif
  ADCVL = 0X00;
  ADCVH = 0X00;
  EADC = 0;
  IPADC = 0;
}

/**************************************************
*函数名称:void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection, ADC_Cycle_TypeDef ADC_Cycle)
*函数功能:ADC初始化配置函数
*入口参数:
ADC_PresSel_TypeDef:ADC_PrescalerSelection:预分频选择
ADC_Cycle_TypeDef:ADC_Cycle:采样时钟周期选择
*出口参数:void
**************************************************/

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
	|| defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F84Ax) || defined(SC92F74Ax)\
	|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92FWxx) || defined(SC92F848x) || defined(SC92F748x) || defined (SC92F859x) || defined (SC92F759x)
void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection,
              ADC_Cycle_TypeDef ADC_Cycle)
{
  uint8_t tmpreg;
  tmpreg = ADC_PrescalerSelection | ADC_Cycle;
  ADCCFG2 = tmpreg;
}
#elif defined(SC92F7003) || defined(SC92F8003) || defined(SC92F740x)
void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection,
              ADC_Cycle_TypeDef ADC_Cycle)
{
	ADCCFG1 = ADC_PrescalerSelection | ADC_Cycle;
}
#elif defined(SC92F742x) || defined(SC92F725X) || defined(SC92F735X) || defined(SC92F730x) || defined(SC92F732X) || defined(SC92F7490) || defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection,
              ADC_Cycle_TypeDef ADC_Cycle)
{
	ADC_Cycle = 0; //SC92F742x无此功能
  ADCCON = (ADCCON & 0xdf) | ADC_PrescalerSelection;
}
#elif defined (SC92L853x) || defined (SC92L753x)
void ADC_Init(ADC_PresSel_TypeDef ADC_PrescalerSelection, ADC_Cycle_TypeDef ADC_Cycle)
{
  ADCCFG2 = ADC_PrescalerSelection;
  ADC_Cycle = 0x00; //该入参在92L系列无效
}
#endif

/**************************************************
*函数名称:void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel, FunctionalState NewState)
*函数功能:ADC输入口配置函数，并且使能相关功能
*入口参数:
ADC_Channel_TypeDef:ADC_Channel:ADC输入口选择
FunctionalState:NewState:ADCx使能/关闭选择
*出口参数:void
**************************************************/

#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
		|| defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F84Ax) || defined(SC92F74Ax)\
		|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92FWxx) || defined(SC92F848x) || defined(SC92F748x)\
		|| defined(SC92F859x) || defined(SC92F759x) || defined (SC92L853x) || defined (SC92L753x)
void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel, FunctionalState NewState)
{
  uint16_t tmpreg;
  ADCCON = (ADCCON & 0XE0) | ADC_Channel;
  tmpreg = (0x0001 << ADC_Channel);

  if (ADC_Channel < ADC_CHANNEL_VDD_D4)
  {
    if (NewState == DISABLE)
    {
      ADCCFG0 &= (~(uint8_t)tmpreg);
      ADCCFG1 &= (~(uint8_t)(tmpreg >> 8));
    }
    else
    {
      ADCCFG0 |= ((uint8_t)tmpreg);
      ADCCFG1 |= ((uint8_t)(tmpreg >> 8));
    }
  }
}
#elif defined(SC92F7003) || defined(SC92F8003) || defined(SC92F740x) || defined(SC92F7490)
void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel, FunctionalState NewState)
{
  uint8_t tmpreg;
  ADCCON = (ADCCON & 0xE0) | ADC_Channel;
  tmpreg = (0x01 << ADC_Channel);

  if (ADC_Channel < ADC_CHANNEL_VDD_D4)
  {
    if (NewState == DISABLE)
    {
      ADCCFG0 &= (~tmpreg);
    }
    else
    {
      ADCCFG0 |= tmpreg;
    }
  }
}
#elif defined(SC92F742x) || defined(SC92F730x) || defined(SC92F725X) || defined(SC92F735X) || defined(SC92F732X)
void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel, FunctionalState NewState)
{
  uint16_t tmpreg;
  ADCCON = (ADCCON & 0xF0) | ADC_Channel;
  if (ADC_Channel < 0x0e)
  {
    tmpreg = (0x0001 << ADC_Channel);
    if (NewState == DISABLE)
    {
      ADCCFG0 &= (~(uint8_t)tmpreg);
      ADCCFG1 &= (~(uint8_t)(tmpreg >> 8));
    }
    else
    {
      ADCCFG0 |= ((uint8_t)tmpreg);
      ADCCFG1 |= ((uint8_t)(tmpreg >> 8));
    }
  }
}
#elif defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
void ADC_ChannelConfig(ADC_Channel_TypeDef ADC_Channel, FunctionalState NewState)
{
  uint16_t tmpreg;
  unsigned char code *IFBAddr = 0x3D;
  ADCCON = (ADCCON & 0xF0) | ADC_Channel;
  if (ADC_Channel < ADC_CHANNEL_Temp)
  {
    tmpreg = (0x0001 << ADC_Channel);
    if (NewState == DISABLE)
    {
      ADCCFG0 &= (~(uint8_t)tmpreg);
      ADCCFG1 &= (~(uint8_t)(tmpreg >> 8));
    }
    else
    {
      ADCCFG0 |= ((uint8_t)tmpreg);
      ADCCFG1 |= ((uint8_t)(tmpreg >> 8));
    }
  }
  else if (ADC_Channel == ADC_CHANNEL_Temp)
  {
    if (NewState == DISABLE)
    {
      TSCFG &= 0X7F;
    }
    else
    {
      TSCFG |= 0x80;
      OPINX = 0xC2;
      OPREG = OPREG & 0X7F | 0x80;
    }
  }
  else if (ADC_Channel == ADC_CHANNEL_9_PGA)
  {
    if (NewState == DISABLE)
    {
      ADCCFG1 &= (~(uint8_t)0x02); //关闭通道9
      PGACON &= 0x7F;              //使能PGA
    }
    else
    {
      ADCCFG1 |= ((uint8_t)0x02); //使能通道9

      IAPADE = 0x01;       //指针指向 IFB 区
      PGACFG = *(IFBAddr); //读取校准值
      IAPADE = 0x00;       //指针指向 ROM 区

      PGACON |= 0x80; //使能PGA
    }
  }
}
#endif
                               
/**************************************************
*函数名称:void ADC_EAINConfig(uint16_t ADC_Channel, FunctionalState NewState)
*函数功能:将对应的ADC输入口设置为模拟输入模式
*入口参数:
ADC_EAIN_TypeDef:ADC_EAIN_Select:选择需要设置的ADC口
FunctionalState:NewState:ADC通道使能/关闭选择
*出口参数:void
**************************************************/
void ADC_EAINConfig(uint16_t ADC_EAIN_Select,
                    FunctionalState NewState)
{
  if (NewState == DISABLE)
  {
    ADCCFG0 &= (~(uint8_t)ADC_EAIN_Select);
#if defined (SC92F854x) || defined (SC92F754x) || defined  (SC92F844xB) || defined (SC92F744xB) || defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)||defined  (SC92F84Ax) || defined (SC92F74Ax)\
		|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92F848x) || defined(SC92F748x) || defined(SC92F859x)  || defined (SC92F759x)\
		|| defined (SC92L853x) || defined (SC92L753x)
    ADCCFG1 &= (~(uint8_t)(ADC_EAIN_Select >> 8));
#endif
  }
  else
  {
    ADCCFG0 |= ((uint8_t)ADC_EAIN_Select);
#if defined (SC92F854x) || defined (SC92F754x) || defined (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined  (SC92F84Ax) || defined (SC92F74Ax)\
		|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92F848x) || defined(SC92F748x) || defined(SC92F859x) || defined (SC92F759x)\
		|| defined (SC92L853x) || defined (SC92L753x)
    ADCCFG1 |= ((uint8_t)(ADC_EAIN_Select >> 8));
#endif
  }
}
/*****************************************************
*函数名称:void ADC_Cmd(FunctionalState NewState)
*函数功能:ADC功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void ADC_Cmd(FunctionalState NewState)
{
  if (NewState == DISABLE)
  {
    ADCCON &= 0X7F;
  }
  else
  {
    ADCCON |= 0x80;
  }
}

/*****************************************************
*函数名称:void ADC_StartConversion(void)
*函数功能:开始一次AD转换
*入口参数:void
*出口参数:void
*****************************************************/
void ADC_StartConversion(void)
{
  ADCCON |= 0X40;
}

/*****************************************************
*函数名称:uint16_t ADC_GetConversionValue(void)
*函数功能:获得一次AD转换数据
*入口参数:void
*出口参数:uint16_t
*****************************************************/
uint16_t ADC_GetConversionValue(void)
{
  uint16_t temp;
  temp = (uint16_t)(ADCVH << 4) + (ADCVL >> 4);
  return temp;
}

/*****************************************************
*函数名称:void ADC_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:ADC中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void ADC_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState == DISABLE)
  {
    EADC = 0;
  }
  else
  {
    EADC = 1;
  }

  /************************************************************/
  if (Priority == LOW)
  {
    IPADC = 0;
  }
  else
  {
    IPADC = 1;
  }
}

/*****************************************************
*函数名称:FlagStatus ADC_GetFlagStatus(void)
*函数功能:获得ADC中断标志状态
*入口参数:void
*出口参数:
FlagStatus:ADC中断标志状态
*****************************************************/
FlagStatus ADC_GetFlagStatus(void)
{
#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
	 || defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)||defined  (SC92F84Ax) || defined (SC92F74Ax)\
	 || defined  (SC92F83Ax) || defined (SC92F73Ax) || defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x) || defined (SC92FWxx)\
	|| defined(SC92F848x) || defined(SC92F748x) || defined(SC92F859x) || defined (SC92F759x)	|| defined (SC92L853x) || defined (SC92L753x)
  return (bool)(ADCCON & 0x20);
#elif defined(SC92F742x) || defined(SC92F730x) || defined(SC92F725X) || defined(SC92F735X) || defined(SC92F732X) || defined(SC92F7490) || defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
  return (bool)(ADCCON & 0x10);
#endif
}

/*****************************************************
*函数名称:void ADC_ClearFlag(void)
*函数功能:清除ADC中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void ADC_ClearFlag(void)
{
#if defined(SC92F854x) || defined(SC92F754x) || defined(SC92F844xB) || defined(SC92F744xB) || defined(SC92F84Ax_2) || defined(SC92F74Ax_2)\
		|| defined(SC92F846xB) || defined(SC92F746xB) || defined(SC92F836xB) || defined(SC92F736xB) || defined(SC92F84Ax) || defined(SC92F74Ax)\
		|| defined(SC92F83Ax) || defined(SC92F73Ax) || defined(SC92F8003) || defined(SC92F740x) || defined(SC92F848x) || defined(SC92F748x)\
		|| defined(SC92F859x) || defined(SC92F759x)	|| defined (SC92L853x) || defined (SC92L753x)
  ADCCON &= 0xdf;
#endif
#if defined(SC92F742x) || defined(SC92F730x) || defined(SC92F725X) || defined(SC92F735X) || defined(SC92F732X) || defined(SC92F7490) || defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
  ADCCON &= 0xef;
#endif
}

#if defined(SC93F833x) || defined(SC93F843x) || defined(SC93F743x)
unsigned int ADC_TS_StandardData = 0x8000;

/*****************************************************
*函数名称:void ADC_TSCmd(PriorityStatus NewState)
*函数功能:ADC 温度传感器功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void ADC_TSCmd(FunctionalState NewState)
{
  if (NewState == DISABLE)
  {
    TSCFG &= 0X7F;
  }
  else
  {
    TSCFG |= 0X80;
  }
}

/*****************************************************
*函数名称:void ADC_CHOPConfig(PriorityStatus NewState)
*函数功能:拉高或者拉低抵消offset的应用控制位
*入口参数:
PriorityStatus:NewState:温度传感器高/低选择
*出口参数:void
*****************************************************/
void ADC_CHOPConfig(PriorityStatus NewState)
{
  if (NewState == LOW)
  {
    TSCFG &= 0XFE;
  }
  else
  {
    TSCFG |= 0x01;
  }
}

/*****************************************************
*函数名称:void ADC_CHOPConfig(PriorityStatus NewState)
*函数功能:获取出厂时ADC 25度时的转换值
*入口参数:void
*出口参数:void
*****************************************************/
uint16_t ADC_Get_TS_StandardData(void)
{
  unsigned int code *IFBAddr = 0x3E;
  IAPADE = 0x01; //指针指向 IFB 区
  ADC_TS_StandardData = *(IFBAddr);
  IAPADE = 0x00; //指针指向 ROM 区
  return ADC_TS_StandardData;
}

/*****************************************************
*函数名称:void ADC_GetTSValue(void)
*函数功能:直接获取float类型温度值，获取温度时会关闭中断
*入口参数:void
*出口参数:
float:温度值
*****************************************************/
float ADC_GetTSValue(void)
{
  unsigned char EADC_Flag = EADC; //获取EA标志位状态
  unsigned int ADC_Value1 = 0, ADC_Value2 = 0, ADC_Value = 0;
  unsigned int code *IFBAddr = 0x3E;

  ADC_Get_TS_StandardData();
  disableInterrupts();   //关闭中断
  ADC_CHOPConfig(LOW);   //拉低抵消offset的应用控制位
  ADC_StartConversion(); //开始AD转换
  while(!ADC_GetFlagStatus());    //等待转换完成
  ADC_ClearFlag();                       //清除转换标志位
  ADC_Value1 = ADC_GetConversionValue(); //获取第一次AD的转换值
  ADC_CHOPConfig(HIGH);                  //拉高抵消offset的应用控制位
  ADC_StartConversion();                 //开始AD转换
  while(!ADC_GetFlagStatus());   //等待转换完成
  ADC_ClearFlag();                           //清除转换标志位
  ADC_Value2 = ADC_GetConversionValue();     //获取第二次AD的转换值
  ADC_Value = (ADC_Value1 + ADC_Value2) / 2; //获取两次AD采样的平均值

  if (EADC_Flag) //还原加入运算前的EA标志位
  {
    enableInterrupts();
  }
  return (25 + ((float)ADC_Value - (float)ADC_TS_StandardData) / 8);
}

/*****************************************************
*函数名称:ADC_PGAConfig(ADC_PGACOM_TypeDef ADC_PGACOM,ADC_PGAGAN_TypeDef ADC_PGAGAN,ADC_PGAIPT_TypeDef ADC_PGAIPT)
*函数功能:PGA相关配置
*入口参数:
ADC_PGACOM_TypeDef:ADC_PGACOM:PGA共模电压选择
ADC_PGAGAN_TypeDef:ADC_PGAGAN:PGA增益放大倍数
ADC_PGAIPT_TypeDef:ADC_PGAIPT:PGA同相/反相选择
*出口参数:void
*****************************************************/
void ADC_PGAConfig(ADC_PGACOM_TypeDef ADC_PGACOM, ADC_PGAGAN_TypeDef ADC_PGAGAN, ADC_PGAIPT_TypeDef ADC_PGAIPT)
{
  PGACON &= 0x8F;
  PGACON |= (ADC_PGACOM | ADC_PGAGAN | ADC_PGAIPT);
}

/*****************************************************
*函数名称:void ADC_PGACmd(PriorityStatus NewState)
*函数功能:ADC PGA功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void ADC_PGACmd(PriorityStatus NewState)
{
  if (NewState == LOW)
  {
    PGACON &= 0XFE;
  }
  else
  {
    PGACON |= 0x01;
  }
}
#endif


/*****************************************************
*函数名称:void ADC_VrefConfig(ADC_Vref_TypeDef ADC_Vref)
*函数功能:ADC 参考电压选择
*入口参数:
ADC_Vref_TypeDef:ADC_Vref:选择ADC参考电压
*出口参数:void
*****************************************************/
void ADC_VrefConfig(ADC_Vref_TypeDef ADC_Vref)
{
  OPINX = 0xC2;
  OPREG = OPREG & 0X7F | ADC_Vref;
}
#endif

/******************* (C) COPYRIGHT 2021 SinOne Microelectronics *****END OF FILE****/
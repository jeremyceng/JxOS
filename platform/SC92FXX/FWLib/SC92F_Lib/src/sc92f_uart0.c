//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_uart0.c
//	作者		:
//	模块功能	: UART0固件库函数C文件
//	局部函数列表:
//  最后更正日期: 2022/01/01
// 	版本		: V1.10005
//  说明        :本文件仅适用于赛元92F/93F/92L系列单片机
//*************************************************************

#include "sc92f_uart0.h"

#if !defined (SC92F742x) && !defined (SC92F827X) && !defined (SC92F837X) && !defined (SC92F7490)

/**************************************************
*函数名称:void UART0_DeInit(void)
*函数功能:UART0相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void UART0_DeInit(void)
{
#if defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
  OTCON &= 0XEF;
#endif
  SCON = 0X00;
  PCON &= 0X7F;
  IE &= 0XEF;
  IP &= 0XEF;
}

#if defined (SC92F7003) || defined (SC92F8003)  || defined (SC92F740x)
/**************************************************
*函数名称:UART0_PinSelection(UART0_PinSelection_TypeDef PinSeletion)
*函数功能:UART0引脚选择
*入口参数:
UART0_PinSelection_TypeDef:PinSeletion:选择UART0引脚为P15P16或P11P20
*出口参数:void
**************************************************/
void UART0_PinSelection(UART0_PinSelection_TypeDef
                        PinSeletion)
{
  OTCON = OTCON & 0XDF | PinSeletion;
}
#endif

/**************************************************
*函数名称:void UART0_Init(uint32_t Uart0Fsys, uint32_t BaudRate, UART0_Mode_Typedef Mode,UART0_Clock_Typedef ClockMode, UART0_RX_Typedef RxMode)
*函数功能:UART0初始化配置函数
*入口参数:
uint32_t:Uart0Fsys:系统时钟频率
uint32_t:BaudRate:波特率
UART0_Mode_Typedef:Mode:UART0工作模式
UART0_Clock_Typedef:ClockMode:波特率时钟源（TIMER1/TIMER2）
UART0_RX_Typedef:RxMode:接收允许选择
*出口参数:void
**************************************************/
void UART0_Init(uint32_t Uart0Fsys, uint32_t BaudRate,
                UART0_Mode_Typedef Mode, UART0_Clock_Typedef ClockMode,
                UART0_RX_Typedef RxMode)
{
#if defined (SC92F725X) || defined (SC92F735X) || defined (SC92F730x ) || defined (SC92F732X)  || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
  {
    SCON  = SCON & 0X2F | Mode | RxMode;	//设置UART工作模式,设置接收允许位

    if(Mode == UART0_Mode_8B ||
        Mode == UART0_Mode_11B_BaudRateFix)
    {
      if(BaudRate == UART0_BaudRate_FsysDIV12 ||
          BaudRate == UART0_BaudRate_FsysDIV64)
      {
        PCON &= 0X7F;
      }
      else if(BaudRate == UART0_BaudRate_FsysDIV4 ||
              BaudRate == UART0_BaudRate_FsysDIV32)
      {
        PCON |= 0X80;
      }
    }
    else
    {
      T2CON = (T2CON & 0xCF) | (ClockMode &
                                0X30);		//设置波特率时钟源

      if((ClockMode & 0X70) == 0X00)
      {
        TMOD |= 0X20;
        if(ClockMode & 0x80)
        {
          PCON |= 0X80;
          Uart0Fsys = Uart0Fsys * 2;
        }
        else
        {
          PCON &= 0X7F;
        }

        if(ClockMode & 0x0F)
        {
          TMCON |= 0x02;
        }
        else
        {
          TMCON &= 0xFD;
          Uart0Fsys = Uart0Fsys / 12;
        }

        TH1 = 256 - (Uart0Fsys / 32 / BaudRate);
        TL1 = TH1;
        TR1 = 1;
      }
      else if((ClockMode & 0X70) == 0X30)
      {
        if(ClockMode & 0x0F)
        {
          TMCON |= 0x04;
        }
        else
        {
          TMCON &= 0xFB;
          Uart0Fsys = Uart0Fsys / 12;
        }

        RCAP2H = (65536 - Uart0Fsys / 32 / BaudRate) /
                 256;
        RCAP2L = (65536 - Uart0Fsys / 32 / BaudRate) %
                 256;
        TR2 = 1;
      }
    }
  }
#elif defined (SC92F848x) || defined (SC92F748x) || defined (SC92F859x) || defined (SC92F759x)
  {
    SCON  = (SCON & 0X2F) | Mode | RxMode;	//设置UART工作模式,设置接收允许位

    if(Mode == UART0_Mode_8B)
    {
      if(BaudRate == UART0_BaudRate_FsysDIV12)
      {
        PCON &= 0X7F;
      }
      else if(BaudRate == UART0_BaudRate_FsysDIV4)
      {
        PCON |= 0X80;
      }
    }
    else
    {
      T2CON = (T2CON & 0xCF) |
              (ClockMode & 0x30);		//设置波特率时钟源

      if(ClockMode & 0x80)
      {
        PCON |= 0x80;
        Uart0Fsys = Uart0Fsys / 16;
      }
      else
      {
        PCON &= 0x7F;
      }

      if((ClockMode & 0x7F)  == UART0_CLOCK_TIMER1)
      {
        TH1 = (Uart0Fsys / BaudRate) / 256;
        TL1 = (Uart0Fsys / BaudRate) % 256;
        TR1 = 0;
      }
      else if((ClockMode & 0x7F)  == UART0_CLOCK_TIMER2)
      {
        RCAP2H = (Uart0Fsys / BaudRate) / 256;
        RCAP2L = (Uart0Fsys / BaudRate) % 256;
        TR2 = 1;
      }
    }
  }
#elif defined (SC92L853x) || defined (SC92L753x)
  {
    SCON = (SCON & 0X2F) | Mode | RxMode;	//设置UART工作模式,设置接收允许位

    /* 如果UART0选择为8位半双工同步通信模式，串行端口在系统时钟的12分频或4分频下运行*/
    if(Mode == UART0_Mode_8B)
    {
      if(BaudRate == UART0_BaudRate_FsysDIV12)
      {
        PCON &= 0X7F;
      }
      else if(BaudRate == UART0_BaudRate_FsysDIV4)
      {
        PCON |= 0X80;
      }
    }
		/* UART0选择模式1/3，波特率为可变 */
    else
    {
      TXCON = (TXCON & 0xCF) | ClockMode;		//设置波特率时钟源

			/* 模式1/3可以在系统时钟的1分频或16分频下运行 */
			if(ClockMode & 0x80)
      {
        PCON |= 0x80;
        Uart0Fsys = Uart0Fsys / 16;
      }
      else
      {
        PCON &= 0x7F;
      }

			/* 配置UART0时钟源 */
      if((ClockMode & 0x7F)  == UART0_CLOCK_TIMER1)//UART0时钟源为TIMER1
      {
        TH1 = (Uart0Fsys / BaudRate) / 256;
        TL1 = (Uart0Fsys / BaudRate) % 256;
        TR1 = 0;
      }
      else if((ClockMode & 0x7F)  == UART0_CLOCK_TIMER2)//UART0时钟源为TIMER2
      {
        RCAPXH = (Uart0Fsys / BaudRate) / 256;
        RCAPXL = (Uart0Fsys / BaudRate) % 256;
        TRX = 1;
      }
    }
  }

#else
  {
    SCON  = (SCON & 0X2F) | Mode |
    RxMode;	//设置UART工作模式,设置接收允许位

    if(Mode == UART0_Mode_8B)
    {
      if(BaudRate == UART0_BaudRate_FsysDIV12)
      {
        PCON &= 0X7F;
      }
      else if(BaudRate == UART0_BaudRate_FsysDIV4)
      {
        PCON |= 0X80;
      }
    }
    else
    {
      T2CON = (T2CON & 0xCF) |
              (ClockMode & 0x30);		//设置波特率时钟源

      if(ClockMode == UART0_CLOCK_TIMER1)
      {
        TH1 = (Uart0Fsys / BaudRate) / 256;
        TL1 = (Uart0Fsys / BaudRate) % 256;
        TR1 = 0;
      }
      else if(ClockMode == UART0_CLOCK_TIMER2)
      {
        RCAP2H = (Uart0Fsys / BaudRate) / 256;
        RCAP2L = (Uart0Fsys / BaudRate) % 256;
#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
        TR2 = 1;
#endif
      }
    }
  }
#endif
}

/*****************************************************
*函数名称:void UART0_SendData8(uint8_t Data)
*函数功能:UART0发送8位数据
*入口参数:
uint8_t:Data:发送的数据
*出口参数:void
*****************************************************/
void UART0_SendData8(uint8_t Data)
{
  SBUF = Data;
}

/**************************************************
*函数名称:uint8_t UART0_ReceiveData8(void)
*函数功能:获得SBUF中的值
*入口参数:void
*出口参数:
uint8_t:UART接收到的8位数据
**************************************************/
uint8_t UART0_ReceiveData8(void)
{
  return SBUF;
}

/*****************************************************
*函数名称:void UART0_SendData9(uint16_t Data)
*函数功能:UART0发送9位数据
*入口参数:
uint16_t:Data:发送的数据
*出口参数:void
*****************************************************/
void UART0_SendData9(uint16_t Data)
{
  uint8_t Data_9Bit;
  Data_9Bit = (Data >> 8);

  if(Data_9Bit)
  {
    SCON |= 0X08;
  }
  else
  {
    SCON &= 0XF7;
  }

  SBUF = (uint8_t)Data;
}

/**************************************************
*函数名称:uint16_t UART0_ReceiveData9(void)
*函数功能:获得SBUF中的值及第九位的值
*入口参数:void
*出口参数:
uint16_t:UART接收到的数据
**************************************************/
uint16_t UART0_ReceiveData9(void)
{
  uint16_t Data9;
  Data9 =  SBUF + ((uint16_t)(SCON & 0X04) << 6);
  SCON &= 0XFB;
  return Data9;
}

/*****************************************************
*函数名称:void UART0_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:UART0中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void UART0_ITConfig(FunctionalState NewState,
                    PriorityStatus Priority)
{
  if(NewState == DISABLE)
  {
    EUART = 0;
  }
  else
  {
    EUART = 1;
  }

  //设置中断优先级
  if(Priority == LOW)
  {
    IPUART = 0;
  }
  else
  {
    IPUART = 1;
  }
}

/*****************************************************
*函数名称:FlagStatus UART0_GetFlagStatus(UART0_Flag_Typedef UART0_Flag)
*函数功能:获得UART0中断标志状态
*入口参数:
UART0_GetFlagStatus:UART0_Flag:中断标志位选择
*出口参数:
FlagStatus:UART0中断标志位置起状态
*****************************************************/
FlagStatus UART0_GetFlagStatus(UART0_Flag_Typedef
                               UART0_Flag)
{
  return (bool)(SCON & UART0_Flag);
}

/*****************************************************
*函数名称:void UART0_ClearFlag(UART0_Flag_Typedef UART0_Flag)
*函数功能:清除UART0中断标志状态
*入口参数:
UART0_Flag_Typedef;UART0_Flag:中断标志位选择
*出口参数:void
*****************************************************/
void UART0_ClearFlag(UART0_Flag_Typedef
                     UART0_Flag)
{
  SCON &=	(~UART0_Flag);
}

#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
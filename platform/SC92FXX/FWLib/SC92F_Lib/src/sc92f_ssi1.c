//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_ssi1.c
//	作者		:
//	模块功能	: SSI1固件库函数C文件
//	局部函数列表:
//  最后更正日期: 2021/09/06
// 	版本		: V1.0001
//  说明        :
//*************************************************************

#include "sc92f_ssi.h"

#if defined (SC92F742x) || defined (SC92F7490)
/**************************************************
*函数名称:void SSI1_DeInit(void)
*函数功能:SSI1相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void SSI1_DeInit(void)
{
  OTCON &= 0X3F;
  SS1CON0 = 0X00;
  SS1CON1 = 0X00;
  SS1CON2 = 0X00;
  SS1DAT = 0X00;
  IE1 &= (~0X01);
  IP1 &= (~0X01);
}

/**************************************************
*函数名称:void SSI1_SPI_Init(SPI_FirstBit_TypeDef FirstBit, SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,SPI_Mode_TypeDef Mode,
							 SPI_ClockPolarity_TypeDef ClockPolarity, SPI_ClockPhase_TypeDef ClockPhase,SPI_TXE_INT_TypeDef SPI_TXE_INT)
*函数功能:SPI初始化配置函数
*入口参数:
SPI_FirstBit_TypeDef:FirstBit:优先传送位选择（MSB/LSB）
SPI_BaudRatePrescaler_TypeDef:BaudRatePrescaler:SPI时钟频率选择
SPI_Mode_TypeDef:Mode:SPI工作模式选择
SPI_ClockPolarity_TypeDef:ClockPolarityLSPI时钟极性选择
SPI_ClockPhase_TypeDef:ClockPhase:SPI时钟相位选择
SPI_TXE_INT_TypeDef:SPI_TXE_INT:发送缓存器中断允许选择
*出口参数:void
**************************************************/
void SSI1_SPI_Init(SPI_FirstBit_TypeDef FirstBit,
                   SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,
                   SPI_Mode_TypeDef Mode,
                   SPI_ClockPolarity_TypeDef ClockPolarity,
                   SPI_ClockPhase_TypeDef ClockPhase,
                   SPI_TXE_INT_TypeDef SPI_TXE_INT)
{
  OTCON = (OTCON & 0X3F) | 0X40;
  SS1CON1 = SS1CON1 & (~0X05) | FirstBit |
            SPI_TXE_INT;
  SS1CON0 = SS1CON0 & 0X80 | BaudRatePrescaler |
            Mode | ClockPolarity | ClockPhase;
}

/*****************************************************
*函数名称:void SSI1_SPI_Cmd(FunctionalState NewState)
*函数功能:SPI功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void SSI1_SPI_Cmd(FunctionalState NewState)
{
  if(NewState != DISABLE)
  {
    SS1CON0 |= 0X80;
  }
  else
  {
    SS1CON0 &= (~0X80);
  }
}

/*****************************************************
*函数名称:void SSI1_SPI_SendData(uint8_t Data)
*函数功能:SPI发送数据
*入口参数:
uint8_t:Data:S{PI发送的8位数据
*出口参数:void
*****************************************************/
void SSI1_SPI_SendData(uint8_t Data)
{
  SS1DAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI1_SPI_ReceiveData(void)
*函数功能:获得SS1DAT中的值
*入口参数:void
*出口参数:
uint8_t:SPI接收到的8位数据
*****************************************************/
uint8_t SSI1_SPI_ReceiveData(void)
{
  return SS1DAT;
}

/**************************************************
*函数名称:void SSI1_TWI_Init(uint8_t TWI_Address)
*函数功能:TWI初始化配置函数
*入口参数:
uint8_t:TWI_Address:TWI作为从机时7位从机地址配置
*出口参数:void
**************************************************/
void SSI1_TWI_Init(uint8_t TWI_Address)
{
  OTCON = OTCON & 0X3F | 0X80;
  SS1CON1 = TWI_Address << 1;
}

/**************************************************
*函数名称:void SSI1_TWI_AcknowledgeConfig(FunctionalState NewState)
*函数功能:TWI接收应答使能函数
*入口参数:
FunctionalState:NewState:接收应答使能/失能选择
*出口参数:void
**************************************************/
void SSI1_TWI_AcknowledgeConfig(FunctionalState
                                NewState)
{
  if (NewState != DISABLE)
  {
    SS1CON0 |= 0X08;
  }
  else
  {
    SS1CON0 &= 0XF7;
  }
}

/**************************************************
*函数名称:void SSI1_TWI_GeneralCallCmd(FunctionalState NewState)
*函数功能:TWI通用地址响应使能函数
*入口参数:
FunctionalState:NewState:通用地址响应使能/失能选择
*出口参数:void
**************************************************/
void SSI1_TWI_GeneralCallCmd(FunctionalState
                             NewState)
{
  if (NewState != DISABLE)
  {
    SS1CON1 |= 0X01;
  }
  else
  {
    SS1CON1 &= 0XFE;
  }
}

/*****************************************************
*函数名称:void SSI1_TWI_Cmd(FunctionalState NewState)
*函数功能:TWI功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void SSI1_TWI_Cmd(FunctionalState NewState)
{
  if(NewState != DISABLE)
  {
    SS1CON0 |= 0X80;
  }
  else
  {
    SS1CON0 &= (~0X80);
  }
}

/**************************************************
*函数名称:FlagStatus SSI0_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState)
*函数功能:获取TWI状态机
*入口参数:
SSI_TWIState_TypeDef:SSI_TWIState:TWI状态机状态
*出口参数:void
**************************************************/
FlagStatus SSI1_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState)
{
  if((SS1CON0&0x07) == SSI_TWIState)
    return SET;
  else
    return RESET;
}

/*****************************************************
*函数名称:void SSI1_TWI_SendData(uint8_t Data)
*函数功能:TWI发送数据
*入口参数:
uint8_t:Data:TWI发送的8位数据
*出口参数:void
*****************************************************/
void SSI1_TWI_SendData(uint8_t Data)
{
  SS1DAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI1_TWI_ReceiveData(void)
*函数功能:获得SS1DAT中的值
*入口参数:void
*出口参数:
uint8_t:TWI接收到的8位数据
*****************************************************/
uint8_t SSI1_TWI_ReceiveData(void)
{
  return SS1DAT;
}

/**************************************************
*函数名称:void SSI1_UART_Init(uint32_t UARTFsys, uint32_t BaudRate, UART_Mode_TypeDef Mode, UART_RX_TypeDef RxMode)
*函数功能:UART初始化配置函数
*入口参数:
uint32_t:UARTFsys:系统时钟频率
uint32_t:BaudRate:波特率
UART_Mode_TypeDef:Mode:UART1工作模式
UART_RX_TypeDef:RxMode:接收允许选择
*出口参数:void
**************************************************/
void SSI1_UART_Init(uint32_t UARTFsys,
                    uint32_t BaudRate, UART_Mode_TypeDef Mode,
                    UART_RX_TypeDef RxMode)
{
  OTCON |= 0xC0;
  SS1CON0 = SS1CON0 & 0X0F | Mode | RxMode;
  SS1CON2 = UARTFsys / BaudRate / 256;
  SS1CON1 = UARTFsys / BaudRate % 256;
}

/*****************************************************
*函数名称:void SSI1_UART_SendData8(uint8_t Data)
*函数功能:UART发送8位数据
*入口参数:
uint8_t:Data:UART发送的8位数据
*出口参数:void
*****************************************************/
void SSI1_UART_SendData8(uint8_t Data)
{
  SS1DAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI1_UART_ReceiveData8(void)
*函数功能:获得SS0DAT中的值
*入口参数:void
*出口参数:
uint8_t:UART接收到的8位数据
*****************************************************/
uint8_t SSI1_UART_ReceiveData8(void)
{
  return SS1DAT;
}

/*****************************************************
*函数名称:void SSI1_UART_SendData9(uint16_t Data)
*函数功能:UART发送9位数据
*入口参数:
Data:发送的数据
*出口参数:void
*****************************************************/
void SSI1_UART_SendData9(uint16_t Data)
{
  uint8_t Data_9Bit;
  Data_9Bit = (Data >> 8);

  if(Data_9Bit)
  {
    SS1CON0	|= 0x08;
  }
  else
  {
    SS1CON0	&= 0xf7;
  }

  SS1DAT = (uint8_t)Data;
}

/*****************************************************
*函数名称:uint16_t SSI1_UART_ReceiveData9(void)
*函数功能:获得SS1DAT中的值及第九位的值
*入口参数:void
*出口参数:
uint16_t:接收到的数据
*****************************************************/
uint16_t SSI1_UART_ReceiveData9(void)
{
  uint16_t Data9;
  Data9 =  SS1DAT + ((uint16_t)(SS1CON0 & 0X04) <<
                     6);
  SS1CON0 &= 0XFB;
  return Data9;
}

/*****************************************************
*函数名称:void SSI1_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:SSI1中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void SSI1_ITConfig(FunctionalState NewState,
                   PriorityStatus Priority)
{
  //中断使能/关闭设置
  if(NewState != DISABLE)
  {
    IE1 |= 0x01;
  }
  else
  {
    IE1 &= 0xFE;
  }

  //中断优先级设置
  if(Priority != LOW)
  {
    IP1 |= 0x01;
  }
  else
  {
    IP1 &= 0xFE;
  }
}

/*****************************************************
*函数名称:FlagStatus SSI1_GetFlagStatus(SSI_Flag_TypeDef SSI_FLAG)
*函数功能:获得SSI1标志状态
*入口参数:
SSI_Flag_TypeDef:SSI_FLAG:所需获取的标志位
*出口参数:
FlagStatus:SSI标志位置起状态
*****************************************************/
FlagStatus SSI1_GetFlagStatus(SSI_Flag_TypeDef
                              SSI_FLAG)
{
  FlagStatus bitstatus = RESET;

  if((SSI_FLAG == SPI_FLAG_SPIF) ||
      (SSI_FLAG == SPI_FLAG_WCOL) ||
      (SSI_FLAG == SPI_FLAG_TXE))
  {
    if((SSI_FLAG & SS1CON1) != (uint8_t)RESET)
    {
      bitstatus = SET;
    }
    else
    {
      bitstatus = RESET;
    }
  }
  else
  {
    if((SSI_FLAG & SS1CON0) != (uint8_t)RESET)
    {
      bitstatus = SET;
    }
    else
    {
      bitstatus = RESET;
    }
  }

  return bitstatus;
}

/*****************************************************
*函数名称:void SSI1_ClearFlag(SSI_Flag_TypeDef SSI_FLAG)
*函数功能:清除SSI1标志状态
*入口参数:
SSI_Flag_TypeDef:SSI_FLAG:所需清除的标志位
*出口参数:void
*****************************************************/
void SSI1_ClearFlag(SSI_Flag_TypeDef SSI_FLAG)
{
  if((SSI_FLAG == SPI_FLAG_SPIF) ||
      (SSI_FLAG == SPI_FLAG_WCOL) ||
      (SSI_FLAG == SPI_FLAG_TXE))
  {
    SS1CON1 &= (~SSI_FLAG);
  }
  else
  {
    SS1CON0 &= (~SSI_FLAG);
  }
}
#endif

/******************* (C) COPYRIGHT 2018 SinOne Microelectronics *****END OF FILE****/
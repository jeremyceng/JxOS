//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_ddic.c
//	作者		:
//	模块功能	: DDIC固件库函数C文件
//	局部函数列表:
//  最后更正日期: 2022/01/20
// 	版本		: V1.10002
//  说明        :
//*************************************************************

#include "sc92f_ddic.h"

#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined (SC92F859x) || defined (SC92F759x) || defined (SC92L853x) || defined (SC92L753x)

#if defined (SC92L853x) || defined (SC92L753x)
uint8_t xdata LCDRAM[30] _at_ 0xF00;
#else
uint8_t xdata LCDRAM[30] _at_ 0x700;
#endif

/**************************************************
*函数名称:void DDIC_DeInit(void)
*函数功能:DDIC相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void DDIC_DeInit(void)
{
  DDRCON = 0X00;
  P0VO = 0X00;
  P1VO = 0X00;
  P2VO = 0X00;
#if defined (SC92L853x) || defined (SC92L753x)
	P5VO = 0X00;
#else
  P3VO = 0X00;
#endif
  OTCON &= 0XF0;
}

/**************************************************
*函数名称:void DDIC_Init(uint8_t P0OutputPin)
*函数功能:DDIC初始化配置函数
*入口参数:
DDIC_DutyCycle_TypeDef:DDIC_DutyCylce:LCD/LED显示占空比控制
DDIC_Pin_TypeDef:P0OutputPin:设置P0口引脚为LCD电压输出口,可以使用或语句操作枚举的多个成员
DDIC_Pin_TypeDef:P1OutputPin:设置P1口引脚为LCD电压输出口,可以使用或语句操作枚举的多个成员
DDIC_Pin_TypeDef:P2OutputPin:设置P2口引脚为LCD电压输出口,可以使用或语句操作枚举的多个成员
DDIC_Pin_TypeDef:P3OutputPin:设置P3口引脚为LCD电压输出口,可以使用或语句操作枚举的多个成员
													注:当型号为SC92L853x或SC92L753x时，实际为设置P5口
*出口参数:void
**************************************************/
void DDIC_Init(DDIC_DutyCycle_TypeDef DDIC_DutyCylce,
               uint8_t P0OutputPin, uint8_t P1OutputPin,
               uint8_t P2OutputPin, uint8_t P3OutputPin)
{
  DDRCON = DDRCON & 0XCF | DDIC_DutyCylce;
  P0VO = P0OutputPin;
  P1VO = P1OutputPin;
  P2VO = P2OutputPin;
#if defined (SC92L853x) || defined (SC92L753x)
	P5VO = P3OutputPin;
#else
  P3VO = P3OutputPin;
#endif
}

/**************************************************
*函数名称:void DDIC_LEDConfig(void)
*函数功能:LED配置函数
*入口参数:void
*出口参数:void
**************************************************/
void DDIC_LEDConfig(void)
{
  DDRCON |= 0X40;
}

/**************************************************
*函数名称:void DDIC_LCDConfig(uint8_t LCDVoltage,
                    DDIC_ResSel_Typedef DDIC_ResSel,
                    DDIC_BiasVoltage_Typedef DDIC_BiasVoltage)
*函数功能:LCD配置函数
*入口参数:
uint8_t:LCDVoltage:LCD电压调节
DDIC_ResSel_Typedef:DDIC_ResSel:LCD电压输出口电阻选择
DDIC_BiasVoltage_Typedef:DDIC_BiasVoltage:LCD显示驱动偏置电压设置
*出口参数:void
**************************************************/
void DDIC_LCDConfig(uint8_t LCDVoltage,
                    DDIC_ResSel_Typedef DDIC_ResSel,
                    DDIC_BiasVoltage_Typedef DDIC_BiasVoltage)
{
  DDRCON = DDRCON & 0XB0 | LCDVoltage;
  OTCON = OTCON & 0XF2 | DDIC_ResSel | DDIC_BiasVoltage;
}
/**************************************************
*函数名称:void DDIC_DMOD_Selcet(DDIC_DMOD_TypeDef DDIC_DMOD)
*函数功能:显示驱动模式选择
*入口参数:
DDIC_DMOD_TypeDef:DDIC_DMOD:选择显示模式
*出口参数:void
**************************************************/
void DDIC_DMOD_Selcet(DDIC_DMOD_TypeDef DDIC_DMOD)
{
  if(DDIC_DMOD == DMOD_LED)
  {
    DDRCON |= 0X40;
  }
  else
  {
    DDRCON &= 0XBF;
  }
}
/*****************************************************
*函数名称:void DDIC_OutputPinOfDutycycleD4(DDIC_OutputPin_TypeDef DDIC_OutputPin)
*函数功能:1/4占空比时segment与common复用管脚选择
*入口参数:
DDIC_OutputPin_TypeDef:DDIC_OutputPin:输出管脚选择
*出口参数:void
*****************************************************/
void DDIC_OutputPinOfDutycycleD4(
  DDIC_OutputPin_TypeDef DDIC_OutputPin)
{
	OTCON &=  ~0X02;
	OTCON = DDIC_OutputPin<<1;
}

/*****************************************************
*函数名称:void DDIC_Cmd(FunctionalState NewState)
*函数功能:显示驱动功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void DDIC_Cmd(FunctionalState NewState)
{
  if(NewState == DISABLE)
  {
    DDRCON &= 0X7F;
  }
  else
  {
    DDRCON |= 0x80;
  }
}

/*****************************************************
*函数名称:void DDIC_Control(DDIC_Control_SEG_TypeDef DDIC_Seg,DDIC_Control_COM_TypeDef DDIC_Com,DDIC_Control_Status DDIC_Contr)
*函数功能:控制输入的SEG和COM脚对应LCD/LED的亮灭
*入口参数:DDIC_Control_SEG_TypeDef DDIC_Seg 选择控制的SEG口
		   DDIC_Control_COM_TypeDef DDIC_Com 选择控制的COM口
		   DDIC_Control_Status DDIC_Contr	控制状态
*出口参数:void
*****************************************************/
void DDIC_Control(DDIC_Control_SEG_TypeDef DDIC_Seg,
                  uint8_t DDIC_Com,
                  DDIC_Control_Status DDIC_Contr)
{
  if(DDIC_Contr)
  {
    LCDRAM[DDIC_Seg] |= DDIC_Com;
  }
  else
  {
    LCDRAM[DDIC_Seg] &= (~DDIC_Com);
  }
}
#endif

#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined (SC92F83Ax) || defined (SC92F73Ax)\
		|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F742x) || defined (SC92F730x) || defined (SC92F725X) || defined (SC92F735X)\
		|| defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) || defined (SC92F848x) || defined (SC92F748x)
/**************************************************
*函数名称:void DDIC_DeInit(void)
*函数功能:DDIC相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void DDIC_DeInit(void)
{
  P0VO   = 0X00;
  OTCON &= 0XF3;
}

/**************************************************
*函数名称:void DDIC_Init(uint8_t P0OutputPin)
*函数功能:DDIC初始化配置函数
*入口参数:
DDIC_Pin_TypeDef:P0OutputPin:设置P0口引脚为LCD电压输出口，可以使用或语句操作枚举的多个成员
*出口参数:void
**************************************************/
void DDIC_Init(uint8_t P0OutputPin)
{
  P0VO = P0OutputPin;
}

/**************************************************
*函数名称:void DDIC_LCDConfig(DDIC_ResSel_Typedef DDIC_ResSel)
*函数功能:LCD配置函数
*入口参数:
DDIC_ResSel_Typedef:DDIC_ResSel:LCD电压输出口电阻选择
*出口参数:void
**************************************************/
void DDIC_LCDConfig(DDIC_ResSel_Typedef DDIC_ResSel)
{
  OTCON = (OTCON & 0XF3) | DDIC_ResSel;
}

/**************************************************
*函数名称:void DDIC_Config_Init(uint8_t P0OutputPin,DDIC_ResSel_Typedef DDIC_ResSel)
*函数功能:LCD初始化及配置函数
*入口参数:
DDIC_Pin_TypeDef:P0OutputPin:设置P0口引脚为LCD电压输出口，可以使用或语句操作枚举的多个成员
DDIC_ResSel_Typedef:DDIC_ResSel:LCD电压输出口电阻选择
*出口参数:void
**************************************************/
void DDIC_Config_Init(uint8_t P0OutputPin,
                      DDIC_ResSel_Typedef DDIC_ResSel)
{
  P0VO = P0OutputPin;
  OTCON = (OTCON & 0XF3) | DDIC_ResSel;
}

#endif
/******************* (C) COPYRIGHT 2019 SinOne Microelectronics *****END OF FILE****/
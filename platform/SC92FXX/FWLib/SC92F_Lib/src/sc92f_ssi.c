//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_ssi.c
//	作者		:
//	模块功能	: SSI固件库函数C文件
//	局部函数列表:
//  最后更正日期: 2021/08/20
// 	版本		: V1.10001
//  说明        :
//*************************************************************

#include "sc92f_ssi.h"

#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
		|| defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined (SC92F84Ax) || defined (SC92F74Ax)\
		|| defined (SC92F83Ax) || defined (SC92F73Ax) || defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x) || defined (SC92F827X)\
		|| defined (SC92F837X) || defined (SC92FWxx) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) || defined (SC92F848x) || defined (SC92F748x)\
		|| defined (SC92F859x) || defined (SC92F759x) 
/**************************************************
*函数名称:void SSI_DeInit(void)
*函数功能:SSI相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void SSI_DeInit(void)
{
  OTCON &= 0X3F;
  SSCON0 = 0X00;
  SSCON1 = 0X00;
  SSCON2 = 0X00;
  SSDAT = 0X00;
  IE1 &= (~0X01);
  IP1 &= (~0X01);
}

#if defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
/**************************************************
*函数名称:SSI_PinSelection(SSI_PinSelection_TypeDef PinSeletion)
*函数功能:SSI引脚选择
*入口参数:
SSI_PinSelection_TypeDef:PinSeletion:选择SSI引脚为P10P27P26或P21P22P23
*出口参数:void
**************************************************/
void SSI_PinSelection(SSI_PinSelection_TypeDef
                      PinSeletion)
{
  OTCON = OTCON & 0XDF | PinSeletion;
}
#endif

/**************************************************
*函数名称:void SSI_SPI_Init(SPI_FirstBit_TypeDef FirstBit, SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,SPI_Mode_TypeDef Mode,
							 SPI_ClockPolarity_TypeDef ClockPolarity, SPI_ClockPhase_TypeDef ClockPhase,SPI_TXE_INT_TypeDef SPI_TXE_INT)
*函数功能:SPI初始化配置函数
*入口参数:
SPI_FirstBit_TypeDef:FirstBit:优先传送位选择（MSB/LSB）
SPI_BaudRatePrescaler_TypeDef:BaudRatePrescaler:SPI时钟频率选择
SPI_Mode_TypeDef:Mode:SPI工作模式选择
SPI_ClockPolarity_TypeDef:ClockPolarity:SPI时钟极性选择
SPI_ClockPhase_TypeDef:ClockPhase:SPI时钟相位选择
SPI_TXE_INT_TypeDef:SPI_TXE_INT:发送缓存器中断允许选择
*出口参数:void
**************************************************/
void SSI_SPI_Init(SPI_FirstBit_TypeDef FirstBit,
                  SPI_BaudRatePrescaler_TypeDef BaudRatePrescaler,
                  SPI_Mode_TypeDef Mode,
                  SPI_ClockPolarity_TypeDef ClockPolarity,
                  SPI_ClockPhase_TypeDef ClockPhase,
                  SPI_TXE_INT_TypeDef SPI_TXE_INT)
{
  OTCON = (OTCON & 0X3F) | 0X40;
  SSCON1 = SSCON1 & (~0X05) | FirstBit |
           SPI_TXE_INT;
  SSCON0 = SSCON0 & 0X80 | BaudRatePrescaler | Mode
           | ClockPolarity | ClockPhase;
}

/*****************************************************
*函数名称:void SSI_SPI_Cmd(FunctionalState NewState)
*函数功能:SPI功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void SSI_SPI_Cmd(FunctionalState NewState)
{
  if(NewState != DISABLE)
  {
    SSCON0 |= 0X80;
  }
  else
  {
    SSCON0 &= (~0X80);
  }
}

/*****************************************************
*函数名称:void SSI_SPI_SendData(uint8_t Data)
*函数功能:SPI发送数据
*入口参数:
uint8_t:Data:发送的数据
*出口参数:void
*****************************************************/
void SSI_SPI_SendData(uint8_t Data)
{
  SSDAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI_SPI_ReceiveData(void)
*函数功能:获得SSDAT中的值
*入口参数:void
*出口参数:
uint8_t:SPI接收到的8位数据
*****************************************************/
uint8_t SSI_SPI_ReceiveData(void)
{
  return SSDAT;
}

/**************************************************
*函数名称:void SSI_TWI_Init(uint8_t TWI_Address)
*函数功能:TWI初始化配置函数
*入口参数:
uint8_t:TWI_Address:TWI作为从机时7位从机地址配置
*出口参数:void
**************************************************/
void SSI_TWI_Init(uint8_t TWI_Address)
{
  OTCON = OTCON & 0X3F | 0X80;
  SSCON1 = TWI_Address << 1;
}

/**************************************************
*函数名称:void SSI_TWI_AcknowledgeConfig(FunctionalState NewState)
*函数功能:TWI接收应答使能函数
*入口参数:
FunctionalState:NewState:接收应答使能/失能选择
*出口参数:void
**************************************************/
void SSI_TWI_AcknowledgeConfig(FunctionalState
                               NewState)
{
  if (NewState != DISABLE)
  {
    SSCON0 |= 0X08;
  }
  else
  {
    SSCON0 &= 0XF7;
  }
}

/**************************************************
*函数名称:void SSI_TWI_GeneralCallCmd(FunctionalState NewState)
*函数功能:TWI通用地址响应使能函数
*入口参数:
FunctionalState:NewState:通用地址响应使能/失能选择
*出口参数:void
**************************************************/
void SSI_TWI_GeneralCallCmd(FunctionalState
                            NewState)
{
  if (NewState != DISABLE)
  {
    SSCON1 |= 0X01;
  }
  else
  {
    SSCON1 &= 0XFE;
  }
}

/**************************************************
*函数名称:FlagStatus SSI_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState)
*函数功能:获取TWI状态机
*入口参数:
SSI_TWIState_TypeDef:SSI_TWIState:TWI状态机状态
*出口参数:void
**************************************************/
FlagStatus SSI_GetTWIStatus(SSI_TWIState_TypeDef SSI_TWIState)
{
  if((SSCON0&0x07) == SSI_TWIState)
    return SET;
  else
    return RESET;
}

/*****************************************************
*函数名称:void SSI_TWI_Cmd(FunctionalState NewState)
*函数功能:TWI功能开关函数
*入口参数:FunctionalState NewState	功能启动/关闭选择
*出口参数:void
*****************************************************/
void SSI_TWI_Cmd(FunctionalState NewState)
{
  if(NewState != DISABLE)
  {
    SSCON0 |= 0X80;
  }
  else
  {
    SSCON0 &= (~0X80);
  }
}

/*****************************************************
*函数名称:void SSI_TWI_SendData(uint8_t Data)
*函数功能:TWI发送数据
*入口参数:
uint8_t:Data:发送的数据
*出口参数:void
*****************************************************/
void SSI_TWI_SendData(uint8_t Data)
{
  SSDAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI_TWI_ReceiveData(void)
*函数功能:获得SSDAT中的值
*入口参数:void
*出口参数:
uint8_t:TWI接收到的8位数据
*****************************************************/
uint8_t SSI_TWI_ReceiveData(void)
{
  return SSDAT;
}

/**************************************************
*函数名称:void SSI_UART1_Init(uint32_t UART1Fsys, uint32_t BaudRate, UART1_Mode_TypeDef Mode, UART1_RX_TypeDef RxMode)
*函数功能:UART1初始化配置函数
*入口参数:
uint32_t:UART1Fsys:系统时钟频率
uint32_t:BaudRate:波特率
UART1_Mode_TypeDef:Mode:UART1工作模式
UART1_RX_TypeDef:RxMode:接收允许选择
*出口参数:void
**************************************************/
void SSI_UART1_Init(uint32_t UART1Fsys,
                    uint32_t BaudRate, UART1_Mode_TypeDef Mode,
                    UART1_RX_TypeDef RxMode)
{
  
  #if  defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x)
  OTCON |= 0xC0;
  SSCON0 = SSCON0 & 0X0F | Mode | RxMode;
  SSCON2 = UART1Fsys / 16 / BaudRate / 256;
  SSCON1 = UART1Fsys / 16 / BaudRate % 256;
  
  #else
  OTCON |= 0xC0;
  SSCON0 = SSCON0 & 0X0F | Mode | RxMode;
  SSCON2 = UART1Fsys / BaudRate / 256;
  SSCON1 = UART1Fsys / BaudRate % 256;
  
  #endif
  
  
}

/*****************************************************
*函数名称:void SSI_UART1_SendData8(uint8_t Data)
*函数功能:UART1发送8位数据
*入口参数:
uint8_t:Data:发送的数据
*出口参数:void
*****************************************************/
void SSI_UART1_SendData8(uint8_t Data)
{
  SSDAT = Data;
}

/*****************************************************
*函数名称:uint8_t SSI_UART1_ReceiveData8(void)
*函数功能:获得SSDAT中的值
*入口参数:void
*出口参数:
uint8_t:UART接收到的8位数据
*****************************************************/
uint8_t SSI_UART1_ReceiveData8(void)
{
  return SSDAT;
}

/*****************************************************
*函数名称:void SSI_UART1_SendData9(uint16_t Data)
*函数功能:UART1发送9位数据
*入口参数:
uint16_t:Data:发送的数据
*出口参数:void
*****************************************************/
void SSI_UART1_SendData9(uint16_t Data)
{
  uint8_t Data_9Bit;
  Data_9Bit = (Data >> 8);

  if(Data_9Bit)
  {
    SSCON0	|= 0x08;
  }
  else
  {
    SSCON0	&= 0xf7;
  }

  SSDAT = (uint8_t)Data;
}

/*****************************************************
*函数名称:uint16_t SSI_UART1_ReceiveData9(void)
*函数功能:获得SSDAT中的值及第九位的值
*入口参数:void
*出口参数:
uint16_t:接收到的数据
*****************************************************/
uint16_t SSI_UART1_ReceiveData9(void)
{
  uint16_t Data9;
  Data9 =  SSDAT + ((uint16_t)(SSCON0 & 0X04) << 6);
  SSCON0 &= 0XFB;
  return Data9;
}

/*****************************************************
*函数名称:void SSI_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:SSI中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void SSI_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if(NewState != DISABLE)
  {
    IE1 |= 0x01;
  }
  else
  {
    IE1 &= 0xFE;
  }

  /************************************************************/
  if(Priority != LOW)
  {
    IP1 |= 0x01;
  }
  else
  {
    IP1 &= 0xFE;
  }
}

/*****************************************************
*函数名称:FlagStatus SSI_GetFlagStatus(SSI_Flag_TypeDef SSI_FLAG)
*函数功能:获得SSI标志状态
*入口参数:
SSI_Flag_TypeDef:SSI_FLAG:所需获取的标志位
*出口参数:
FlagStatus:SSI标志位置起状态
*****************************************************/
FlagStatus SSI_GetFlagStatus(SSI_Flag_TypeDef
                             SSI_FLAG)
{
  FlagStatus bitstatus = RESET;

  if((SSI_FLAG == SPI_FLAG_SPIF) ||
      (SSI_FLAG == SPI_FLAG_WCOL) ||
      (SSI_FLAG == SPI_FLAG_TXE))
  {
    if((SSI_FLAG & SSCON1) != (uint8_t)RESET)
    {
      bitstatus = SET;
    }
    else
    {
      bitstatus = RESET;
    }
  }
  else
  {
    if((SSI_FLAG & SSCON0) != (uint8_t)RESET)
    {
      bitstatus = SET;
    }
    else
    {
      bitstatus = RESET;
    }
  }

  return bitstatus;
}

/*****************************************************
*函数名称:void SSI_ClearFlag(SSI_Flag_TypeDef SSI_FLAG)
*函数功能:清除SSI标志状态
*入口参数:
SSI_Flag_TypeDef:SSI_FLAG:所需清除的标志位
*出口参数:void
*****************************************************/
void SSI_ClearFlag(SSI_Flag_TypeDef SSI_FLAG)
{
  if((SSI_FLAG == SPI_FLAG_SPIF) ||
      (SSI_FLAG == SPI_FLAG_WCOL) ||
      (SSI_FLAG == SPI_FLAG_TXE))
  {
    SSCON1 &= (~SSI_FLAG);
  }
  else
  {
    SSCON0 &= (~SSI_FLAG);
  }
}
#endif

/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
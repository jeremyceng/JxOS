//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_option.c
//	作者		:
//	模块功能	: Customer Option寄存器配置C文件
//	局部函数列表:
//  最后更正日期: 2022/01/24
// 	版本		: V1.0005
//  说明        :
//*************************************************************

#include "sc92f_option.h"

/*****************************************************
*函数名称:void OPTION_WDT_Cmd(FunctionalState NewState)
*函数功能:WDT功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void OPTION_WDT_Cmd(FunctionalState NewState)
{
  OPINX = 0XC1;

  if(NewState == DISABLE)
  {
    OPREG &= 0X7F;
  }
  else
  {
    OPREG |= 0X80;
  }
}

/*****************************************************
*函数名称:void OPTION_XTIPLL_Cmd(FunctionalState NewState)
*函数功能:外接晶振使能
*入口参数:
FunctionalState:NewState:启动/关闭选择
*出口参数:void
*****************************************************/
#if !defined(SC92F848x) && !defined(SC92F748x) 
void OPTION_XTIPLL_Cmd(FunctionalState NewState)
{
  OPINX = 0XC1;

  if(NewState == DISABLE)
  {
    OPREG &= 0XBF;
  }
  else
  {
    OPREG |= 0X40;
  }
}
#endif
/*****************************************************
*函数名称:void OPTION_SYSCLK_Init(SYSCLK_PresSel_TypeDef SYSCLK_PresSel)
*函数功能:系统时钟分频初始化
*入口参数:
SYSCLK_PresSel_TypeDef:SYSCLK_PresSel:选择系统时钟分频
*出口参数:void
*****************************************************/
void OPTION_SYSCLK_Init(SYSCLK_PresSel_TypeDef
                        SYSCLK_PresSel)
{
  OPINX = 0XC1;
  OPREG = OPREG & 0XCF | SYSCLK_PresSel;
}

/*****************************************************
*函数名称:void OPTION_RST_PIN_Cmd(FunctionalState NewState)
*函数功能:外部复位管脚（P17）使能
*入口参数:
FunctionalState:NewState:使能/关闭选择
*出口参数:void
*****************************************************/
#if !defined(SC92F848x) && !defined(SC92F748x)  && !defined(SC92F859x) && !defined (SC92F759x)  && !defined(SC92L853x) && !defined (SC92L753x)
void OPTION_RST_PIN_Cmd(FunctionalState NewState)
{
  OPINX = 0XC1;

  if(NewState == DISABLE)
  {
    OPREG |= 0X08;
  }
  else
  {
    OPREG &= 0XF7;
  }
}
#endif

/*****************************************************
*函数名称:void OPTION_LVR_Init(LVR_Config_TypeDef LVR_Config)
*函数功能:LVR 电压选择
*入口参数:
LVR_Config_TypeDef:LVR_Config:选择LVR电压
*出口参数:void
*****************************************************/
void OPTION_LVR_Init(LVR_Config_TypeDef
                     LVR_Config)
{
  OPINX = 0XC1;
  OPREG = OPREG & 0XF8 | LVR_Config;
}

/*****************************************************
*函数名称:void OPTION_ADC_VrefConfig(ADC_Vref_TypeDef ADC_Vref)
*函数功能:ADC 参考电压选择
*入口参数:
ADC_Vref_TypeDef:ADC_Vref:选择ADC参考电压
*出口参数:void
*****************************************************/
void OPTION_ADC_VrefConfig(ADC_Vref_TypeDef
                           ADC_Vref)
{
  OPINX = 0xC2;
  OPREG = OPREG & 0X7F | ADC_Vref;
}

/**************************************************
*函数名称:void OPTION_IAP_SetOperateRange(IAP_OperateRange_TypeDef IAP_OperateRange)
*函数功能:允许IAP操作的范围设置
*入口参数:
IAP_OperateRange_TypeDef:IAP_OperateRange:IAP操作范围
*出口参数:void
**************************************************/
void OPTION_IAP_SetOperateRange(
  IAP_OperateRange_TypeDef IAP_OperateRange)
{
  OPINX = 0xC2;
  OPREG = (OPREG & 0xF3) | IAP_OperateRange;
}

#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax)\
	|| defined (SC92F73Ax) || defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F740x)\
	|| defined (SC92F8003) || defined (SC92F7003)
/*****************************************************
*函数名称:void OPTION_XTIPLL_SetRange(XTIPLL_Range_TypeDef XTIPLL_Range)
*函数功能:外部高频晶振频率范围
*入口参数:
XTIPLL_Range_TypeDef:XTIPLL_Range:外部晶振频率选择
*出口参数:void
*****************************************************/
void OPTION_XTIPLL_SetRange(XTIPLL_Range_TypeDef
                            XTIPLL_Range)
{
  OPINX = 0XC2;
  OPREG = OPREG & 0XBF | XTIPLL_Range;
}
#endif

#if defined (SC92F742x)||defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax) \
		||defined (SC92F74Ax_2)||defined (SC92F84Ax_2)||defined (SC92F844xB)||defined (SC92F744xB) \
		||defined (SC92F859x) || defined (SC92F759x) ||defined (SC92F848x) || defined (SC92F748x) || defined (SC92L853x) || defined (SC92L753x)
/**************************************************
*函数名称:void OPTION_JTG_Cmd(FunctionalState NewState)
*函数功能:JTAG模式使能开关
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
**************************************************/
void OPTION_JTG_Cmd(FunctionalState NewState)
{
  OPINX = 0xC2;

  if(NewState == DISABLE)
  {
    OPREG |= 0X10;	   //1 JTAG无效
  }
  else
  {
    OPREG &= 0XEF;	   //0 JTAG有效
  }
}
#endif
/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
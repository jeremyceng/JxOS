//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92F_LPD.c
//	作者		:
//	模块功能	: USCI1固件库函数C文件
//  最后更正日期: 2022/01/19
// 	版本		: V1.10000
//  说明    :该文件仅适用于SC92F系列芯片
//*************************************************************

#include "sc92f_lpd.h"

#if defined (SC92L853x) || defined (SC92L753x) 

/**************************************************
*函数名称:void USCI1_DeInit(void)
*函数功能:USCI1相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void LPD_DeInit(void)
{
	/* 配置相关寄存器复位 */
	LPDCFG = 0x00;
	/* 中断相关寄存器复位 */
	IE2 &= 0x7F;
	IP2 &= 0x7F;
}

/**************************************************
*函数名称:void LPD_VtripConfig(LPD_Vtrip_TypeDef LPD_Vtrip)
*函数功能:设置LPD门限电压阈值
*入口参数:
LPD_Vtrip_TypeDef:LPD_Vtrip:LPD门限电压阈值
*出口参数:void
**************************************************/
void LPD_VtripConfig(LPD_Vtrip_TypeDef LPD_Vtrip)
{
	LPDCFG &= 0xF1;	//复位门限电压阈值寄存器
	LPDCFG = LPD_Vtrip << 1;	//设置门限电压阈值寄存器
}

/**************************************************
*函数名称:void LPD_Cmd(FunctionalState NewState)
*函数功能:使能LPD功能
*入口参数:
FunctionalState:NewState:功能使能/失能
*出口参数:void
**************************************************/
void LPD_Cmd(FunctionalState NewState)
{
	if(NewState == ENABLE)
	{
		LPDCFG &= 0xFE;
	}
	else
	{
		LPDCFG |= 0x01;
	}
}

/*****************************************************
*函数名称:void LPD_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:LPD中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void LPD_ITConfig(FunctionalState NewState, PriorityStatus Priority)
{
	/* 中断开关 */
  if (NewState != DISABLE)
  {
    IE2 |= 0x01;
  }
  else
  {
    IE2 &= 0xFE;
  }

	/* 中断优先级 */
  if (Priority != LOW)
  {
    IP2 |= 0x01;
  }
  else
  {
    IP2 &= 0xFE;
  }
}

/*****************************************************
*函数名称:FlagStatus LPD_GetFlagStatus(LPD_Flag_Typedef LPD_Flag)
*函数功能:获得LPD中断标志状态
*入口参数:
LPD_GetFlagStatus:LPD_Flag:中断标志位选择
*出口参数:
FlagStatus:LPD中断标志位置起状态
*****************************************************/
FlagStatus LPD_GetFlagStatus(LPD_Vtrip_TypeDef LPD_Flag)
{
  return (bool)(SCON & LPD_Flag);
}

/*****************************************************
*函数名称:void LPD_ClearFlag(LPD_Flag_Typedef LPD_Flag)
*函数功能:清除LPD中断标志状态
*入口参数:
LPD_Flag_Typedef;LPD_Flag:中断标志位选择
*出口参数:void
*****************************************************/
void LPD_ClearFlag(LPD_Vtrip_TypeDef LPD_Flag)
{
  SCON &=	(~LPD_Flag);
}

#endif
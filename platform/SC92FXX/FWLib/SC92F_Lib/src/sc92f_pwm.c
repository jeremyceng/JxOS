//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_pwm.c
//	作者		:
//	模块功能	: PWM固件库函数C文件
//	局部函数列表:
//  最后更正日期: 2022/01/05
// 	版本		: V1.10003
//  说明    :本文件仅适用于赛元92F/93F/92L系列单片机
//*************************************************************

#include "sc92f_pwm.h"

#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)\
||defined  (SC92F859x) || defined (SC92F759x)
uint16_t xdata PWMREG[8] _at_
0x740;	//PWM占空比调节寄存器
uint16_t pwm_tmpreg[8] = {0, 0, 0, 0, 0, 0, 0, 0};		//PWM占空比调节寄存器缓存数组

/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  static uint8_t i;
  PWMCFG = 0X00;
  PWMCON = 0X00;
  IE1 &= 0XFD;
  IP1 &= 0XFD;

  for(i = 0; i < 8; i++)
  {
    PWMREG[i] = 0;
  }
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCFG = (PWMCFG & 0XCF) |
           PWM_PresSel;					//预分频
  PWMCFG = (PWMCFG & 0XF0) | (uint8_t)(
             PWM_Period / 256);	//周期高4位
  PWMCON = (uint8_t)(PWM_Period &
                     0X00FF);				//周期低8位
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
uint8_t:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  uint8_t i;

  for(i = 0; i < 8; i++)
  {
    if(PWM_OutputPin & (0x01 << i))
    {
      if(PWM_OutputState == PWM_OUTPUTSTATE_DISABLE)
      {
        pwm_tmpreg[i] &= 0X7FFF;
      }
      else
      {
        pwm_tmpreg[i] |= 0X8000;
      }

      PWMREG[i] = pwm_tmpreg[i];
    }
  }
}


/**************************************************
*函数名称:void PWM_PolarityConfig(uint8_t PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
uint8_t:PWM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  uint8_t i;

  for(i = 0; i < 8; i++)
  {
    if(PWM_OutputPin & (0x01 << i))
    {
      if(PWM_Polarity == PWM_POLARITY_NON_INVERT)
      {
        pwm_tmpreg[i] &= 0XBFFF;
      }
      else
      {
        pwm_tmpreg[i] |= 0X4000;
      }

      PWMREG[i] = pwm_tmpreg[i];
    }
  }
}
/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin,
  uint16_t PWM_DutyCycle)
{
  uint8_t i;

  for(i = 0; i < 8; i++)
  {
    if(PWM_OutputPin & (0x01 << i))
    {
      pwm_tmpreg[i] = pwm_tmpreg[i] & 0XF000 |
                      PWM_DutyCycle;
      PWMREG[i] = pwm_tmpreg[i];
    }
  }
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCFG |= 0X80;
  }
  else
  {
    PWMCFG &= 0X7F;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= 0XFD;
  }

  if(Priority == LOW)
  {
    IP1 &= ~0X02;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:
FlagStatus:PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCFG & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCFG &= 0XBF;
}
#endif

#if defined (SC92F846xB) || defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB)|| defined (SC92F83Ax) || defined (SC92F73Ax)|| defined (SC92F84Ax) || defined (SC92F74Ax) || defined (SC92F848x) || defined (SC92F748x)
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  PWMCFG = 0X00;
  PWMCON = 0X00;
  PWMPRD = 0X00;
  PWMDTYA = 0X00;
  PWMDTY0 = 0X00;
  PWMDTY1 = 0X00;
  PWMDTY2 = 0X00;
  PWMDTYB = 0X00;
  PWMDTY3 = 0X00;
  PWMDTY4 = 0X00;
  PWMDTY5 = 0X00;
  IE1 &= ~0X02;
  IP1 &= ~0X02;
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCFG  = (PWMCFG & 0X3F) | (PWM_PresSel << 6);					//预分频
  PWMDTYA = (PWMDTYA & 0X3F) | ((uint8_t)( PWM_Period % 4) << 6);	//周期低两位
  PWMPRD  = (uint8_t)(PWM_Period >> 2);							//周期高八位
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  if(PWM_OutputState == PWM_OUTPUTSTATE_ENABLE)
  {
    PWMCON |= PWM_OutputPin;
  }
  else
  {
    PWMCON &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_PolarityConfig(uint8_t PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  if(PWM_Polarity == PWM_POLARITY_INVERT)
  {
    PWMCFG |= PWM_OutputPin;
  }
  else
  {
    PWMCFG &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin,
															uint16_t PWM_DutyCycle)
{
  PWMDTYB &= 0X7F;		//设置PWM为独立模式

  switch(PWM_OutputPin)	//设置占空比
  {
    case PWM0:
      PWMDTYA = PWMDTYA & 0xfc | (PWM_DutyCycle % 4);
      PWMDTY0 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM1:
      PWMDTYA = PWMDTYA & 0xf3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY1 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM2:
      PWMDTYA = PWMDTYA & 0xcf | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY2 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM3:
      PWMDTYB = PWMDTYB & 0xfc | (PWM_DutyCycle % 4);
      PWMDTY3 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM4:
      PWMDTYB = PWMDTYB & 0xf3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY4 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM5:
      PWMDTYB = PWMDTYB & 0xcf | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY5 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    default:
      break;
  }
}

/**************************************************
*函数名称:void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMxPWMy互补工作模式配置函数
*入口参数:
PWM_ComplementaryOutputPin_TypeDef:PWM_ComplementaryOutputPin:PWMxPWMy互补通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin,
																	uint16_t PWM_DutyCycle)
{
  PWMDTYB |= 0X80;					//设置PWM为互补模式

  switch(PWM_ComplementaryOutputPin)	//设置占空比
  {
    case PWM0PWM3:
      PWMDTYA = PWMDTYA & 0xfc | (PWM_DutyCycle % 4);
      PWMDTY0 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM1PWM4:
      PWMDTYA = PWMDTYA & 0xf3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY1 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM2PWM5:
      PWMDTYA = PWMDTYA & 0xcf | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY2 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    default:
      break;
  }
}

/**************************************************
*函数名称:void PWM_DeadTimeConfig(uint8_t PWM012_RisingDeadTime, uint8_t PWM345_fallingDeadTime)
*函数功能:PWM互补工作模式下死区时间配置函数
*入口参数:
uint8_t:PWM012_RisingDeadTime:PWM死区上升时间
uint8_t:PWM345_fallingDeadTime:PWM死区下降时间
*出口参数:void
**************************************************/
void PWM_DeadTimeConfig(uint8_t PWM012_RisingDeadTime,
                        uint8_t PWM345_fallingDeadTime)
{
  PWMDTY3 = (PWM012_RisingDeadTime |
             (PWM345_fallingDeadTime << 4));
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCON |= 0X80;
  }
  else
  {
    PWMCON &= ~0X80;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= 0XFD;
  }

  if(Priority == LOW)
  {
    IP1 &= 0XFD;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:
FlagStatus:PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCON & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCON &= 0XBF;
}
#endif

#if defined (SC92F7003) || defined (SC92F8003) || defined (SC92F740x)
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  PWMCFG = 0X00;
  PWMCON0 = 0X00;
  PWMPRD = 0X00;
  PWMDTYA = 0X00;
  PWMDTY0 = 0X00;
  PWMDTY1 = 0X00;
  PWMDTY2 = 0X00;
  PWMCON1 = 0X00;
  PWMDTYB = 0X00;
  PWMDTY3 = 0X00;
  PWMDTY4 = 0X00;
  PWMDTY5 = 0X00;
  PWMDTY6 = 0X00;
  IE1 &= 0XFD;
  IP1 &= 0XFD;
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCON0 = (PWMCON0 & 0XCC) | PWM_PresSel |
            (uint8_t)(PWM_Period &
                      0X0003);	//预分频及周期的低2位
  PWMPRD = (uint8_t)(PWM_Period >>
                     2);									    //周期高八位
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  if(PWM_OutputState == PWM_OUTPUTSTATE_ENABLE)
  {
    PWMCON1 |= PWM_OutputPin;
  }
  else
  {
    PWMCON1 &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_PWM2Selection(PWM2_OutputPin_TypeDef PWM2_OutputPin)
*函数功能:PWM2管脚选择函数
*入口参数:
PWM2_OutputPin_TypeDef:PWM2_OutputPin:PWM2管脚选择
*出口参数:void
**************************************************/
void PWM_PWM2Selection(PWM2_OutputPin_TypeDef
                       PWM2_OutputPin)
{
  PWMCON0 = PWMCON0 & 0XFB | PWM2_OutputPin;
}

/**************************************************
*函数名称:void PWM_PWM5Selection(PWM5_OutputPin_TypeDef PWM5_OutputPin)
*函数功能:PWM5管脚选择函数
*入口参数:
PWM5_OutputPin_TypeDef:PWM5_OutputPin:PWM5管脚选择
*出口参数:void
**************************************************/
void PWM_PWM5Selection(PWM5_OutputPin_TypeDef
                       PWM5_OutputPin)
{
  PWMCON0 = PWMCON0 & 0XF7 | PWM5_OutputPin;
}

/**************************************************
*函数名称:void PWM_PolarityConfig(uint8_t PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  if(PWM_Polarity == PWM_POLARITY_INVERT)
  {
    PWMCFG |= PWM_OutputPin;
  }
  else
  {
    PWMCFG &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin,
  uint16_t PWM_DutyCycle)
{
  if(PWM_OutputPin != PWM6)
  {
    PWMCON1 &= 0X7F;		//设置PWM为独立模式
  }

  switch(PWM_OutputPin)	//设置占空比
  {
    case PWM0:
      PWMDTYA = PWMDTYA & 0XFC | (PWM_DutyCycle % 4);
      PWMDTY0 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM1:
      PWMDTYA = PWMDTYA & 0XF3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY1 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM2:
      PWMDTYA = PWMDTYA & 0XCF | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY2 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM3:
      PWMDTYA = PWMDTYA & 0X3F | ((PWM_DutyCycle % 4) <<
                                  6);
      PWMDTY3 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM4:
      PWMDTYB = PWMDTYB & 0XFC | (PWM_DutyCycle % 4);
      PWMDTY4 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM5:
      PWMDTYB = PWMDTYB & 0XF3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY5 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM6:
      PWMDTYB = PWMDTYB & 0XCF | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY6 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    default:
      break;
  }
}

/**************************************************
*函数名称:void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMxPWMy互补工作模式配置函数
*入口参数:
PWM_ComplementaryOutputPin_TypeDef:PWM_ComplementaryOutputPin:PWMxPWMy互补通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin,
  uint16_t PWM_DutyCycle)
{
  PWMCON1 |= 0X80;					//设置PWM为互补模式

  switch(PWM_ComplementaryOutputPin)	//设置占空比
  {
    case PWM0PWM3:
      PWMDTYA = PWMDTYA & 0XFC | (PWM_DutyCycle % 4);
      PWMDTY0 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM1PWM4:
      PWMDTYA = PWMDTYA & 0XF3 | ((PWM_DutyCycle % 4) <<
                                  2);
      PWMDTY1 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    case PWM2PWM5:
      PWMDTYA = PWMDTYA & 0XCF | ((PWM_DutyCycle % 4) <<
                                  4);
      PWMDTY2 = (uint8_t)(PWM_DutyCycle >> 2);
      break;

    default:
      break;
  }
}

/**************************************************
*函数名称:void PWM_DeadTimeConfig(uint8_t PWM012_RisingDeadTime, uint8_t PWM345_fallingDeadTime)
*函数功能:PWM互补工作模式下死区时间配置函数
*入口参数:
uint8_t:PWM012_RisingDeadTime:PWM死区上升时间
uint8_t:PWM345_fallingDeadTime:PWM死区下降时间
*出口参数:void
**************************************************/
void PWM_DeadTimeConfig(uint8_t
                        PWM012_RisingDeadTime,
                        uint8_t PWM345_fallingDeadTime)
{
  PWMDTY3 = (PWM012_RisingDeadTime |
             (PWM345_fallingDeadTime << 4));
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCON0 |= 0X80;
  }
  else
  {
    PWMCON0 &= ~0X80;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= 0XFD;
  }

  if(Priority == LOW)
  {
    IP1 &= 0XFD;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:
FlagStatus:PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCON0 & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCON0 &= 0XBF;
}
#endif

#if defined (SC92F742x) || defined (SC92F730x) || defined (SC92F725X) || defined(SC92F735X)
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  PWMCFG0 = 0X00;
  PWMCON  = 0X00;
  PWMPRD  = 0X00;
  PWMCFG1 = 0X00;
  PWMDTY0 = 0X00;
  PWMDTY1 = 0X00;
  PWMDTY2 = 0X00;
#if !defined (SC92F730x)
  PWMDTY3 = 0X00;
#endif
  PWMDTY4 = 0X00;
  PWMDTY5 = 0X00;
  IE1 &= ~0X02;
  IP1 &= ~0X02;
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCON = (PWMCON & 0XF8) | PWM_PresSel;	//预分频
  PWMPRD = PWM_Period;					//周期配置
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
uint8_t:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  if(PWM_OutputState == PWM_OUTPUTSTATE_DISABLE)
  {
      PWMCON = PWMCON & (~((PWM_OutputPin & 0x07) << 3));
      PWMCON = PWMCON | (PWM_OutputPin & 0x07) << 3;
  }
  else
  {
    PWMCON = PWMCON | (PWM_OutputPin & 0x07) << 3;
    PWMCFG0 = PWMCFG0 | (PWM_OutputPin & 0x38) >> 3;
  }
}

/**************************************************
*函数名称:void PWM_PolarityConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_PolarityConfig:PWM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  if(PWM_Polarity == PWM_POLARITY_NON_INVERT)
  {
      PWMCFG0 = PWMCFG0 & ~((PWM_OutputPin & 0x07) << 3);
    PWMCFG1 = PWMCFG1 & ~((PWM_OutputPin & 0x38));
  }
  else
  {
    PWMCFG0 = PWMCFG0 | (PWM_OutputPin & 0x07) << 3;
    PWMCFG1 = PWMCFG1 | (PWM_OutputPin & 0x38);
  }

}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(
  PWM_OutputPin_TypeDef PWM_OutputPin,
  uint16_t PWM_DutyCycle)
{
  switch(PWM_OutputPin)	//设置占空比
  {
    case PWM0:
      PWMDTY0 = PWM_DutyCycle;
      break;

    case PWM1:
      PWMDTY1 = PWM_DutyCycle;
      break;

    case PWM2:
      PWMDTY2 = PWM_DutyCycle;
      break;
#if !defined (SC92F730x)

    case PWM3:
      PWMDTY3 = PWM_DutyCycle;
      break;
#endif

    case PWM4:
      PWMDTY4 = PWM_DutyCycle;
      break;

    case PWM5:
      PWMDTY5 = PWM_DutyCycle;
      break;

    default:
      break;
  }
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCON |= 0X80;
  }
  else
  {
    PWMCON &= ~0X80;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= ~0X02;
  }

  if(Priority == LOW)
  {
    IP1 &= ~0X02;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:
FlagStatus:PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCON & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCON &= 0XBF;
}
#endif

#if defined (SC92F827X) || defined (SC92F837X)
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  PWMCFG = 0X00;
  PWMCON = 0X00;
  PWMPRD = 0X00;
  PWMDTY0 = 0X00;
  PWMDTY1 = 0X00;
#if !defined (SC92F827X)
  PWMDTY2 = 0X00;
  PWMDTY3 = 0X00;
#if !defined (SC92F837X)
  PWMDTY4 = 0X00;
  PWMDTY5 = 0X00;
#endif
#endif
  IE1 &= ~0X02;
  IP1 &= ~0X02;
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCON = (PWMCON & 0XF8) | PWM_PresSel;	//预分频
  PWMPRD = PWM_Period;					//周期配置
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  if(PWM_OutputState == PWM_OUTPUTSTATE_ENABLE)
  {
    PWMCON |= PWM_OutputPin;
  }
  else
  {
    PWMCON &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_PolarityConfig(uint8_t PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  if(PWM_Polarity == PWM_POLARITY_INVERT)
  {
    PWMCFG |= PWM_OutputPin;
  }
  else
  {
    PWMCFG &= (~PWM_OutputPin);
  }
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(
  PWM_OutputPin_TypeDef PWM_OutputPin,
  uint16_t PWM_DutyCycle)
{
  switch(PWM_OutputPin)	//设置占空比
  {
    case PWM0:
      PWMDTY0 = PWM_DutyCycle;
      break;

    case PWM1:
      PWMDTY1 = PWM_DutyCycle;
      break;
#if !defined (SC92F827X)

    case PWM2:
      PWMDTY2 = PWM_DutyCycle;
      break;

    case PWM3:
      PWMDTY3 = PWM_DutyCycle;
      break;
#if !defined (SC92F837X)

    case PWM4:
      PWMDTY4 = PWM_DutyCycle;
      break;

    case PWM5:
      PWMDTY5 = PWM_DutyCycle;
      break;
#endif
#endif

    default:
      break;
  }
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCON |= 0X80;
  }
  else
  {
    PWMCON &= ~0X80;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= 0XFD;
  }

  if(Priority == LOW)
  {
    IP1 &= 0XFD;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:FlagStatus	PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCON & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCON &= 0XBF;
}
#endif

#if defined (SC92F732X) || defined (SC93F833x) || defined (SC93F843x) || defined (SC93F743x) 
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  PWMCFG = 0X00;
  PWMCON = 0X00;
  PWMPRD = 0X00;
  PWMDTYA = 0X00;
  PWMDTY0 = 0X00;
  PWMDTY1 = 0X00;
  PWMDTY2 = 0X00;
  IE1 &= ~0X02;
  IP1 &= ~0X02;
}

/**************************************************
*函数名称:void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
              uint16_t PWM_Period)
{
  PWM_Period -= 1;
  PWMCON = (PWMCON & 0XF8) | PWM_PresSel;	//预分频
  PWMPRD = PWM_Period;					//周期配置
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
  if(PWM_OutputState == PWM_OUTPUTSTATE_DISABLE)
  {
    PWMCON = PWMCON & (~(PWM_OutputPin << 3));
  }
  else
  {
    PWMCON = PWMCON | (PWM_OutputPin << 3);
  }
}

/**************************************************
*函数名称:void PWM_PolarityConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:WM_OutputPin:PWMx选择
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
  if(PWM_Polarity == PWM_POLARITY_NON_INVERT)
  {
    PWMCFG = PWMCFG & (~PWM_OutputPin<<3);
  }
  else
  {
    PWMCFG = PWMCFG | (PWM_OutputPin<<3);
  }
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(
  PWM_OutputPin_TypeDef PWM_OutputPin,
  uint16_t PWM_DutyCycle)
{
  switch(PWM_OutputPin)	//设置占空比
  {
    case PWM0:
      PWMDTY0 = PWM_DutyCycle;
      break;

    case PWM1:
      PWMDTY1 = PWM_DutyCycle;
      break;

    case PWM2:
      PWMDTY2 = PWM_DutyCycle;
      break;

    default:
      break;
  }
}

/*****************************************************
*函数名称:void PWM_Cmd(FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_Cmd(FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    PWMCON |= 0X80;
  }
  else
  {
    PWMCON &= ~0X80;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= ~0X02;
  }

  if(Priority == LOW)
  {
    IP1 &= ~0X02;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:void
*出口参数:
FlagStatus:PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatus(void)
{
  return (bool)(PWMCON & 0X40);
}

/*****************************************************
*函数名称:void PWM_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:void
*出口参数:void
*****************************************************/
void PWM_ClearFlag(void)
{
  PWMCON &= 0XBF;
}

/**************************************************
*函数名称:void PWM_PWM0Selection(PWM0_OutputPin_TypeDef PWM0_OutputPin)
*函数功能:PWM0管脚选择函数
*入口参数:
PWM0_OutputPin_TypeDef:PWM0_OutputPin:PWM0管脚选择
*出口参数:void
**************************************************/
void PWM_PWM0Selection(PWM0_OutputPin_TypeDef
                       PWM0_OutputPin)
{
  PWMCFG = PWMCFG & 0XFE | PWM0_OutputPin;
}

/**************************************************
*函数名称:void PWM_PWM1Selection(PWM1_OutputPin_TypeDef PWM1_OutputPin)
*函数功能:PWM1管脚选择函数
*入口参数:
PWM1_OutputPin_TypeDef:PWM1_OutputPin:PWM1管脚选择
*出口参数:void
**************************************************/
void PWM_PWM1Selection(PWM1_OutputPin_TypeDef
                       PWM1_OutputPin)
{
  PWMCFG = PWMCFG & 0XFD | PWM1_OutputPin;
}

/**************************************************
*函数名称:void PWM_PWM1Selection(PWM2_OutputPin_TypeDef PWM2_OutputPin)
*函数功能:PWM2管脚选择函数
*入口参数:
PWM2_OutputPin_TypeDef:PWM2_OutputPin:PWM1管脚选择
*出口参数:void
**************************************************/
void PWM_PWM2Selection(PWM2_OutputPin_TypeDef
                       PWM2_OutputPin)
{
  PWMCFG = PWMCFG & 0XFB | PWM2_OutputPin;
}

/**************************************************
*函数名称:void PMM_DutyModeSelection(PWM_DutyMode_TypeDef PWM_DutyMode)
*函数功能:PWM占空比微调模式选择
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWM通道
PWM_DutyMode_TypeDef:PWM_DutyMode:PWM微调模式
*出口参数:void
**************************************************/
void PMM_DutyModeSelection(PWM_OutputPin_TypeDef
                           PWM_OutputPin, PWM_DutyMode_TypeDef PWM_DutyMode)
{
  PWMDTYA = PWMDTYA & (~(0x03 << (PWM_OutputPin -
                                  1))) | (PWM_DutyMode << (PWM_OutputPin - 1));
}
#endif

#if defined (SC92FWxx)

uint8_t xdata PWMREG[80] _at_
0x700;	//PWM占空比调节寄存器
uint8_t PWMREG_Status[10];
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
	static uint8_t i;
	PWMCFG0 = 0X00;
	PWMCON0 = 0X00;
	PWMCFG1 = 0X00;
	PWMCON1 = 0X00;
	IE1 &= 0XFD;
	IP1 &= 0XFD;

	for(i = 0; i < 80; i++)
	{
		PWMREG[i] = 0;
	}
}

/**************************************************
*函数名称:PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数:  
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel,
							uint16_t PWM_Period)
{
	if((PWM_PresSel & 0X0F) == PWM0_Type)
	{
		PWM_Period -= 1;
		PWMCFG0 = (PWMCFG0 & 0XCF) | (PWM_PresSel &
																	0XF0);					//预分频
		PWMCFG0 = (PWMCFG0 & 0XF0) | (uint8_t)(
								PWM_Period / 256);	//周期高4位
		PWMCON0 = (uint8_t)(PWM_Period &
												0X00FF);				//周期低8位
	}
	else
		if ((PWM_PresSel & 0X0F) == PWM1_Type)
		{
			PWM_Period -= 1;
			PWMCFG1 = (PWMCFG1 & 0XCF) | (PWM_PresSel &
																		0XF0);					//预分频
			PWMCFG1 = (PWMCFG1 & 0XF0) | (uint8_t)(
									PWM_Period / 256);	//周期高4位
			PWMCON1 = (uint8_t)(PWM_Period &
													0X00FF);				//周期低8位
		}
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择(该型号入参不支持围未或操作)
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
													 PWM_OutputState_TypeDef PWM_OutputState)
{
	if(PWM_OutputState == ENABLE)
	{
		PWMREG[PWM_OutputPin] |= 0x80;
	}
	else
	{
		PWMREG[PWM_OutputPin] &= 0x7F;
	}
}

/**************************************************
*函数名称:void PWM_PolarityConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择(该型号入参不支持围未或操作)
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
												PWM_Polarity_TypeDef PWM_Polarity)
{
	if(PWM_Polarity == PWM_POLARITY_INVERT)
	{
		PWMREG[PWM_OutputPin] |= 0x40;
	}
	else
	{
		PWMREG[PWM_OutputPin] &= 0xBF;
	}
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择(该型号入参不支持围未或操作)
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin,
	uint16_t PWM_DutyCycle)
{
	PWMREG[PWM_OutputPin + 1] = PWM_DutyCycle;
	PWMREG[PWM_OutputPin] = (PWMREG[PWM_OutputPin] * 0xF0) |(PWM_DutyCycle / 256);
}

/*****************************************************
*函数名称:void PWM_Cmd(PWM_Type_TypeDef PWM_Type,FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_CmdEX(PWM_Type_TypeDef PWM_Type,
							 FunctionalState NewState)
{
	if(PWM_Type == PWM0_Type)
	{
		if (NewState != DISABLE)
		{
			PWMCFG0 |= 0X80;
		}
		else
		{
			PWMCFG0 &= 0X7F;
		}
	}
	else
	{
		if (NewState != DISABLE)
		{
			PWMCFG1 |= 0X80;
		}
		else
		{
			PWMCFG1 &= 0X7F;
		}
	}
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
									PriorityStatus Priority)
{
	if (NewState != DISABLE)
	{
		IE1 |= 0X02;
	}
	else
	{
		IE1 &= 0XFD;
	}

	if(Priority == LOW)
	{
		IP1 &= ~0X02;
	}
	else
	{
		IP1 |= 0X02;
	}
}

/*****************************************************
*函数名称:FlagStatus PWM1_Type_GetFlagStatus(void)
*函数功能:获得PWM中断标志状态
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
*出口参数:FlagStatus	PWM中断标志状态
*****************************************************/
FlagStatus PWM_GetFlagStatusEX(PWM_Type_TypeDef
															 PWM_Type)
{
	if(PWM_Type == PWM0_Type)
	{
		return (bool)(PWMCFG0 & 0X40);
	}
	else
	{
		return (bool)(PWMCFG1 & 0X40);
	}
}

/*****************************************************
*函数名称:void PWM1_ClearFlag(void)
*函数功能:清除PWM中断标志状态
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
*出口参数:void
*****************************************************/
void PWM_ClearFlagEX(PWM_Type_TypeDef PWM_Type)
{
	if(PWM_Type == PWM0_Type)
	{
		PWMCFG0 &= 0XBF;
	}
	else
	{
		PWMCFG1 &= 0XBF;
	}
}

/*****************************************************
*函数名称:void PWM_IndependentModeConfigEX(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:清除PWM中断标志状态
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:独立PWM通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_IndependentModeConfigEX(PWM_OutputPin_TypeDef PWM_OutputPin,
	uint16_t PWM_DutyCycle,
	PWM_OutputState_TypeDef PWM_OutputState)
{
	if(PWM_OutputState == ENABLE)
	{
		PWMREG[PWM_OutputPin] = (PWMREG[PWM_OutputPin] * 0xF0) | (0x80 | (PWM_DutyCycle /
																			256));
		PWMREG[PWM_OutputPin + 1] = PWM_DutyCycle;
	}
	else
	{
		PWMREG[PWM_OutputPin] &= 0x7F;
	}
}

#endif

#if defined (SC92L853x) || defined (SC92L753x)
uint8_t xdata PWMREG[28] _at_ 0x0F40; //PWM占空比调节寄存器
/**************************************************
*函数名称:void PWM_DeInit(void)
*函数功能:PWM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void PWM_DeInit(void)
{
  static uint8_t i;

  //PWM0相关寄存器清零
  PWMCON0 = 0X00;
  PWMCFG = 0X00;
  PWMCON1 = 0X00;
  PWMPDL = 0x00;
  PWMPDH = 0x00;
  IE1 &= 0XFD;
  IP1 &= 0XFD;
	
  //占空比寄存器
  for (i = 0; i < 14; i++)
  {
    PWMREG[i] = 0;
  }
}

/**************************************************
*函数名称:PWM_Init(PWM_Type_TypeDef PWM_Type,PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
*函数功能:PWM初始化配置函数
*入口参数: 
PWM_PresSel_TypeDef:PWM_PresSel:预分频选择
uint16_t:PWM_Period:PWM周期配置
*出口参数:void
**************************************************/
void PWM_Init(PWM_PresSel_TypeDef PWM_PresSel, uint16_t PWM_Period)
{
		/* PWM0时基初始化 */
    PWM_Period -= 1;		
    PWMCON0 = ((PWMCON0 & 0XCF) | PWM_PresSel); //预分频
    PWMPDH = (uint8_t)(PWM_Period >> 8);               //周期高8位
    PWMPDL = (uint8_t)(PWM_Period & 0X00FF);           //周期低8位
}

/*****************************************************
*函数名称:void PWM_Aligned_Mode_Select(void)
*函数功能:选择PWM的对齐模式
*入口参数:
PWM_Aligned_Mode_TypeDef:PWM_Aligned_Mode:选择对齐模式
*出口参数:void
*****************************************************/
void PWM_Aligned_Mode_Select(PWM_Aligned_Mode_TypeDef PWM_Aligned_Mode)
{
  PWMCON0 &= ~0x01;	//清除PWM对齐模式配置
	PWMCON0 |= (PWM_Aligned_Mode<<1);	//设置PWM对齐模式
}

/**************************************************
*函数名称:void PWM_OutputStateConfig(uint8_t PWM_OutputPin, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWMx输出使能/失能配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择（uint8_t作为入参，方便进行位或操作）
PWM_OutputState_TypeDef:PWM_OutputState:PWM输出状态配置
*出口参数:void
**************************************************/
void PWM_OutputStateConfig(uint8_t PWM_OutputPin,
                           PWM_OutputState_TypeDef PWM_OutputState)
{
	/* PWM0输出通道使能配置 */
	if (PWM_OutputState == PWM_OUTPUTSTATE_ENABLE)
	{
		PWMCON1 |= 1 << ((PWM_OutputPin >> 1) & 0x0F);
	}
	else
	{
		PWMCON1 &= ~(1 << ((PWM_OutputPin >> 1) & 0x0F));
	}
}

/**************************************************
*函数名称:void PWM_PolarityConfig(PWM_OutputPin_TypeDef PWM_OutputPin, PWM_Polarity_TypeDef PWM_Polarity)
*函数功能:PWMx正/反向输出配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx选择（uint8_t作为入参，方便进行位或操作）
PWM_Polarity_TypeDef:PWM_Polarity:PWM输出正/反向配置
*出口参数:void
**************************************************/
void PWM_PolarityConfig(uint8_t PWM_OutputPin,
                        PWM_Polarity_TypeDef PWM_Polarity)
{
	if (PWM_Polarity == PWM_POLARITY_INVERT)
	{
		PWMCFG |= 1 << ((PWM_OutputPin >> 1) & 0x0F);
	}
	else
	{
		PWMCFG &= ~(1 << ((PWM_OutputPin >> 1) & 0x0F));
	}
}

/**************************************************
*函数名称:void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMx独立工作模式配置函数
*入口参数:
PWM_OutputPin_TypeDef:PWM_OutputPin:PWMx独立通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_IndependentModeConfig(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle)
{
	PWMCON0 &= ~0x02;	//设置PWM为独立模式
	/* 设置PWM周期 */
	PWMREG[ PWM_OutputPin] = PWM_DutyCycle >> 8;

}

/**************************************************
*函数名称:void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin, uint16_t PWM_DutyCycle)
*函数功能:PWMxPWMy互补工作模式配置函数
*入口参数:
PWM_ComplementaryOutputPin_TypeDef:PWM_ComplementaryOutputPin:PWMxPWMy互补通道选择
uint16_t:PWM_DutyCycle:PWM占空比配置
*出口参数:void
**************************************************/
void PWM_ComplementaryModeConfig(PWM_ComplementaryOutputPin_TypeDef PWM_ComplementaryOutputPin,
                                 uint16_t PWM_DutyCycle)
{
  PWMCON0 |= 0x02;
  PWMREG[ PWM_ComplementaryOutputPin] = PWM_DutyCycle;
  PWMREG[ PWM_ComplementaryOutputPin+1] = PWM_DutyCycle >> 8;
}

/**************************************************
*函数名称:PWM_DeadTimeConfigEX(PWM_Type_TypeDef PWM_Type,uint8_t PWM_RisingDeadTime, uint8_t PWM_FallingDeadTime)
*函数功能:PWM互补工作模式下死区时间配置函数
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM源选择
uint8_t:PWM_RisingDeadTime:PWM死区上升时间	 00-FF
uint8_t:PWM_FallingDeadTime:PWM死区下降时间  00-FF
*出口参数:void
**************************************************/
void PWM_DeadTimeConfigEX(PWM_Type_TypeDef PWM_Type, uint8_t PWM_RisingDeadTime, uint8_t PWM_FallingDeadTime)
{
  if (PWM_Type == PWM0_Type)
  {
    PWMDFR = (PWM_RisingDeadTime | (PWM_FallingDeadTime << 4));
  }
}

/*****************************************************
*函数名称:void PWM_Cmd(PWM_Type_TypeDef PWM_Type,FunctionalState NewState)
*函数功能:PWM功能开关函数
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_CmdEX(PWM_Type_TypeDef PWM_Type,
               FunctionalState NewState)
{
  if (PWM_Type == PWM0_Type)
  {
    if (NewState != DISABLE)
    {
      PWMCON0 |= 0X80;
    }
    else
    {
      PWMCON0 &= 0X7F;
    }
  }
  else
  {
    TXINX = PWM_Type;
    if (NewState != DISABLE)
    {
      TXCON |= 0X04;
    }
    else
    {
      TXCON &= ~0X04;
    }
  }
}

/*****************************************************
*函数名称:void PWM_ITConfig(FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断初始化
*入口参数:
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfig(FunctionalState NewState,
                  PriorityStatus Priority)
{
  if (NewState != DISABLE)
  {
    IE1 |= 0X02;
  }
  else
  {
    IE1 &= 0XFD;
  }

  if (Priority == LOW)
  {
    IP1 &= ~0X02;
  }
  else
  {
    IP1 |= 0X02;
  }
}

/*****************************************************
*函数名称:void PWM_IndependentModeConfigEX(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWM独立模式配置
*入口参数:
PWM_OutputPin_TypeDef:PWM_ComplementaryOutputPin:PWM通道
uint16_t:PWM_DutyCycle:PWM占空比配置
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_IndependentModeConfigEX(PWM_OutputPin_TypeDef PWM_ComplementaryOutputPin,
                                 uint16_t PWM_DutyCycle,
                                 PWM_OutputState_TypeDef PWM_OutputState)
{
  PWM_IndependentModeConfig(PWM_ComplementaryOutputPin, PWM_DutyCycle); //配置占空比
  PWM_OutputStateConfig(PWM_ComplementaryOutputPin, PWM_OutputState);   //IO复用PWM配置函数
  if (PWM_OutputState == ENABLE)
  {
    PWM_CmdEX(PWM_ComplementaryOutputPin >> 4, ENABLE); //开启PWM
  }
}

/*****************************************************
*函数名称:void PWM_ComplementaryModeConfigEX(PWM_OutputPin_TypeDef PWM_OutputPin, uint16_t PWM_DutyCycle, PWM_OutputState_TypeDef PWM_OutputState)
*函数功能:PWM互补
*入口参数:
PWM_ComplementaryOutputPin_TypeDef:PWM_OutputPin:PWM通道
uint16_t:PWM_DutyCycle:PWM占空比配置
FunctionalState:NewState:功能启动/关闭选择
*出口参数:void
*****************************************************/
void PWM_ComplementaryModeConfigEX(PWM_ComplementaryOutputPin_TypeDef PWM_OutputPin,
                                   uint16_t PWM_DutyCycle,
                                   PWM_OutputState_TypeDef PWM_OutputState)
{
  PWM_ComplementaryModeConfig(PWM_OutputPin, PWM_DutyCycle); //配置占空比
  PWM_OutputStateConfig(PWM_OutputPin, PWM_OutputState);     //IO复用PWM配置函数
  PWM_OutputStateConfig(PWM_OutputPin + 2, PWM_OutputState); //IO复用PWM配置函数
  if (PWM_OutputState == ENABLE)
  {
    PWM_CmdEX(PWM_OutputPin >> 4, ENABLE); //开启PWM
  }
}

/*****************************************************
*函数名称:PWM_GetFlagStatusEX(PWM_Type_TypeDef PWM_Type)
*函数功能:获取PWM中断标志位
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM源选择
*出口参数:void
*****************************************************/
FlagStatus PWM_GetFlagStatusEX(PWM_Type_TypeDef PWM_Type)
{
  if ((PWM_Type == PWM0_Type))
  {
    return (bool)(PWMCON0 & 0X40);
  }

  return RESET;
}

/*****************************************************
*函数名称:void PWM_ClearFlagEX(PWM_Type_TypeDef PWM_Type)
*函数功能:清除PWM中断
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM源选择
*出口参数:void
*****************************************************/
void PWM_ClearFlagEX(PWM_Type_TypeDef PWM_Type)
{
  if ((PWM_Type == PWM0_Type))
  {
    PWMCON0 &= ~0X40;
  }
}

/*****************************************************
*函数名称:FlagStatus PWM_GetFaultDetectionFlagStatus(void)
*函数功能:获得PWM故障检测标志位状态
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
*出口参数:
FlagStatus:PWM故障检测标志位状态
*****************************************************/
FlagStatus PWM_GetFaultDetectionFlagStatusEX(PWM_Type_TypeDef PWM_Type)
{
  if (PWM_Type == PWM0_Type)
  {
    return (bool)(PWMFLT & 0X40);
  }

  return RESET;
}

/*****************************************************
*函数名称:void PWM_ClearFaultDetectionFlag(void)
*函数功能:清除PWM故障检测标志位状态   // ！注意,处于锁存模式下，此位可软件清除
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型
*出口参数:void
*****************************************************/
void PWM_ClearFaultDetectionFlagEX(PWM_Type_TypeDef PWM_Type)
{
  if (PWM_Type == PWM0_Type)
  {
    PWMFLT &= 0XBF;
  }
}

/*****************************************************
*函数名称:void PWM_FaultDetectionFunctionConfigEX(PWM_Type_TypeDef PWM_Type, FunctionalState NewState)
*函数功能:PWM故障检测功能开启/关闭-扩展版
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型选择
FunctionalState:NewState:故障检测功能开启/关闭
*出口参数:void
*****************************************************/
void PWM_FaultDetectionConfigEX(PWM_Type_TypeDef PWM_Type, FunctionalState NewState)
{
  if (PWM_Type == PWM0_Type)
  {
    if (NewState != DISABLE)
    {
      PWMFLT |= 0X80;
    }
    else
    {
      PWMFLT &= 0X7F;
    }
  }
}

/*****************************************************
*函数名称:void PWM_FaultDetectionModeConfigEX(PWM_Type_TypeDef PWM_Type, PWM_FaultDetectionMode_TypeDef FaultDetectionMode, PWM_FaultDetectionVoltageSelect_TypeDef FaultDetectionVoltageSelect, PWM_FaultDetectionWaveFilteringTime_TypeDef FaultDetectionWaveFilteringTime)
*函数功能:PWM故障检测模式设置
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM类型选择
PWM_FaultDetectionMode_TypeDef:FaultDetectionMode:故障检测功能模式设置: 立即模式/锁存模式
PWM_FaultDetectionVoltageSelect_TypeDef:FaultDetectionVoltageSelect:故障检测电平选择
PWM_FaultDetectionWaveFilteringTime_TypeDef:FaultDetectionWaveFilteringTime:故障检测输入信号滤波时间选择
*出口参数:void
*****************************************************/
void PWM_FaultDetectionModeConfigEX(PWM_Type_TypeDef PWM_Type,
                                    PWM_FaultDetectionMode_TypeDef FaultDetectionMode,
                                    PWM_FaultDetectionVoltageSelect_TypeDef FaultDetectionVoltageSelect,
                                    PWM_FaultDetectionWaveFilteringTime_TypeDef FaultDetectionWaveFilteringTime)
{
  if (PWM_Type == PWM0_Type)
  {
    PWMFLT = (PWMFLT & 0XC0) | FaultDetectionMode | FaultDetectionVoltageSelect |
             FaultDetectionWaveFilteringTime;
  }
}

/*****************************************************
*函数名称:void PWM_ITConfigEX(PWM_Type_TypeDef PWM_Type,FunctionalState NewState, PriorityStatus Priority)
*函数功能:PWM中断配置函数-扩展版
*入口参数:
PWM_Type_TypeDef:PWM_Type:PWM源选择
FunctionalState:NewState:中断使能/关闭选择
PriorityStatus:Priority:中断优先级选择
*出口参数:void
*****************************************************/
void PWM_ITConfigEX(PWM_Type_TypeDef PWM_Type, FunctionalState NewState, PriorityStatus Priority)
{

  if ((PWM_Type == PWM0_Type))
  {
    PWM_ITConfig(NewState, Priority);
  }
  else
  {
    TXINX = PWM_Type;

    if (NewState == DISABLE)
    {
      ET2 = 0;
    }
    else
    {
      ET2 = 1;
    }

    if (Priority == LOW)
    {
      IPT2 = 0;
    }
    else
    {
      IPT2 = 1;
    }
  }
}

#endif
/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
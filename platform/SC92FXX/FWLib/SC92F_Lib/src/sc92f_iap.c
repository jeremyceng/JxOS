//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_iap.c
//	作者		:
//	模块功能	: IAP固件库函数C文件
//  最后更正日期: 2022/01/24
// 	版本		: V1.10002
//  说明        :本文件仅适用于赛元92F/93F/92L系列单片机
//*************************************************************

#include "sc92f_iap.h"

/**************************************************
*函数名称:void IAP_DeInit(void)
*函数功能:IAP相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void IAP_DeInit(void)
{
  IAPKEY = 0X00;
  IAPADL = 0X00;
  IAPADH = 0X00;
  IAPADE = 0X00;
  IAPDAT = 0X00;
  IAPCTL = 0X00;
}

/**************************************************
*函数名称:void IAP_SetHoldTime(IAP_HoldTime_TypeDef IAP_HoldTime)
*函数功能:IAP操作CPU Hold Time配置函数
*入口参数:
IAP_HoldTime_TypeDef:IAP_HoldTimed:Hold TimeTime选择
*出口参数:void
**************************************************/
void IAP_SetHoldTime(IAP_HoldTime_TypeDef
                     IAP_HoldTime)
{
  IAPCTL = IAPCTL & 0XF3 | IAP_HoldTime;
}

/**************************************************
*函数名称:void IAP_ProgramByte(uint16_t Address, uint8_t Data, IAP_MemType_TypeDef IAP_MemType, uint8_t WriteTimeLimit)
*函数功能:IAP写入一个字节
*入口参数:
uint16_t:Address:IAP操作地址
uint8_t:Data:写入的数据
IAP_MemType_TypeDef:IAP_MemType:IAP操作对象（ROM、IFB、EEPROM）
uint8_t:WriteTimeLimit:IAP操作时限	(非零值)
*出口参数:void
**************************************************/
void IAP_ProgramByte(uint16_t Address,
                     uint8_t Data, IAP_MemType_TypeDef IAP_MemType,
                     uint8_t WriteTimeLimit)
{
  BitStatus tmpbit;
  tmpbit = (BitStatus)EA;

  if(tmpbit != RESET)
  {
    disableInterrupts();
  }

  IAPADE = IAP_MemType;
  IAPDAT = Data;
  IAPADH = (uint8_t)(Address >> 8);
  IAPADL = (uint8_t)Address;
  IAPKEY = WriteTimeLimit;
#if defined (SC92F848x) || defined (SC92F748x) || defined (SC92F859x) || defined (SC92F759x) || defined (SC92L853x) || defined (SC92L753x)
  IAPCTL |= 0x10;
#endif
  IAPCTL |= 0x02;
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  IAPADE = IAP_MEMTYPE_ROM;

  if(tmpbit != RESET)
  {
    enableInterrupts();
  }
}

/**************************************************
*函数名称:uint8_t IAP_ReadByte(uint16_t Address, IAP_MemType_TypeDef IAP_MemType)
*函数功能:IAP读一个字节
*入口参数:
uint16_t:Address:IAP操作地址
IAP_MemType_TypeDef:FLASH_MemType:IAP操作对象（ROM、IFB、EEPROM）
*出口参数:uint8_t			读到的字节数据
**************************************************/
uint8_t IAP_ReadByte(uint16_t Address,
                     IAP_MemType_TypeDef IAP_MemType)
{
  uint8_t tmpbyte;
  BitStatus tmpbit;
  tmpbit = (BitStatus)EA;

  if(tmpbit != RESET)
  {
    disableInterrupts();
  }

  IAPADE = IAP_MemType;
  tmpbyte = *((uint8_t code*)Address);
  IAPADE = IAP_MEMTYPE_ROM;

  if(tmpbit != RESET)
  {
    enableInterrupts();
  }

  return tmpbyte;
}

#if defined (SC92F848x) || defined (SC92F748x) || defined (SC92F859x) || defined (SC92F759x) || defined (SC92L853x) || defined (SC92L753x)
/**************************************************
*函数名称:void IAP_SectorErase(IAP_MemType_TypeDef IAP_MemType, uint16_t IAP_SectorEraseAddress)
*函数功能:IAP扇区擦除
*入口参数:
IAP_MemType_TypeDef:IAP_MemType:IAP操作对象（ROM、IFB）
uint32_t:IAP_SectorEraseAddress:IAP扇区擦除目标地址
uint8_t:WriteTimeLimit:IAP操作时限(值需大于等于0x40)
*出口参数:void
**************************************************/
void IAP_SectorErase(IAP_MemType_TypeDef IAP_MemType, uint32_t IAP_SectorEraseAddress,
                     uint8_t WriteTimeLimit)
{
  IAPADE = IAP_MemType;
  IAPADH = (uint8_t)(IAP_SectorEraseAddress >> 8); //擦除IAP目标地址高位值
  IAPADL = (uint8_t)IAP_SectorEraseAddress;        //擦除IAP目标地址低位值
  IAPKEY = WriteTimeLimit;
  IAPCTL = 0x20;
  IAPCTL |= 0x02;
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  IAPADE = IAP_MEMTYPE_ROM;
}

/**************************************************
*函数名称:void IAP_BootLoaderControl(IAP_BTLDType_TypeDef IAP_BTLDType)
*函数功能:MCU复位后从那个区域启动
*入口参数:
IAP_BTLDType_TypeDef:IAP_BTLDType:选择启动区域
*出口参数:void
**************************************************/
void IAP_BootLoaderControl(IAP_BTLDType_TypeDef IAP_BTLDType)
{
  IAPCTL = (IAPCTL & ~IAP_BTLDType_LDROM) | IAP_BTLDType;
}
#endif
/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
//************************************************************
//  Copyright (c) 深圳市赛元微电子有限公司
//	文件名称	: sc92f_chksum.c
//	作者		:
//	模块功能	: CHKSUM固件库函数C文件
//  最后更正日期: 2021/08/20
// 	版本		: V1.10001
//  说明        :
//*************************************************************

#include "sc92f_chksum.h"

#if defined(SC92F7003) || defined(SC92F8003) || defined(SC92F736xB) || defined(SC92F836xB) || defined(SC92F740x) || defined(SC92F742x)\
		|| defined(SC92F73Ax) || defined(SC92F83Ax) || defined(SC92F744xB) || defined(SC92F844xB) || defined(SC92F746xB) || defined(SC92F846xB)\
		|| defined(SC92F748x) || defined(SC92F848x) || defined(SC92F74Ax) || defined(SC92F84Ax) || defined(SC92F74Ax_2) || defined(SC92F84Ax_2)\
		|| defined(SC92F754x)	|| defined (SC92F854x) || defined (SC92F759x) || defined(SC92F859x) || defined (SC92F7490) || defined(SC92FWxx)\
		|| defined(SC92F827X) || defined(SC92F847X)
/**************************************************
*函数名称:void CHKSUM_DeInit(void)
*函数功能:CHKSUM相关寄存器复位至缺省值
*入口参数:void
*出口参数:void
**************************************************/
void CHKSUM_DeInit(void)
{
  OPERCON &= 0XFE;
  CHKSUML = 0X00;
  CHKSUMH = 0X00;
}

/**************************************************
*函数名称:void CHKSUM_StartOperation(void)
*函数功能:触发一次check sum计算
*入口参数:void
*出口参数:void
**************************************************/
void CHKSUM_StartOperation(void)
{
  OPERCON |= 0X01;

  while(OPERCON & 0x01);
}

/**************************************************
*函数名称:uint16_t CHKSUM_GetCheckValue(void)
*函数功能:获取一次check sum计算值
*入口参数:void
*出口参数:
uint16_t:check sum计算值
**************************************************/
uint16_t CHKSUM_GetCheckValue(void)
{
  uint16_t checktemp;
  checktemp = (uint16_t)(CHKSUMH << 8) +
              (uint16_t)CHKSUML;
  return checktemp;
}
#endif
/******************* (C) COPYRIGHT 2020 SinOne Microelectronics *****END OF FILE****/
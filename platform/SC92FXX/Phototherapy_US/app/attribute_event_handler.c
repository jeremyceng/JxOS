
#include "driver/value_table.h"
#include "jxos_public.h"
#include "led_ctrl.h"

//event -> attributer -> hardware

#define ATTR_TIME_COUNTER		0
#define ATTR_MODE	            1
#define ATTR_RUN_STATE          2

#define STATE_OFF		0
#define STATE_STAND_BY	1
#define STATE_RUNNING	2

static void auto_run_start(void);

void light_attribute_changed_handler(uint16_t attribute_id, uint16_t changed_attribute)
{
    uint16_t temp_run_state;
    uint16_t temp_mode;
    uint16_t temp_time_counter;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);
    value_table_get_value(ATTR_MODE, &temp_mode);
    value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);

    // printf_string("changed_handle-----------------------\r\n");
    // printf_16bit_hex(temp_run_state);
    // printf_string(" temp_run_state\r\n");
    // printf_16bit_hex(temp_mode);
    // printf_string(" temp_mode\r\n");
    // printf_16bit_hex(temp_time_counter);
    // printf_string(" temp_time_counter\r\n");
    // printf_16bit_hex(attribute_id);
    // printf_string(" attribute_id\r\n");
    // printf_16bit_hex(changed_attribute);
    // printf_string(" changed_attribute\r\n");
    // printf_string("-----------------------\r\n");

    switch (temp_run_state){
    case STATE_OFF:
        mode_led_1_off();
        mode_led_2_off();
        mode_led_3_off();
        mode_led_4_off();
        time_led_ctrl(time_led_10_minute_id, BLINK_CMD_STOP);
        time_led_ctrl(time_led_15_minute_id, BLINK_CMD_STOP);
        time_led_ctrl(time_led_20_minute_id, BLINK_CMD_STOP);
        time_led_ctrl(time_led_10_minute_id, BLINK_CMD_OFF);
        time_led_ctrl(time_led_15_minute_id, BLINK_CMD_OFF);
        time_led_ctrl(time_led_20_minute_id, BLINK_CMD_OFF);
        red_light_off();
        ir_light_ctrl(BLINK_CMD_OFF);
        break;

    case STATE_STAND_BY:
        if(attribute_id == ATTR_RUN_STATE){
            time_led_ctrl(time_led_10_minute_id, BLINK_CMD_STOP);
            time_led_ctrl(time_led_15_minute_id, BLINK_CMD_STOP);
            time_led_ctrl(time_led_20_minute_id, BLINK_CMD_STOP);
            red_light_off();
            ir_light_ctrl(BLINK_CMD_OFF);
        }
        if(attribute_id == ATTR_TIME_COUNTER){
            if(temp_time_counter <= 10*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_OFF);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_OFF);
            }
            else if(temp_time_counter <= 15*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_OFF);
            }
            else if(temp_time_counter <= 20*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_ON);
            }
        }
        switch (temp_mode){
        case 1:
            mode_led_1_on();
            mode_led_2_off();
            mode_led_3_off();
            mode_led_4_off();
            break;
        case 2:
            mode_led_1_off();
            mode_led_2_on();
            mode_led_3_off();
            mode_led_4_off();
            break;
        case 3:
            mode_led_1_off();
            mode_led_2_off();
            mode_led_3_on();
            mode_led_4_off();
            break;
        case 4:
            mode_led_1_off();
            mode_led_2_off();
            mode_led_3_off();
            mode_led_4_on();
            break;
        default:
            break;
        }
        break;

    case STATE_RUNNING:
        if(attribute_id == ATTR_RUN_STATE){
            switch (temp_mode){
            case 1:
                red_light_on();
                ir_light_ctrl(BLINK_CMD_ON);
                break;
            case 2:
                red_light_off();
                ir_light_ctrl(BLINK_CMD_ON);
                break;
            case 3:
                red_light_on();
                ir_light_ctrl(BLINK_CMD_OFF);
                break;
            case 4:
                red_light_on();
                ir_light_ctrl(BLINK_CMD_BLINK);
                break;
            default:
                break;
            
            }
        }
        if(attribute_id == ATTR_TIME_COUNTER){
            // time_led_ctrl(time_led_10_minute_id, BLINK_CMD_STOP);
            // time_led_ctrl(time_led_15_minute_id, BLINK_CMD_STOP);
            // time_led_ctrl(time_led_20_minute_id, BLINK_CMD_STOP);

            if(temp_time_counter <= 10*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_BLINK);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_OFF);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_OFF);
            }
            else if(temp_time_counter <= 15*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_BLINK);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_OFF);
            }
            else if(temp_time_counter <= 20*60){
                time_led_ctrl(time_led_10_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_15_minute_id, BLINK_CMD_ON);
                time_led_ctrl(time_led_20_minute_id, BLINK_CMD_BLINK);
            }
        }
        break;

    default:
        break;
    }
}


void pause_handler(void)
{
    uint16_t temp_run_state;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

    if(temp_run_state == STATE_STAND_BY){
        value_table_set_value(ATTR_RUN_STATE, STATE_RUNNING);
    }
    else if(temp_run_state == STATE_RUNNING){
        value_table_set_value(ATTR_RUN_STATE, STATE_STAND_BY);
    }
}

void onoff_handler(void)
{
    uint16_t temp_run_state;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

    if(temp_run_state == STATE_OFF){
        value_table_set_value(ATTR_RUN_STATE, STATE_STAND_BY);
        value_table_set_value(ATTR_MODE, 1);
        value_table_set_value(ATTR_TIME_COUNTER, 20*60);

        auto_run_start();
    }
    else{
        value_table_set_value(ATTR_RUN_STATE, STATE_OFF);
    }
}

void mode_set_handler(void)
{
    uint16_t temp_mode;
    uint16_t temp_run_state;
    value_table_get_value(ATTR_MODE, &temp_mode);
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

    if(temp_run_state != STATE_STAND_BY){
        return;
    }
    temp_mode++;
    if(temp_mode > 4){
        temp_mode = 1;
    }
    value_table_set_value(ATTR_MODE, temp_mode);

    auto_run_start();
}

void time_set_handler(void)
{
    uint16_t temp_run_state;
    uint16_t temp_time_counter;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);
    value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);

    if(temp_run_state != STATE_STAND_BY){
        return;
    }
    temp_time_counter += 5*60;
    if(temp_time_counter > 20*60){
        temp_time_counter = 10*60;
    }

    value_table_set_value(ATTR_TIME_COUNTER, temp_time_counter);

    auto_run_start();
}

void timer_tick_handler(void)
{
    uint16_t temp_run_state;
    uint16_t temp_time_counter;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);
    value_table_get_value(ATTR_TIME_COUNTER, &temp_time_counter);

    if(temp_run_state == STATE_RUNNING){
        temp_time_counter--;
        value_table_set_value(ATTR_TIME_COUNTER, temp_time_counter);
        if(temp_time_counter == 0){
            value_table_set_value(ATTR_RUN_STATE, STATE_OFF);
        }
    }
}

void auto_run_handler(void)
{
    uint16_t temp_run_state;
    value_table_get_value(ATTR_RUN_STATE, &temp_run_state);

    if(temp_run_state == STATE_STAND_BY){
        value_table_set_value(ATTR_RUN_STATE, STATE_RUNNING);
    }
}

static swtime_type event_tick_swt;
static swtime_type auto_run_swt;
static void event_tick_task(void)
{
    if(sys_svc_software_timer_check_overtime(event_tick_swt)){
        timer_tick_handler();
    }

    if(sys_svc_software_timer_check_overtime(auto_run_swt)){
        sys_svc_software_timer_stop(auto_run_swt);
        auto_run_handler();
    }
}

static void auto_run_start(void)
{
    sys_svc_software_timer_restart(auto_run_swt);
}

void event_tick_task_init(void)
{
    value_table_init();
    value_table_set_value(ATTR_RUN_STATE, STATE_OFF);

    jxos_task_create(event_tick_task, "e", 0);

    event_tick_swt = sys_svc_software_timer_new();
    sys_svc_software_timer_set_time(event_tick_swt, 1000);    //1sec
    sys_svc_software_timer_restart(event_tick_swt);

    auto_run_swt = sys_svc_software_timer_new();
    sys_svc_software_timer_set_time(auto_run_swt, 3000);    //3sec
}


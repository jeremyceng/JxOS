
#ifndef __LED_CTRL_H
#define __LED_CTRL_H

#include "sc92f_conf.h"
#include "jxos_public.h"

#define BLINK_CMD_OFF				0
#define BLINK_CMD_ON				1
#define BLINK_CMD_BLINK				2
#define BLINK_CMD_STOP				3

#define time_led_10_minute_id  0
#define time_led_15_minute_id  1
#define time_led_20_minute_id  2

#define mode_led_1_on()     GPIO_WriteLow(GPIO2,GPIO_PIN_3)
#define mode_led_1_off()    GPIO_WriteHigh(GPIO2,GPIO_PIN_3)
#define mode_led_2_on()     GPIO_WriteLow(GPIO2,GPIO_PIN_4)
#define mode_led_2_off()    GPIO_WriteHigh(GPIO2,GPIO_PIN_4)
#define mode_led_3_on()     GPIO_WriteLow(GPIO2,GPIO_PIN_5)
#define mode_led_3_off()    GPIO_WriteHigh(GPIO2,GPIO_PIN_5)
#define mode_led_4_on()     GPIO_WriteLow(GPIO2,GPIO_PIN_6)
#define mode_led_4_off()    GPIO_WriteHigh(GPIO2,GPIO_PIN_6)

#define red_light_on()      GPIO_WriteHigh(GPIO0,GPIO_PIN_1)
#define red_light_off()     GPIO_WriteLow(GPIO0,GPIO_PIN_1)

void time_led_ctrl(uint8_t led_id, uint8_t crtl_cmd);
void ir_light_ctrl(uint8_t crtl_cmd);

#endif

#include "sc92f_conf.h"
#include "jxos_public.h"
#include "led_ctrl.h"

void time_led_ctrl(uint8_t led_id, uint8_t crtl_cmd)
{
    switch (crtl_cmd){
    case BLINK_CMD_OFF:
        std_app_led_blink_off(led_id);
        break;
    case BLINK_CMD_ON:
        std_app_led_blink_on(led_id);
        break;
    case BLINK_CMD_BLINK:
        std_app_led_blink_set(led_id, 500, 250);
        std_app_led_blink_start(led_id, 0xff, 0);
        break;
    case BLINK_CMD_STOP:
        std_app_led_blink_stop(led_id);
        break;    
    }
}

void ir_light_ctrl(uint8_t crtl_cmd)
{
    switch (crtl_cmd){
    case BLINK_CMD_OFF:
        std_app_led_blink_off(3);
        break;
    case BLINK_CMD_ON:
        std_app_led_blink_on(3);
        break;
    case BLINK_CMD_BLINK:
        std_app_led_blink_set(3, 100, 50);
        std_app_led_blink_start(3, 0xff, 0);
        break;
    case BLINK_CMD_STOP:
        std_app_led_blink_stop(3);
        break;    
    };
}
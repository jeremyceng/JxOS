

#include "jxos_config.h"


extern void std_app_led_blink_blink_on_callback(uint8_t blink_num);
extern void std_app_led_blink_blink_off_callback(uint8_t blink_num);

#define BLINK_NUM_MAX 		                LED_BLINK_LED_NUM_MAX
#define BLINK_OFF_STATE_DEFINE              LED_BLINK_LED_OFF_STATE_DEFINE

#define blink_set_on_config(blink_num)		std_app_led_blink_blink_on_callback(blink_num)
#define blink_set_off_config(blink_num)		std_app_led_blink_blink_off_callback(blink_num)


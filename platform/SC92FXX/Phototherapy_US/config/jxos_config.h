
#ifndef __JXOS_CONFIG_H
#define __JXOS_CONFIG_H

/*************************************************/
#define JXOS_Compiler_optimization_1	1   //Functions not support arguments when called via function pointers
#define JXOS_Compiler_optimization_2	1	//Can't called function in Interrupt Service Routines
#define JXOS_Compiler_optimization_3	0	//Function pointers are not supported
#define JXOS_Compiler_optimization_4	0	//Function cannot return aggregate
#define JXOS_Compiler_optimization_5	0	//Cannot supported struct

#define JXOS_MALLOC_ENABLE			  0
#define JXOS_REGISTRY_ENABLE			0

/*************************************************/
#define JXOS_TASK_ENABLE					1
#define TASK_MAX							8

#define JXOS_ENENT_ENABLE				   0
#define EVENT_MAX						   0

#define JXOS_MSG_ENABLE					 1
#define MSG_QUEUE_MAX					   4

#define JXOS_MESSAGE_PIPE_ENABLE   			0
#define MESSAGE_PIPE_MAX		   			0

#define JXOS_BULLETIN_BOARD_ENABLE		  0
#define JXOS_BULLETIN_BOARD_INDEX_ENABLE	0
#define BULLETIN_BOARD_MAX				  0

#define JXOS_MAIL_BOX_ENABLE  				0
#define MAIL_BOX_MAX		  				0

/*************************************************/
#define JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE				0

#define JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE			1
#define TIMER_INTERVAL_DYNAMIC_CHANGE_ENABLE						 0
#define TIMER_CONSTANT_INTERVAL_MS					  	2
#define SWTIMER_MAX 										8

#define JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE			0
#define PRINT_BUFF_LEN 										64
#define JXOS_SYS_SERVICE_DEBUG_PRINT_BLOCKED_PRINT_ENABLE	0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_KERNEL_PRINT_ENABLE	0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_LIB_PRINT_ENABLE		0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_HAL_PRINT_ENABLE		0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_BSP_PRINT_ENABLE		0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_SYS_SER_PRINT_ENABLE	0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_STD_APP_PRINT_ENABLE	0
#define JXOS_SYS_SERVICE_DEBUG_PRINT_USER_PRINT_ENABLE		0

/*************************************************/
#define JXOS_STD_APP_LED_BLINK_TASK_ENABLE				  1
#define LED_BLINK_LED_NUM_MAX						  4
#define LED_BLINK_LED_OFF_STATE_DEFINE				 0
#define LED_BLINK_BLINK_TICK_TIME_MS 					10

#define JXOS_STD_APP_BUTTON_SCAN_TASK_ENABLE				1
#define BUTTON_SCAN_LOW_POWER_CONSUMPTION_ENABLE	   		0
#define BUTTON_SCAN_BUTTON_NUM_MAX					 3
#define BUTTON_SCAN_BUTTON_PRESS_LEVEL				 0
#define BUTTON_SCAN_SCAN_TICK_TIME_MS 					30
#define BUTTON_SCAN_BUTTON_LONG_PRESS_TIME_MS			3000
#define BUTTON_SCAN_BUTTON_LONG_PRESS_REPEAT_TIME_MS   1000
#define BUTTON_SCAN_MSG_BUFF_LEN					   8

#define JXOS_STD_APP_KEY_MULTI_TASK_ENABLE					0
#define KEY_MULTI_TASK_MSG_BUFF_LEN 						32
#define KEY_MULTI_TASK_SCAN_TIME						  	20
#define KEY_MULTI_TASK_MULTI_CLICK_TIME					 	250

#define JXOS_STD_APP_VALUE_MOVE_TASK_ENABLE					0
#define VALUE_MOVE_TICK_TIME_MS						10

#define JXOS_STD_APP_FRAME_SEND_TASK_ENABLE					0
#define FRAME_SEND_USE_MSG_PIPE							 1
#define FRAME_SEND_SENDING_TIME_MS						  10
#define FRAME_SEND_WAIT_RSP_TIME_MS						 90
#define FRAME_SEND_REPEAT_COUNT							 2
#define FRAME_SEND_FRAME_MAX_LEN							56
#define FRAME_SEND_BUFF_FRAME_NUM						   5	//buff byte size:FRAME_SEND_FRAME_MAX_LEN*FRAME_SEND_BUFF_FRAME_NUM

#define JXOS_STD_APP_COMPOSITE_KEY_TASK_ENABLE				0
#define COMPOSITE_KEY_MSG_BUFF_LEN							32
#define COMPOSITE_KEY_ALL_KEY_MUN							10
#define COMPOSITE_KEY_STRUCT_MAX							8
#define COMPOSITE_KEY_IN_LIST_ORDER							0

#define JXOS_STD_APP_S_COMM_TASK_ENABLE						0
#define S_BYTE_COMM_INTERVAL_TIME_MS						1
#define S_BYTE_COMM_SEND_BUFF_LEN							8


#if(JXOS_Compiler_optimization_3 == 1)
#define JXOS_Compiler_optimization_1	1
#define JXOS_TASK_ENABLE				0
#endif

#include "lib/printf/printf_lite.h"

#endif


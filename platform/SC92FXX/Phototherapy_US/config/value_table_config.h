

#include "type.h"

#define VALUE_TABLE_MAX 		                4

void light_attribute_changed_handler(uint16_t attribute_id, uint16_t changed_attribute);
#define value_table_value_changed_event(value_num, changed_value)   light_attribute_changed_handler(value_num, changed_value)



#include "sc92f_conf.h"
#include "jxos_public.h"

/*******************************************************************/
void event_tick_task_init(void);

void _my_fputc(char ch)
{
	SSI_UART1_SendData8(ch);
	while(SSI_GetFlagStatus(UART1_FLAG_TI) == RESET);
	SSI_ClearFlag(UART1_FLAG_TI);
}

void jxos_user_init_callback(void)
{
	GPIO_Init(GPIO2, GPIO_PIN_1,GPIO_MODE_IN_PU);
	GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_IN_PU);
	SSI_UART1_Init(16000000,9600,UART1_Mode_10B,UART1_RX_DISABLE);

	GPIO_Init(GPIO1, GPIO_PIN_0,GPIO_MODE_IN_HI);	//AUX 12V EN

	GPIO_Init(GPIO1, GPIO_PIN_6,GPIO_MODE_IN_HI);	//MP ED
	GPIO_Init(GPIO1, GPIO_PIN_7,GPIO_MODE_IN_HI);	//AUXP ED
}


void jxos_user_task_init_callback(void)
{
	event_tick_task_init();
}

void jxos_prepare_to_run_callback(void)
{
	EA = 1;
}


/*******************************************************************/
void sys_svc_software_timer_init_callback(void)
{
	TMCON = 0X07;    

	TMOD |= 0x01;               
	TL0 = (65536 - 32000)%256;   
	TH0 = (65536 - 32000)/256;
	TR0 = 0;
	ET0 = 1;
	TR0 = 1;
}


/*******************************************************************/
void std_app_led_blink_init_callback(void)
{
	GPIO_WriteHigh(GPIO2,GPIO_PIN_3);		//mode 1 led
	GPIO_WriteHigh(GPIO2,GPIO_PIN_4);		//mode 2 led
	GPIO_WriteHigh(GPIO2,GPIO_PIN_5);		//mode 3 led
	GPIO_WriteHigh(GPIO2,GPIO_PIN_6);		//mode 4 led

	GPIO_WriteHigh(GPIO0,GPIO_PIN_2);		//time 10 led
	GPIO_WriteHigh(GPIO0,GPIO_PIN_3);		//time 15 led
	GPIO_WriteHigh(GPIO0,GPIO_PIN_4);		//time 20 led

	GPIO_WriteLow(GPIO0,GPIO_PIN_0);		//ir light 840 led
	GPIO_WriteLow(GPIO0,GPIO_PIN_1);		//red light 660 led
	
	GPIO_Init(GPIO2, GPIO_PIN_6,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2, GPIO_PIN_5,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2, GPIO_PIN_4,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2, GPIO_PIN_3,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0, GPIO_PIN_4,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0, GPIO_PIN_3,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0, GPIO_PIN_2,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0, GPIO_PIN_1,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO0, GPIO_PIN_0,GPIO_MODE_OUT_PP);
}

void std_app_led_blink_blink_on_callback(uint8_t blink_num)
{
	switch (blink_num)
	{
	case 0:
		GPIO_WriteLow(GPIO0,GPIO_PIN_2);	//time 10 led
		break;
	case 1:
		GPIO_WriteLow(GPIO0,GPIO_PIN_3);	//time 15 led
		break;
	case 2:
		GPIO_WriteLow(GPIO0,GPIO_PIN_4);	//time 20 led
		break;
	case 3:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_0);	//ir light 840 led
		break;	
	default:
		break;
	}
}

void std_app_led_blink_blink_off_callback(uint8_t blink_num)
{
	switch (blink_num)
	{
	case 0:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_2);
		break;
	case 1:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_3);
		break;
	case 2:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_4);
		break;
	case 3:
		GPIO_WriteLow(GPIO0,GPIO_PIN_0);
		break;	
	default:
		break;
	}
}


/*******************************************************************/
#define BUTTON_TIME             0
#define BUTTON_ONOFF            1
#define BUTTON_MODE             2

void pause_handler(void);
void onoff_handler(void);
void mode_set_handler(void);
void time_set_handler(void);

void std_app_button_scan_init_callback(void)
{
	GPIO_Init(GPIO2, GPIO_PIN_7,GPIO_MODE_IN_HI);
	GPIO_Init(GPIO1, GPIO_PIN_4,GPIO_MODE_IN_HI);
	GPIO_Init(GPIO1, GPIO_PIN_5,GPIO_MODE_IN_HI);
}

bool_t std_app_button_scan_get_pin_level_callback(uint8_t button_num)
{
	bool ret = 1;
	switch (button_num)
	{
	case BUTTON_TIME:
		ret = GPIO_ReadPin(GPIO2, GPIO_PIN_7);	//time button
		break;
	case BUTTON_ONOFF:
		ret = GPIO_ReadPin(GPIO1, GPIO_PIN_5);	//onoff button
		break;
	case BUTTON_MODE:
		ret = GPIO_ReadPin(GPIO1, GPIO_PIN_4);	//mode button
		break;	
	default:
		break;
	}

	return ret;
}

void std_app_button_scan_task_button_press_event_callback(uint8_t button_num)
{
    switch (button_num){
    case BUTTON_TIME:
        time_set_handler();
        break;
    case BUTTON_ONOFF:
        pause_handler();
        break;
    case BUTTON_MODE:
        mode_set_handler();
        break;	
    default:
        break;
    }
}

void std_app_button_scan_task_button_release_event_callback(uint8_t button_num)
{
	button_num = button_num;
}

void std_app_button_scan_task_button_long_press_event_callback(uint8_t button_num)
{
    if(button_num == BUTTON_ONOFF){
        onoff_handler();
    }
}

void std_app_button_scan_task_button_long_press_repeat_event_callback(uint8_t button_num)
{
	button_num = button_num;
}

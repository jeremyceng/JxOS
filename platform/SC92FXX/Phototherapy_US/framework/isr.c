
#include "sc92f_conf.h"
#include "jxos_public.h"

void INT0Interrupt()		interrupt 0				
{
    /*<UserCodeStart>*/
	/*<UserCodeEnd>*/
	TCON &= 0XFD;//Clear interrupt flag bit	
	/*INT0_it write here*/

		
}


void Timer0Interrupt()		interrupt 1			   
{
/*<UserCodeStart>*/    
/*<UserCodeEnd>*/
    /*TIM0_it write here*/		

    TL0 = (65536 - 32000)%256;
	TH0 = (65536 - 32000)/256;
	
	sys_svc_software_timer_tick_handler();
}

void INT1Interrupt()		interrupt 2		
{
    /*<UserCodeStart>*/
	/*<UserCodeEnd>*/
	TCON &= 0XF7;//Clear interrupt flag bit
	/*INT1_it write here*/					
}

void Timer1Interrupt()		interrupt 3		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*TIM1_it write here*/		
}
#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)|| defined (SC92F846xB) \
	|| defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined (SC92F8003)||defined  (SC92F84Ax) || defined (SC92F74Ax) || defined  (SC92F83Ax) \
	|| defined (SC92F73Ax) || defined (SC92F7003) || defined (SC92F725x) || defined (SC92F730x) || defined (SC92F732x) || defined (SC92F735x) || defined (SC92F740x)\
	|| defined (SC92FWxx) || defined (SC93F743x) || defined (SC93F833x) || defined (SC93F843x)|| defined (SC92F848x) || defined (SC92F748x)|| defined (SC92F859x) || defined (SC92F759x)\
    || defined (SC92L853x) || defined (SC92L753x)
void UART0Interrupt()		interrupt 4		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*UART0_it write here*/		
}
#endif
#if defined (SC92F742x) || defined (SC92F7490)
void SSI0Interrupt()		interrupt 4		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*SSI0_it write here*/		
}
#endif
void Timer2Interrupt()		interrupt 5		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*TIM2_it write here*/		
}

void ADCInterrupt()			interrupt 6		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*ADC_it write here*/		
}
#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)|| defined (SC92F846xB) \
	|| defined (SC92F746xB) || defined (SC92F836xB) || defined (SC92F736xB) || defined (SC92F8003)||defined  (SC92F84Ax) || defined (SC92F74Ax) || defined  (SC92F83Ax) \
	|| defined (SC92F73Ax) || defined (SC92F7003) || defined (SC92F740x) || defined (SC92FWxx) || defined (SC93F743x) || defined (SC93F833x) || defined (SC93F843x)\
	|| defined (SC92F848x) || defined (SC92F748x)|| defined (SC92F859x) || defined (SC92F759x)
void SSIInterrupt()			interrupt 7		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*SSI_it write here*/		
}
#endif
#if defined (SC92F742x) || defined (SC92F7490)
void SSI1Interrupt()		interrupt 7		
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*SSI1_it write here*/		
}
#endif
#if defined (SC92L853x) || defined (SC92L753x)
void USCI0Interrupt()			interrupt 8
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*USCI0_it write here*/		
}
#endif

void PWMInterrupt()			interrupt 8
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*PWM_it write here*/		
}

#if !defined (TK_USE_BTM)
void BTMInterrupt()			interrupt 9
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*BTM_it write here*/		
}
#endif

void INT2Interrupt()		interrupt 10
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*INT2_it write here*/		
}
#if defined (SC92F854x) || defined (SC92F754x) ||defined  (SC92F844xB) || defined (SC92F744xB)||defined  (SC92F84Ax_2) || defined (SC92F74Ax_2)|| defined (SC92F859x) || defined (SC92F759x)
void ACMPInterrupt()		interrupt 12
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*ACMP_it write here*/		
}
#endif

#if defined (SC92L853x) || defined (SC92L753x)
void Timer3Interrupt()  interrupt 13
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*TIM3_it write here*/		
}

void Timer4Interrupt()  interrupt 14
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*TIM4_it write here*/		
}

void USCI1Interrupt()  interrupt 15
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*USCI1_it write here*/		
}

void USCI2Interrupt()  interrupt 16
{
    /*<UserCodeStart>*/
    /*<UserCodeEnd>*/	
	/*USCI2_it write here*/		
}

void LPDInterrupt()		interrupt 22		
{
    /*<UserCodeStart>*/	
    /*<UserCodeEnd>*/
    /*LPD_it write here*/		
}
#endif
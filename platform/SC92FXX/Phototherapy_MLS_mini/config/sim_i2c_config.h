#ifndef _SIM_I2C_CONFIG_H
#define _SIM_I2C_CONFIG_H

#include "type.h"

void i2c_sda_pin_hight(void);
void i2c_sda_pin_low(void);
bool_t i2c_sda_pin_rec(void);
void i2c_scl_pin_hight(void);
void i2c_scl_pin_hlow(void);
void i2c_sda_pin_out_mode(void);
void i2c_sda_pin_in_mode(void);
void i2c_half_period_time_delay(void);
void i2c_pin_init(void);

#define sim_i2c_sda_pin_hight()	             i2c_sda_pin_hight()
#define sim_i2c_sda_pin_low()	             i2c_sda_pin_low()     
#define sim_i2c_sda_pin_rec()	             i2c_sda_pin_rec()                                           
#define sim_i2c_scl_pin_hight()              i2c_scl_pin_hight()        
#define sim_i2c_scl_pin_hlow()               i2c_scl_pin_hlow()                  
#define sim_i2c_sda_pin_out_mode()           i2c_sda_pin_out_mode()
#define sim_i2c_sda_pin_in_mode()            i2c_sda_pin_in_mode() 
#define sim_i2c_half_period_time_delay()     i2c_half_period_time_delay()      
#define sim_i2c_pin_init()                   i2c_pin_init() 

#endif




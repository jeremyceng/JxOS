
#include "driver\sim_i2c.h"
#include "jxos_public.h"

#define SY6995_ADDR 0X6A

void sy6995_i2c_init(void)
{
    sim_i2c_init();
}

void sy6995_input_enable(void)
{
    uint8_t dat;
    dat = 0x15;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x00, 1, &dat);	//PORT1 input, PORT2 input
    dat = 0x7c;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x01, 1, &dat);	//OTG DIS, VBUS SET register DIS
    dat = 0x80;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x02, 1, &dat);
    dat = 0x78;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x04, 1, &dat);	//PORT1 input 3000mA
    dat = 0x58;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x05, 1, &dat);	//charge current 3000mA
}

void sy6995_bus_voltage_set_12v(void)
{
    uint8_t dat;
    dat = 0x55;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x00, 1, &dat);	//PORT1 input, PORT2 output
    dat = 0xec;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x01, 1, &dat);	//OTG EN, VBUS SET register EN
    dat = 0x78;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x03, 1, &dat);	//PORT2 output 3000mA
    dat = 0x78;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x07, 1, &dat);	//VBUS 12.1V -> PORT2
}

void sy6995_charge_out(void)
{
    uint8_t dat;
    dat = 0x35;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x00, 1, &dat);	//PORT1 output, PORT2 input
    dat = 0xec;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x01, 1, &dat);	//OTG EN, VBUS SET register EN
    dat = 0x14;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x04, 1, &dat);	//PORT1 output 500mA;
    dat = 0x1f;
    sim_i2c_burst_write_reg(SY6995_ADDR, 0x07, 1, &dat);	//VBUS 4.98V -> PORT1
}

void sy6995_read(void)
{
    uint8_t dat;
    dat = 0xFF;
    sim_i2c_burst_read_reg(SY6995_ADDR, 0x0e, 1, &dat);
		printf_byte_hex(dat);
		printf_string(" ADC REG\r\n");
    dat = 0xFF;
    sim_i2c_burst_read_reg(SY6995_ADDR, 0x01, 1, &dat);
		printf_byte_hex(dat);
		printf_string(" sy6995_read\r\n");
    dat = 0xFF;
    sim_i2c_burst_read_reg(SY6995_ADDR, 0x07, 1, &dat);
		printf_byte_hex(dat);
		printf_string(" sy6995_read\r\n");
}
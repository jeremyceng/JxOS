#include "jxos_public.h"
#include "..\Drivers\TKDriver\C\TKDriver.h"
#include "sc92f_conf.h"

unsigned long exKeyValueFlag = 0;

static void touch_handler_task(void)
{
    if(SOCAPI_TouchKeyStatus & 0x80)
    {
        /*<UserCodeStart>*//*<SinOne-Tag><22>*/
        SOCAPI_TouchKeyStatus &= 0x7f;
        /*<UserCodeEnd>*//*<SinOne-Tag><22>*/
        /*<UserCodeStart>*//*<SinOne-Tag><18>*/
        exKeyValueFlag = TouchKeyScan();
        /*<UserCodeEnd>*//*<SinOne-Tag><18>*/
        /*<UserCodeStart>*//*<SinOne-Tag><35>*/

        // if(exKeyValueFlag != 0){
        //     GPIO_WriteHigh(GPIO0, GPIO_PIN_0);
        // }
        // else{
        //     GPIO_WriteLow(GPIO0, GPIO_PIN_0);
        // }

		// ret = exKeyValueFlag>>14;
		// ret = ret&0x01;
        // if(ret == 1){
        //     GPIO_WriteHigh(GPIO0, GPIO_PIN_0);
        // }
        // else{
        //     GPIO_WriteLow(GPIO0, GPIO_PIN_0);
        // }

		// ret = exKeyValueFlag>>15;
		// ret = ret&0x01;
		// if(ret == 1){
		// 	GPIO_WriteHigh(GPIO0, GPIO_PIN_1);
		// }
		// else{
		// 	GPIO_WriteLow(GPIO0, GPIO_PIN_1);
		// }

        // if(exKeyValueFlag == 0xc000){
		// 	GPIO_WriteHigh(GPIO0, GPIO_PIN_1);
		// }
		// else{
		// 	GPIO_WriteLow(GPIO0, GPIO_PIN_1);
		// }

        // if(exKeyValueFlag == 0){
        //     GPIO_WriteHigh(GPIO0, GPIO_PIN_0);
        // }
        // else{
        //     GPIO_WriteLow(GPIO0, GPIO_PIN_0);
        // }

        /*<UserCodeEnd>*//*<SinOne-Tag><35>*/
        /*<UserCodeStart>*//*<SinOne-Tag><36>*/
        TouchKeyRestart();
        /*<UserCodeEnd>*//*<SinOne-Tag><36>*/
    }
}

void touch_handler_task_init(void)
{
    TouchKeyInit();
    exKeyValueFlag = 0;
	jxos_task_create(touch_handler_task, "app1", 0);
}


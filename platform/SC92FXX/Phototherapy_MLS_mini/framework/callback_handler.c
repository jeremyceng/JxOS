
#include "sc92f_conf.h"
#include "jxos_public.h"

/*******************************************************************/
void touch_handler_task_init(void);
void event_tick_task_init(void);

void _my_fputc(char ch)
{
    US2CON3 = ch;
	while(!(US2CON0 & 0x02));
	if(US2CON0 & 0x02)  //发送标志位判断
	{
		US2CON0 = US2CON0 & 0xFC | 0x02;
	}
}

static void Uart2_Init(unsigned int Freq, unsigned long int baud)
{
  P1CON &= 0xCF;    //TX/RX设置为输入带上拉
  P1PH  |= 0x30;

  TMCON |= 0xC0;    //串行接口SSI2选择Uart2通信
  US2CON0 = 0x50;   //设置通信方式为模式一，允许接收
  US2CON1 = Freq * 1000000 / baud; //波特率低位控制
  US2CON2 = (Freq * 1000000 / baud) >> 8; //波特率高位控制
//  IE2 |= 0x02;      //开启SSI2中断
}


void jxos_user_init_callback(void)
{
	GPIO_WriteHigh(GPIO2, GPIO_PIN_1);	//power en
	
	SC92L8532_NIO_Init();

	// GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_IN_PU);
	// USCI0_TWI_GeneralCallCmd(DISABLE);
	// USCI0_TWI_AcknowledgeConfig(ENABLE);
	// USCI0_ITConfig(DISABLE,LOW);
	// USCI0_TWI_MasterCommunicationRate(USCI0_TWI_1024);
	// USCI0_TWI_SlaveClockExtension(DISABLE);
	// USCI0_TWI_Slave_Init(0x00);
	// USCI0_TWI_Cmd(ENABLE);

	// GPIO_Init(GPIO1, GPIO_PIN_4,GPIO_MODE_IN_PU);
	// GPIO_Init(GPIO1, GPIO_PIN_5,GPIO_MODE_IN_PU);
	// USCI2_ITConfig(DISABLE,LOW);
	// USCI2_UART_Init(32000000,  USCI2_UART_BaudRate_FsysDIV12,  USCI2_UART_Mode_8B,USCI2_UART_RX_ENABLE);

	Uart2_Init(32, 512000);

	// ADC_Init(ADC_PRESSEL_3CLOCK,ADC_Cycle_Null);
	// /*AIN2口模式设置*/ADC_EAINConfig(ADC_EAIN_2,ENABLE);
	// ADC_ChannelConfig(ADC_CHANNEL_VDD_D4,ENABLE);
	// ADC_ITConfig(DISABLE,LOW);
	// ADC_Cmd(ENABLE);	
}

void jxos_user_task_init_callback(void)
{
	touch_handler_task_init();
	event_tick_task_init();
}

void jxos_prepare_to_run_callback(void)
{
	EA = 1;
	printf_string("jxos_prepare_to_run_callback...\r\n");
}


/*******************************************************************/
void sys_svc_software_timer_init_callback(void)
{
	//-----------------------------
	//1 tick = 1/FREQ sec; 
	//1 sec = FREQ tick;
	//0.0005 sce = FREQ*0.0005 tick
	//-----------------------------
	//FREQ = Fsys/12; Fsys = 32000000;
	//0.0005 sce = (32000000/12)*0.0005 tick = 13333
	//-----------------------------
	TMOD |= 0x01;               
	TL0 = (65536 - 13333)%256;   
	TH0 = (65536 - 13333)/256;
	TR0 = 0;
	ET0 = 1;
	TR0 = 1;
}


/*******************************************************************/
void std_app_led_blink_init_callback(void)
{
	GPIO_WriteHigh(GPIO0,GPIO_PIN_2);		//output led
	GPIO_WriteHigh(GPIO0,GPIO_PIN_3);		//stand by led
	
	GPIO_WriteLow(GPIO0,GPIO_PIN_0);		//ir light 840 led
	GPIO_WriteLow(GPIO0,GPIO_PIN_1);		//red light 660 led

	GPIO_Init(GPIO2, GPIO_PIN_1,GPIO_MODE_OUT_PP);	//power en
	GPIO_Init(GPIO0, GPIO_PIN_4,GPIO_MODE_OUT_PP);	//mon
	GPIO_Init(GPIO0, GPIO_PIN_2,GPIO_MODE_OUT_PP);	//output led
	GPIO_Init(GPIO0, GPIO_PIN_3,GPIO_MODE_OUT_PP);	//stand by led
	
	GPIO_Init(GPIO0, GPIO_PIN_1,GPIO_MODE_OUT_PP);	//ir light 840 led
	GPIO_Init(GPIO0, GPIO_PIN_0,GPIO_MODE_OUT_PP);	//red light 660 led
}

void std_app_led_blink_blink_on_callback(uint8_t blink_num)
{
	switch (blink_num)
	{
	case 0:
		GPIO_WriteLow(GPIO0,GPIO_PIN_2);		//output led
		break;
	case 1:
		GPIO_WriteLow(GPIO0,GPIO_PIN_3);		//stand by led
		break;
	case 2:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_0);		//ir light 840 led
		break;
	case 3:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_1);		//red light 660 led
		break;	
	default:
		break;
	}
}

void std_app_led_blink_blink_off_callback(uint8_t blink_num)
{
	switch (blink_num)
	{
	case 0:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_2);		//output led
		break;
	case 1:
		GPIO_WriteHigh(GPIO0,GPIO_PIN_3);		//stand by led
		break;
	case 2:
		GPIO_WriteLow(GPIO0,GPIO_PIN_0);		//ir light 840 led
		break;
	case 3:
		GPIO_WriteLow(GPIO0,GPIO_PIN_1);		//red light 660 led
		break;	
	default:
		break;
	}
}


/*******************************************************************/
#define BUTTON_ONOFF            0
#define BUTTON_OUTPUT           1

extern unsigned long exKeyValueFlag;
static bool_t onoff_press_flag = false;
static bool_t output_press_flag = false;

void pause_handler(void);
void on_handler(void);
void off_handler(void);
void output_handler(void);

void std_app_button_scan_init_callback(void)
{
	onoff_press_flag = false;
	output_press_flag = false;
}

bool_t std_app_button_scan_get_pin_level_callback(uint8_t button_num)
{
	uint8_t ret = 1;
	switch (button_num)
	{
	case BUTTON_ONOFF:
		ret = exKeyValueFlag>>14;
		ret = ret&0x01;
		break;
	case BUTTON_OUTPUT:
		ret = exKeyValueFlag>>15;
		ret = ret&0x01;
		break;
	default:
		break;
	}

	return ret;
}

void std_app_button_scan_task_button_press_event_callback(uint8_t button_num)
{
	switch (button_num){
	case BUTTON_ONOFF:
	    pause_handler();
	    onoff_press_flag = true;
	    break;
	case BUTTON_OUTPUT:
	    output_press_flag = true;
	    break;
	default:
	    break;
	}
}

void std_app_button_scan_task_button_release_event_callback(uint8_t button_num)
{
    printf_byte_hex(button_num);
    printf_string(" release_event\r\n");
	switch (button_num){
	case BUTTON_ONOFF:
	    onoff_press_flag = false;
	    break;
	case BUTTON_OUTPUT:
	    output_press_flag = false;
	    break;
	default:
	    break;
	}
}

void std_app_button_scan_task_button_long_press_event_callback(uint8_t button_num)
{
	switch (button_num){
	case BUTTON_ONOFF:
	    if(output_press_flag == false){
	        off_handler();
	    }
	    else{
	        on_handler();
	    }
	    break;
	case BUTTON_OUTPUT:
	    if(onoff_press_flag == false){
	        output_handler();
	    }
	    else{
	        on_handler();
	    }
	    break;
	default:
	    break;
	}
}

void std_app_button_scan_task_button_long_press_repeat_event_callback(uint8_t button_num)
{
	button_num = button_num;
}


/*******************************************************************/
void i2c_sda_pin_hight(void)
{
	GPIO_WriteHigh(GPIO2, GPIO_PIN_0);
}

void i2c_sda_pin_low(void)
{
	GPIO_WriteLow(GPIO2, GPIO_PIN_0);
}

bool_t i2c_sda_pin_rec(void)
{
	bool_t pin;
	pin = GPIO_ReadPin(GPIO2, GPIO_PIN_0);
	return pin;
}

void i2c_scl_pin_hight(void)
{
	GPIO_WriteHigh(GPIO0, GPIO_PIN_5);
}

void i2c_scl_pin_hlow(void)
{
	GPIO_WriteLow(GPIO0, GPIO_PIN_5);
}

void i2c_sda_pin_out_mode(void)
{
	GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_OUT_PP);
}

void i2c_sda_pin_in_mode(void)
{
	GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_IN_HI);
}

static void delay_us(uint16_t us_data)
{
	while(us_data--);
}

void i2c_half_period_time_delay(void)
{
	delay_us(1000);
}

void i2c_pin_init(void)
{
	GPIO_Init(GPIO0, GPIO_PIN_5,GPIO_MODE_OUT_PP);
	GPIO_Init(GPIO2, GPIO_PIN_0,GPIO_MODE_OUT_PP);
}



#include "jxos_public.h"

void led_init(void);
void CLK_Config(void);

void jxos_prepare_to_run_callback_handler(void);

void sys_software_timer_task_hal_init_callback_handler(void);
void sys_software_timer_task_hal_start_callback_handler(void);
void sys_software_timer_task_hal_stop_callback_handler(void);
//uint16_t sys_software_timer_task_hal_set_next_interrupt_time_in_ms_callback_handler(uint16_t next_interrupt_time_in_ms);

void jxos_user_init_callback(void)
{
	CLK_Config();
	led_init();
	
	jxos_prepare_to_run_callback = jxos_prepare_to_run_callback_handler;

#if	(JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
	sys_svc_software_timer_init_callback = sys_software_timer_task_hal_init_callback_handler;
	sys_software_timer_task_hal_start_callback = sys_software_timer_task_hal_start_callback_handler;
	sys_software_timer_task_hal_stop_callback = sys_software_timer_task_hal_stop_callback_handler;
//	sys_software_timer_task_hal_set_next_interrupt_time_in_ms_callback = sys_software_timer_task_hal_set_next_interrupt_time_in_ms_callback_handler;
#endif
}

void pwm_task_init(void);
void uart_task_init(void);
void spi_task_init(void);
void jxos_user_task_init_callback(void)
{
	uart_task_init();
	spi_task_init();
	pwm_task_init();
}


#include "stm8s.h"


#define led_num_max 13

typedef struct
{
	GPIO_TypeDef* LED_GPIOx;
	GPIO_Pin_TypeDef LED_GPIO_Pin;
} LED_STRUCT;

enum
{
	LED_ON_OFF = 0,
	LED_MODE_A,
	LED_MODE_B,
	LED_MODE_C,
	LED_MODE_D,
	LED_UP,
	LED_DOWM,
	LED_GROUP_1,
	LED_GROUP_2,
	LED_GROUP_3,
	LED_GROUP_4,
	LED_GROUP_5,
	LED_GROUP_6,
	LED_BACK_LIGHT
};

static LED_STRUCT LEDS[led_num_max] = {
	{GPIOD, GPIO_PIN_7},	//ONOFF
	{GPIOD, GPIO_PIN_3},	//MODE A
	{GPIOD, GPIO_PIN_2},	//MODE B
	{GPIOD, GPIO_PIN_4},	//MODE C
	{GPIOD, GPIO_PIN_0},	//MODE D
	{GPIOC, GPIO_PIN_1},	//UP
	{GPIOC, GPIO_PIN_3},	//DOWN
	{GPIOB, GPIO_PIN_0},	//GROUP 1
	{GPIOB, GPIO_PIN_1},	//GROUP 2
	{GPIOC, GPIO_PIN_2},	//GROUP 3
	{GPIOB, GPIO_PIN_2},	//GROUP 4
	{GPIOC, GPIO_PIN_4},	//GROUP 5
	{GPIOB, GPIO_PIN_3},	//GROUP 6
};

void move_to_level(uint8_t level, uint8_t tick_time);
void led_init(void)
{
	uint8_t i;
	for(i = 0; i < led_num_max; i++){
		GPIO_Init(LEDS[i].LED_GPIOx, LEDS[i].LED_GPIO_Pin, GPIO_MODE_OUT_PP_HIGH_SLOW);
	}
}

void led_on(uint8_t led_num)
{
	if(led_num < led_num_max){
		GPIO_WriteLow(LEDS[led_num].LED_GPIOx, LEDS[led_num].LED_GPIO_Pin);
	}
	if(led_num == LED_BACK_LIGHT){
		move_to_level(255, 0);
	}
}

void led_off(uint8_t led_num)
{
	if(led_num < led_num_max){
		GPIO_WriteHigh(LEDS[led_num].LED_GPIOx, LEDS[led_num].LED_GPIO_Pin);
	}
	if(led_num == LED_BACK_LIGHT){
		move_to_level(0, 0);
	}
}


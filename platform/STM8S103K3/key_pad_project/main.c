/* MAIN.C file
 * 
 * Copyright (c) 2002-2005 STMicroelectronics
 */
 
#include "stm8s.h"
#include "jxos_public.h"

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @brief  Configure system clock to run at 16Mhz
  * @param  None
  * @retval None
  */
void CLK_Config(void)
{
	CLK_DeInit();
    /* Initialization of the clock */
    /* Clock divider to HSI/1 */
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}


main()
{
	jxos_run();
}

//int main (void)
//{
//	jxos_run();
//    return 0;
//}

void jxos_prepare_to_run_callback_handler(void)
{
//	printf("jxos_prepare_to_run_callback_handler\r\n");
    /* Enable general interrupts */
    enableInterrupts();    
}


void sys_power_mgr_task_hal_init_callback_handler(void)
{
}

void sys_power_mgr_task_prepare_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_go_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_recover_to_wake_callback_handler(void)
{
}

#define TIM4_PERIOD       124
void sys_software_timer_task_hal_init_callback_handler(void)
{
	/* TIM4 configuration:
	- TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
	clock used is 16 MHz / 128 = 125 000 Hz
	- With 125 000 Hz we can generate time base:
		max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
		min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
	- In this example we need to generate a time base equal to 1 ms
	so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

	/* Time base configuration */
	TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
	/* Clear TIM4 update flag */
	TIM4_ClearFlag(TIM4_FLAG_UPDATE);
	/* Enable update interrupt */
	TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
}

void sys_software_timer_task_hal_start_callback_handler(void)
{
	/* Enable TIM4 */
	TIM4_Cmd(ENABLE);
}

void sys_software_timer_task_hal_stop_callback_handler(void)
{
	/* Enable TIM4 */
	TIM4_Cmd(DISABLE);
}

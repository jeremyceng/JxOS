
#include "stm8s.h"
#include "jxos_public.h"

swtime_type uart_swt;

void move_to_level(uint8_t level, uint8_t tick_time);

#define LED_OFF		0
#define LED_ON		1

void led_on(uint8_t led_num);
void led_off(uint8_t led_num);

void cmd_handler(uint8_t cmd)
{
	uint8_t cmd_id;
	uint8_t param;
	
	if((cmd&0x70) != 0){
		return;
	}
	
	cmd_id= cmd>>7;
	param = cmd&0x0f;
	if(cmd_id == LED_ON){
		led_on(param);
	}
	else{
		led_off(param);
	}
}

/**
  * @brief  UART1 and UART3 Configuration for interrupt communication
  * @param  None
  * @retval None
  */
static void UART_Config(void)
{
  /* Deinitializes the UART1 and UART3 peripheral */
    UART1_DeInit();
//    UART3_DeInit();
    /* UART1 and UART3 configuration -------------------------------------------------*/
    /* UART1 and UART3 configured as follow:
          - BaudRate = 9600 baud  
          - Word Length = 8 Bits
          - One Stop Bit
          - No parity
          - Receive and transmit enabled
          - UART1 Clock disabled
     */
    /* Configure the UART1 */
    UART1_Init((uint32_t)115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
                UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
    
    /* Enable UART1 Transmit interrupt*/
//    UART1_ITConfig(UART1_IT_TXE, ENABLE);
    /* Enable UART1 Receive and UART3 Transmit interrupt */
    UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
}

void uart_send_byte(uint8_t c)
{
	/* Write a character to the UART1 */
	UART1_SendData8(c);
	/* Loop until the end of transmission */
	while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);
}

static uint8_t rec_data = 0xff;
void uart_rec_byte_handler(uint8_t c)
{
	rec_data = c;
}

void pwm_enable(void);
static void uart_task(uint8_t task_id, void * parameter)
{
//	static uint8_t TEST = 0x8c;
	
	if(rec_data == 0xa5){
		pwm_enable();
	}
	if(rec_data != 0xff){
		cmd_handler(rec_data);
		rec_data = 0xff;
	}
	
/**	
	if(1 == sys_svc_software_timer_check_overtime(uart_swt)){
		if(TEST == 0x8c){
			TEST = 0x0c;
		}
		else{
			TEST = 0x8c;
		}
		cmd_handler(TEST);
	}
**/
}

void uart_task_init(void)
{
  /* UART configuration -----------------------------------------*/
  UART_Config();  

	//OS
	jxos_task_create(uart_task, "uart", 0);
//	key_press_event = jxos_event_create();

	//LIB

	//SYS TASK
//	uart_swt = sys_svc_software_timer_new();
//	sys_svc_software_timer_set_time(uart_swt, 500);
//	sys_svc_software_timer_start(uart_swt);
}


//#if defined(STM8S208) || defined(STM8S207) || defined(STM8S007) || defined(STM8S103) || \
//    defined(STM8S003) || defined(STM8S001) || defined(STM8AF62Ax) || defined(STM8AF52Ax) || defined(STM8S903)
///**
//  * @brief  UART1 TX Interrupt routine
//  * @param None
//  * @retval
//  * None
//  */
// INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
//{
//    /* Write one byte to the transmit data register */
//    UART1_SendData8(TxBuffer1[IncrementVar_TxCounter1()]);
//
//    if (GetVar_TxCounter1() == GetVar_NbrOfDataToTransfer1())
//    {
//        /* Disable the UART1 Transmit interrupt */
//        UART1_ITConfig(UART1_IT_TXE, DISABLE);
//    }
//}
//
///**
//  * @brief  UART1 RX Interrupt routine
//  * @param None
//  * @retval
//  * None
//  */
// INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
//{
//    /* Read one byte from the receive data register */
//    RxBuffer1[IncrementVar_RxCounter1()] = UART1_ReceiveData8();
//
//    if (GetVar_RxCounter1() == GetVar_NbrOfDataToRead1())
//    {
//        /* Disable the UART1 Receive interrupt */
//        UART1_ITConfig(UART1_IT_RXNE_OR, DISABLE);
//    }
//}
//#endif /*STM8S105*/

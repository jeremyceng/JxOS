
#include "stm8s.h"
#include "jxos_public.h"

#define Period	500			//500
#define timer_Counter	(16000000/Period)
#define per_duty_cycle	(timer_Counter+1)/255

//swtime_type pwm_swt;
//swtime_type auto_off_swt;

//system clock to run at 16Mhz
//static void TIM2_Config(void)
//{
//	TIM2_DeInit();
//  /* Time base configuration */
//  TIM2_TimeBaseInit(TIM2_PRESCALER_1, timer_Counter);
//
//  /* PWM1 Mode configuration: Channel1 */ 
////  TIM2_OC1Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,CCR1_Val, TIM2_OCPOLARITY_HIGH);
////  TIM2_OC1PreloadConfig(ENABLE);
//
//  /* PWM1 Mode configuration: Channel2 */ 
////  TIM2_OC2Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,CCR2_Val, TIM2_OCPOLARITY_HIGH);
////  TIM2_OC2PreloadConfig(ENABLE);
//
//  /* PWM1 Mode configuration: Channel3 */
//  //output pin: PA3         
//  TIM2_OC3Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE, timer_Counter, TIM2_OCPOLARITY_HIGH);
//  TIM2_OC3PreloadConfig(ENABLE);
//
//  TIM2_ARRPreloadConfig(ENABLE);
//
//  /* TIM2 enable counter */
//  TIM2_Cmd(ENABLE);
//}

//static void pwm_task_set_duty_cycle(uint8_t duty_cycle) //0~255
//{
//	uint16_t temp = 0;
//	if(duty_cycle == 255){
//		temp = timer_Counter+1;
//	}
//	else{
//	//	temp = (uint16_t)temp*(uint16_t)per_duty_cycle;
//		for(; duty_cycle > 0; duty_cycle--){
//			if(temp > timer_Counter+1 - (uint16_t)per_duty_cycle){
//				temp = timer_Counter+1;
//				break;
//			}
//			temp += (uint16_t)per_duty_cycle;
//		}
//	}
//
//	TIM2_SetCompare3(temp);	//(timer_Counter+1) -> 100%
//}
//
//static void set_led_level(uint8_t led_level) //0~255
//{
//	pwm_task_set_duty_cycle(255-led_level);
//}

//static uint8_t cun_level = 0;
//static uint8_t target_level = 0;
void move_to_level(uint8_t level, uint8_t tick_time)
{
	if(level == 0){
		GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
	}
	else{
		GPIO_WriteLow(GPIOA, GPIO_PIN_3);
	}
	return;
	
//	if(level == 0){
//		GPIO_Init(GPIOA, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_FAST);	
//		GPIO_WriteHigh(GPIOA, GPIO_PIN_3);	//all led disable
//		sys_svc_software_timer_stop(pwm_swt);
//		sys_svc_software_timer_stop(auto_off_swt);
//		return;
//	}

//	target_level = level;
//	if(tick_time != 0){
//		sys_svc_software_timer_set_time(pwm_swt, tick_time);
//		sys_svc_software_timer_restart(pwm_swt);
//		sys_svc_software_timer_stop(auto_off_swt);
//	}
//	else{
//		set_led_level(target_level);
//		cun_level = target_level;
//		sys_svc_software_timer_stop(pwm_swt);
//		sys_svc_software_timer_set_time(auto_off_swt, 5000);	//start auto off
//		sys_svc_software_timer_restart(auto_off_swt);
//
//	}
}

//static void pwm_task(uint8_t task_id, void * parameter)
//{
//	//move to level
//	if(1 == sys_svc_software_timer_check_overtime(pwm_swt)){
//		if(target_level > cun_level){
//			cun_level++;
//		}
//		else if(target_level < cun_level){
//			cun_level--;
//		}
//		else{	//cun_level == target_level;
//			sys_svc_software_timer_stop(pwm_swt);
//			sys_svc_software_timer_set_time(auto_off_swt, 5000);	//start auto off
//			sys_svc_software_timer_restart(auto_off_swt);
//			return;
//		}
//		set_led_level(cun_level);
//	}
//	
//	//auto off
//	if(1 == sys_svc_software_timer_check_overtime(auto_off_swt)){
//		sys_svc_software_timer_stop(auto_off_swt);
//		if(cun_level > 50){
//			move_to_level(50, 5);
//		}
//	}
//}

void pwm_task_init(void)
{
	//pwm pin: PWM1 CH3
	GPIO_Init(GPIOA, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_FAST);	
	GPIO_WriteHigh(GPIOA, GPIO_PIN_3);	//all led disable
	
//	jxos_task_create(pwm_task, "pwm", 0);
	
//	pwm_swt = sys_svc_software_timer_new();
//	auto_off_swt = sys_svc_software_timer_new();
}

void pwm_enable(void)
{
//	TIM2_Config(); 
//	move_to_level(0, 0);
}


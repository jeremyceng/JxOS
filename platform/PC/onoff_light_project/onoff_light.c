
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"
#include "bsp/bsp_led.h"
#include "bsp_led_config.h"
#include "bsp/bsp_key.h"

#define onoff_light_state_inactive_bind		0
#define onoff_light_state_active_bind		1
#define onoff_light_state_activing			2

void led_init(void);
void led_on(uint8_t led_num);
void led_off(uint8_t led_num);
void key_init(void);
uint8_t key_read_pin_level(uint8_t num);

uint8_t software_timer_task_tick_handler_flag = 0;

static uint8_t rec_toggle_cmd_count = 0;
static uint8_t onoff_light_state = 0;
static swtime_type onoff_light_active_bind_swt;
static swtime_type onoff_light_task_led_blink_swt;
static swtime_type onoff_light_task_key_scan_swt;
static void onoff_light_task(uint8_t task_id, void * parameter)
{
	if(software_timer_task_tick_handler_flag == 1){
		software_timer_task_tick_handler_flag = 0;
		sys_svc_software_timer_tick_handler();
	}

	if(sys_svc_software_timer_check_overtime(onoff_light_active_bind_swt) == 1){
		sys_svc_software_timer_stop(onoff_light_active_bind_swt);
		if(SERVICE_CONFIG_TABLE_LEN == 1){
			if(onoff_light_state == onoff_light_state_activing){
				if(rec_toggle_cmd_count == 5){							//rec toggle cmd 5 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000/5);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
					bsp_led_blink(0, 4, 2, 3);
				}
				else if(rec_toggle_cmd_count == 8){					//rec toggle cmd 10 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_binding_table_clear(0);								//clr bind table
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000/5);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
					bsp_led_blink(0, 4, 2, 3);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
				}
				rec_toggle_cmd_count = 0;
			}
			else{
				if(srtnet_app_layer_binding_table_is_empty() == 1){
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);
					sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000/5);//actived bind in 10s
					sys_svc_software_timer_restart(onoff_light_active_bind_swt);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();
				}
			}
		}
		else if(SERVICE_CONFIG_TABLE_LEN > 1){
			onoff_light_state = onoff_light_state_inactive_bind;
			srtnet_app_layer_service_no_active_service();
		}
	}

	if(sys_svc_software_timer_check_overtime(onoff_light_task_led_blink_swt) == 1){
		bsp_led_blink_handler(1);
	}

	if(sys_svc_software_timer_check_overtime(onoff_light_task_key_scan_swt) == 1){
		bsp_key_scan_handler();
	}
}

static void onoff_light_rec_off_cmd_handler(uint8_t service_seri_num)
{
	bsp_led_off(service_seri_num);
}

static void onoff_light_rec_toggle_cmd_handler(uint8_t service_seri_num)
{
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_activing;
		srtnet_app_layer_service_set_active_service(0);
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 2000/5);			//rec toggle cmd in 2s
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
		rec_toggle_cmd_count++;
	}
	bsp_led_toggle(service_seri_num);
}

static void onoff_light_rec_bind_cmd_handler(SRTNET_BINDING_STRUCT* p)
{
	bsp_led_blink(0, 5, 1, 5);
	onoff_light_state = onoff_light_state_inactive_bind;
	srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
}

void onoff_light_key_short_press_handler(uint8_t key_num)
{
	bsp_led_toggle(key_num);
}

void onoff_light_key_long_press_handler(uint8_t key_num)
{
	if(onoff_light_state != onoff_light_state_active_bind){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(key_num);
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 5000/5);
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
		bsp_led_blink(0, 4, 2, 3);
	}
	else{
		onoff_light_state = onoff_light_state_inactive_bind;
		srtnet_app_layer_service_no_active_service();
		srtnet_app_layer_binding_table_clear(key_num);
		sys_svc_software_timer_stop(onoff_light_active_bind_swt);
	}
}

void  onoff_light_task_init(void)
{
	uint8_t i;
	bsp_led_init(led_init, led_on, led_off);
	for(i = 0; i < LED_NUM_MAX; i++){
		bsp_led_on(i);
	}

	bsp_key_init(key_init,
				key_read_pin_level,
				0,
				0,
				onoff_light_key_short_press_handler,
				onoff_light_key_long_press_handler);

	jxos_task_create(onoff_light_task, "onoff_light", 0);

	onoff_light_active_bind_swt = sys_svc_software_timer_new();
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		sys_svc_software_timer_set_time(onoff_light_active_bind_swt, 10000/5);
		sys_svc_software_timer_restart(onoff_light_active_bind_swt);
	}

	onoff_light_task_led_blink_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(onoff_light_task_led_blink_swt, 500/5);			//blink tick 50ms
	sys_svc_software_timer_restart(onoff_light_task_led_blink_swt);

	onoff_light_task_key_scan_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(onoff_light_task_key_scan_swt, 40/5);			//blink tick 50ms
	sys_svc_software_timer_restart(onoff_light_task_key_scan_swt);

	srtnet_app_layer_service_id_binding_rec_cmd_bind_callback = onoff_light_rec_bind_cmd_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback = onoff_light_rec_toggle_cmd_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_off_callback	= onoff_light_rec_off_cmd_handler;

	srtnet_comm_layer_request_local_frame_head_write(0xF1);

	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(0);							//active service num 0
	}
	else if(SERVICE_CONFIG_TABLE_LEN > 1){
		onoff_light_state = onoff_light_state_inactive_bind;
		srtnet_app_layer_service_no_active_service();
	}

}


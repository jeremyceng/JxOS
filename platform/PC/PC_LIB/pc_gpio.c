
#include <windows.h>

static void (*pc_gpio_exti_rising_isr)(unsigned char  num) = 0;
static void (*pc_gpio_exti_falling_isr)(unsigned char  num) = 0;
static unsigned char  pin[10] = {0};
static unsigned char  last_pin[10] = {0};
static DWORD WINAPI EXTI0_SIM(LPVOID p)
{
	unsigned char  key_code = 0;
	while(1){
		pin[key_code] = ((GetAsyncKeyState(key_code+'0') & 0x8000) ? 0 : 1);
		if(last_pin[key_code] != pin[key_code]){
//			printf("EXTI0_SIM %4x\r\n", pin);
			if(pin[key_code] == 1){
				if(pc_gpio_exti_rising_isr != 0){
					pc_gpio_exti_rising_isr(key_code);
				}
			}
			else{
				if(pc_gpio_exti_falling_isr != 0){
					pc_gpio_exti_falling_isr(key_code);
				}
			}

		}
		last_pin[key_code] = pin[key_code];
		key_code++;
		if(key_code >= 10){
			key_code = 0;
		}
	}
    return 0;
}

void pc_gpio_exti_init(void)
{
	CreateThread(NULL, 0, EXTI0_SIM, 0, 0, NULL);
}

void pc_gpio_exti_rising_isr_reg(void (*pc_gpio_exti_rising_isr_handler)(unsigned char  num))
{
	pc_gpio_exti_rising_isr = pc_gpio_exti_rising_isr_handler;
}

void pc_gpio_exti_falling_isr_reg(void (*pc_gpio_exti_falling_isr_handler)(unsigned char  num))
{
	pc_gpio_exti_falling_isr = pc_gpio_exti_falling_isr_handler;
}

unsigned char  pc_gpio_read(unsigned char  num)		//'0' ~ '9'
{
	if(num > 9){
		return 1;
	}
	return (unsigned char )((GetAsyncKeyState(num+'0') & 0x8000) ? 0 : 1);
}



#ifndef _PC_COM_H
#define _PC_COM_H

void com_init(void);
void com_send_data(unsigned char* data, unsigned char len);
void com_rec_callback_reg(void (*com_rec_callback_handler)(unsigned char* str, unsigned char len));

#endif

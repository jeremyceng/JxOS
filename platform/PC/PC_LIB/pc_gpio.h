

#ifndef _PC_GPIO_H
#define _PC_GPIO_H


void pc_gpio_exti_init(void);
void pc_gpio_exti_rising_isr_reg(void (*pc_gpio_exti_rising_isr_handler)(unsigned char num));
void pc_gpio_exti_falling_isr_reg(void (*pc_gpio_exti_falling_isr_handler)(unsigned char  num));
unsigned char  pc_gpio_read(unsigned char  num);		//'0' ~ '9'

#endif

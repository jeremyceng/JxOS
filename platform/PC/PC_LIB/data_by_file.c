
#include "windows.h"
#include "stdio.h"
#include "lib/type.h"

uint8_t hex_to_string(char* str, uint8_t* str_len,
                   uint8_t* hex, uint8_t hex_len)
{
    char temp[8];
    uint8_t i;

    if(hex_len > 255/3-1){
        return 0;
    }

    *str_len = hex_len*3+1;
    memset(str, 0, *str_len);

    for(i = 0; i < hex_len; i++){
        itoa(hex[i], temp, 16);
//		printf("=====>hex:%x, i:%d, 0,%c 1,%c\r\n", hex[i], i, temp[0], temp[1]);
        if(temp[1] == 0){
			temp[1] = temp[0];
            temp[0] = '0';
        }
        str[i*3+0] = temp[0];
        str[i*3+1] = temp[1];
        str[i*3+2] = ' ';
    }
//    //printf("str %s\r\n", str);

    return 1;
}

uint8_t _string_to_hex(uint8_t* hex, char* str)
{
    //printf("_string_to_hex -> str[0] %c str[1] %c str[2] %c\r\n", str[0], str[1], str[2]);
    if((str[0] >= '0')&&(str[0] <= '9')){
        str[0] -= '0';
    }
    else if((str[0] >= 'A')&&(str[0] <= 'F')){
        str[0] -= 'A';
        str[0] += 0x0A;
    }
    else if((str[0] >= 'a')&&(str[0] <= 'f')){
        str[0] -= 'a';
        str[0] += 0x0A;
    }
    else{
        return 0;
    }

    if((str[1] >= '0')&&(str[1] <= '9')){
        str[1] -= '0';
    }
    else if((str[1] >= 'A')&&(str[1] <= 'F')){
        str[1] -= 'A';
        str[1] += 0x0A;
    }
    else if((str[1] >= 'a')&&(str[1] <= 'f')){
        str[1] -= 'a';
        str[1] += 0x0A;
    }
    else{
        return 0;
    }

    //printf("ok! str[2] %c, %d",  str[2],  str[2]);
    if(str[2] != ' '){
        //printf("ERR! str[2] %c, %d\r\n",  str[2],  str[2]);
        return 0;
    }

    *hex = str[0];
    *hex <<= 4;
    *hex += str[1];

    return 1;
}

uint8_t string_to_hex(uint8_t* hex, uint8_t* hex_len,
                      char* str)
{
    uint32_t i = 0;
    uint32_t j = 0;

    while(1){
        if(str[i] == 0){
            break;
        }
        //printf("string_to_hex->i%d,j%d\r\n",i,j);
        if(_string_to_hex(&(hex[j]), &(str[i])) == 0){
            i = 0;
            break;
        }
        if(i > 0xffff-3){
            i = 0;
            break;
        }
        i += 3;
        j++;
    }

    if(i == 0){
        return 0;
    }
    else{
//        //printf("hex ");
//        for(i = 0; i < j; i++){
//            //printf("%d ", hex[i]);
//        }
//        //printf("\r\n");
        *hex_len = j;
        return 1;
    }
}


uint8_t rec_data_by_file(uint8_t* rec_data, uint8_t* rec_data_len,
                         char* file_name)
{
    char buff[255];
    uint8_t i;
    FILE *fp = NULL;
    fp = fopen(file_name, "r");
    if(fp == NULL){
        return 0;
    }

    while(1){
        fgets(buff, 255, (FILE*)fp);
        if( feof(fp) ){
            //printf("-->%s", buff );
            //printf("the end\r\n");
            fclose(fp);
            break;
        }
    }

    i = 0;
    while(1){
        if(i == 255){
            i = 0;
            break;
        }
        if((buff[i] == '\n')
            &&(i != 0)){
            buff[i] = 0;
            break;
        }
        i++;
    }
    if(i == 0){
        return 0;
    }

    return string_to_hex(rec_data, rec_data_len, buff);
}

uint8_t send_data_by_file(uint8_t* send_data, uint8_t send_data_len,
                         char* file_name)
{
    char buff[255];
    uint8_t len;
    FILE *fp = NULL;
    fp = fopen(file_name, "a+");//"w+");
    if(fp == NULL){
        return 0;
    }

    if(hex_to_string(buff, &len, send_data, send_data_len) == 1){
        if(len > 255-2){
            return 0;
        }

        buff[len-1] = '\n';
        buff[len] = 0;
//        fprintf(fp, "This is testing for fprintf...\n");
        fputs(buff, fp);
        fclose(fp);
        return 1;
    }
    else{
        return 0;
    }
}

void data_by_file_add_mark(uint8_t* check_data, uint8_t check_data_len, uint8_t check_data_mark)
{
	check_data[check_data_len] = check_data_mark;
}

uint8_t data_by_file_check_mark(uint8_t* check_data, uint8_t check_data_len, uint8_t check_data_mark)
{
	if(check_data_mark == check_data[check_data_len-1]){
		return 1;
	}
	else{
		return 0;
	}
}

uint8_t len;
uint8_t test[10] = {0xaa, 0xbb, 0x00, 0x00, 0x12, 0x34};
uint8_t test2[10] = {0xcc, 0xdd, 0x00, 0x00, 0x12, 0x34};
uint8_t test_buff[10];
void com_by_file_test(void)
{
	int i;

    send_data_by_file(test, 6, "test.txt");
    send_data_by_file(test2, 6, "test.txt");
    rec_data_by_file(test_buff, &len, "test.txt");

    for(i = 0; i < len; i++){
        //printf("%d ", test_buff[i]);
    }
    //printf("\r\n");
}


#ifndef _PC_TIMER_H
#define _PC_TIMER_H

void pc_timer0_init(unsigned short int int_time_ms);
void pc_timer0_isr_reg(void (*pc_timer0_isr_handler)(void));

#endif

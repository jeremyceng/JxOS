
#include "jxos_public.h"
#include "..\JSnet\PHY\jsnet_phy.h"
#include "..\JSnet\DLK\jsnet_dlk.h"

#define local_DEVICE 	2
#if (local_DEVICE == 1)
static uint8_t Along_addr[2] = {0x12, 0x34};
static uint8_t Ashort_addr[1] = {0xAA};
static uint8_t Blong_addr[2] = {0x56, 0x78};
static uint8_t Bshort_addr[1] = {0xBB};
#else
static uint8_t Along_addr[2] = {0x56, 0x78};
static uint8_t Ashort_addr[1] = {0xBB};
static uint8_t Blong_addr[2] = {0x12, 0x34};
static uint8_t Bshort_addr[1] = {0xAA};
#endif

/********************************************************************/
#include <windows.h>
#include "string.h"

uint8_t rec_data_by_file(uint8_t* rec_data, uint8_t* rec_data_len,
                         char* file_name);
uint8_t send_data_by_file(uint8_t* send_data, uint8_t send_data_len,
                         char* file_name);
void data_by_file_add_mark(uint8_t* check_data, uint8_t check_data_len, uint8_t check_data_mark);
uint8_t data_by_file_check_mark(uint8_t* check_data, uint8_t check_data_len, uint8_t check_data_mark);

static uint8_t len = 0;
static uint8_t test_buff[64];
static uint8_t tmp_len;
static uint8_t tmp_test_buff[10];
static uint8_t file_lock = 0;
static DWORD WINAPI rec_data_file_task(LPVOID p)
{
	uint8_t i;

	while(1){
		if(rec_data_by_file(tmp_test_buff, &tmp_len, "test.txt") == 0){
			printf("continue\r\n");
			Sleep(250);
			continue;
		}
//        printf("tmp_len %d\r\n", tmp_len);
		if((len != tmp_len)||
		(memcmp(test_buff, tmp_test_buff, tmp_len) != 0)){
			memcpy(test_buff, tmp_test_buff, tmp_len);
			len = tmp_len;
			if(data_by_file_check_mark(test_buff, len, Ashort_addr[0]) == 1){
				printf("pass\r\n");
			}
			else{
				printf("rec data by file: ");
				for(i = 0; i < len; i++){
					printf("%x ", test_buff[i]);
				}
				printf("  len:%d\r\n", len);

				phy_data_receive_handler(test_buff, len-1);
			}
		}
		Sleep(500);
	}
	return 0;
}
/********************************************************************/


static JXOS_MESSAGE_PIPE_HANDLE pc_key_msg_pipe;
uint8_t common_callback_handler(void)
{
    return 1;
}

uint8_t data_send(uint8_t* send_data, uint8_t data_len)
{
	data_by_file_add_mark(send_data, data_len, Ashort_addr[0]);
	send_data_by_file(send_data, data_len+1, "test.txt");

	printf("-->data_send:");
    while(data_len){
        printf("0x%2x ", *send_data);
        data_len--;
        send_data++;
    }
	printf("\r\n");

	phy_data_send_finish_handler();

    return 1;
}

uint8_t data_rec(uint8_t* send_data, uint8_t data_len)
{
    while(data_len){
        printf("0x%2x ", *send_data);
        data_len--;
        send_data++;
    }
    return 1;
}


static void jsnet_test_task(uint8_t task_id, void * parameter)
{
    char key_code;
    uint8_t len;
	uint8_t SEND[3] = {0X11, 0X22, 0X33};

    if(jxos_message_pipe_check_empty(pc_key_msg_pipe) == 0){
        if(jxos_message_pipe_receive(pc_key_msg_pipe, "recpck", &key_code, &len) == 1){
            printf("\r\nrec key_code:");
            printf(" %c\r\n", key_code);

#if(JSNET_DLK_DEVICE_TYPE != JSNET_DLK_DEVICE_TYPE_DONGLE)
			if(key_code == 's'){
				jsnet_dlk_request_scan();
			}
			else if(key_code == 'o'){
#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
				jsnet_dlk_request_discoverable();
#endif
			}
			else if(key_code == 'c'){
				jsnet_dlk_request_connect(Blong_addr, 2);
			}
			else if(key_code == 'a'){
				jsnet_dlk_request_announce(Blong_addr, 2);
			}
			else if(key_code == 'd'){
				jsnet_dlk_request_disconnect();
			}
			else if(key_code == 'p'){
				jsnet_dlk_request_data_poll(Bshort_addr, 1);
			}
			else if(key_code == 't'){
				jsnet_dlk_request_data_send(Bshort_addr, 1, SEND, 3);
			}
#endif
		}
	}
}

void confirm_scan_handler(uint8_t* open_node_adddr, uint8_t open_node_adddr_len)
{
	uint8_t i = 0;
	printf("---find node: 0x");
	for(; i < open_node_adddr_len; i++){
		printf("%x", open_node_adddr[i]);
	}
	printf("\r\n");
}

void jsnet_test_task_init(void)
{
	CreateThread(NULL, 0, rec_data_file_task, 0, 0, NULL);

    jxos_task_create(jsnet_test_task, "jn_test", 0);

    pc_key_msg_pipe = jxos_message_pipe_get_handle("pc_key_pipe");
    jxos_message_pipe_receiver_name_register(pc_key_msg_pipe, "recpck");

    //phy_check_tx_busy_callback_register(common_callback_handler);
    phy_rx_disable_callback_register(common_callback_handler);
    phy_tx_disable_callback_register(common_callback_handler);
    phy_rx_enable_callback_register(common_callback_handler);
    phy_tx_enable_callback_register(common_callback_handler);
    phy_data_send_callback_register(data_send);

    phy_task_init();
    dlk_task_init();

	jsnet_dlk_request_reset();
    jsnet_dlk_parameter_local_node_addr_set(Along_addr, 2);
    jsnet_dlk_parameter_local_node_addr_set(Ashort_addr, 1);
	jsnet_dlk_parameter_set_local_key(Ashort_addr, 1);
    jsnet_dlk_request_receive_enable();
	jsnet_dlk_confirm_scan = confirm_scan_handler;
}



#include<Windows.h>
#include "jxos_public.h"

#include "lib\value_move_group.h"
#include "lib\value_move_circle_group.h"

#define VALUE_MOVE_CIRCLE_EVENT_NUM_MAX        8
static VALUE_MOVE_CIRCLE_GROUP_STRUCT value_group;
static VALUE_MOVE_CIRCLE_STRUCT values[VALUE_MOVE_CIRCLE_EVENT_NUM_MAX];

static uint8_t obj_to_mun(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
    uint8_t i;
    for(i = 0; i < VALUE_MOVE_CIRCLE_EVENT_NUM_MAX; i++){
        if(value_move_circle == &(values[i])){
            return i;
        }
    }
    return 0xff;
}

static void value_changed(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle)
{
    uint8_t v_num = obj_to_mun(value_move_circle);
    printf("value_move_circle %d changed to %d\r\n", v_num, value_move_circle_get_current_value(value_move_circle));
}

static void value_move_test_task(uint8_t task_id, void * parameter)
{
    printf("value_move_test_task\r\n");
    value_move_circle_group_tick_handler(&value_group);
    Sleep(500);
}

void value_move_test_task_init(void)
{
    uint8_t i;
    VALUE_MOVE_STRUCT* value_move_p;
    VALUE_MOVE_CIRCLE_STRUCT* value_move_circle_p;
    VALUE_MOVE_CIRCLE_STRUCT value_move_circle_n;

    value_move_circle_n.value_move.current_value = 0;
    value_move_circle_n.value_move.target_value = 0;
    value_move_circle_n.value_move.current_value_changed_event_callbakc = value_changed;

    value_move_circle_n.begin_value = 0;
    value_move_circle_n.end_value = 0;

    value_move_circle_group_init(&value_group, VALUE_MOVE_CIRCLE_EVENT_NUM_MAX, values);

    for(i = 0; i < VALUE_MOVE_CIRCLE_EVENT_NUM_MAX; i++){
        value_move_circle_new(&value_group, &value_move_circle_n);
    }

    value_move_circle_p = &(values[0]);
    value_move_circle_init_current_value(value_move_circle_p, 0);
    value_move_circle_move_by_tick(value_move_circle_p, 10, 16, 13);

//    value_move_p = &(values[1].value_move);
//    value_move_init_current_value(value_move_p, 10);
//    value_move_to_value_by_step(value_move_p, 16, 2);

	jxos_task_create(value_move_test_task, "value_move_test", 0);
}

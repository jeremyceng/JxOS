
#include "jxos_public.h"
#include "stdio.h"

int main (void)
{
	jxos_run();
    return 0;
}


void jxos_prepare_to_run_callback_handler(void)
{
//	printf("jxos_prepare_to_run_callback_handler\r\n");
}

/*********************************************************/
#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	return true;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
//	printf("%c", byte);
}
#endif

/*********************************************************/
void sys_power_mgr_task_hal_init_callback_handler(void)
{
}

void sys_power_mgr_task_prepare_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_go_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_recover_to_wake_callback_handler(void)
{
}

/*********************************************************/
#include "../PC_LIB/pc_timer.h"
void sys_software_timer_task_hal_init_callback_handler(void)
{
//	printf("sys_software_timer_task_hal_init_callback_handler\r\n");
//	pc_timer0_init(1);
//	pc_timer0_isr_reg(sys_svc_software_timer_tick_handler);
}

void sys_software_timer_task_hal_start_callback_handler(void)
{
//	printf("sys_software_timer_task_hal_start_callback_handler\r\n");
}

void sys_software_timer_task_hal_stop_callback_handler(void)
{
//	printf("sys_software_timer_task_hal_stop_callback_handler\r\n");
}

/*********************************************************/
#if	(JXOS_STD_APP_KEY_TASK_ENABLE == 1)
#include "../hal_pc/pc_gpio.h"
static void pc_gpio_exti_falling_isr_handler(uint8_t num)
{
	std_app_key_task_key_press_interrupt_handler();
}
void std_app_key_task_hal_init_callback_handler(void)
{
//	printf("std_app_key_task_hal_init_callback_handler\r\n");
	pc_gpio_exti_init();
	pc_gpio_exti_falling_isr_reg(pc_gpio_exti_falling_isr_handler);
}

uint8_t std_app_key_task_hal_read_pin_level_callback_handler(uint8_t key_num)
{
//	printf("std_app_key_task_hal_read_pin_level_callback_handler\r\n");
	return pc_gpio_read(key_num);
}
#endif
/*********************************************************/
void std_app_frame_send_task_hal_send_data_callback_handler(uint8_t* p, uint8_t len)
{
    printf("------------------");
    while(1){
        if(len == 0){
            printf("\r\n");
            break;
        }

        printf("%c", *p);
        p++;
        len--;
    }
}

void print_hex(uint8_t* hex, uint8_t len)
{
	uint8_t i;
	for(i = 0; i < len; i++){
		printf("0x%x ", *hex);
		hex++;
	}
	printf("\r\n");
}

void print_data(uint8_t* data, uint8_t len)
{
	uint8_t i;
	for(i = 0; i < len; i++){
		printf("%c", *data);
		data++;
	}
}

/****
JXOS_MAIL_BOX_HANDLE mpipe;
uint8_t mpipe_space[18] = {0};
void jxos_user_init_callback(void)
{
	uint8_t ret;
	uint8_t rbuff[5] = {0};

	mpipe = jxos_mail_box_create(mpipe_space, 18, 4, "mpipe");

	ret = jxos_mail_box_receiver_name_register(mpipe, "r_name");
	printf("register ret: %d\r\n", ret);


	ret = jxos_mail_box_send(mpipe, "abcd");
	printf("ret: %d\r\n", ret);

	ret = jxos_mail_box_send(mpipe, "1234");
	printf("ret: %d\r\n", ret);

	ret = jxos_mail_box_send(mpipe, "5678");
	printf("ret: %d\r\n", ret);

	ret = jxos_mail_box_send(mpipe, "!@#$");
	printf("ret: %d\r\n", ret);

	ret = jxos_mail_box_send(mpipe, "-_-!");
	printf("ret: %d\r\n", ret);


	ret = jxos_mail_box_receive(mpipe, "r_name", rbuff);
	printf("ret: %d\r\n", ret);
	printf("rbuff: %s \r\n", rbuff);

	ret = jxos_mail_box_receive(mpipe, "r_name", rbuff);
	printf("ret: %d\r\n", ret);
	printf("rbuff: %s \r\n", rbuff);

	ret = jxos_mail_box_receive(mpipe, "r_name", rbuff);
	printf("ret: %d\r\n", ret);
	printf("rbuff: %s \r\n", rbuff);

	ret = jxos_mail_box_receive(mpipe, "r_name", rbuff);
	printf("ret: %d\r\n", ret);
	printf("rbuff: %s \r\n", rbuff);

	ret = jxos_mail_box_send(mpipe, "-_-!");
	printf("ret: %d\r\n", ret);

	ret = jxos_mail_box_receive(mpipe, "r_name", rbuff);
	printf("ret: %d\r\n", ret);
	printf("rbuff: %s \r\n", rbuff);
}

void jxos_user_task_init_callback(void)
{

}
***/
/***
char putchar(char c)
{
//	Send_Data_To_UART0(c);
	TI = 0;
	SBUF = c;
	while(TI==0);
	return 0;
}
***/
//	while(1){
//		set_WDCLR;
//		sleep();
//		if(KEY == 1){
//			sleep();
//			delay_1ms(25);
//			KEY = 0;
//		}
//		IDL();
//	}



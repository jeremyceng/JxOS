#include "hal/hal_interrupt.h"
#include "hal/hal_timer.h"
#include "jxos_public.h"
#include "bsp/bsp_rf_driver.h"

void rf_rx_enable(void);

static uint8_t rf_rec_data_msg_buff[32];
static JXOS_MSG_HANDLE rf_rec_data_msg;
static swtime_type led_blink_timer = 0;
static JXOS_EVENT_HANDLE event_rf_receiver_ok = 0;

static void rf_dongle_task(uint8_t task_id, void * parameter)
{
	uint8_t i;
	static uint8_t led = 0;
	if(1 == sys_svc_software_timer_check_overtime(led_blink_timer)){
		if(0 == led){
			led = 1;
		}
		else{
			led = 0;
		}
	}

	if(1 == jxos_event_wait(event_rf_receiver_ok)){
		for(i = 0; i < bsp_rf_receive_buff_len; i++){
			jxos_msg_send(rf_rec_data_msg, &(bsp_rf_receive_buff[i]));
		}
	}
}

static void bsp_rf_receive_ok_call_back_handler(void)
{
	jxos_event_set(event_rf_receiver_ok);
}

static void bsp_rf_send_ok_call_back_handler(void)
{
	rf_rx_enable();
}

static void rf_timer_isr_callback_handler(void)
{
    bsp_rf_send_tick_handler();
    bsp_rf_receive_tick_handler();
}

static uint8_t text1[] = {0x01,0x02,0x03,0x04,0xaa,0x06,0x07,0x08};
void  rf_dongle_task_init(void)
{
	bsp_rf_receive_init();
	bsp_rf_send_init();
    bsp_rf_receive_ok_call_back = bsp_rf_receive_ok_call_back_handler;
	bsp_rf_send_ok_call_back = bsp_rf_send_ok_call_back_handler;

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 5;
    TIM_TimeBaseInit(TIM0, &TIM_TimeBaseInitStruct);
    TIM_ITConfig(TIM0, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM0, ENABLE);
    TIM0_IRQHandler_callback = sys_svc_software_timer_tick_handler;

    TIM1_IRQHandler_callback = rf_timer_isr_callback_handler;

    rf_rec_data_msg = jxos_msg_create(rf_rec_data_msg_buff, 32, 1, "rf_rec_data_msg");

    led_blink_timer = sys_svc_software_timer_new();
    sys_svc_software_timer_set_time(led_blink_timer, 500/5);
    sys_svc_software_timer_start(led_blink_timer);

	event_rf_receiver_ok = jxos_event_create();
	jxos_task_create(rf_dongle_task, "rf_dongle", 0);

//	memcpy(bsp_rf_receive_buff, "0123456789", 10);
	memcpy(bsp_rf_receive_buff, text1, sizeof(text1));
	bsp_rf_receive_buff_len = sizeof(text1);
	jxos_event_set(event_rf_receiver_ok);
}


/**
static uint8_t frame_find_head(uint8_t* index, uint8_t* rec_buff, uint8_t rec_buff_len)
{
	uint8_t i;
	uint8_t temp_1, temp_2;

    if(rec_buff_len <= 1){
        return 0;
    }

    for(i = 0; i < rec_buff_len-1; i++){
        temp_1 = rec_buff[i];
        temp_2 = rec_buff[i+1];
        printf("rec_buff.at(index) %x, rec_buff.at(index+1) %x, index: %d\r\n", temp_1, temp_2, i);
        if((temp_1 == FRAME_HEAD)&&(temp_2 != FRAME_HEAD)){
            if(rec_buff[i+1] != 0){
                *index = i+2;
                printf("find_start OK!!, index: %d, frame_len %d\r\n", *index, rec_buff[i+1]);
                return 1;
            }
        }
    }
    printf("NOT find_start!!");
    return 0;
}

static uint8_t frame_copy(uint8_t* frame_buff_out, uint8_t* frame_buff_out_len, uint8_t* rec_buff, uint8_t index)
{
    uint8_t offset = 0;
    uint8_t i;
	uint8_t temp_1, temp_2;
	uint8_t frame_payload_len = rec_buff[index-1];
	if(frame_payload_len == FRAME_escape_character){
		frame_payload_len = FRAME_HEAD;
	}

    for(i = 0; (i+offset) < frame_payload_len; i++){
        temp_1 = rec_buff[index+i+offset];
        if(temp_1 == FRAME_HEAD){
            temp_2 = rec_buff[index+i+1+offset];
            if(temp_2 == FRAME_HEAD){
                offset++;
            }
            else{
                printf("frame_copy error\r\n");
                return 0;
            }
        }
        frame_buff_out[i] = temp_1;
    }
	*frame_buff_out_len = i;
    return 1;
}

static void uart_rx_data_handler(void)
{
    uint8_t frame_payload_len;
    uint8_t index;
	uint8_t temp_buff[64] = {0};
	uint8_t temp_buff_len = ringbuff_check_used_space(&uart_rx_buffer_mgr);

	ringbuff_read_data(&uart_rx_buffer_mgr, temp_buff_len, temp_buff);

	if((temp_buff_len+frame_buffer_len) > 64){
		frame_buffer_len = 0;
	}
	if((temp_buff_len+frame_buffer_len) == 0){
		return;
	}

	memcpy(&(frame_buffer[frame_buffer_len]), temp_buff, temp_buff_len);
	frame_buffer_len += temp_buff_len;

	printf("START frame_buffer\r\n");
    print_hex(frame_buffer, frame_buffer_len);
	printf("\r\n");

    while(1){
        if(frame_find_head(&index, frame_buffer, frame_buffer_len) == true){    //��ͷ
            frame_payload_len = frame_buffer[index-1];
			if(frame_payload_len == FRAME_escape_character){
				frame_payload_len = FRAME_HEAD;
			}
			printf("frame_payload_len %x\r\n",frame_payload_len);
            if((frame_payload_len+index) <= frame_buffer_len){    //������֡
                if(frame_copy(temp_buff, &temp_buff_len, frame_buffer, index)){
//					rx_power_off();
					bsp_rf_send(temp_buff, temp_buff_len);
					printf("COPY OK\r\n");
					print_hex(temp_buff, temp_buff_len);
					printf("\r\n");
                }

                if((frame_payload_len+index) == frame_buffer_len){
                    printf("finish\r\n");
					frame_buffer_len = 0;
                    break;
                }
                else{
					printf("redo!!!\r\n");
					printf("frame_buffer_len %d, frame_payload_len %d, index %d\r\n", frame_buffer_len, frame_payload_len, index);
					temp_buff_len = (frame_buffer_len - (frame_payload_len+index));
					memcpy(temp_buff, &(frame_buffer[frame_payload_len+index]), temp_buff_len);
                    memcpy(frame_buffer, temp_buff, temp_buff_len);
					frame_buffer_len = temp_buff_len;
                }
            }
            else{                                       //������֡
                break;
            }
        }
        else{                                            //��ͷ
        	break;
        }
    }

	printf("LAST frame_buffer\r\n");
    print_hex(frame_buffer, frame_buffer_len);
	printf("\r\n");
}
**/


#include "jxos_public.h"

void jxos_prepare_to_run_callback_handler(void);

void sys_debug_print_task_hal_init_callback_handler(void);
uint8_t sys_debug_print_task_send_finish_check_callback_handler(void);
void sys_debug_print_task_send_byte_callback_handler(uint8_t byte);

void jxos_user_init_callback(void)
{
	jxos_prepare_to_run_callback = jxos_prepare_to_run_callback_handler;

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
	sys_debug_print_task_hal_init_callback = sys_debug_print_task_hal_init_callback_handler;
	sys_debug_print_task_send_finish_check_callback = sys_debug_print_task_send_finish_check_callback_handler;
	sys_debug_print_task_send_byte_callback = sys_debug_print_task_send_byte_callback_handler;
#endif
}

void  rf_dongle_task_init(void);
void  uart_task_init(void);
void jxos_user_task_init_callback(void)
{
	rf_dongle_task_init();
	uart_task_init();
}

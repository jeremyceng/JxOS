
#ifndef __BSP_ASYNC_COMM_CONFIG__H
#define __BSP_ASYNC_COMM_CONFIG__H

#include "lib/type.h"

#define FRAME_PAYLOAD_LEN_MAX   16
#define REC_BUFF_LEN_MAX        32

#endif

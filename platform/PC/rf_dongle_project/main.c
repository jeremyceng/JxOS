
#include "jxos_public.h"

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
	sys_debug_print_task_user_print_str("jxos_prepare_to_run_callback_handler\r\n");
}

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	return true;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
	printf("%c", byte);
}
#endif

void rf_rx_disable(void)
{
}

void rf_rx_enable(void)
{
}

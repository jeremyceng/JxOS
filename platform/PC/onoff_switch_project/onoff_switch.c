
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"


/*********************************************
1.实现 HAL
2.配置 BSP
3.配置 SRTnet COMM LAYE
4.配置 SRTnet APP LAYE
5.实现用户逻辑(key_task, led_task, onoff_switch)
6.配置 JXOS
6.配置 sys service
*********************************************/

/*********************************************
短按 toggel
双击 off
长按（5s）bunding
*********************************************/
static JXOS_MSG_HANDLE key_msg;
static swtime_type keep_wake_swt;
static uint8_t onoff_switch_keep_wake_id;

void onoff_switch_short_press(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_short_press\r\n");
	srtnet_app_layer_service_id_onoff_send_cmd_toggle(key_num);
}

void onoff_switch_long_press(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_long_press\r\n");
	srtnet_app_layer_service_id_binding_send_cmd_bind(key_num);
}

void onoff_switch_double_click(uint8_t key_num)
{
	sys_debug_print_task_user_print_str("onoff_switch_double_click\r\n");
	srtnet_app_layer_service_id_onoff_send_cmd_off(key_num);
}

static void onoff_switch_task(uint8_t task_id, void * parameter)
{
	uint8_t msg_item;
	uint8_t key_num;
	uint8_t key_msg_type;
	if(jxos_msg_receive(key_msg, &msg_item) == 1){
		key_msg_type = msg_item&0x0f;
		key_num = msg_item>>4;

		switch(key_msg_type){
		case 0:
			onoff_switch_long_press(key_num);
			break;
		case 1:
			onoff_switch_short_press(key_num);
			break;
		case 2:
			onoff_switch_double_click(key_num);
			break;
		default:
			break;
		}

		sys_svc_software_timer_restart(keep_wake_swt);
		sys_power_mgr_task_keep_wake_mark_set(onoff_switch_keep_wake_id);
//		led_on();
	}

	if(sys_svc_software_timer_check_overtime(keep_wake_swt) == 1){
		sys_svc_software_timer_stop(keep_wake_swt);
		sys_power_mgr_task_keep_wake_mark_reset(onoff_switch_keep_wake_id);
//		led_off();
	}
}

void  onoff_switch_task_init(void)
{
	srtnet_comm_layer_transmitter_device_request_local_addr_write(0xADD1);
	srtnet_comm_layer_request_local_frame_head_write(0xF1);

	keep_wake_swt = sys_svc_software_timer_new();
	sys_svc_software_timer_set_time(keep_wake_swt, 5);

	onoff_switch_keep_wake_id = sys_power_mgr_task_keep_wake_mark_get_id();

	jxos_task_create(onoff_switch_task, "onoff_switch", 0);

    key_msg = jxos_msg_get_handle("std_app_key_msg");
}





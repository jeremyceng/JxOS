
#include "jxos_public.h"
#include "hal/hal_timer.h"
#include "hal/hal_exti.h"

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
	sys_debug_print_task_user_print_str("jxos_prepare_to_run_callback_handler\r\n");
}

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	return true;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
	printf("%c", byte);
}
#endif

void sys_power_mgr_task_hal_init_callback_handler(void)
{
}

void sys_power_mgr_task_prepare_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_go_to_sleep_callback_handler(void)
{
}

void sys_power_mgr_task_recover_to_wake_callback_handler(void)
{
}

void sys_software_timer_task_hal_init_callback_handler(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 5;
    TIM_TimeBaseInit(TIM0, &TIM_TimeBaseInitStruct);

    TIM_ITConfig(TIM0, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM0, ENABLE);
}

void std_app_key_task_hal_init_callback_handler(void)
{
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
    EXTI_Init(&EXTI_InitStruct);
}

#include<Windows.h>
uint8_t std_app_key_task_hal_read_pin_level_callback_handler(uint8_t key_mun)
{
	//key_mun --> keyboard number key 0~9
	return ((GetAsyncKeyState(key_mun+0x30) & 0x8000) ? 0 : 1);
}

void srtnet_comm_layer_task_hal_timer_init_callback_handler(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 5;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStruct);

    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM1, ENABLE);
}

void Timer0_ISR_callback_handler()
{
	sys_svc_software_timer_tick_handler();
}

void Timer1_ISR_callback_handler()
{
	srtnet_comm_layer_tick_handler();
}

void exint0_ISR_callback_handler(void)
{
	std_app_key_task_key_press_interrupt_handler();
}


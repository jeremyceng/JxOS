
#include "../../kernel/jxos_task.h"
#include "../../kernel/jxos_msg.h"
#include "../../kernel/jxos_event.h"

#include "../SRTnet/application_layer/application_layer_service_user.h"
#include "../SRTnet/communication_layer/communication_layer.h"
#include "../SRTnet/application_layer/application_layer.h"

/***************************************
1.����			application_layer_config.h
2.��ʼ����ַ	srtnet_comm_request_local_addr_write()
3.��ʼ��֡ͷ	srtnet_comm_request_local_frame_head_write()
4.

***************************************/
/*********************************************
1.ʵ�� HAL
2.���� BSP
3.���� SRTnet COMM LAYE
4.���� SRTnet APP LAYE
5.ʵ���û��߼�(key_task, led_task, onoff_switch)
6.���� JXOS
6.���� sys service
*********************************************/

/*********************************************
�̰� toggel
˫�� off
������5s��bunding
*********************************************/
static JXOS_MSG_HANDLE key_msg;
void onoff_switch_short_press(uint8_t key_num)
{
	printf("onoff_switch_short_press\r\n");
	service_onoff_send_cmd_toggle(key_num);
}

void onoff_switch_long_press(uint8_t key_num)
{
	printf("onoff_switch_long_press\r\n");
	service_bunding_send_cmd_bund(key_num);
}

void onoff_switch_double_click(uint8_t key_num)
{
	printf("onoff_switch_double_click\r\n");
	service_onoff_send_cmd_off(key_num);
}

static void onoff_switch_task(uint8_t task_id, void * parameter)
{
	uint8_t msg_item;
	uint8_t key_num;
	uint8_t key_msg_type;
	if(jxos_msg_receive(key_msg, &msg_item) == 1){
		key_msg_type = msg_item&0x0f;
		key_num = msg_item>>4;

		switch(key_msg_type){
		case 0:
			onoff_switch_long_press(key_num);
			break;
		case 1:
			onoff_switch_short_press(key_num);
			break;
		case 2:
			onoff_switch_double_click(key_num);
			break;
		default:
			break;
		}
	}
}


void  onoff_switch_task_init(void)
{
	srtnet_comm_request_local_addr_write(0xADD1);
	srtnet_comm_request_local_frame_head_write(0xF1);

	jxos_task_create(onoff_switch_task, "onoff_switch", 0);

    key_msg = jxos_msg_get_handle("key_msg");
}




